import { Url } from 'url';
import { Duplex } from 'stream';
import * as SerialPort from 'serialport';

import { UParIfaceStream, UParIfaceOnConnect, uparIfaceRegister } from "./iface";

class SerialIface extends UParIfaceStream {
  constructor(device: string, baudrate: number) {
    super(new SerialPort(device, {
      baudRate: baudrate,
      autoOpen: false,
    }) as any as Duplex, 250);
  }

  open(cb: UParIfaceOnConnect) {
    (this._stm as any as SerialPort).open((err) => {
      if (err) {
        cb(err);
      } else {
        cb(null, this);
      }
    });
  }

  close() {
    (this._stm as any as SerialPort).close();
  }
}

function connect(url: Url, cb: UParIfaceOnConnect) {
  let device = url.pathname || "/dev/ttyUSB0";
  let baudrate = url.hash ? parseInt(url.hash.slice(1), 10) : 115200;
  const iface = new SerialIface(device, baudrate);
  iface.open(cb);
}

uparIfaceRegister("serial:", connect);
