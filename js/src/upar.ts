import { ustr_t, unum_bits_t, ustr_new, ustr_getuv, ustr_getui, ustr_getsi, ustr_getsl, ustr_getbd, ustr_gethd, ustr_putuv, ustr_putui, ustr_putsi, ustr_getrn, ustr_putrn, ustr_putsl, ustr_putbd, ustr_puthd } from "./ustr";

const enum upar_field {
  set = 1 << 0,
  type = 1 << 1,
  sub = 1 << 2,
  get = 1 << 3,

  name = 1 << 4,
  unit = 1 << 5,
  item = 1 << 6,
  info = 1 << 7,
  hint = 1 << 8,

  def = 1 << 9,
  min = 1 << 10,
  max = 1 << 11,
  stp = 1 << 12,
  opt = 1 << 13,

  poll = 1 << 14,
}

const enum upar_type {
  uint = 1,
  sint = 2,
  ufix = 3,
  sfix = 4,
  real = 5,
  cstr = 6,
  hbin = 7,
  time = 8,
  date = 9,
  mac = 10,
  ipv4 = 11,
  ipv6 = 12,
}

const enum upar_flag {
  getable = 1 << 4,
  setable = 1 << 5,
  persist = 1 << 6,
  options = 1 << 7,
}

export interface udate_t {
  year: number;
  mon: number;
  date: number;
}

export interface utime_t {
  hour: number;
  min: number;
  sec: number;
}

export type upar_val = number | string | udate_t | utime_t | number[];

export interface upar_txt {
  name?: string;
  unit?: string;
  item?: string;
  info?: string;
  hint?: string;
}

export interface upar_opt extends upar_txt {
  val: upar_val;
}

export interface upar_info extends upar_txt {
  id: number;
  type?: upar_type;
  size?: number | unum_bits_t;
  frac?: number;
  sub?: [number, number];
  val?: upar_val;

  getable?: true;
  setable?: true;
  persist?: true;
  options?: true;

  def?: upar_val;
  min?: upar_val;
  max?: upar_val;
  stp?: upar_val;
  opt?: upar_opt[];

  poll?: number;
}

export interface upar_map {
  [id: number]: upar_info;
}

export function upar_get(res: ustr_t, par: upar_info): upar_val {
  let val: upar_val;
  const bits: unum_bits_t = par.size as unum_bits_t;

  switch (par.type) {
    case upar_type.uint:
      val = ustr_getui(res, bits);
      break;
    case upar_type.sint:
      val = ustr_getsi(res, bits);
      break;
    case upar_type.ufix:
      val = ustr_getui(res, bits) / (1 << par.frac);
      break;
    case upar_type.sfix:
      val = ustr_getsi(res, bits) / (1 << par.frac);
      break;
    case upar_type.real:
      val = ustr_getrn(res, bits);
      break;
    case upar_type.cstr:
      val = ustr_getsl(res);
      break;
    case upar_type.hbin:
      val = ustr_getbd(res, par.size);
      break;
    case upar_type.date: {
      const year = ustr_getsi(res, 16);
      const mon = ustr_getui(res, 8);
      const date = ustr_getui(res, 8);
      val = { year, mon, date };
    } break;
    case upar_type.time: {
      const hour = ustr_getui(res, 8);
      const min = ustr_getui(res, 8);
      const sec = ustr_getui(res, 8);
      val = { hour, min, sec };
    } break;
    case upar_type.mac:
      val = ustr_getbd(res, 6);
      break;
    case upar_type.ipv4:
      val = ustr_getbd(res, 4);
      break;
    case upar_type.ipv6:
      val = ustr_gethd(res, 8);
      break;
  }

  return val;
}

export function upar_put(req: ustr_t, par: upar_info, val: upar_val) {
  const bits: unum_bits_t = par.size as unum_bits_t;

  switch (par.type) {
    case upar_type.uint:
      ustr_putui(req, Math.abs(parseInt(`${val}`, 0)), bits);
      break;
    case upar_type.sint:
      ustr_putsi(req, parseInt(`${val}`, 0), bits);
      break;
    case upar_type.ufix:
      ustr_putui(req, Math.abs(Math.round(parseFloat(`${val}`) * (1 << par.frac))), bits);
      break;
    case upar_type.sfix:
      ustr_putsi(req, Math.round(parseFloat(`${val}`) * (1 << par.frac)), bits);
      break;
    case upar_type.real:
      ustr_putrn(req, parseFloat(`${val}`), bits);
      break;
    case upar_type.cstr:
      ustr_putsl(req, `${val}`);
      break;
    case upar_type.hbin:
      ustr_putbd(req, val as number[], par.size);
      break;
    case upar_type.date: {
      const { year, mon, date } = val as udate_t;
      ustr_putsi(req, year, 16);
      ustr_putui(req, mon, 8);
      ustr_putui(req, date, 8);
    } break;
    case upar_type.time: {
      const { hour, min, sec } = val as utime_t;
      ustr_putui(req, hour, 8);
      ustr_putui(req, min, 8);
      ustr_putui(req, sec, 8);
    } break;
    case upar_type.mac:
      ustr_putbd(req, val as number[], 6);
      break;
    case upar_type.ipv4:
      ustr_putbd(req, val as number[], 4);
      break;
    case upar_type.ipv6:
      ustr_puthd(req, val as number[], 8);
      break;
  }
}

function num2str(num: number, nlz: number = 0, rdx: number = 10) {
  return ((new Array(nlz)).join('0') + num.toString(rdx)).slice(-nlz);
}

export function upar_show(par: upar_info, val: upar_val): string {
  switch (par.type) {
    case upar_type.date: {
      const { year, mon, date } = val as udate_t;
      return `${num2str(year, 4)}-${num2str(mon, 2)}-${num2str(date, 2)}`
    }
    case upar_type.time: {
      const { hour, min, sec } = val as utime_t;
      return `${num2str(hour, 2)}:${num2str(min, 2)}:${num2str(sec, 2)}`
    }
    case upar_type.hbin: {
      const buf = Buffer.from(val as number[]);
      return buf.toString('hex');
    }
    case upar_type.mac: {
      const mac = val as number[];
      return `${num2str(mac[0], 2, 16)}:${num2str(mac[1], 2, 16)}:${num2str(mac[2], 2, 16)}:${num2str(mac[3], 2, 16)}:${num2str(mac[4], 2, 16)}:${num2str(mac[5], 2, 16)}`;
    }
    case upar_type.ipv4: {
      const ip = val as number[];
      return `${num2str(ip[0])}.${num2str(ip[1])}.${num2str(ip[2])}.${num2str(ip[3])}`;
    }
  }
  if (par.opt) {
    for (let opt of par.opt) {
      if (opt.val == val) {
        return opt.name;
      }
    }
  }
  return `${val}`;
}

export type upar_read_res = [Error, null] | [null, upar_val];

export function upar_read(par: upar_info, str: string): upar_read_res {
  if (par.opt) {
    for (let opt of par.opt) {
      if (opt.name == str) {
        return [null, opt.val];
      }
    }
  }
  switch (par.type) {
    case upar_type.date: {
      const m = str.match(/^(\d{4})\-(\d{2})\-(\d{2})$/);
      if (m) {
        const year = parseInt(m[1], 10);
        const mon = parseInt(m[2], 10);
        const date = parseInt(m[3], 10);
        if (year < 0) return [new Error("Year out of range"), null];
        if (mon < 1 || mon > 12) return [new Error("Month out of range"), null];
        if (date < 1 || date > 31) return [new Error("Date out of range"), null];
        return [null, { year, mon, date } as udate_t];
      } else return [new Error("Invalid date format"), null];
    }
    case upar_type.time: {
      const m = str.match(/^(\d{2}):(\d{2}):(\d{2})$/);
      if (m) {
        const hour = parseInt(m[1], 10);
        const min = parseInt(m[2], 10);
        const sec = parseInt(m[3], 10);
        if (hour < 0 || hour > 23) return [new Error("Hour out of range"), null];
        if (min < 0 || min > 59) return [new Error("Minute out of range"), null];
        if (sec < 0 || sec > 59) return [new Error("Second out of range"), null];
        return [null, { hour, min, sec } as utime_t];
      } else return [new Error("Invalid time format"), null];
    }
    case upar_type.hbin: {
      const buf = Buffer.from(str, 'hex');
      if (!buf) return [new Error("Invalid hex data"), null];
      if (buf.length != par.size) return [new Error(`${par.size} bytes required`), null];
      const val = new Array(par.size);
      for (let i = 0; i < val.length; i++) {
        val[i] = buf.readUInt8(i);
      }
      return [null, val];
    }
    case upar_type.mac: {
      const m = str.split(':');
      if (m.length == 6) {
        const val: number[] = [];
        for (let i = 1; i <= 6; i++) {
          const part = parseInt(m[i], 16);
          if (typeof part != "number") return [new Error(`Invalid MAC component ${i}`), null];
          val.push(part);
        }
        return [null, val];
      } else return [new Error("Invalid MAC"), null];
    }
    case upar_type.ipv4: {
      const m = str.split('.');
      if (m.length == 4) {
        const val: number[] = [];
        for (let i = 1; i <= 6; i++) {
          const part = parseInt(m[i], 10);
          if (typeof part != "number") return [new Error(`Invalid IP component ${i}`), null];
          val.push(part);
        }
        return [null, val];
      } else return [new Error("Invalid IP"), null];
    }
    case upar_type.uint:
    case upar_type.sint: {
      const m = str.match(/^(?:0([xob]))?([0-9a-fA-F])$/);
      if (m) {
        const rdx = m[1] ? (m[1] == 'x' ? 16 : m[1] == 'o' ? 8 : m[1] == 'b' ? 2 : 10) : 10;
        const val = parseInt(m[2], rdx);
        if (!isNaN(val)) return [null, val];
        else return [new Error("Invalid integer"), null];
      } else return [new Error("Invalid integer"), null];
    }
    case upar_type.ufix:
    case upar_type.sfix:
    case upar_type.real: {
      const val = parseFloat(str);
      if (!isNaN(val)) return [null, val];
      else return [new Error("Invalid number"), null];
    }
    case upar_type.cstr: {
      const buf = Buffer.from(str, "utf8");
      if (buf.length > par.size) return [new Error("Too long string"), null];
      return [null, str];
    }
  }
  return [null, str];
}

export function upar_req(): ustr_t {
  return ustr_new();
}

export function upar_req_def(req: ustr_t, par: upar_info) {
  const qry =
    upar_field.type
    | upar_field.sub
    | upar_field.name
    | upar_field.unit
    | upar_field.item
    | upar_field.info
    | upar_field.hint
    | upar_field.get
    | upar_field.def
    | upar_field.min
    | upar_field.max
    | upar_field.stp
    | upar_field.opt
    | upar_field.poll;

  ustr_putuv(req, qry); // put query
  ustr_putuv(req, par.id); // put id
}

export function upar_req_get(req: ustr_t, par: upar_info) {
  const qry = upar_field.get;

  ustr_putuv(req, qry); // put query
  ustr_putuv(req, par.id); // put id
}

export function upar_req_set(req: ustr_t, par: upar_info) {
  const qry = upar_field.set | upar_field.get;

  ustr_putuv(req, qry); // put query
  ustr_putuv(req, par.id); // put id
  upar_put(req, par, par.val); // put value
}

export function upar_res(par: upar_info, res: ustr_t): boolean {
  const fields = ustr_getuv(res);

  if (fields & upar_field.type) {
    const type = ustr_getuv(res);
    par.type = type & 0xf;
    if (type & upar_flag.getable) {
      par.getable = true;
    }
    if (type & upar_flag.setable) {
      par.setable = true;
    }
    if (type & upar_flag.persist) {
      par.persist = true;
    }
    if (type & upar_flag.options) {
      par.options = true;
    }

    if (par.type == upar_type.ufix
      || par.type == upar_type.sfix) {
      par.size = (type >> 8) & 0xff;
      par.frac = type >> 16;
    } else {
      par.size = type >> 8;
    }
  }
  // sub parameters
  if (fields & upar_field.sub) {
    par.sub = [ustr_getuv(res), ustr_getuv(res)];
  }
  // get
  if (fields & upar_field.get) {
    par.val = upar_get(res, par);
  }
  // text
  if (fields & upar_field.name) {
    par.name = ustr_getsl(res);
  }
  if (fields & upar_field.unit) {
    par.unit = ustr_getsl(res);
  }
  if (fields & upar_field.item) {
    par.item = ustr_getsl(res);
  }
  if (fields & upar_field.info) {
    par.info = ustr_getsl(res);
  }
  if (fields & upar_field.hint) {
    par.hint = ustr_getsl(res);
  }
  // val
  if (fields & upar_field.def) {
    par.def = upar_get(res, par);
  }
  if (fields & upar_field.min) {
    par.min = upar_get(res, par);
  }
  if (fields & upar_field.max) {
    par.max = upar_get(res, par);
  }
  if (fields & upar_field.stp) {
    par.stp = upar_get(res, par);
  }
  // opt
  if (fields & upar_field.opt) {
    par.opt = new Array(ustr_getuv(res));
    for (let i = 0; i < par.opt.length; i++) {
      const opt_fields = ustr_getuv(res);
      const opt: upar_opt = { val: upar_get(res, par) };
      // text
      if (opt_fields & upar_field.name) {
        opt.name = ustr_getsl(res);
      }
      if (opt_fields & upar_field.unit) {
        opt.unit = ustr_getsl(res);
      }
      if (opt_fields & upar_field.item) {
        opt.item = ustr_getsl(res);
      }
      if (opt_fields & upar_field.info) {
        opt.info = ustr_getsl(res);
      }
      if (opt_fields & upar_field.hint) {
        opt.hint = ustr_getsl(res);
      }
      par.opt[i] = opt;
    }
  }
  // polling interval
  if (fields & upar_field.poll) {
    par.poll = ustr_getuv(res);
  }

  return true;
}
