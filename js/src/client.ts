import { ustr_t } from "./ustr";
import { upar_info, upar_map, upar_req, upar_req_def, upar_req_get, upar_req_set, upar_res } from "./upar";
import { uparIfaceConnect, UParIfaceHandle } from "./iface";

function upar_list(handle: UParIfaceHandle, pars: upar_map, req_pars: upar_info[], cb: (err?: Error) => void) {
  if (req_pars.length == 0) {
    cb();
    return;
  }
  
  const req = upar_req();
  
  for (const par of req_pars) {
    const len = req.len;
    upar_req_def(req, par);
    if (req.len >= handle.limit()) {
      req.len = len;
      break;
    }
  }
  
  handle.io(req, (err?: Error, res?: ustr_t) => {
    if (err) {
      cb(err);
      return;
    }

    for (; res.pos < res.len; ) {
      const par = req_pars[0];
      upar_res(par, res);
      req_pars.shift();
      pars[par.id] = par;
      if (par.sub) {
        const { sub: [fst, lst] } = par;
        for (let id = fst; id <= lst; id ++) {
          req_pars.push({ id });
        }
      }
    }

    upar_list(handle, pars, req_pars, cb);
  });
}

function upar_find(pars: upar_map, id: number, path: string[]): number | null {
  const par = pars[id];
  if (!par) return null;
  if (!path.length) return id;
  const { sub } = pars[id];
  if (!sub) return null;
  const [fst, lst] = sub;
  for (let i = fst; i <= lst; i++) {
    if (pars[i].name == path[0]) {
      return upar_find(pars, i, path.slice(1));
    }
  }
  return null;
}

export class UParClient {
  _pars: upar_map = {};
  _url: string = "";
  _iface: UParIfaceHandle;

  open(url: string, cb: (error?: Error) => void) {
    this._url = url;
    uparIfaceConnect(url, (error?: Error, handle?: UParIfaceHandle) => {
      if (error) {
        cb(error);
        return;
      }
      
      upar_list(handle, this._pars, [ { id: 0 } ], (error?: Error) => {
        if (error) {
          handle.close();
          cb(error);
          return;
        }

        this._iface = handle;
        cb(null);
      });
    });
  }

  def(id: number): upar_info | null {
    return this._pars[id];
  }

  find(path: string[]): number | null {
    return upar_find(this._pars, 0, path);
  }

  get(pars: upar_info[], cb: (error?: Error, pars?: upar_info[]) => void) {
    const req = upar_req();
    for (let par of pars) {
      upar_req_get(req, par);
    }
    this._iface.io(req, (err?: Error, res?: ustr_t) => {
      if (err) {
        cb(err);
        return;
      }

      if (res) {
        for (let par of pars) {
          upar_res(par, res);
        }
        cb(null, pars);
      }
    });
  }

  set(pars: upar_info[], cb: (error?: Error, pars?: upar_info[]) => void) {
    const req = upar_req();
    for (let par of pars) {
      upar_req_set(req, par);
    }
    this._iface.io(req, (err?: Error, res?: ustr_t) => {
      if (err) {
        cb(err);
        return;
      }

      if (res) {
        for (let par of pars) {
          upar_res(par, res);
        }
        cb(null, pars);
      }
    });
  }
}
