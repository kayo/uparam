import { ustr_t, ustr_getui, ustr_putui } from "./ustr";

let crc32_table: Uint32Array | undefined;

function crc32_init() {
  const POLYNOMIAL = 0xEDB88320;
  crc32_table = new Uint32Array(256);
  for (let byte = 0; byte < 256; byte++) {
    let remainder = byte;
    for (let bit = 8; bit > 0; bit--) {
      remainder = remainder & 1 ?
        (remainder >>> 1) ^ POLYNOMIAL :
        (remainder >>> 1);
    }
    crc32_table[byte] = remainder;
  }
}

const crc32_initial = ~0 >>> 0;

export class crc32 {
  crc: number = crc32_initial;

  constructor() {
    if (crc32_table === undefined) {
      crc32_init();
    }
  }

  reset() {
    this.crc = crc32_initial;
  }

  empty(): boolean {
    return this.crc == crc32_initial;
  }

  update(str: ustr_t) {
    let i: number = str.pos, l: number = str.len;
    for (; i < l; i++) {
      this.crc = (this.crc >>> 8) ^ crc32_table[(this.crc ^ str.buf.readUInt8(i)) & 0xff];
    }
  }

  digest(): number {
    return (this.crc ^ crc32_initial) >>> 0;
  }
}

function ustr_crc32(str: ustr_t): number {
  let crc = new crc32();
  crc.update(str);
  return crc.digest();
}

export function ustr_crc32_put(str: ustr_t) {
  const crc = ustr_crc32(str);
  ustr_putui(str, crc, 32);
}

export function ustr_crc32_get(str: ustr_t): boolean {
  if (str.len < 4) {
    return false;
  }
  str.pos = str.len - 4;
  const crc_in = ustr_getui(str, 32);
  str.pos = 0;
  str.len -= 4;
  const crc = ustr_crc32(str);
  return crc == crc_in;
}

export function ustr_crc32_try(crc: crc32, str: ustr_t): boolean {
  if (str.len < 4) {
    return false;
  }
  str.pos = str.len - 4;
  const crc_in = ustr_getui(str, 32);
  str.pos = 0;
  str.len -= 4;
  crc.update(str);
  if (crc.digest() == crc_in) {
    return true;
  } else {
    str.pos = str.len;
    str.len += 4;
    crc.update(str);
    return false;
  }
}
