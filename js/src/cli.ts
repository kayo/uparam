import { createInterface } from "readline";
import { upar_val, upar_info, upar_map, upar_show, upar_read } from "./upar";
import { UParClient } from "./client";

import "./serial"; // add serial backend
import "./socket"; // add socket backend

const url = process.argv[2];

if (!url) {
    console.log(`Usage: ${process.argv[1]} <url>[ <command>]*`);
    console.log("Commands:");
    console.log("  [<param/name>]?             Describe parameter");
    console.log("  <param/name>                Get parameter value");
    console.log("  <param/name>=<new-value>    Set parameter value");
    console.log("Without commands it runs interactive shell.");
    process.exit(0);
}

const cmds = process.argv.slice(3);

const client = new UParClient();

function parse_cmd(cmd: string): { op: 'def' | 'get' | 'set', path: string[], data?: string } | null {
    const m = cmd.match(/^([^?=]*)(?:([?=])(.*))?$/);
    if (!m) {
        return null;
    }
    const path = m[1] ? m[1].split(/[\.]/) : [];
    switch (m[2]) {
        case '?': return { op: 'def', path };
        case '=': return { op: 'set', path, data: m[3] };
        default: return { op: 'get', path };
    }
}

function execute_cmd(cmd: string, cb: () => void) {
    const { op, path, data } = parse_cmd(cmd);
    const id = client.find(path);
    if (typeof id != "number") {
        console.log("Parameter not exists!");
        cb();
        return;
    }
    switch (op) {
        case 'def': show_def(client, id, cb); break;
        case 'get': get_val(client, id, cb); break;
        case 'set': set_val(client, id, data, cb); break;
    }
}

function complete_path(path: string[], end?: string): string[] | null {
    const id = client.find(path);
    if (typeof id == "number") {
        const par = client.def(id);
        const names = [];
        if (end === undefined) {
            names.push('?');
            if (par.setable) {
                names.push('=');
            }
        }
        if (par.sub) {
            const { sub: [fst, lst] } = par;
            for (let i = fst; i <= lst; i++) {
                const { name } = client.def(i);
                if (name) {
                    if (end === undefined || name.substring(0, end.length) == end) {
                        names.push((path.length == 0 || end !== undefined ? '' : '.') + name);
                    }
                }
            }
        }
        return names;
    }
    return null;
}

function complete_cmd(cmd_line: string): [string[], string] {
    const cmd = cmd_line.split(' ').pop();
    const { path, op, data } = parse_cmd(cmd);
    switch (op) {
        case 'get': {
            const names = complete_path(path);
            if (names) {
                return [names, ''];
            } else {
                const end = path.pop();
                const names = complete_path(path, end);
                if (names) {
                    return [names, end];
                }
            }
        } break;
        case 'set': {
            const id = client.find(path);
            if (typeof id == "number") {
                const par = client.def(id);
                const names = [];
                if (par.opt) {
                    for (let opt of par.opt) {
                        if (opt.name && (!data || opt.name.substr(0, data.length) == data)) {
                            names.push(opt.name);
                        }
                    }
                }
                return [names, data];
            }
        } break;
    }
    return [[], cmd];
}

function show_tree(pars: upar_map, id: number = 0, ident: string = "") {
    const par = pars[id];
    if (id == 0) {
        console.log("<root>");
    }
    if (par.name) {
        console.log(ident + par.name + (par.val !== undefined ? ' = ' + upar_show(par, par.val) : ''));
    }
    if (par.sub) {
        const { sub: [fst, lst] } = par;
        for (let id = fst; id <= lst; id++) {
            show_tree(pars, id, ident + "  ");
        }
    }
}

function wrap_unit(par: upar_info, val: upar_val): string {
    const _val = upar_show(par, val);
    return par.unit ? `${_val} [${par.unit}]` : _val;
}

function show_def(client: UParClient, id: number, cb: () => void) {
    const par = client.def(id);
    let out = par.name ? `${par.name} #${par.id}` : `#${par.id}`;

    if (par.getable || par.setable || par.persist) {
        out += ` (${par.getable ? 'r' : '-'}${par.setable ? 'w' : '-'}${par.persist ? 'p' : '-'})`;
    }

    if (par.info) out += `\n  ; ${par.info}`;
    if (par.item) out += `\n  ! ${par.item}`;
    if (par.hint) out += `\n  ? ${par.hint}`;
    if (par.val) out += `\n  = ${wrap_unit(par, par.val)}`;
    
    if (par.def != undefined || par.min != undefined || par.max != undefined || par.stp != undefined) {
        if (par.def != undefined) out += `\n  * ${wrap_unit(par, par.def)}`;
        if (par.min != undefined || par.max != undefined || par.stp != undefined) {
            out += ' [';
            if (par.min != undefined) out += upar_show(par, par.min);
            out += ' .. ';
            if (par.max != undefined) out += upar_show(par, par.max);
            if (par.stp != undefined) out += `; ${upar_show(par, par.stp)}`;
            out += ']';
        }
    }
    
    if (par.opt) {
        for (var opt of par.opt) {
            out += `\n  @${opt.name} = ${opt.val}`;
            if (opt.info) out += `\n    ; ${opt.info}`;
            if (opt.item) out += `\n    ! ${opt.item}`;
            if (opt.hint) out += `\n    ? ${opt.hint}`;
        }
    }

    if (par.poll) {
        out += `\n  % ${par.poll} mS`;
    }

    if (par.sub) {
        const { sub: [fst, lst] } = par;
        for (let i = fst; i <= lst; i++) {
            const par = client.def(i);
            out += par.name ? `\n  > ${par.name} #${par.id}` : `  > #${par.id}`;
        }
    }

    console.log(out);
    cb();
}

function get_val(client: UParClient, id: number, cb: () => void) {
    const par = client.def(id);

    if (!par.getable) {
        console.log("Cannot get parameter value!");
        cb();
        return;
    }

    client.get([par], (err?: Error, pars?: upar_info[]) => {
        if (err) {
            console.log(err);
            process.exit(1);
        }

        if (pars) {
            const par = pars[0];
            console.log(`= ${upar_show(par, par.val)}`);
            cb();
        }
    });
}

function set_val(client: UParClient, id: number, val: string, cb: () => void) {
    const par = client.def(id);

    if (!par.setable) {
        console.log("Cannot set parameter value!");
        cb();
        return;
    }

    const res = upar_read(par, val);

    if (res[0]) {
        console.log(res[0]);
        cb();
        return;
    }
    
    par.val = res[1];
    
    client.set([par], (err?: Error, pars?: upar_info[]) => {
        if (err) {
            console.log(err);
            process.exit(1);
        }

        if (pars) {
            const par = pars[0];
            console.log(`= ${upar_show(par, par.val)}`);
            cb();
        }
    });
}

client.open(url, (error?: Error) => {
    if (error) {
        console.error(`Unable to connect: ${error}`);
        process.exit(1);
    }

    if (cmds.length > 0) {
        // batch mode
        const exec = () => {
            if (!cmds.length) {
                process.exit(0);
            }

            execute_cmd(cmds.shift(), exec);
        }

        exec();
    } else {
        // interactive shell
        const rl = createInterface({
            input: process.stdin,
            output: process.stdout,
            completer: complete_cmd,
        });

        rl.on('line', (cmd) => {
            execute_cmd(cmd, () => {
                rl.prompt();
            })
        });

        rl.setPrompt("> ");
        rl.prompt();
    }
});
