export interface ustr_t {
  buf: Buffer;
  res: (str: ustr_t, len: number) => void;
  pos: number;
  len: number;
}

function roundup32(x) {
  x--;
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  x++;
  return x;
}

function ustr_res(str: ustr_t, len: number) {
  if (str.buf.length < len) {
    const buf = Buffer.allocUnsafe(roundup32(len));
    str.buf.copy(buf);
    str.buf = buf;
  }
}

export function ustr_new(len: number = 32): ustr_t {
  return {
    buf: Buffer.allocUnsafe(len),
    res: ustr_res,
    pos: 0,
    len: 0,
  };
}

export function ustr_from(buf: Buffer): ustr_t {
  return {
    buf: buf,
    res: ustr_res,
    pos: 0,
    len: buf.length,
  };
}

export function ustr_putuv(str: ustr_t, val: number) {
  let bytes: number = 1;
  let _val: number = val;

  for (; _val >= (1 << 7); _val >>= 7, bytes++);

  str.res(str, str.len + bytes);

  let pos = str.len;
  let end = pos + bytes - 1;

  for (; pos < end; val >>= 7, pos++) {
    str.buf.writeUInt8((val & 0x7f) | (1 << 7), pos);
  }

  str.buf.writeUInt8(val & 0x7f, pos);

  str.len += bytes;
}

export function ustr_getuv(str: ustr_t): number {
  let bytes: number = 1;
  let pos: number = str.pos;

  for (; str.buf.readUInt8(pos) & (1 << 7); pos++ , bytes++) {
    if (pos >= str.len) {
      return -1;
    }
  }

  let shift: number = 0;
  let val = 0;

  for (; ; str.pos++ , shift += 7) {
    val += (str.buf.readUInt8(str.pos) & 0x7f) << shift;

    if (!(str.buf.readUInt8(str.pos) & (1 << 7))) {
      str.pos++;
      break;
    }
  }

  return val;
}

export function ustr_putsl(str: ustr_t, val: string) {
  const buf = Buffer.from(val, 'utf8');
  const len = str.len + buf.length;
  ustr_putuv(str, buf.length);
  str.res(str, len);
  buf.copy(str.buf, str.len);
  str.len = len;
}

export function ustr_getsl(str: ustr_t): string {
  const len = ustr_getuv(str);
  const val = str.buf.toString('utf8', str.pos, str.pos + len);
  str.pos += len;
  return val;
}

export function ustr_putbd(str: ustr_t, val: number[], len: number) {
  str.res(str, str.len + len);
  let i = 0;
  for (; i < val.length; i++) {
    str.buf.writeUInt8(val[i], str.len++);
  }
  for (; i < len; i++) {
    str.buf.writeUInt8(0, str.len++);
  }
}

export function ustr_getbd(str: ustr_t, len: number): number[] {
  const val: number[] = new Array(len);
  let i = 0;
  for (; i < len; i++) {
    val[i] = str.buf.readUInt8(str.pos++);
  }
  return val;
}

export function ustr_puthd(str: ustr_t, val: number[], len: number) {
  str.res(str, str.len + len);
  let i = 0;
  for (; i < val.length; i++) {
    str.buf.writeUInt16LE(val[i], str.len++);
  }
  for (; i < len; i++) {
    str.buf.writeUInt16LE(0, str.len++);
  }
}

export function ustr_gethd(str: ustr_t, len: number): number[] {
  const val: number[] = new Array(len);
  let i = 0;
  for (; i < len; i++) {
    val[i] = str.buf.readUInt16LE(str.pos++);
  }
  return val;
}

export type unum_bits_t = 8 | 16 | 32;

export function ustr_getui(str: ustr_t, bits: unum_bits_t): number {
  let bytes = bits >> 3;
  let val = str.buf.readUIntLE(str.pos, bytes);
  str.pos += bytes;
  return val;
}

export function ustr_getsi(str: ustr_t, bits: unum_bits_t): number {
  let bytes = bits >> 3;
  let val = str.buf.readIntLE(str.pos, bytes);
  str.pos += bytes;
  return val;
}

export function ustr_putui(str: ustr_t, val: number, bits: unum_bits_t) {
  let bytes = bits >> 3;
  let len = str.len + bytes;
  str.res(str, len);
  str.buf.writeUIntLE(val, str.len, bytes);
  str.len = len;
}

export function ustr_putsi(str: ustr_t, val: number, bits: unum_bits_t) {
  let bytes = bits >> 3;
  let len = str.len + bytes;
  str.res(str, len);
  str.buf.writeIntLE(val, str.len, bytes);
  str.len = len;
}

export function ustr_getrn(str: ustr_t, bits: unum_bits_t): number {
  let bytes = bits >> 3;
  let val = bytes == 4 ?
    str.buf.readFloatLE(str.pos) :
    str.buf.readDoubleLE(str.pos);
  str.pos += bytes;
  return val;
}

export function ustr_putrn(str: ustr_t, val: number, bits: unum_bits_t) {
  let bytes = bits >> 3;
  let len = str.len + bytes;
  str.res(str, len);
  bytes == 4 ?
    str.buf.writeFloatLE(val, str.len) :
    str.buf.writeDoubleLE(val, str.len);
  str.len = len;
}
