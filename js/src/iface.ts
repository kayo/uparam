import { ustr_t, ustr_from } from './ustr';
import { crc32, ustr_crc32_try, ustr_crc32_put } from "./crc32";
import { parse, Url } from 'url';
import { Duplex } from 'stream';

export interface UParIfaceOnResponse {
  (error: Error);
  (error: null, response: ustr_t);
}

export interface UParIfaceHandle {
  limit(): number;
  io(req: ustr_t, cb: UParIfaceOnResponse);
  close();
}

export interface UParIfaceOnConnect {
  (error: Error);
  (error: null, handle: UParIfaceHandle);
}

export interface UParIfaceConnect {
  (url: Url, cb: UParIfaceOnConnect);
}

const ifaces: { [protocol: string]: UParIfaceConnect } = {};

export function uparIfaceRegister(protocol: string, connect: UParIfaceConnect) {
  ifaces[protocol] = connect;
}

export function uparIfaceConnect(to: string, cb: UParIfaceOnConnect) {
  const url = parse(to);
  const connect = ifaces[url.protocol];
  if (!connect) {
    cb(new Error(`Unsupported protocol: ${url.protocol}`));
    return;
  }
  connect(url, cb);
}

export class UParIfaceStream implements UParIfaceHandle {
  _stm: Duplex;
  _tim: number;
  _ret: number;

  constructor(stream: Duplex, timeout: number = 100, retries: number = 3) {
    this._stm = stream;
    this._tim = timeout;
    this._ret = retries;
    this._stm.on('data', (data: Buffer) => { this._res_data(data); });
  }

  limit(): number {
    return 1024 - 4;
  }

  _res_datas: Buffer[] = [];
  _res_crc32: crc32 = new crc32();
  _res_timer: any;

  _res_data(data: Buffer) {
    clearTimeout(this._res_timer);

    this._res_datas.push(data);

    if (ustr_crc32_try(this._res_crc32, ustr_from(data))) {
      this._res_crc32.reset();
      this._res_done();
    } else {
      this._res_pend();
    }
  }

  _res_pend() {
    this._res_timer = setTimeout(() => {
      this._res_done();
    }, this._tim);
  }

  _res_done() {
    this._res_timer = null;
    const r = this._q[0];
    const datas = this._res_datas;
    this._res_datas = [];
    if (datas.length > 0 && this._res_crc32.empty()) {
      this._q.shift();
      const data = Buffer.concat(datas);
      const res = ustr_from(data);
      res.len -= 4;
      //console.log("recv:", res.buf.slice(0, res.len));
      //console.log("recv:", res.len);
      r.cb(null, res);
    } else {
      this._res_crc32.reset();
      if (r.ret > 0) {
        r.ret--;
      } else {
        this._q.shift();
        r.cb(new Error("IO Error"));
      }
    }
    // resume queued requests
    this._deq();
  }

  _q: { req: ustr_t, cb: UParIfaceOnResponse, ret: number }[] = [];

  io(req: ustr_t, cb: UParIfaceOnResponse) {
    ustr_crc32_put(req);
    //console.log("send:", req.buf.slice(0, req.len));
    //console.log("send:", req.len);
    this._q.push({ req, cb, ret: this._ret });
    this._deq();
  }

  _deq() {
    if (!this._res_timer && this._q.length) {
      const r = this._q[0];
      this._stm.write(r.req.buf.slice(0, r.req.len));
      this._res_pend();
    }
  }

  close() { }
}
