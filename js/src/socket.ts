import { Url } from 'url';
import { Socket } from 'net';

import { UParIfaceStream, UParIfaceOnConnect, uparIfaceRegister } from "./iface";

class SocketIface extends UParIfaceStream {
    constructor(public conn: {host: string, port: number} | {path: string}) {
        super(new Socket({
            readable: true,
            writable: true,
        }), 500);
    }
    
    open(cb: UParIfaceOnConnect) {
        (this._stm as any as Socket).connect(this.conn, (err) => {
            if (err) {
                cb(err);
            } else {
                cb(null, this);
            }
        });
    }
    
    close() {
        (this._stm as any as Socket).destroy();
    }
}

function connect(url: Url, cb: UParIfaceOnConnect) {
    const { hostname: host, port, pathname: path } = url;
    const iface = new SocketIface(host ? {host, port: parseInt(port, 10) || 23} : {path});
    iface.open(cb);
}

uparIfaceRegister("socket:", connect);
