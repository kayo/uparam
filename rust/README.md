# UParam client

This directory contains interfaces for uparam-aware devices written in [Rust](https://rust-lang.org/):

1. Client library ([uparam](uparam))
2. Command-line interface ([uparam-cli](uparam-cli))
3. Terminal user interface ([uparam-tui](uparam-tui))
