mod config;
mod iface;
mod input;
mod result;

use flexi_logger::Logger;
use structopt::StructOpt;

use uparam::{reconnect, Manager};

#[cfg(feature = "tcp")]
use {unicom_nres::DefaultResolver, unicom_tcp::TcpSocket};

#[cfg(feature = "unix")]
use unicom_unix::UnixSocket;

#[cfg(feature = "serial")]
use unicom_serial::SerialPort;

use iface::Iface;
use result::Result;

#[derive(Debug, StructOpt)]
#[structopt(about)]
struct Args {
    /// Specify device URL
    #[structopt(short = "d", long, env)]
    device: Option<String>,

    /// Specify log directory
    #[structopt(short = "l", long, env)]
    logdir: Option<String>,

    /// Prints version information
    #[structopt(short = "V", long)]
    version: bool,
}

#[paw::main]
#[tokio::main]
async fn main(args: Args) -> Result<()> {
    if args.version {
        println!("{}", env!("CARGO_PKG_VERSION"));
        return Ok(());
    }

    if let Some(path) = args.logdir {
        Logger::with_env()
            .log_to_file()
            .directory(path)
            .suppress_timestamp()
            .append()
            .start()?;
    }

    let url = args.device.ok_or("Device URL is missing")?;

    let manager = Manager::default();

    manager.register(TcpSocket::new(DefaultResolver::default()))?;

    #[cfg(unix)]
    manager.register(UnixSocket::default())?;

    manager.register(SerialPort::default())?;

    let options = manager
        .from_url(&url)?
        .with_reconnect_strategy(reconnect::Delayed::unlimited());

    let client = options.open();

    let iface = Iface::new(client, url)?;

    iface.run().await?;

    Ok(())
}
