use std::{
    fmt::Write,
    io::{stdout, Stdout},
};

use futures::{select, FutureExt, StreamExt};
use tokio::{io::stdin, signal::ctrl_c};

use log::{debug, info};
use serde::{Deserialize, Serialize};
use termion::{
    event::{Event as InputEvent, Key},
    input::MouseTerminal,
    raw::{IntoRawMode, RawTerminal},
    screen::AlternateScreen,
};

use tui::{
    backend::{Backend, TermionBackend},
    layout::{Alignment, Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    widgets::{Block, Borders, List, Paragraph, Text, Widget},
    Frame, Terminal,
};

use uparam::{
    types::{Par, ParId},
    Event as ClientEvent, Handle, Ident,
};

use crate::{config::Config, input::TermReadAsync, Result};

#[derive(Serialize, Deserialize)]
pub struct ViewConfig {
    params: Vec<Ident<'static>>,
}

pub struct ViewParam {
    group: ParId,
    selected: usize,
}

pub struct ViewBoard {
    params: Vec<ParId>,
    selected: usize,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum ViewTab {
    Board,
    Param,
}

pub struct Iface {
    handle: Handle,
    device: String,
    config: Config,
    term: Terminal<TermionBackend<AlternateScreen<MouseTerminal<RawTerminal<Stdout>>>>>,
    size: Rect,
    /// view tab
    view: ViewTab,
    /// board view state
    board: ViewBoard,
    /// params view state
    param: ViewParam,
    /// show info
    info: bool,
    /// show help
    help: bool,
}

impl Iface {
    pub fn new(handle: Handle, device: String) -> Result<Self> {
        let config = Config::new(env!("CARGO_PKG_NAME"), &device);

        let stdout = stdout().into_raw_mode()?;
        let stdout = MouseTerminal::from(stdout);
        let stdout = AlternateScreen::from(stdout);
        let backend = TermionBackend::new(stdout);
        let mut term = Terminal::new(backend)?;
        term.hide_cursor()?;

        Ok(Self {
            handle,
            device,
            config,
            term,
            size: Rect::default(),
            view: ViewTab::Param,
            board: ViewBoard {
                params: vec![],
                selected: 0,
            },
            param: ViewParam {
                group: 0,
                selected: 0,
            },
            info: true,
            help: true,
        })
    }

    pub async fn run(mut self) -> Result<()> {
        let mut client_events = self.handle.on().fuse();
        let mut input_events = stdin().events_stream().fuse();

        loop {
            // redraw view
            self.render()?;

            select! {
                // client event
                event = client_events.next() => match event {
                    Some(event) => self.on_client(&event),
                    None => break,
                },
                // input event
                event = input_events.next() => match event {
                    Some(Ok(event)) => // handle keypress
                        if self.on_input(&event) {
                            // continue
                        } else {
                            // quit
                            break;
                        },
                    Some(Err(_error)) => (),
                    None => break,
                },
                event = ctrl_c().fuse() => break,
            }
        }

        self.save();
        // clear screen
        self.term.clear()?;

        Ok(())
    }

    fn on_input(&mut self, event: &InputEvent) -> bool {
        use self::Key::*;

        debug!("input event: {:?}", event);

        if let InputEvent::Key(key) = event {
            match key {
                Esc | Char('q') | Ctrl('c') => {
                    return false;
                }
                // toggle info
                Char('i') | Ctrl('i') => {
                    self.info = !self.info;
                }
                // toggle help
                Char('h') | Ctrl('h') => {
                    self.help = !self.help;
                }
                // switch view
                Char('\t') | Ctrl('o') => {
                    self.view = if self.view == ViewTab::Board {
                        ViewTab::Param
                    } else {
                        ViewTab::Board
                    };
                }
                _ => (),
            }
        }

        let param_view = &mut self.param;
        let board_view = &mut self.board;

        match self.view {
            ViewTab::Board => {
                let len = board_view.params.len();
                if let InputEvent::Key(key) = event {
                    match key {
                        // goto previous
                        Up | Ctrl('p') => {
                            if board_view.selected > 0 {
                                board_view.selected -= 1;
                            } else if len > 0 {
                                board_view.selected = len - 1;
                            }
                        }
                        // goto next
                        Down | Ctrl('n') => {
                            if board_view.selected + 1 < len {
                                board_view.selected += 1;
                            } else {
                                board_view.selected = 0;
                            }
                        }
                        // unselect
                        Char(' ') | Ctrl('m') | Backspace => {
                            board_view.params.remove(board_view.selected);
                            let len = board_view.params.len();
                            if len > 0 {
                                if board_view.selected >= len {
                                    board_view.selected = len - 1;
                                } else {
                                    board_view.selected = 0;
                                }
                            } else {
                                board_view.selected = 0;
                            }
                        }
                        _ => {}
                    }
                }
            }
            // select parameters to view in dashboard
            ViewTab::Param => {
                if let Ok(group) = self.handle.param(param_view.group) {
                    if let Some(param) = group.children().unwrap().nth(param_view.selected) {
                        let len = group.children().unwrap().count();
                        if let InputEvent::Key(key) = event {
                            match key {
                                // goto previous
                                Up | Ctrl('n') => {
                                    if param_view.selected > 0 {
                                        param_view.selected -= 1;
                                    } else if len > 0 {
                                        param_view.selected = len - 1;
                                    }
                                }
                                // goto next
                                Down | Ctrl('p') => {
                                    if param_view.selected + 1 < len {
                                        param_view.selected += 1;
                                    } else {
                                        param_view.selected = 0;
                                    }
                                }
                                // goto parent
                                Left | Ctrl('u') | Backspace => {
                                    if let Some(id) = group.parent_id().unwrap() {
                                        param_view.group = id;
                                        group
                                            .parent()
                                            .map(|param| {
                                                param
                                                    .children()
                                                    .unwrap()
                                                    .position(|param| param.id() == group.id())
                                                    .map(|id| param_view.selected = id)
                                            })
                                            .unwrap();
                                    }
                                }
                                // goto child
                                Right | Ctrl('d') | Char('\n') => {
                                    if param.has_children().unwrap() {
                                        param_view.group = param.id();
                                        param_view.selected = 0;
                                    }
                                }
                                // select
                                Char(' ') | Ctrl('m') => {
                                    param
                                        .with_def(|def| {
                                            if def.getable || def.setable {
                                                if let Some(pos) = board_view
                                                    .params
                                                    .iter()
                                                    .position(|id| id == &param.id())
                                                {
                                                    board_view.params.remove(pos);
                                                    let len = board_view.params.len();
                                                    if len == 0 || board_view.selected >= len {
                                                        board_view.selected = 0;
                                                    }
                                                } else {
                                                    board_view.params.push(param.id());
                                                }
                                            }
                                        })
                                        .unwrap();
                                }
                                _ => {}
                            }
                        }
                    }
                }
            }
        }

        true
    }

    fn on_client(&mut self, event: &ClientEvent) {
        use self::ClientEvent::*;
        debug!("client event: {:?}", event);
        match event {
            Retrieved => {
                self.load();
            }
            _ => (),
        }
    }

    fn render(&mut self) -> Result<()> {
        let size = self.term.size().unwrap();

        if size != self.size {
            debug!("Resize terminal: {:?}", size);
            self.term.resize(size).unwrap();
            self.size = size;
        }

        let handle = &self.handle;
        let view = self.view;
        let show_info = self.info;
        let show_help = self.help;
        let param_view = &self.param;
        let board_view = &self.board;
        let url = &self.device;

        let param_title = if let Ok(param) = handle.param(param_view.group) {
            if let Some(path) = param.path().unwrap() {
                format!("[ Param ({}) ]", path)
            } else {
                "[ Param (<root>) ]".into()
            }
        } else {
            "[ Param (<none>) ]".into()
        };
        let board_title = format!("[ Board ({}) ]", url);

        /*
        .------------.------------.
        | main panel | info panel |
        |------------'------------|
        |        help panel       |
        '-------------------------'
         */

        self.term.draw(|mut f| {
            let (main_panel, help_panel) = if show_help {
                let chunks = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints([Constraint::Percentage(90), Constraint::Min(2)].as_ref())
                    .split(size);
                (chunks[0], Some(chunks[1]))
            } else {
                (size, None)
            };

            let (main_panel, info_panel) = if show_info {
                let chunks = Layout::default()
                    .direction(Direction::Horizontal)
                    .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
                    .split(main_panel);
                (chunks[0], Some(chunks[1]))
            } else {
                (main_panel, None)
            };

            let style = Style::default().fg(Color::White);
            let group_style = (style.fg(Color::LightMagenta), style.fg(Color::Magenta));
            let param_style = (style.fg(Color::LightGreen), style.fg(Color::Green));
            let frame_style = (style.fg(Color::Green), style.fg(Color::Gray));

            if view == ViewTab::Param {
                if let Ok(param) = handle.param(param_view.group) {
                    let items = param.children().unwrap().enumerate().map(|(i, param)| {
                        param
                            .with_def(|def| {
                                let mut t = String::new();

                                if let Some(item) = &def.item {
                                    write!(t, "{}", item).unwrap();
                                } else if let Some(info) = &def.info {
                                    write!(t, "{}", info).unwrap();
                                } else if let Some(name) = &def.name {
                                    write!(t, "({})", name).unwrap();
                                } else {
                                    write!(t, "({})", def.id).unwrap();
                                }

                                if let Some(val) = &def.val {
                                    write!(t, ": {}", val).unwrap();
                                }

                                if let Some(unit) = &def.unit {
                                    write!(t, " {}", unit).unwrap();
                                }

                                Text::styled(
                                    t,
                                    if def.sub.is_some() {
                                        if i == param_view.selected {
                                            group_style.0
                                        } else {
                                            group_style.1
                                        }
                                    } else if i == param_view.selected {
                                        param_style.0
                                    } else {
                                        param_style.1
                                    },
                                )
                            })
                            .unwrap()
                    });

                    let block = Block::default()
                        .borders(Borders::ALL)
                        .title(&param_title)
                        .title_style(frame_style.0)
                        .border_style(frame_style.0);

                    List::new(items).block(block).render(&mut f, main_panel);
                }
            }

            if view == ViewTab::Board {
                let items = board_view
                    .params
                    .iter()
                    .map(|id| handle.param(id))
                    .filter_map(|par| par.ok())
                    .enumerate()
                    .map(|(i, param)| {
                        param
                            .with_def(|def| {
                                let mut t = String::new();

                                if let Some(item) = &def.item {
                                    write!(t, "{}", item).unwrap();
                                } else if let Some(info) = &def.info {
                                    write!(t, "{}", info).unwrap();
                                } else if let Some(path) = param.path().unwrap() {
                                    write!(t, "({})", path).unwrap();
                                } else {
                                    write!(t, "(#{})", def.id).unwrap();
                                }

                                if let Some(val) = &def.val {
                                    write!(t, ": {}", val).unwrap();
                                }

                                if let Some(unit) = &def.unit {
                                    write!(t, " {}", unit).unwrap();
                                }

                                Text::styled(
                                    t,
                                    if i == board_view.selected {
                                        param_style.0
                                    } else {
                                        param_style.1
                                    },
                                )
                            })
                            .unwrap()
                    });

                let block = Block::default()
                    .borders(Borders::ALL)
                    .title(&board_title)
                    .title_style(frame_style.0)
                    .border_style(frame_style.0);

                List::new(items).block(block).render(&mut f, main_panel);
            }

            if let Some(info_panel) = info_panel {
                if let Some(param) = match view {
                    ViewTab::Board => {
                        if !board_view.params.is_empty() {
                            if let Ok(param) = handle.param(board_view.params[board_view.selected])
                            {
                                Some(param)
                            } else {
                                None
                            }
                        } else {
                            None
                        }
                    }
                    ViewTab::Param => {
                        if let Ok(param) = handle.param(param_view.group) {
                            param.children().unwrap().nth(param_view.selected)
                        } else {
                            None
                        }
                    }
                } {
                    info_block(
                        &param.path().unwrap(),
                        &param.get_def().unwrap(),
                        &mut f,
                        info_panel,
                    );
                }
            }

            if let Some(help_panel) = help_panel {
                help_block(&mut f, help_panel);
            }
        })?;

        Ok(())
    }

    fn load(&mut self) {
        if let Some(ViewConfig { params }) = self.config.load() {
            info!("Loaded parameters: {:?}", params);
            self.board.params = params
                .iter()
                .map(|ident| self.handle.param(ident).map(|param| param.id()))
                .filter_map(|par| par.ok())
                .collect();
        }
    }

    fn save(&self) {
        self.config.save(&ViewConfig {
            params: self
                .board
                .params
                .iter()
                .map(|id| {
                    self.handle.param(id).map(|param| {
                        param
                            .path()
                            .unwrap()
                            .map(Ident::from)
                            .unwrap_or_else(|| Ident::from(id))
                    })
                })
                .filter_map(|par| par.ok())
                .collect(),
        })
    }
}

fn info_block<B: Backend>(path: &Option<String>, def: &Par, f: &mut Frame<B>, area: Rect) {
    let style = Style::default();
    let pure_style = style;
    let frame_style = style.fg(Color::LightBlue);
    let cons_field_style = style.fg(Color::Green);
    let text_field_style = style.fg(Color::Magenta);
    let cons_value_style = style.fg(Color::Cyan);
    let text_value_style = style.fg(Color::Yellow);

    let mut text = Vec::new();

    // id
    text.push(Text::styled("Id", cons_field_style));
    text.push(Text::raw(": "));
    text.push(Text::styled(def.id.to_string(), cons_value_style));
    text.push(Text::raw("\n"));

    // type
    if let Some(type_) = def.type_ {
        text.push(Text::styled("Type", cons_field_style));
        text.push(Text::raw(": "));
        text.push(Text::styled(type_.to_string(), cons_value_style));
        text.push(Text::raw("\n"));

        // size
        if let Some(size) = def.size {
            text.push(Text::styled("Size", cons_field_style));
            text.push(Text::raw(": "));
            text.push(Text::styled(size.to_string(), cons_value_style));
            text.push(Text::raw("\n"));
        }

        // frac
        if let Some(frac) = def.frac {
            text.push(Text::styled("Fraction", cons_field_style));
            text.push(Text::raw(": "));
            text.push(Text::styled(frac.to_string(), cons_value_style));
            text.push(Text::raw("\n"));
        }
    }

    // text fields
    for (field, value) in &[
        ("Name", &def.name),
        ("Info", &def.info),
        ("Item", &def.item),
        ("Hint", &def.hint),
        ("Unit", &def.unit),
    ] {
        if let Some(value) = value {
            text.push(Text::styled(*field, text_field_style));
            text.push(Text::raw(": "));
            text.push(Text::styled(value, text_value_style));
            text.push(Text::raw("\n"));
        }
    }

    // constraint fields
    for (field, value) in &[
        ("Default", &def.def),
        ("Min", &def.min),
        ("Max", &def.max),
        ("Step", &def.stp),
    ] {
        if let Some(value) = value {
            text.push(Text::styled(*field, cons_field_style));
            text.push(Text::raw(": "));
            text.push(Text::styled(value.to_string(), cons_value_style));
            text.push(Text::raw("\n"));
        }
    }

    // option fields
    if !def.opt.is_empty() {
        text.push(Text::styled("Options", cons_field_style));
        text.push(Text::raw(":\n"));
        for opt in &def.opt {
            text.push(Text::raw("  - "));
            text.push(Text::styled("Value", cons_field_style));
            text.push(Text::raw(": "));
            text.push(Text::styled(opt.val.to_string(), cons_value_style));
            text.push(Text::raw("\n"));
            // text fields
            for (field, value) in &[
                ("Name", &opt.name),
                ("Info", &opt.info),
                ("Item", &opt.item),
                ("Hint", &opt.hint),
                ("Unit", &opt.unit),
            ] {
                if let Some(value) = value {
                    text.push(Text::raw("    "));
                    text.push(Text::styled(*field, text_field_style));
                    text.push(Text::raw(": "));
                    text.push(Text::styled(value, text_value_style));
                    text.push(Text::raw("\n"));
                }
            }
        }
    }

    let title = if let Some(path) = path {
        format!("[ Info ({}) ]", path)
    } else {
        "[ Info ]".into()
    };

    let block = Block::default()
        .title(&title)
        .borders(Borders::ALL)
        .title_style(frame_style)
        .border_style(frame_style);

    Paragraph::new(text.iter())
        .block(block)
        .style(pure_style)
        .alignment(Alignment::Left)
        .wrap(true)
        .render(f, area)
}

fn help_block<B: Backend>(f: &mut Frame<B>, area: Rect) {
    let style = Style::default();
    let keys_style = style.fg(Color::Red);
    let text_style = style;

    let text = [
        Text::styled("[Q]", keys_style),
        Text::raw(": Quit, "),
        Text::styled("[TAB]", keys_style),
        Text::raw(": Switch view, "),
        Text::styled("[↑] [↓] [←] [→]", keys_style),
        Text::raw(" Tree navigation, "),
        Text::styled("[SPACE]", keys_style),
        Text::raw(" Select/deselect, "),
        Text::styled("[I]", keys_style),
        Text::raw(" Toggle info, "),
        Text::styled("[H]", keys_style),
        Text::raw(" Toggle help"),
    ];

    Paragraph::new(text.iter())
        .block(Block::default().title("[ Help ]").borders(Borders::ALL))
        .style(text_style)
        .alignment(Alignment::Right)
        .wrap(true)
        .render(f, area)
}
