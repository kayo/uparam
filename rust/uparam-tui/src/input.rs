use std::{
    io::{Error as IoError, ErrorKind as IoErrorKind, Write},
    iter::empty,
    pin::Pin,
};

use bytes::{Buf, BytesMut};
use futures::{
    task::{Context, Poll},
    Future, Stream,
};
use pin_project::pin_project;
use tokio::io::AsyncRead;
use tokio_util::codec::{Decoder, FramedRead};

use termion::{
    event::{self, Event, Key},
    raw::IntoRawMode,
};

/// A stream of input keys.
#[pin_project]
pub struct KeysStream<R> {
    #[pin]
    inner: EventsStream<R>,
}

impl<R: AsyncRead> Stream for KeysStream<R> {
    type Item = Result<Key, IoError>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        use self::Poll::*;

        match self.project().inner.poll_next(cx) {
            Ready(Some(Ok(Event::Key(key)))) => Ready(Some(Ok(key))),
            Ready(Some(Err(error))) => Ready(Some(Err(error))),
            Ready(Some(_)) => Pending,
            Ready(None) => Ready(None),
            Pending => Pending,
        }
    }
}

/// An iterator over input events.
#[pin_project]
pub struct EventsStream<R> {
    #[pin]
    inner: EventsAndRawStream<R>,
}

impl<R: AsyncRead> Stream for EventsStream<R> {
    type Item = Result<Event, IoError>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        self.project()
            .inner
            .poll_next(cx)
            .map(|event| event.map(|option| option.map(|(event, _raw)| event)))
    }
}

/// An iterator over input events and the bytes that define them
type EventsAndRawStream<R> = FramedRead<R, EventsAndRawDecoder>;

pub struct EventsAndRawDecoder;

impl Decoder for EventsAndRawDecoder {
    type Item = (Event, Vec<u8>);
    type Error = IoError;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        match src.len() {
            0 => Ok(None),
            1 => match src[0] {
                b'\x1B' => {
                    src.advance(1);
                    Ok(Some((Event::Key(Key::Esc), vec![b'\x1B'])))
                }
                c => {
                    if let Ok(res) = parse_event(c, &mut empty()) {
                        src.advance(1);
                        Ok(Some(res))
                    } else {
                        Ok(None)
                    }
                }
            },
            _ => {
                let (off, res) = if let Some((c, cs)) = src.split_first() {
                    let mut it = cs.iter().cloned().map(Ok);
                    if let Ok(res) = parse_event(*c, &mut it) {
                        (1 + cs.len() - it.len(), Ok(Some(res)))
                    } else {
                        (0, Ok(None))
                    }
                } else {
                    (0, Ok(None))
                };

                src.advance(off);
                res
            }
        }
    }
}

fn parse_event<I>(item: u8, iter: &mut I) -> Result<(Event, Vec<u8>), IoError>
where
    I: Iterator<Item = Result<u8, IoError>>,
{
    let mut buf = vec![item];
    let result = {
        let mut iter = iter.inspect(|byte| {
            if let Ok(byte) = byte {
                buf.push(*byte);
            }
        });
        event::parse_event(item, &mut iter)
    };
    result
        .or_else(|_| Ok(Event::Unsupported(buf.clone())))
        .map(|e| (e, buf))
}

#[pin_project]
pub struct ReadLineFuture<R> {
    #[pin]
    source: R,
    buffer: Vec<u8>,
}

impl<R> ReadLineFuture<R> {
    fn new(source: R) -> Self {
        Self {
            source,
            buffer: Vec::with_capacity(30),
        }
    }
}

impl<R: AsyncRead> Future for ReadLineFuture<R> {
    type Output = Result<Option<String>, IoError>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        use self::Poll::*;

        let mut byte = [0u8; 1];

        let this = self.project();

        match this.source.poll_read(cx, &mut byte) {
            Ready(Ok(1)) => match byte[0] {
                0 | 3 | 4 => return Ready(Ok(None)),
                0x7f => {
                    this.buffer.pop();
                }
                b'\n' | b'\r' => {
                    let string = String::from_utf8(this.buffer.clone())
                        .map_err(|e| IoError::new(IoErrorKind::InvalidData, e))?;
                    this.buffer.clear();
                    return Ready(Ok(Some(string)));
                }
                c => {
                    this.buffer.push(c);
                }
            },
            Ready(Ok(_)) => (),
            Ready(Err(e)) => return Ready(Err(e)),
            Pending => (),
        }

        Pending
    }
}

/// Extension to `Read` trait.
pub trait TermReadAsync: Sized {
    /// An iterator over input events.
    fn events_stream(self) -> EventsStream<Self>
    where
        Self: Sized;

    /// An iterator over key inputs.
    fn keys_stream(self) -> KeysStream<Self>
    where
        Self: Sized;

    /// Read a line.
    ///
    /// EOT and ETX will abort the prompt, returning `None`. Newline or carriage return will
    /// complete the input.
    fn read_line_future(self) -> ReadLineFuture<Self>;

    /// Read a password.
    ///
    /// EOT and ETX will abort the prompt, returning `None`. Newline or carriage return will
    /// complete the input.
    fn read_passwd_future<W: Write>(self, writer: &mut W) -> ReadLineFuture<Self> {
        if let Err(error) = writer.into_raw_mode() {
            panic!("Failed to turn terminal into raw mode: {}", error);
        } else {
            self.read_line_future()
        }
    }
}

impl<R: AsyncRead + TermReadAsyncEventsAndRaw> TermReadAsync for R {
    fn events_stream(self) -> EventsStream<Self> {
        EventsStream {
            inner: self.events_and_raw_stream(),
        }
    }
    fn keys_stream(self) -> KeysStream<Self> {
        KeysStream {
            inner: self.events_stream(),
        }
    }

    fn read_line_future(self) -> ReadLineFuture<Self> {
        ReadLineFuture::new(self)
    }
}

/// Extension to `TermReadAsync` trait. A separate trait in order to maintain backwards compatibility.
pub trait TermReadAsyncEventsAndRaw {
    /// An iterator over input events and the bytes that define them.
    fn events_and_raw_stream(self) -> EventsAndRawStream<Self>
    where
        Self: Sized;
}

impl<R: AsyncRead> TermReadAsyncEventsAndRaw for R {
    fn events_and_raw_stream(self) -> EventsAndRawStream<Self> {
        FramedRead::new(self, EventsAndRawDecoder)
    }
}
