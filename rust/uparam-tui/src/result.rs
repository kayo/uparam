use flexi_logger::FlexiLoggerError as LogError;
use std::{
    error::Error as StdError,
    fmt::{self, Display, Formatter, Result as FmtResult},
    io::Error as IoError,
    result::Result as StdResult,
};
use uparam::{low, Error as ParError};

pub type Result<T> = StdResult<T, Error>;

pub enum Error {
    ParError(ParError),
    IoError(IoError),
    CmdError(String),
    LogError(LogError),
}

impl StdError for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::Error::*;

        match self {
            ParError(error) => {
                "Client error: ".fmt(f)?;
                error.fmt(f)
            }
            IoError(error) => {
                "IO error: ".fmt(f)?;
                error.fmt(f)
            }
            CmdError(error) => {
                "Command error: ".fmt(f)?;
                error.fmt(f)
            }
            LogError(error) => {
                "Logger error: ".fmt(f)?;
                error.fmt(f)
            }
        }
    }
}

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        (self as &dyn Display).fmt(f)
    }
}

impl From<ParError> for Error {
    fn from(error: ParError) -> Self {
        Self::ParError(error)
    }
}

impl From<IoError> for Error {
    fn from(error: IoError) -> Self {
        Self::IoError(error)
    }
}

impl From<LogError> for Error {
    fn from(error: LogError) -> Self {
        Self::LogError(error)
    }
}

impl From<low::Error> for Error {
    fn from(error: low::Error) -> Self {
        Self::ParError(error.into())
    }
}

impl From<unicom::Error> for Error {
    fn from(error: unicom::Error) -> Self {
        Self::ParError(error.into())
    }
}

impl<'a> From<&'a str> for Error {
    fn from(error: &str) -> Self {
        Self::CmdError(error.into())
    }
}

impl From<String> for Error {
    fn from(error: String) -> Self {
        Self::CmdError(error)
    }
}
