use dirs::config_dir;
use serde::{de::DeserializeOwned, Serialize};
use serde_json::{from_reader, to_writer_pretty};
use std::fs::{create_dir_all, File};
use std::path::PathBuf;

pub struct Config {
    path: Option<PathBuf>,
}

impl Config {
    pub fn new<B: AsRef<str>, N: AsRef<str>>(base: B, name: N) -> Self {
        let path = config_dir().map(|dir| {
            dir.join(base.as_ref())
                .join(name.as_ref().replace(':', "-").replace('/', "-"))
                .with_extension(".json")
        });

        Self { path }
    }

    pub fn load<T: DeserializeOwned>(&self) -> Option<T> {
        if let Some(path) = &self.path {
            let f = File::open(path).ok()?;
            from_reader(f).ok()
        } else {
            None
        }
    }

    pub fn save<T: Serialize>(&self, data: &T) {
        if let Some(path) = &self.path {
            path.parent()
                .and_then(|dir| create_dir_all(dir).ok())
                .and_then(|_| {
                    let f = File::create(path).ok()?;
                    to_writer_pretty(f, data).ok()
                });
        }
    }
}
