# UParam terminal user interface

Basic terminal-based monitoring tool for uparam-aware devices.

## Key features

1. Displaying tree of device parameters
2. Selecting parameters for monitoring
3. Displaying values of selected parameters in realtime

## Usage

```shell
$ uparam-tui -d socket://my-device/

```

See the screencast here: [vimeo](https://vimeo.com/296073588).
