use crate::{
    options::Options,
    store::Store,
    types::{ParId, Val},
    Status,
};
use std::{
    collections::HashMap,
    sync::{
        atomic::{AtomicU32, Ordering},
        Mutex, MutexGuard, RwLock, RwLockReadGuard, RwLockWriteGuard,
    },
};

pub type Updates = HashMap<ParId, Val>;

pub(crate) struct State {
    /// Options
    options: Options,

    /// Params store
    params: RwLock<Store>,

    /// Current status
    status: AtomicU32,

    /// Values to set
    updates: Mutex<Updates>,
}

impl State {
    pub fn new(options: Options) -> Self {
        let params = RwLock::new(Store::default());
        let status = AtomicU32::new(Status::default() as _);
        let updates = Mutex::new(HashMap::default());

        Self {
            options,
            params,
            status,
            updates,
        }
    }

    pub fn options(&self) -> &Options {
        &self.options
    }

    pub fn params(&self) -> RwLockReadGuard<Store> {
        self.params.read().unwrap()
    }

    pub fn params_mut(&self) -> RwLockWriteGuard<Store> {
        self.params.write().unwrap()
    }

    pub fn status(&self) -> Status {
        unsafe { *((&self.status.load(Ordering::SeqCst)) as *const u32 as *const Status) }
    }

    pub fn set_status(&self, status: Status) {
        self.status.store(status as u32, Ordering::SeqCst);
    }

    pub fn updates_mut(&self) -> MutexGuard<Updates> {
        self.updates.lock().unwrap()
    }
}
