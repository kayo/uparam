use crate::{
    codec::{Codec, Message},
    connect::BoxedReconnectStrategyState,
    options::Options,
    result::{Error, Result},
    shim::{
        channel::{mpmc, mpsc},
        codec::Framed,
        time::{delay_for, timeout, Duration, Instant},
    },
    types::Par,
    Event, State, Status, Store,
};
use futures::{SinkExt, StreamExt};
use log::{debug, error, info, trace, warn};
use std::sync::{Arc, RwLockReadGuard, RwLockWriteGuard};
use unicom::BoxedConnection;

type Channel = Framed<BoxedConnection, Codec>;

pub struct Process {
    /// State
    state: Arc<State>,

    /// Reconnect strategy state
    reconnect: BoxedReconnectStrategyState,

    /// Commands stream to process
    commands: mpsc::Receiver<()>,

    /// Events sink to send to
    events: mpmc::Sender<Event>,
}

impl Drop for Process {
    fn drop(&mut self) {
        info!("Close client: {}", self.options().connector.url());
    }
}

impl Process {
    pub(crate) fn new(
        state: Arc<State>,
        commands: mpsc::Receiver<()>,
        events: mpmc::Sender<Event>,
    ) -> Self {
        info!("Open client for: {}", state.options().connector.url());

        let reconnect = state.options().reconnect.create_state();

        Self {
            state,
            reconnect,
            commands,
            events,
        }
    }
}

impl Process {
    fn options(&self) -> &Options {
        self.state.options()
    }

    fn params(&self) -> RwLockReadGuard<Store> {
        self.state.params()
    }

    fn params_mut(&self) -> RwLockWriteGuard<Store> {
        self.state.params_mut()
    }

    fn set_params(&self, params: Store) {
        *self.params_mut() = params;
    }

    fn set_status(&self, status: Status) {
        if self.state.status() != status {
            self.state.set_status(status);
            self.send_event(Event::Status(status));
        }
    }

    fn send_event(&self, event: Event) {
        mpmc::send(&self.events, event);
    }

    /// get "set" messages for updated parameters
    fn updates(&self) -> Vec<Message> {
        let params = self.params();
        self.state
            .updates_mut()
            .drain()
            .map(move |(id, val)| {
                params
                    .by_id(&id)
                    .map(|par| Message::Set(par.to_thin().with_val(val)))
            })
            .filter(Option::is_some)
            .map(Option::unwrap)
            .collect()
    }

    fn update_values(&self, msgs: Vec<Message>) {
        msgs.into_iter()
            .filter_map({
                let now = Instant::now();
                let mut params = self.params_mut();

                move |msg| {
                    let Par { id, val, .. } = msg.into_inner();
                    if let Some(val) = &val {
                        if let Some(ent) = params.by_id_mut(&id) {
                            if ent.update_val(val, now) {
                                let val = ent.read_val(val.clone());
                                return Some(Event::Update(id, val));
                            }
                        }
                    }
                    None
                }
            })
            .collect::<Vec<_>>()
            .into_iter()
            .for_each(|event| {
                self.send_event(event);
            });
    }
}

impl Process {
    /// Run client process
    pub async fn run(&mut self) -> Result<()> {
        loop {
            // Connect to device
            let mut chan = self.connect().await?;

            // Do introspection
            self.introspect(&mut chan).await?;

            // Process requests
            if let Err(error) = self.process(&mut chan).await {
                error!("Processing error: {}", error);

                let (delay, attempt) = {
                    // turn to offline
                    self.set_status(Status::Offline);
                    // get optional reconnection delay
                    if let Some(delay) = self.reconnect.disconnect() {
                        // add the maximum number of reconnection attempts
                        (delay, self.reconnect.attempts())
                    } else {
                        //break Err(Error::Disconnected);
                        break Ok(());
                    }
                };

                //let now = Instant::now();
                //let next = now + delay;

                if delay <= Duration::from_millis(1) {
                    warn!("Attempt reconnect #{}", attempt);
                } else {
                    warn!("Attempt reconnect #{} after {:?}", attempt, delay);
                    //delay_until(next.into()).await;
                    delay_for(delay).await;
                }
            } else {
                break Ok(());
            }
        }
    }

    async fn connect(&mut self) -> Result<Channel> {
        let (capacity, connect) = {
            let options = self.options();
            (options.request_limit, options.connector.connect())
        };

        let io = connect.await?;

        let channel: Channel = {
            self.set_status(Status::Online);
            self.reconnect.connected();

            Framed::new(io, Codec::with_request_limit(capacity))
        };

        Ok(channel)
    }

    async fn exchange(
        &self,
        chan: &mut Channel,
        msgs: Vec<Message>,
    ) -> Result<(Vec<Message>, Vec<Message>)> {
        let (mut attempts, request_timeout) = {
            let options = self.options();
            (options.request_attempts, options.request_timeout)
        };

        'outer: loop {
            chan.send(msgs.clone()).await?;

            let mut request_delay = request_timeout;

            'inner: loop {
                debug!("Request delay: {:?}", request_delay);
                match timeout(request_delay, chan.next()).await {
                    // success response received
                    Ok(Some(Ok(msgs))) => return Ok(msgs),
                    // invalid response received
                    Ok(Some(Err(error))) => {
                        warn!("Error when receiving response: {}", error);
                        break 'inner;
                    }
                    // channel closed (disconnected)
                    Ok(None) => {
                        warn!("Channel closed.");
                        break 'outer;
                    }
                    // timeout reached (no response received)
                    Err(_) => {
                        let recv_time = chan.codec().receive_time();
                        let recv_time_elapsed = recv_time.elapsed();
                        if recv_time_elapsed > request_timeout {
                            warn!("Response timeout: {:?}", recv_time_elapsed);
                            // retry
                            break 'inner;
                        } else {
                            request_delay = request_timeout - recv_time_elapsed;
                        }
                    }
                }
            }

            // retry request then it possible
            if attempts > 0 {
                warn!("Retry: {}", attempts);
                attempts -= 1;
            } else {
                break 'outer;
            }
        }

        Err(Error::Disconnected)
    }

    /// retrieve parameter definitions
    async fn introspect(&self, chan: &mut Channel) -> Result<()> {
        let mut params = Store::default();
        let mut queued = vec![Message::Def(Par::from_id(0))];

        self.set_status(Status::Active);
        self.send_event(Event::Retrieving);

        loop {
            if !queued.is_empty() {
                debug!(
                    "Retrieving parameters #{:?} definition",
                    queued.iter().map(|msg| msg.id).collect::<Vec<_>>()
                );

                let (complete, mut queued_tail) = self.exchange(chan, queued).await?;

                for msg in complete {
                    let par = msg.into_inner();
                    trace!("Parameter definition: {:?}", par);
                    if let Some((from, to)) = par.sub {
                        for id in from..=to {
                            queued_tail.push(Message::Def(Par::from_id(id)));
                        }
                    }
                    let id = par.id;
                    params.put(par);
                    self.send_event(Event::Retrieve(id));
                }
                queued = queued_tail;
            } else {
                info!("Successfully retrieved parameter definitions");
                self.set_params(params);
                self.send_event(Event::Retrieved);
                break;
            }
        }

        Ok(())
    }

    async fn process(&mut self, chan: &mut Channel) -> Result<()> {
        loop {
            self.set_status(Status::Online);
            let poll_delay = {
                let poll_interval = self.options().poll_interval();
                self.params()
                    .next_poll(poll_interval)
                    .map(|poll_time| {
                        let now = Instant::now();
                        if poll_time < now {
                            Duration::default()
                        } else {
                            poll_time - now
                        }
                    })
                    .unwrap_or(poll_interval.1)
            };
            match timeout(poll_delay, self.commands.next()).await {
                // poll timeout reached
                Err(_) => self.get_values(chan).await?,
                // notification received
                Ok(Some(_)) => self.set_values(chan).await?,
                // notification channel closed
                Ok(None) => break Ok(()),
            }
        }
    }

    async fn set_values(&self, chan: &mut Channel) -> Result<()> {
        let mut msgs = self.updates();

        if !msgs.is_empty() {
            debug!(
                "Write values: {:?}",
                msgs.iter()
                    .map(|par| (par.id, &par.val))
                    .collect::<Vec<_>>()
            );
        }

        loop {
            if msgs.is_empty() {
                self.set_status(Status::Online);
                break;
            } else {
                self.set_status(Status::Active);

                let (complete, unprocessed) = self.exchange(chan, msgs).await?;

                self.update_values(complete);

                msgs = unprocessed;
            }
        }

        Ok(())
    }

    async fn get_values(&self, chan: &mut Channel) -> Result<()> {
        let now = Instant::now();
        let mut msgs = self
            .params()
            .outdated(now, self.options().poll_interval())
            .into_iter()
            .map(Message::Get)
            .collect::<Vec<_>>();

        if msgs.is_empty() {
            debug!("No values to read");
        } else {
            debug!(
                "Read outdated values: {:?}",
                msgs.iter().map(|par| par.id).collect::<Vec<_>>()
            );
            self.send_event(Event::Updating);

            loop {
                self.set_status(Status::Active);

                let (complete, unprocessed) = self.exchange(chan, msgs).await?;

                self.update_values(complete);

                if unprocessed.is_empty() {
                    debug!("Done read values");
                    self.send_event(Event::Updated);
                    break;
                } else {
                    debug!("Continue read values: {:?}", unprocessed.len());
                    msgs = unprocessed;
                }
            }
        }

        Ok(())
    }
}
