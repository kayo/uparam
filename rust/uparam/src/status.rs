/// Connection status
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum Status {
    /// Client offline
    Offline,
    /// Client online and idle (inactive)
    Online,
    /// Client online and active
    Active,
}

impl Default for Status {
    fn default() -> Self {
        Self::Offline
    }
}
