use super::{Duration, ReconnectStrategy, ReconnectStrategyState};

/// Reconnect with delay
///
/// Optionally you can set the number of attempts
#[derive(Debug, Clone, Copy)]
pub struct DelayedReconnect {
    // param
    attempt_delay: Duration,
    attempts_limit: Option<usize>,
}

impl Default for DelayedReconnect {
    fn default() -> Self {
        Self {
            // param
            attempt_delay: Duration::from_secs(5),
            attempts_limit: None,
        }
    }
}

impl DelayedReconnect {
    pub fn unlimited() -> Self {
        Self::default()
    }
    pub fn limited(attempts_limit: usize) -> Self {
        Self::default().with_limit(attempts_limit)
    }
    pub fn with_limit(mut self, attempts_limit: usize) -> Self {
        self.attempts_limit = Some(attempts_limit);
        self
    }
    pub fn with_delay(mut self, attempt_delay: Duration) -> Self {
        self.attempt_delay = attempt_delay;
        self
    }
}

impl ReconnectStrategy for DelayedReconnect {
    fn create_state(&self) -> Box<dyn ReconnectStrategyState + Send + Sync> {
        Box::new(DelayedReconnectState {
            params: *self,
            attempts_left: 0,
        })
    }
}

#[derive(Debug, Clone, Copy)]
struct DelayedReconnectState {
    params: DelayedReconnect,
    attempts_left: usize,
}

impl ReconnectStrategyState for DelayedReconnectState {
    fn connected(&mut self) {
        // reset attempts counter
        self.attempts_left = 0;
    }

    fn disconnect(&mut self) -> Option<Duration> {
        if let Some(attempts_limit) = self.params.attempts_limit {
            if self.attempts_left >= attempts_limit {
                return None;
            }
        }
        // increment attempts counter
        self.attempts_left += 1;
        Some(self.params.attempt_delay)
    }

    fn attempts(&self) -> usize {
        self.attempts_left
    }
}

#[cfg(test)]
mod test {
    use super::{super::test, *};

    #[test]
    fn delayed_reconnect_params() {
        test(&DelayedReconnect::default());
        test(&DelayedReconnect::unlimited());
        test(&DelayedReconnect::limited(18));
        test(&DelayedReconnect::default().with_limit(11));
        test(&DelayedReconnect::default().with_delay(Duration::from_millis(500)));
        test(
            &DelayedReconnect::default()
                .with_limit(11)
                .with_delay(Duration::from_millis(500)),
        );
    }

    #[test]
    fn delayed_reconnect() {
        let mut r = DelayedReconnect::limited(3)
            .with_delay(Duration::from_millis(500))
            .create_state();
        assert_eq!(r.attempts(), 0);
        assert_eq!(r.disconnect(), Some(Duration::from_millis(500)));
        assert_eq!(r.attempts(), 1);
        assert_eq!(r.disconnect(), Some(Duration::from_millis(500)));
        assert_eq!(r.attempts(), 2);
        assert_eq!(r.disconnect(), Some(Duration::from_millis(500)));
        assert_eq!(r.attempts(), 3);
        assert_eq!(r.disconnect(), None);
        assert_eq!(r.attempts(), 3);
        assert_eq!(r.disconnect(), None);
        r.connected();
        assert_eq!(r.attempts(), 0);
        assert_eq!(r.disconnect(), Some(Duration::from_millis(500)));
        assert_eq!(r.attempts(), 1);
    }
}
