use super::{Duration, ReconnectStrategy, ReconnectStrategyState};

/// Reconnect immediately
///
/// Optionally you can set the number of attempts
#[derive(Debug, Clone, Copy, Default)]
pub struct ImmediateReconnect {
    attempts_limit: Option<usize>,
}

impl ImmediateReconnect {
    pub fn unlimited() -> Self {
        Self::default()
    }
    pub fn limited(attempts_limit: usize) -> Self {
        Self::default().with_limit(attempts_limit)
    }
    pub fn with_limit(mut self, attempts_limit: usize) -> Self {
        self.attempts_limit = Some(attempts_limit);
        self
    }
}

impl ReconnectStrategy for ImmediateReconnect {
    fn create_state(&self) -> Box<dyn ReconnectStrategyState + Send + Sync> {
        Box::new(ImmediateReconnectState {
            params: *self,
            attempts_left: 0,
        })
    }
}

#[derive(Debug, Clone, Copy, Default)]
struct ImmediateReconnectState {
    params: ImmediateReconnect,
    attempts_left: usize,
}

impl ReconnectStrategyState for ImmediateReconnectState {
    fn connected(&mut self) {
        // reset attempts counter
        self.attempts_left = 0;
    }

    fn disconnect(&mut self) -> Option<Duration> {
        if let Some(attempts_limit) = self.params.attempts_limit {
            if self.attempts_left >= attempts_limit {
                return None;
            }
        }
        // increment attempts counter
        self.attempts_left += 1;
        Some(Duration::from_secs(0))
    }

    fn attempts(&self) -> usize {
        self.attempts_left
    }
}

#[cfg(test)]
mod test {
    use super::{super::test, *};

    #[test]
    fn immediate_reconnect_params() {
        test(&ImmediateReconnect::default());
        test(&ImmediateReconnect::unlimited());
        test(&ImmediateReconnect::limited(18));
        test(&ImmediateReconnect::default().with_limit(11));
    }

    #[test]
    fn immediate_reconnect() {
        let mut r = ImmediateReconnect::limited(3).create_state();
        assert_eq!(r.attempts(), 0);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(0)));
        assert_eq!(r.attempts(), 1);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(0)));
        assert_eq!(r.attempts(), 2);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(0)));
        assert_eq!(r.attempts(), 3);
        assert_eq!(r.disconnect(), None);
        assert_eq!(r.attempts(), 3);
        assert_eq!(r.disconnect(), None);
        r.connected();
        assert_eq!(r.attempts(), 0);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(0)));
        assert_eq!(r.attempts(), 1);
    }
}
