use super::{Duration, ReconnectStrategy, ReconnectStrategyState};
use core::ops::{Bound, RangeBounds};

/// Reconnect with progressive incremented delay
///
/// Optionally you can set the number of attempts
#[derive(Debug, Clone, Copy)]
pub struct ProgressiveReconnect {
    /// minimum delay
    attempt_delay_min: Duration,
    /// maximum delay
    attempt_delay_max: Bound<Duration>,
    /// linear delay increment
    attempt_delay_lin: Duration,
    /// proportional delay increment in percentages
    attempt_delay_prop: u32,
    /// attempts limit
    attempts_limit: Option<usize>,
}

impl Default for ProgressiveReconnect {
    fn default() -> Self {
        Self {
            attempt_delay_min: Duration::from_secs(1),
            attempt_delay_max: Bound::Included(Duration::from_secs(30)),
            attempt_delay_lin: Duration::from_secs(0),
            attempt_delay_prop: 25,
            attempts_limit: None,
        }
    }
}

impl ProgressiveReconnect {
    pub fn unlimited() -> Self {
        Self::default()
    }
    pub fn limited(attempts_limit: usize) -> Self {
        Self::default().with_limit(attempts_limit)
    }
    pub fn with_limit(mut self, attempts_limit: usize) -> Self {
        self.attempts_limit = Some(attempts_limit);
        self
    }
    pub fn with_delay<R: RangeBounds<Duration>>(mut self, attempt_delay_range: R) -> Self {
        if let Bound::Included(bound) = attempt_delay_range.start_bound() {
            self.attempt_delay_min = *bound;
        }
        match attempt_delay_range.end_bound() {
            Bound::Included(bound) => self.attempt_delay_max = Bound::Included(*bound),
            Bound::Excluded(bound) => self.attempt_delay_max = Bound::Excluded(*bound),
            _ => (),
        }
        self
    }
    pub fn with_linear(mut self, linear_increment: Duration) -> Self {
        self.attempt_delay_lin = linear_increment;
        self
    }
    pub fn without_linear(mut self) -> Self {
        self.attempt_delay_lin = Duration::from_secs(0);
        self
    }
    pub fn with_proportional(mut self, proportional_increment: u32) -> Self {
        self.attempt_delay_prop = 100 + proportional_increment;
        self
    }
    pub fn without_proportional(mut self) -> Self {
        self.attempt_delay_prop = 100;
        self
    }
}

impl ReconnectStrategy for ProgressiveReconnect {
    fn create_state(&self) -> Box<dyn ReconnectStrategyState + Send + Sync> {
        Box::new(ProgressiveReconnectState {
            params: *self,
            attempt_delay: self.attempt_delay_min,
            attempts_left: 0,
        })
    }
}

#[derive(Debug, Clone, Copy)]
pub struct ProgressiveReconnectState {
    params: ProgressiveReconnect,
    attempt_delay: Duration,
    attempts_left: usize,
}

impl ReconnectStrategyState for ProgressiveReconnectState {
    fn connected(&mut self) {
        // reset attempts counter
        self.attempts_left = 0;
        // reset attempt delay to minimum
        self.attempt_delay = self.params.attempt_delay_min;
    }

    fn disconnect(&mut self) -> Option<Duration> {
        if let Some(attempts_limit) = self.params.attempts_limit {
            if self.attempts_left >= attempts_limit {
                return None;
            }
        }
        // increment attempts counter
        self.attempts_left += 1;
        let attempt_delay = self.attempt_delay;
        // increment attempt delay
        if match self.params.attempt_delay_max {
            Bound::Included(bound) => self.attempt_delay <= bound,
            Bound::Excluded(bound) => self.attempt_delay < bound,
            _ => false,
        } {
            self.attempt_delay = self.attempt_delay * self.params.attempt_delay_prop / 100
                + self.params.attempt_delay_lin;
            // clamp delay to max
            match self.params.attempt_delay_max {
                Bound::Included(bound) if self.attempt_delay > bound => self.attempt_delay = bound,
                Bound::Excluded(bound) if self.attempt_delay >= bound => {
                    self.attempt_delay = bound - Duration::from_millis(1);
                }
                _ => (),
            }
        }
        Some(attempt_delay)
    }

    fn attempts(&self) -> usize {
        self.attempts_left
    }
}

#[cfg(test)]
mod test {
    use super::{super::test, *};

    #[test]
    fn progressive_reconnect() {
        test(&ProgressiveReconnect::default());
        test(&ProgressiveReconnect::unlimited());
        test(&ProgressiveReconnect::limited(18));
        test(&ProgressiveReconnect::default().with_limit(11));
        test(
            &ProgressiveReconnect::default()
                .with_delay(Duration::from_millis(500)..Duration::from_secs(30)),
        );
        test(
            &ProgressiveReconnect::default()
                .with_limit(11)
                .with_delay(Duration::from_millis(500)..Duration::from_secs(30)),
        );
    }

    #[test]
    fn progressive_reconnect_linear() {
        let mut r = ProgressiveReconnect::limited(3)
            .with_delay(Duration::from_secs(1)..=Duration::from_secs(10))
            .without_proportional()
            .with_linear(Duration::from_secs(5))
            .create_state();
        assert_eq!(r.attempts(), 0);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(1)));
        assert_eq!(r.attempts(), 1);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(6)));
        assert_eq!(r.attempts(), 2);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(10)));
        assert_eq!(r.attempts(), 3);
        assert_eq!(r.disconnect(), None);
        assert_eq!(r.attempts(), 3);
        assert_eq!(r.disconnect(), None);
        r.connected();
        assert_eq!(r.attempts(), 0);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(1)));
        assert_eq!(r.attempts(), 1);
    }

    #[test]
    fn progressive_reconnect_proportional() {
        let mut r = ProgressiveReconnect::limited(4)
            .with_delay(Duration::from_secs(1)..=Duration::from_secs(10))
            .without_linear()
            .with_proportional(200)
            .create_state();
        assert_eq!(r.attempts(), 0);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(1)));
        assert_eq!(r.attempts(), 1);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(3)));
        assert_eq!(r.attempts(), 2);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(9)));
        assert_eq!(r.attempts(), 3);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(10)));
        assert_eq!(r.attempts(), 4);
        assert_eq!(r.disconnect(), None);
        assert_eq!(r.attempts(), 4);
        assert_eq!(r.disconnect(), None);
        r.connected();
        assert_eq!(r.attempts(), 0);
        assert_eq!(r.disconnect(), Some(Duration::from_secs(1)));
        assert_eq!(r.attempts(), 1);
    }
}
