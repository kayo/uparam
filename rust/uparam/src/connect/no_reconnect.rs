use super::{ReconnectStrategy, ReconnectStrategyState};

/// Do not attempt reconnect at all
#[derive(Debug, Clone, Copy, Default)]
pub struct NoReconnect;

impl ReconnectStrategy for NoReconnect {
    fn create_state(&self) -> Box<dyn ReconnectStrategyState + Send + Sync> {
        Box::new(NoReconnect)
    }
}

impl ReconnectStrategyState for NoReconnect {}

#[cfg(test)]
mod test {
    use super::{super::test, *};

    #[test]
    fn no_reconnect() {
        test(&NoReconnect);
        assert_eq!(NoReconnect.create_state().disconnect(), None);
    }
}
