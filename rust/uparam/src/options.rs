use crate::{reconnect, shim::time::Duration, BoxedReconnectStrategy, ReconnectStrategy};
use std::sync::Arc;
use unicom::BoxedConnector;

/// Client options
#[derive(Clone)]
pub struct Options {
    /// Connector
    pub(crate) connector: BoxedConnector,
    /// Reconnect strategy
    pub(crate) reconnect: BoxedReconnectStrategy,
    /// Minimum polling interval
    pub(crate) min_poll_interval: Duration,
    /// Maximum polling interval
    pub(crate) max_poll_interval: Duration,
    /// Default request buffer size
    pub(crate) request_limit: usize,
    /// Request timeout
    pub(crate) request_timeout: Duration,
    /// Request attempts
    pub(crate) request_attempts: usize,
}

impl From<BoxedConnector> for Options {
    fn from(connector: BoxedConnector) -> Self {
        Self {
            connector,
            reconnect: Arc::new(reconnect::No),
            min_poll_interval: Duration::from_secs(0),
            max_poll_interval: Duration::from_secs(3),
            request_limit: 1024,
            request_timeout: Duration::from_millis(500),
            request_attempts: 5,
        }
    }
}

impl Options {
    /// Set reconnect strategy
    pub fn set_reconnect_strategy(
        &mut self,
        reconnect_strategy: impl ReconnectStrategy + Send + Sync + 'static,
    ) {
        self.reconnect = Arc::new(reconnect_strategy);
    }

    /// Set reconnect strategy
    pub fn with_reconnect_strategy(
        mut self,
        reconnect_strategy: impl ReconnectStrategy + Send + Sync + 'static,
    ) -> Self {
        self.reconnect = Arc::new(reconnect_strategy);
        self
    }

    /// Set minimum polling interval
    pub fn set_min_poll_interval(&mut self, poll_interval: Duration) {
        self.min_poll_interval = poll_interval;
    }

    /// Set minimum polling interval
    pub fn with_min_poll_interval(mut self, poll_interval: Duration) -> Self {
        self.min_poll_interval = poll_interval;
        self
    }

    /// Set maximum polling interval
    pub fn set_max_poll_interval(&mut self, poll_interval: Duration) {
        self.max_poll_interval = poll_interval;
    }

    /// Set maximum polling interval
    pub fn with_max_poll_interval(mut self, poll_interval: Duration) -> Self {
        self.max_poll_interval = poll_interval;
        self
    }

    /// Set requests limit before reconnect
    pub fn set_request_limit(&mut self, request_limit: usize) {
        self.request_limit = request_limit;
    }

    /// Set requests limit before reconnect
    pub fn with_request_limit(mut self, request_limit: usize) -> Self {
        self.request_limit = request_limit;
        self
    }

    /// Set request timeout
    pub fn set_request_timeout(&mut self, request_timeout: Duration) {
        self.request_timeout = request_timeout;
    }

    /// Set request timeout
    pub fn with_request_timeout(mut self, request_timeout: Duration) -> Self {
        self.request_timeout = request_timeout;
        self
    }

    /// Set request attemps before reconnect
    pub fn set_request_attempts(&mut self, request_attempts: usize) {
        self.request_attempts = request_attempts;
    }

    /// Set request attemps before reconnect
    pub fn with_request_attempts(mut self, request_attempts: usize) -> Self {
        self.request_attempts = request_attempts;
        self
    }

    /// Get polling interval range
    pub fn poll_interval(&self) -> (Duration, Duration) {
        (self.min_poll_interval, self.max_poll_interval)
    }

    /// Gets response timeout
    pub fn response_timeout(&self) -> Duration {
        self.request_timeout * (self.request_attempts as u32 + 1)
    }
}
