use crate::{Options, Result};
use core::ops::Deref;
use unicom::{Manager as BaseManager, ToUrl};

/// Backends manager
#[derive(Default, Clone)]
#[repr(transparent)]
pub struct Manager(BaseManager);

impl Deref for Manager {
    type Target = BaseManager;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Manager {
    /// Create option using URL
    pub fn from_url(&self, device_url: impl ToUrl) -> Result<Options> {
        let connector = self.create(device_url)?;
        Ok(Options::from(connector))
    }
}
