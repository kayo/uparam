pub mod codec {
    #[cfg(feature = "tokio")]
    pub use tokio_util::codec::{Decoder, Encoder, Framed};

    #[cfg(feature = "async-std")]
    pub use futures_codec::{Decoder, Encoder, Framed};
}

pub mod time {
    #[cfg(feature = "tokio")]
    pub use tokio_rs::time::{delay_for, timeout, Duration, Elapsed as TimeoutError, Instant};

    #[cfg(feature = "async-std")]
    pub use {
        async_std_rs::{
            future::{timeout, TimeoutError},
            task::sleep as delay_for,
        },
        std::{
            future::Future,
            time::{Duration, Instant},
        },
    };
}

pub mod channel {
    #[cfg(feature = "tokio")]
    pub use tokio_rs::sync::mpsc;

    #[cfg(feature = "tokio")]
    pub mod mpmc {
        use futures::{future, stream, StreamExt};
        use tokio_rs::sync::broadcast;

        pub type Channel<T> = broadcast::Sender<T>;
        pub type Sender<T> = Channel<T>;
        pub type Receiver<T> = stream::FilterMap<
            broadcast::Receiver<T>,
            future::Ready<Option<T>>,
            fn(Result<T, broadcast::RecvError>) -> future::Ready<Option<T>>,
        >;

        pub fn channel<T>(capacity: usize) -> Channel<T> {
            broadcast::channel(capacity).0
        }

        pub fn subscribe<T: Clone>(channel: &Channel<T>) -> Receiver<T> {
            channel
                .subscribe()
                .filter_map(|item| future::ready(item.ok()))
        }

        pub fn send<T: Clone>(sender: &Sender<T>, value: T) {
            let _ = sender.send(value);
        }
    }

    #[cfg(feature = "async-std")]
    pub use futures::channel::mpsc;

    #[cfg(feature = "async-std")]
    pub mod mpmc {
        use broadcaster::BroadcastChannel;
        use futures::channel::mpsc;

        pub type Channel<T> = BroadcastChannel<T, mpsc::Sender<T>, mpsc::Receiver<T>>;
        pub type Sender<T> = Channel<T>;
        pub type Receiver<T> = Channel<T>;

        pub fn channel<T: Clone + Send>(capacity: usize) -> Channel<T> {
            BroadcastChannel::with_cap(capacity)
        }

        pub fn subscribe<T: Clone + Send>(channel: &Channel<T>) -> Receiver<T> {
            channel.clone()
        }

        pub fn send<T: Clone + Send>(sender: &Sender<T>, value: T) {
            let _ = sender.try_send(&value);
        }
    }
}

pub mod task {
    #[cfg(feature = "tokio")]
    pub use tokio_rs::task::spawn;

    #[cfg(feature = "async-std")]
    pub use async_std_rs::task::spawn;
}
