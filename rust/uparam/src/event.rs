use crate::{
    types::{ParId, Val},
    Status,
};

/// Parameters event
#[derive(Debug, Clone, PartialEq)]
pub enum Event {
    /// Changed status of connection
    Status(Status),

    /// Start updating parameters definitions
    Retrieving,
    /// Updated parameter definition
    Retrieve(ParId),
    /// End updating parameters definitions
    Retrieved,

    /// Start updating parameters values
    Updating,
    /// Updated parameter value
    Update(ParId, Val),
    /// End updating parameters values
    Updated,
}
