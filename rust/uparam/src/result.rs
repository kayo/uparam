use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    io::Error as IoError,
    result::Result as StdResult,
};
use unicom::Error as BackendError;

use crate::low;

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    FailedProto(low::Error),
    FailedBackend(BackendError),
    NotFound,
    CantGet,
    CantSet,
    FailedIo(IoError),
    FailedQueue,
    FailedResolve,
    Disconnected,
}

impl StdError for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::Error::*;
        match self {
            FailedProto(error) => write!(f, "Failed protocol: {}", error),
            FailedBackend(reason) => write!(f, "Failed backend: {}", reason),
            NotFound => f.write_str("Cannot find parameter"),
            CantGet => f.write_str("Cannot get parameter"),
            CantSet => f.write_str("Cannot set parameter"),
            FailedIo(error) => write!(f, "Failed IO: {}", error),
            FailedQueue => write!(f, "Failed queue"),
            FailedResolve => write!(f, "Failed resolving"),
            Disconnected => f.write_str("Disconnected"),
        }
    }
}

impl From<low::Error> for Error {
    fn from(e: low::Error) -> Self {
        Error::FailedProto(e)
    }
}

impl From<BackendError> for Error {
    fn from(e: BackendError) -> Self {
        Error::FailedBackend(e)
    }
}

impl From<IoError> for Error {
    fn from(e: IoError) -> Self {
        Error::FailedIo(e)
    }
}
