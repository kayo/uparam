use crate::{
    shim::time::Instant,
    types::{ParId, Val},
    Entry, Error, Handle, Ident, Result, Status,
};
use futures::Stream;
use std::{
    fmt::{Display, Error as FmtError, Formatter, Result as FmtResult},
    iter::FusedIterator,
    ops::{Bound, RangeBounds},
};

#[derive(Clone)]
pub struct Param {
    /// Parameter id
    id: ParId,

    /// Handle
    handle: Handle,
}

impl Param {
    /// Get parameter identifier
    pub fn id(&self) -> ParId {
        self.id
    }

    /// Get parent parameter identifier
    pub fn parent_id(&self) -> Result<Option<ParId>> {
        self.with_def(|par| {
            if par.parent_id < ParId::max_value() {
                Some(par.parent_id)
            } else {
                None
            }
        })
    }

    /// Get parameter definition
    pub fn with_def<R>(&self, fun: impl FnOnce(&Entry) -> R) -> Result<R> {
        self.handle.with_def(&self.id, fun)
    }

    /// Get parameter definition
    pub fn get_def(&self) -> Result<Entry> {
        self.handle.get_def(&self.id)
    }

    /// Get current parameter value
    pub fn get_val(&self) -> Result<Val> {
        self.handle.get_val(&self.id)
    }

    /// Get parameter polled time
    pub fn polled(&self) -> Result<Instant> {
        self.handle.with_def(&self.id, |par| par.poll_time.into())
    }

    /// Listen parameter value changes
    pub fn on_val(&self) -> Result<impl Stream<Item = Val> + Send + Sync + Unpin> {
        self.handle.on_val(&self.id)
    }

    /// Set new parameter value
    pub async fn set_val(&mut self, val: Val) -> Result<Val> {
        self.handle.set_val(&self.id, val).await
    }

    /// Get full parameter path
    pub fn path(&self) -> Result<Option<String>> {
        self.handle.with_def(&self.id, |par| par.path.clone())
    }

    /// Get parent parameter
    pub fn parent(&self) -> Result<Param> {
        self.with_def(|par| par.parent_id()).and_then(|id| {
            id.map(|id| Param {
                id,
                handle: self.handle.clone(),
            })
            .ok_or(Error::NotFound)
        })
    }

    /// Get child parameter by name
    pub fn child(&self, name: impl AsRef<str>) -> Result<Param> {
        self.handle.child(&self.id, name).map(|id| Param {
            id,
            handle: self.handle.clone(),
        })
    }

    pub fn has_children(&self) -> Result<bool> {
        self.with_def(|par| par.sub.is_some())
    }

    /// Get children parameters
    pub fn children(&self) -> Result<ParamsIter> {
        let range = if let Some((st, ed)) = self.with_def(|par| par.sub)? {
            st..=ed
        } else {
            1..=0
        };
        Ok(ParamsIter::new(self.handle.clone(), range))
    }

    /// Display parameter
    pub fn format(&self, f: &mut Formatter, level: usize) -> FmtResult {
        self.with_def(|ent| ent.format(f, level, &ent.val))
            .map_err(|_| FmtError::default())?
    }

    /// Display parameter with children
    pub fn format_recursive(&self, f: &mut Formatter, level: usize) -> FmtResult {
        self.format(f, level)?;
        let level = level + 1;
        for child in self.children().map_err(|_| FmtError::default())? {
            child.format_recursive(f, level)?;
        }
        Ok(())
    }
}

impl Display for Param {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        self.format_recursive(f, 0)
    }
}

impl Handle {
    /// Get root parameter or group
    pub fn root(&self) -> Result<Param> {
        if self.status() == Status::Offline {
            return Err(Error::NotFound);
        }

        Ok(Param {
            id: ParId::default(),
            handle: self.clone(),
        })
    }

    /// Get parameter by name
    pub fn param<'a>(&self, ident: impl Into<Ident<'a>>) -> Result<Param> {
        if self.status() == Status::Offline {
            return Err(Error::NotFound);
        }

        match ident.into() {
            Ident::Id(id) => {
                self.with_def(&id, |_| ())?;
                Ok(Param {
                    id,
                    handle: self.clone(),
                })
            }
            Ident::Path(name) => self.child(&ParId::max_value(), name).map(|id| Param {
                id,
                handle: self.clone(),
            }),
        }
    }
}

pub struct ParamsIter {
    cur_id: ParId,
    out_id: ParId,
    handle: Handle,
}

impl ParamsIter {
    pub(crate) fn new(handle: Handle, range: impl RangeBounds<ParId>) -> Self {
        let cur_id = match range.start_bound() {
            Bound::Included(id) => *id,
            Bound::Excluded(id) => *id + 1,
            Bound::Unbounded => 0,
        };
        let out_id = match range.end_bound() {
            Bound::Included(id) => *id + 1,
            Bound::Excluded(id) => *id,
            Bound::Unbounded => 0,
        };
        Self {
            cur_id,
            out_id,
            handle,
        }
    }
}

impl Iterator for ParamsIter {
    type Item = Param;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cur_id < self.out_id {
            let id = self.cur_id;
            self.cur_id += 1;
            return Some(Param {
                id,
                handle: self.handle.clone(),
            });
        }
        None
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let len = (self.out_id - self.cur_id) as usize;
        (len, Some(len))
    }
}

impl FusedIterator for ParamsIter {}
impl ExactSizeIterator for ParamsIter {}
