mod delayed_reconnect;
mod immediate_reconnect;
mod no_reconnect;
mod progressive_reconnect;

use std::{ops::Deref, sync::Arc};

pub(self) use crate::shim::time::Duration;

/// The built-in reconnect strategies
pub mod reconnect {
    pub use super::delayed_reconnect::DelayedReconnect as Delayed;
    pub use super::immediate_reconnect::ImmediateReconnect as Immediate;
    pub use super::no_reconnect::NoReconnect as No;
    pub use super::progressive_reconnect::ProgressiveReconnect as Progressive;
}

/// Boxed reconnect strategy state
pub type BoxedReconnectStrategy = Arc<dyn ReconnectStrategy + Send + Sync>;

/// Boxed reconnect strategy state
pub type BoxedReconnectStrategyState = Box<dyn ReconnectStrategyState + Send + Sync>;

/// Reconnect strategy
pub trait ReconnectStrategy {
    fn create_state(&self) -> BoxedReconnectStrategyState;
}

/// Reconnect strategy
pub trait ReconnectStrategyState {
    /// Hook which shall called then connection established
    fn connected(&mut self) {}

    /// Hook which shall called then connection distablished
    ///
    /// This hook may return time interval for reconnecting attempt.
    /// This hook also will be called when reconnecting attempt failed.
    fn disconnect(&mut self) -> Option<Duration> {
        None
    }

    /// Get the number of failed attempts
    fn attempts(&self) -> usize {
        0
    }
}

impl<T> ReconnectStrategy for T
where
    T: Deref<Target = dyn ReconnectStrategy + Send + Sync>,
{
    fn create_state(&self) -> BoxedReconnectStrategyState {
        self.deref().create_state()
    }
}

#[cfg(test)]
pub(crate) fn test<RS: ReconnectStrategy>(rs: &RS) {
    let _rss = rs.create_state();
}
