use bytes::{Buf, BufMut, BytesMut};
use crc::{
    crc32::{Digest, IEEE},
    Hasher32,
};
use log::trace;
use std::{
    cmp::Ordering,
    collections::VecDeque,
    hash::{Hash, Hasher},
    ops::{Deref, DerefMut},
};

use crate::{
    low::{get_res, put_def_req, put_get_req, put_set_req},
    result::Error,
    shim::{
        codec::{Decoder, Encoder},
        time::Instant,
    },
    types::Par,
};

pub struct Codec {
    /// Assumed request buffer size
    ///
    /// This dictates maximum request size which can fit in device buffer.
    request_limit: usize,

    /// Sent messages
    sent_msgs: VecDeque<Message>,

    /// Not sent messages
    not_sent_msgs: VecDeque<Message>,

    /// Last received time
    recv_time: Instant,

    /// Current received length
    recv_len: usize,

    /// Currect received digest
    recv_digest: Digest,
}

impl Codec {
    /// Create codec with request size limit
    pub fn with_request_limit(request_limit: usize) -> Self {
        Codec {
            request_limit,
            sent_msgs: VecDeque::new(),
            not_sent_msgs: VecDeque::new(),
            recv_time: Instant::now(),
            recv_len: 0,
            recv_digest: Digest::new(IEEE),
        }
    }

    fn start_receive(&mut self) {
        self.recv_time = Instant::now();
        self.recv_len = 0;
        self.recv_digest.reset();
    }

    fn update_receive(&mut self, buf: &mut BytesMut) -> bool {
        let mut tmp_buf = buf.clone();
        let new_len = buf.len() - 4;

        let mut sum_buf = tmp_buf.split_off(new_len);
        let sum = sum_buf.get_u32_le();

        (&mut self.recv_digest as &mut dyn Hasher32).write(&tmp_buf[self.recv_len..]);

        self.recv_time = Instant::now();
        self.recv_len = new_len;

        if sum != self.recv_digest.sum32() {
            // checksum mismatch
            // may be message incomplete
            // continue data reception
            false
        } else {
            buf.truncate(new_len);
            true
        }
    }

    pub fn receive_time(&self) -> Instant {
        self.recv_time
    }
}

impl Default for Codec {
    fn default() -> Self {
        Codec::with_request_limit(1024)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum MessageType {
    Def,
    Set,
    Get,
}

#[derive(Debug, Clone)]
pub enum Message {
    Def(Par),
    Set(Par),
    Get(Par),
}

impl Message {
    pub fn into_inner(self) -> Par {
        use self::Message::*;
        match self {
            Def(par) => par,
            Set(par) => par,
            Get(par) => par,
        }
    }

    pub fn get_type(&self) -> MessageType {
        use self::MessageType::*;
        match self {
            Message::Def(_) => Def,
            Message::Set(_) => Set,
            Message::Get(_) => Get,
        }
    }
}

impl Eq for Message {}

impl PartialEq for Message {
    fn eq(&self, other: &Self) -> bool {
        self.get_type() == other.get_type() && self.id == other.id
    }
}

impl Hash for Message {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.get_type().hash(state);
        self.id.hash(state);
    }
}

impl Ord for Message {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.get_type().cmp(&other.get_type()) {
            Ordering::Equal => self.id.cmp(&other.id),
            ordering => ordering,
        }
    }
}

impl PartialOrd for Message {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Deref for Message {
    type Target = Par;
    fn deref(&self) -> &Self::Target {
        use self::Message::*;
        match self {
            Def(par) => par,
            Get(par) => par,
            Set(par) => par,
        }
    }
}

impl DerefMut for Message {
    fn deref_mut(&mut self) -> &mut Self::Target {
        use self::Message::*;
        match self {
            Def(par) => par,
            Get(par) => par,
            Set(par) => par,
        }
    }
}

impl Encoder for Codec {
    type Item = Vec<Message>;
    type Error = Error;

    fn encode(&mut self, msgs: Vec<Message>, buf: &mut BytesMut) -> Result<(), Self::Error> {
        use self::Message::*;
        // request size limit must reserve four bytes for checksum
        let request_limit = self.request_limit - 4;
        buf.reserve(request_limit * 3 / 2);
        let mut limit_reached = false;
        // format messages to send
        for msg in msgs {
            if !limit_reached {
                // store previous valid length
                let last_valid_len = buf.len();
                // format message
                match &msg {
                    Def(par) => put_def_req(buf, &par),
                    Get(par) => put_get_req(buf, &par),
                    Set(par) => put_set_req(buf, &par)?,
                }
                // check request buffer limit
                if buf.len() > request_limit {
                    limit_reached = true;
                    buf.truncate(last_valid_len);
                    // queue message because we cannot send this
                    self.not_sent_msgs.push_back(msg);
                } else {
                    trace!("Outgoing message: {:?}", msg);
                    // add message to sended
                    self.sent_msgs.push_back(msg);
                }
            } else {
                // queue unsended messages
                self.not_sent_msgs.push_back(msg);
            }
        }
        // postprocess request
        let mut digest = Digest::new(IEEE);
        let digest: &mut dyn Hasher32 = &mut digest;

        digest.write(&buf);

        buf.put_u32_le(digest.sum32());
        //trace!("Outgoing data: {:?}", buf);

        // set last received time to handle timeouts correctly
        self.start_receive();

        Ok(())
    }
}

impl Decoder for Codec {
    /// Processed and unprocessed messages
    type Item = (Vec<Message>, Vec<Message>);
    type Error = Error;

    fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        //trace!("Incoming data: {:?} {}", buf, buf.len());

        // buffer too short
        if buf.len() < 5 {
            // may be message incomplete
            // continue data reception
            return Ok(None);
        }

        if !self.update_receive(buf) {
            //warn!("Checksum mismatched ({} bytes)", buf.len());
            // may be message incomplete
            // continue data reception
            return Ok(None);
        }

        let mut data_buf = buf.clone().freeze(); // FIXME:
        let mut processed_msgs = Vec::new();
        let mut unprocessed_msgs = Vec::new();

        // process received messages
        while let Some(mut msg) = self.sent_msgs.pop_front() {
            if data_buf.has_remaining() {
                get_res(&mut data_buf, &mut *msg)?;
                trace!("Incoming message: {:?}", msg);
                processed_msgs.push(msg);
            } else {
                unprocessed_msgs.push(msg);
            }
        }

        // add previously unsended messages to unprocessed
        while let Some(msg) = self.not_sent_msgs.pop_front() {
            unprocessed_msgs.push(msg);
        }

        if data_buf.has_remaining() {
            Err(Error::FailedQueue)
        } else {
            buf.clear();
            Ok(Some((processed_msgs, unprocessed_msgs)))
        }
    }
}
