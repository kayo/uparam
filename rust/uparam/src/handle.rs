use crate::{
    options::Options,
    result::{Error, Result},
    shim::{
        channel::{mpmc, mpsc},
        task::spawn,
        time::timeout,
    },
    types::{ParId, Val},
    Entry, Event, Process, State, Status, Store, Updates,
};
use futures::{future, future::Either, Stream, StreamExt};
use log::{error, info, warn};
use std::sync::{Arc, MutexGuard, RwLockReadGuard};
use unicom::Url;

#[derive(Clone)]
pub struct Handle {
    /// State
    state: Arc<State>,

    /// Commands sink to send to
    commands: mpsc::Sender<()>,

    /// Events to subscribe to
    events: mpmc::Sender<Event>,
}

impl Handle {
    fn options(&self) -> &Options {
        self.state.options()
    }

    fn params(&self) -> RwLockReadGuard<Store> {
        self.state.params()
    }

    fn updates_mut(&self) -> MutexGuard<Updates> {
        self.state.updates_mut()
    }
}

impl Handle {
    /// Get target device URL
    pub fn url(&self) -> &Url {
        self.state.options().connector.url()
    }

    /// Get current connection status
    pub fn status(&self) -> Status {
        self.state.status()
    }

    /// Listen client events
    pub fn on(&self) -> impl Stream<Item = Event> + Send + Sync + Unpin {
        mpmc::subscribe(&self.events)
    }

    /// Await online state
    pub async fn online(&self) -> Result<()> {
        if self.status() != Status::Offline {
            return Ok(());
        }

        match self
            .on()
            .filter(|event| future::ready(event == &Event::Retrieved))
            .next()
            .await
        {
            Some(_) => Ok(()),
            None => Err(Error::Disconnected),
        }
    }

    /// Find parameter by path
    pub fn find(&self, path: impl AsRef<str>) -> Result<ParId> {
        self.params().find_id(path.as_ref()).ok_or(Error::NotFound)
    }

    /// Get parameter definition
    pub fn with_def<R>(&self, id: &ParId, fun: impl FnOnce(&Entry) -> R) -> Result<R> {
        self.params().by_id(id).map(fun).ok_or(Error::NotFound)
    }

    /// Get parameter definition
    pub fn get_def(&self, id: &ParId) -> Result<Entry> {
        self.with_def(id, |par| par.clone())
    }

    /// Get parameter children
    pub fn child<'i>(&self, id: &ParId, name: impl AsRef<str>) -> Result<ParId> {
        if *id < ParId::max_value() {
            let path = self.with_def(id, |par| par.join_path(name))?;
            self.find(&path)
        } else {
            self.find(name)
        }
    }

    /// Get current parameter value
    pub fn get_val(&self, id: &ParId) -> Result<Val> {
        self.with_def(id, |par| {
            if par.getable {
                par.val
                    .as_ref()
                    .map(|val| par.read_val(val.clone()))
                    .ok_or(Error::CantGet)
            } else {
                Err(Error::CantGet)
            }
        })?
    }

    /// Listen parameter value changes
    pub fn on_val(&self, id: &ParId) -> Result<impl Stream<Item = Val> + Send + Sync + Unpin> {
        if self.with_def(id, |par| par.getable).unwrap_or(false) {
            Ok(self.on().filter_map({
                let id = *id;
                move |event| {
                    future::ready(match event {
                        Event::Update(pid, val) if id == pid => Some(val),
                        _ => None,
                    })
                }
            }))
        } else {
            Err(Error::CantGet)
        }
    }

    /// Set new parameter value
    pub async fn set_val(&mut self, id: &ParId, val: Val) -> Result<Val> {
        let val = match self.with_def(id, |par| {
            if par.setable {
                match par.cast_val(val) {
                    Err(error) => Err(error),
                    Ok(val) => match &par.val {
                        // value already same
                        Some(cval) if cval == &val => Ok(Either::Left(par.read_val(val))),
                        _ => Ok(Either::Right(val)),
                    },
                }
            } else {
                Err(Error::CantSet)
            }
        })? {
            Err(error) => return Err(error),
            Ok(Either::Left(val)) => return Ok(val),
            Ok(Either::Right(val)) => val,
        };

        self.updates_mut().insert(*id, val);
        let _ = self.commands.try_send(());

        match timeout(
            self.options().response_timeout(),
            self.on()
                .filter_map({
                    let id = *id;
                    move |event| {
                        future::ready(match event {
                            Event::Update(pid, val) if id == pid => Some(val),
                            _ => None,
                        })
                    }
                })
                .next(),
        )
        .await
        {
            Err(_) => {
                warn!("Timeout reached when setting value");
                Err(Error::Disconnected)
            }
            Ok(None) => {
                error!("Error when setting value");
                Err(Error::Disconnected)
            }
            Ok(Some(val)) => Ok(val),
        }
    }
}

impl Options {
    /// Open client with this options
    pub fn open_process(self) -> (Handle, Process) {
        let state = Arc::new(State::new(self));

        let (commands_sender, commands_receiver) = mpsc::channel(1);
        let events_channel = mpmc::channel(64);

        let handle = Handle {
            state: state.clone(),
            commands: commands_sender,
            events: events_channel.clone(),
        };

        let process = Process::new(state, commands_receiver, events_channel);

        (handle, process)
    }

    /// Open client with this options and run processing
    pub fn open(self) -> Handle {
        let (handle, mut process) = self.open_process();

        spawn(async move {
            if let Err(error) = process.run().await {
                error!("Processing ended with error: {}", error);
            } else {
                info!("Processing ended successfully");
            }
        });

        handle
    }
}
