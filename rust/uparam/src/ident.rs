use crate::types::ParId;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Ident<'i> {
    Id(ParId),
    Path(Cow<'i, str>),
}

impl<'i, 'j: 'i> From<&'j Ident<'j>> for Ident<'i> {
    fn from(ident: &'j Ident<'j>) -> Self {
        ident.clone()
    }
}

impl<'i, 'j> From<&'j ParId> for Ident<'i> {
    fn from(id: &'j ParId) -> Self {
        Ident::Id(*id)
    }
}

impl<'i> From<ParId> for Ident<'i> {
    fn from(id: ParId) -> Self {
        Ident::Id(id)
    }
}

impl<'i> From<&'i str> for Ident<'i> {
    fn from(path: &'i str) -> Self {
        Ident::Path(path.into())
    }
}

impl<'i> From<&'i String> for Ident<'i> {
    fn from(path: &'i String) -> Self {
        Ident::Path(path.into())
    }
}

impl<'i> From<String> for Ident<'i> {
    fn from(path: String) -> Self {
        Ident::Path(path.into())
    }
}
