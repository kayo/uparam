use crate::{
    shim::time::{Duration, Instant},
    types::{Par, ParId, Val},
    Result,
};
use std::{
    collections::HashMap,
    ops::{Deref, DerefMut},
};

#[derive(Debug, Default)]
pub(crate) struct Store {
    par_by_id: HashMap<ParId, Entry>,
    id_by_path: HashMap<String, ParId>,
}

impl Store {
    /// Put parameter definition
    pub fn put(&mut self, def: Par) {
        let id = def.id;
        let parent_id = self
            .par_by_id
            .iter()
            .find(|(_, ent)| {
                if let Some((from, to)) = ent.sub {
                    (from..=to).contains(&id)
                } else {
                    false
                }
            })
            .map(|(_, ent)| ent.id)
            .unwrap_or(ParId::max_value());

        self.par_by_id.insert(
            id,
            Entry {
                def,
                path: None,
                parent_id,
                poll_time: Instant::now(),
            },
        );

        if let Some(path) = self.make_path(id) {
            //debug!("Add path: #{} {}", id, path);
            self.id_by_path.insert(path.clone(), id);
            self.par_by_id.get_mut(&id).unwrap().path = Some(path);
        }
    }

    /// Get parameter definition by id
    pub fn by_id(&self, id: &ParId) -> Option<&Entry> {
        self.par_by_id.get(id)
    }

    /// Get parameter definition by id
    pub fn by_id_mut(&mut self, id: &ParId) -> Option<&mut Entry> {
        self.par_by_id.get_mut(id)
    }

    /// Get parameter id by path
    pub fn find_id(&self, path: &str) -> Option<ParId> {
        if path == "" {
            Some(0)
        } else {
            self.id_by_path.get(path).map(|id| *id)
        }
    }

    fn make_path(&self, id: ParId) -> Option<String> {
        if let Some(par) = self.par_by_id.get(&id) {
            if let Some(name) = &par.def.name {
                Some(
                    if let Some(parent_id) = self.par_by_id.get(&id).and_then(|ent| ent.parent_id())
                    {
                        if let Some(parent_path) = self.make_path(parent_id) {
                            parent_path + "." + &name
                        } else {
                            name.clone()
                        }
                    } else {
                        name.clone()
                    },
                )
            } else {
                None
            }
        } else {
            None
        }
    }

    /// Get outdated parameters
    pub fn outdated(&self, cur_time: Instant, def_poll: (Duration, Duration)) -> Vec<Par> {
        self.par_by_id
            .iter()
            .filter(|(_, ent)| {
                ent.next_poll(def_poll)
                    .map(|next_time| next_time < cur_time)
                    .unwrap_or(false)
            })
            .map(|(_, ent)| ent.to_thin())
            .collect()
    }

    /// Get duration for next update
    pub fn next_poll(&self, def_poll: (Duration, Duration)) -> Option<Instant> {
        self.par_by_id
            .iter()
            .map(|(_, ent)| ent.next_poll(def_poll))
            .filter(Option::is_some)
            .map(Option::unwrap)
            .min()
    }
}

#[derive(Debug, Clone)]
pub struct Entry {
    /// Definition
    def: Par,
    /// Full path
    pub path: Option<String>,
    /// Parent id
    pub parent_id: ParId,
    /// Last polled time
    pub poll_time: Instant,
}

impl Deref for Entry {
    type Target = Par;

    fn deref(&self) -> &Self::Target {
        &self.def
    }
}

impl DerefMut for Entry {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.def
    }
}

impl Entry {
    pub fn parent_id(&self) -> Option<ParId> {
        if self.parent_id < ParId::max_value() {
            Some(self.parent_id)
        } else {
            None
        }
    }

    #[inline]
    fn get_poll(&self) -> Option<Duration> {
        self.poll.map(u64::from).map(Duration::from_millis)
    }

    #[inline]
    fn next_poll(&self, (min_poll, max_poll): (Duration, Duration)) -> Option<Instant> {
        if self.getable {
            Some(self.poll_time + self.get_poll().unwrap_or(max_poll).max(min_poll))
        } else {
            None
        }
    }

    /// Join path with specified sub path
    pub fn join_path(&self, name: impl AsRef<str>) -> String {
        let name = name.as_ref();
        self.path
            .as_ref()
            .map(|path| {
                if path.len() > 0 && name.len() > 0 {
                    path.clone() + "." + name
                } else if name.len() > 0 {
                    name.into()
                } else {
                    path.clone()
                }
            })
            .unwrap_or_else(|| name.into())
    }

    /// Update value
    ///
    /// Return true if the value actually updated
    pub fn update_val(&mut self, val: &Val, time: Instant) -> bool {
        //if self.setable {
        self.poll_time = time;
        if self.val.as_ref().map(|pval| pval != val).unwrap_or(false) {
            self.val = Some(val.clone());
            return true;
        }
        //}
        false
    }

    /// convert value for reading
    pub fn read_val(&self, val: Val) -> Val {
        self.opt_val_to_name(val.clone()).unwrap_or(val)
    }

    /// convert value for writing
    pub fn cast_val(&self, val: Val) -> Result<Val> {
        let val = self.opt_name_to_val(val)?;
        Ok(self.def.cast_val(val)?)
    }
}
