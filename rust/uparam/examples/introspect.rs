use std::{env::args, process::exit};

#[cfg(feature = "async-std")]
use async_std_rs as async_std;
use futures::StreamExt;
use pretty_env_logger as logger;
#[cfg(feature = "tokio")]
use tokio_rs as tokio;
use uparam::{reconnect, Event, Manager};

#[cfg_attr(feature = "tokio", tokio::main)]
#[cfg_attr(feature = "async-std", async_std::main)]
async fn main() {
    logger::init();

    let backends = Manager::default();

    let nres = unicom_nres::DefaultResolver::default();
    backends.register(unicom_tcp::TcpSocket::new(nres)).unwrap();
    #[cfg(unix)]
    backends
        .register(unicom_unix::UnixSocket::default())
        .unwrap();
    //backends.register(unicom_serial::SerialPort::default()).unwrap();

    let url = if let Some(url) = args().skip(1).next() {
        url
    } else {
        println!("Missing device url");
        exit(-1);
    };

    let options = backends
        .from_url(&url)
        .unwrap()
        .with_reconnect_strategy(reconnect::Progressive::unlimited());

    let handle = options.open();

    let mut events = handle.on();

    while let Some(event) = events.next().await {
        match event {
            Event::Status(_status) => {
                //println!("Client status: {:?}", _status);
            }
            Event::Retrieving => {
                println!("Retrieving parameters definitions");
            }
            Event::Retrieved => {
                println!("All parameters definitions retrieved");
                println!("{}", handle.root().unwrap());
                break;
            }
            Event::Retrieve(_id) => {
                //println!("Parameter #{} definition retrieved", _id);
            }
            Event::Update(id, val) => {
                println!("Updated parameter #{} with value {:?}", id, val);
            }
            _ => (),
        }
    }
}
