# UParam command-line client

Full-featured command-line tool for interfacing uparam-aware devices.

## Key features

1. Displaying info of parameters
2. Getting values of parameters
3. Setting values of parameters

## Modes of operation

1. Batch mode
2. Shell mode

## Batch mode

With any commands in command-line the batch mode will be activated.

```shell
$ uparam-cli -d socket://my-device/ control? sensor.Tenv control.mode=auto
Id: 3
Name: control
Info: Main controls
Children:
  - state
  - mode
  - pump
  - heater
21.396452190101
auto
```

The example above:

1. Connects to "my-device" using TCP socket
2. Displays info about parameter "control"
3. Gets current value of parameter "sensor.Tenv"
4. Sets value "auto" to parameter "control.mode"

## Shell mode

Without any commands in command-line the shell mode will be activated.

```shell
$ uparam-cli -d socket://my-device/
uparam-cli v0.1.0
Command-line interface and interactive shell for controlling uparam-aware devices.

Enter commands:
    <param>?          Describe parameter
    <param>           Get current parameter value
    <param>=<value>   Set new parameter value
    <param>!          Reset parameter to default
Press Ctrl-D to terminate shell or Ctrl-C to interrupt immediatelly.

[socket://my-device/] enter parameter name
```

You can enter commands and get results:

```shell
[socket://my-device/] control?
Id: 3
Name: control
Info: Main controls
Children:
  - state
  - mode
  - pump
  - heater

[socket://my-device/] sensor.Tenv
21.396452190101

[socket://my-device/] control.mode=auto
auto
```

Of course autocomplete of parameter names and value also supported:

```shell
[socket://my-device/] c[TAB]
[socket://my-device/] control[TAB]
.state        .mode         .pump         .heater         ? (describe)
[socket://my-device/] control.m[TAB]
[socket://my-device/] control.mode[TAB]
? (describe)          (get value)           =<value> (set value)  ! (reset to default)
[socket://my-device/] control.mode=[TAB]
auto         manual
[socket://my-device/] control.mode=m[TAB]
[socket://my-device/] control.mode=manual
```

See the screencast here: [vimeo](https://vimeo.com/295980395).
