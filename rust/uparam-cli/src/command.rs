use crate::{
    lexer::{unescape, Lexeme, LexemeKind, Lexer},
    result::{Error, Result},
};
use ansi_term::{Color, Style};
use std::str::FromStr;
use uparam::{types::Val, Handle, Param};

/// Command-line interface command
#[derive(Debug, Clone, PartialEq)]
pub enum Command {
    /// Get parameter definition
    Def(String),
    /// Get parameter value
    Get(String),
    /// Set parameter value
    Set(String, Val),
    /// Reset parameter value
    Res(String),
}

impl FromStr for Command {
    type Err = Error;

    fn from_str(line: &str) -> Result<Self> {
        use self::LexemeKind::*;
        let mut lexer = Lexer::new(line).filter(|Lexeme(kind, _)| *kind != Spc);

        Ok(match (lexer.next(), lexer.next(), lexer.next()) {
            (Some(Lexeme(Par, path)), Some(Lexeme(Cmd, "?")), None) => Command::Def(path.into()),
            (Some(Lexeme(Par, path)), None, None) => Command::Get(path.into()),
            (Some(Lexeme(Par, path)), Some(Lexeme(Cmd, "=")), Some(Lexeme(Val, val))) => {
                let val = if val.starts_with('"') && val.ends_with('"')
                    || val.starts_with('\'') && val.ends_with('\'')
                {
                    unescape(&val[1..val.len() - 1]).parse()
                } else {
                    val.parse()
                }?;
                Command::Set(path.into(), val)
            }
            (Some(Lexeme(Par, path)), Some(Lexeme(Cmd, "!")), None) => Command::Res(path.into()),
            _ => return Err(format!("Invalid command: {}", line).into()),
        })
    }
}

impl Command {
    pub fn path(&self) -> &String {
        use self::Command::*;
        match self {
            Def(path) => path,
            Get(path) => path,
            Set(path, _val) => path,
            Res(path) => path,
        }
    }

    pub async fn run(&self, handle: &Handle) {
        use self::Command::*;

        let style = Style::new();
        let val_style = style.italic().fg(Color::Yellow);
        let bad_style = style.fg(Color::Red);

        let mut param = match handle.param(self.path()) {
            Ok(param) => param,
            Err(error) => {
                eprintln!("Error: {}", error);
                return;
            }
        };
        match self {
            Def(_path) => show_info(&param),
            Get(_path) => {
                if let Ok(val) = param.get_val() {
                    println!("{}", val_style.paint(val.to_string()));
                }
            }
            Set(_path, val) => match param.set_val(val.clone()).await {
                Ok(val) => println!("{}", val_style.paint(val.to_string())),
                Err(error) => eprintln!("{}", bad_style.paint(error.to_string())),
            },
            Res(path) => match param.with_def(|par| par.def.clone()) {
                Ok(Some(def_val)) => match param.set_val(def_val.clone()).await {
                    Ok(val) => println!("{}", val_style.paint(val.to_string())),
                    Err(error) => eprintln!("{}", bad_style.paint(error.to_string())),
                },
                Ok(None) => eprintln!(
                    "{}",
                    bad_style.paint(format!("Missing default value for parameter {}", path))
                ),
                Err(error) => eprintln!("{}", bad_style.paint(error.to_string())),
            },
        }
    }
}

/// Command-line interface commands sequence
#[derive(Debug, Clone, PartialEq)]
pub struct Commands(pub Vec<Command>);

impl FromStr for Commands {
    type Err = Error;

    fn from_str(line: &str) -> Result<Self> {
        use self::LexemeKind::*;
        let mut commands = Vec::new();
        let mut lexer = Lexer::new(line).filter(|Lexeme(kind, _)| *kind != Spc);
        let mut prev = None;
        loop {
            let path = match if prev.is_some() {
                prev.take()
            } else {
                lexer.next()
            } {
                Some(Lexeme(Par, path)) => path,
                Some(Lexeme(Cmd, "?")) => {
                    // get root definition
                    commands.push(Command::Def("".into()));
                    continue;
                }
                Some(Lexeme(kind, text)) => {
                    return Err(format!("Unexpected token: {} ({:?})", text, kind).into());
                }
                None => break,
            };
            match lexer.next() {
                Some(Lexeme(Cmd, "?")) => commands.push(Command::Def(path.into())),
                Some(Lexeme(Cmd, "=")) => match lexer.next() {
                    Some(Lexeme(Val, val)) => {
                        let val = if val.starts_with('"') && val.ends_with('"')
                            || val.starts_with('\'') && val.ends_with('\'')
                        {
                            unescape(&val[1..val.len() - 1]).parse()
                        } else {
                            val.parse()
                        }?;
                        commands.push(Command::Set(path.into(), val))
                    }
                    Some(Lexeme(_, text)) => {
                        return Err(format!("Unexpected token: {} (Value expecting)", text).into())
                    }
                    None => commands.push(Command::Set(path.into(), "".parse()?)),
                },
                Some(Lexeme(Cmd, "!")) => commands.push(Command::Res(path.into())),
                Some(lexeme) => {
                    commands.push(Command::Get(path.into()));
                    prev = Some(lexeme);
                }
                None => commands.push(Command::Get(path.into())),
            }
        }
        Ok(Commands(commands))
    }
}

impl Commands {
    pub fn from_args<A: AsRef<[S]>, S: AsRef<str>>(args: A) -> Result<Self> {
        args.as_ref()
            .iter()
            .map(|arg| arg.as_ref().parse())
            .collect::<Result<Vec<_>>>()
            .map(Commands)
    }

    pub async fn run(&self, handle: &Handle) {
        for command in &self.0 {
            command.run(handle).await;
        }
    }
}

fn show_info(param: &Param) {
    param
        .with_def(|def| {
            let style = Style::default();
            let cons_field_style = style.fg(Color::Green);
            let text_field_style = style.fg(Color::Purple);
            let cons_value_style = style.fg(Color::Cyan);
            let text_value_style = style.fg(Color::Yellow);

            // id
            println!(
                "{}: {}",
                cons_field_style.paint("Id"),
                cons_value_style.paint(def.id.to_string())
            );

            // type
            if let Some(type_) = def.type_ {
                println!(
                    "{}: {}",
                    cons_field_style.paint("Type"),
                    cons_value_style.paint(type_.to_string())
                );

                // size
                if let Some(size) = def.size {
                    println!(
                        "{}: {}",
                        cons_field_style.paint("Size"),
                        cons_value_style.paint(size.to_string()),
                    );
                }

                // frac
                if let Some(frac) = def.frac {
                    println!(
                        "{}: {}",
                        cons_field_style.paint("Fraction"),
                        cons_value_style.paint(frac.to_string())
                    );
                }
            }

            // text fields
            for (field, value) in &[
                ("Name", &def.name),
                ("Info", &def.info),
                ("Item", &def.item),
                ("Hint", &def.hint),
                ("Unit", &def.unit),
            ] {
                if let Some(value) = value {
                    println!(
                        "{}: {}",
                        text_field_style.paint(*field),
                        text_value_style.paint(value)
                    );
                }
            }

            // constraint fields
            for (field, value) in &[
                ("Default", &def.def),
                ("Min", &def.min),
                ("Max", &def.max),
                ("Step", &def.stp),
            ] {
                if let Some(value) = value {
                    println!(
                        "{}: {}",
                        cons_field_style.paint(*field),
                        cons_value_style.paint(value.to_string())
                    );
                }
            }

            // option fields
            if !def.opt.is_empty() {
                println!("{}:", cons_field_style.paint("Options"));

                for opt in &def.opt {
                    println!(
                        "  - {}: {}",
                        cons_field_style.paint("Value"),
                        cons_value_style.paint(opt.val.to_string())
                    );
                    // text fields
                    for (field, value) in &[
                        ("Name", &opt.name),
                        ("Info", &opt.info),
                        ("Item", &opt.item),
                        ("Hint", &opt.hint),
                        ("Unit", &opt.unit),
                    ] {
                        if let Some(value) = value {
                            println!(
                                "    {}: {}",
                                text_field_style.paint(*field),
                                text_value_style.paint(value)
                            );
                        }
                    }
                }
            }

            if def.sub.is_some() {
                println!("{}:", text_field_style.paint("Children"));

                for param in param.children().unwrap() {
                    param
                        .with_def(|def| {
                            if let Some(name) = &def.name {
                                println!("  - {}", text_value_style.paint(name));
                            } else {
                                println!("  - #{}", text_value_style.paint(def.id.to_string()));
                            }
                        })
                        .unwrap();
                }
            }
        })
        .unwrap();
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn commands_parse() {
        assert_eq!(
            Commands::from_str("some.param? param.subparam param.abc.def=\"abc def\"").unwrap(),
            Commands(vec![
                Command::Def("some.param".into()),
                Command::Get("param.subparam".into()),
                Command::Set("param.abc.def".into(), "abc def".parse().unwrap())
            ])
        );
        assert_eq!(
            Commands::from_str(" param = 123  param.subparam=\"ab c\\\" def \"").unwrap(),
            Commands(vec![
                Command::Set("param".into(), "123".parse().unwrap()),
                Command::Set("param.subparam".into(), "ab c\" def ".parse().unwrap())
            ])
        );
    }
}
