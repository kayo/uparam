use futures::StreamExt;
use log::info;

use uparam::{Event, Handle};

use crate::command::Commands;

pub struct Batch {
    handle: Handle,
    commands: Commands,
}

impl Batch {
    pub fn new(handle: Handle, commands: Commands) -> Self {
        Self { handle, commands }
    }

    pub async fn run(self) {
        let mut events = self.handle.on();

        loop {
            if let Some(event) = events.next().await {
                match event {
                    Event::Retrieving => {
                        info!("Connecting... ");
                    }
                    Event::Retrieved => {
                        info!("Connected");
                        break;
                    }
                    _ => {}
                }
            } else {
                eprintln!("Disconnected.");
            }
        }

        info!("Run commands...");
        self.commands.run(&self.handle).await;
        info!("Done");
    }
}
