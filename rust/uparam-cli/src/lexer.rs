use lazy_static::lazy_static;
use regex::Regex;
use std::{borrow::Cow, iter::FusedIterator};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum LexemeKind {
    Par,
    Cmd,
    Val,
    Spc,
    End,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Lexeme<'s>(pub LexemeKind, pub &'s str);

/// lexer context
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Ctx {
    Par,
    Cmd,
    Val,
}

pub struct Lexer<'s> {
    src: &'s str,
    ctx: Ctx,
}

impl<'s> Lexer<'s> {
    pub fn new(src: &'s str) -> Self {
        Self { src, ctx: Ctx::Par }
    }
}

impl<'s> Iterator for Lexer<'s> {
    type Item = Lexeme<'s>;

    fn next(&mut self) -> Option<Self::Item> {
        lazy_static! {
            // quoted string literals
            static ref STR: Regex = Regex::new(
                concat!("^(:?",
                        // double-quoted string literal
                        r#""(?:\\\\|\\"|[^"])*""#,
                        "|",
                        // single-quoted string literal
                        r#"'(?:\\\\|\\'|[^'])*'"#,
                        ")")
            ).unwrap();

            // parameter id/path
            static ref PAR: Regex = Regex::new(
                // parameter identifier path
                r#"^[^\s=?!]+"#,
            ).unwrap();

            // command-tag token
            static ref DEF: Regex = Regex::new(
                // def parameter command
                r#"^[?]"#,
            ).unwrap();

            static ref SET: Regex = Regex::new(
                // set parameter command
                r#"^[=]"#,
            ).unwrap();

            static ref RES: Regex = Regex::new(
                // set parameter command
                r#"^[!]"#,
            ).unwrap();

            // parameter value
            static ref VAL: Regex = Regex::new(
                r#"^[^\s]+"#,
            ).unwrap();

            // spaces between commands
            static ref SPC: Regex = Regex::new(
                r#"^\s+"#,
            ).unwrap();
        }

        if self.src.is_empty() {
            None
        } else {
            use self::LexemeKind::*;

            let res = if let Some(m) = SPC.find(self.src) {
                // process spaces in any context
                Some((Spc, m, self.ctx))
            } else {
                match self.ctx {
                    Ctx::Par => {
                        if let Some(m) = PAR.find(self.src) {
                            Some((Par, m, Ctx::Cmd))
                        } else if let Some(m) = DEF.find(self.src) {
                            Some((Cmd, m, Ctx::Par))
                        } else if let Some(m) = RES.find(self.src) {
                            Some((Cmd, m, Ctx::Par))
                        } else {
                            None
                        }
                    }
                    Ctx::Cmd => {
                        if let Some(m) = DEF.find(self.src) {
                            Some((Cmd, m, Ctx::Par))
                        } else if let Some(m) = SET.find(self.src) {
                            Some((Cmd, m, Ctx::Val))
                        } else if let Some(m) = RES.find(self.src) {
                            Some((Cmd, m, Ctx::Par))
                        } else if let Some(m) = PAR.find(self.src) {
                            Some((Par, m, Ctx::Cmd))
                        } else {
                            None
                        }
                    }
                    Ctx::Val => {
                        if let Some(m) = STR.find(self.src) {
                            Some((Val, m, Ctx::Par))
                        } else if let Some(m) = VAL.find(self.src) {
                            Some((Val, m, Ctx::Par))
                        } else {
                            None
                        }
                    }
                }
            };

            let (kind, text, src, ctx) = if let Some((kind, m, ctx)) = res {
                let text = &self.src[..m.end()];
                (kind, text, &self.src[m.end()..], ctx)
            } else {
                let kind = LexemeKind::End;
                let text = &self.src[..];
                (kind, text, &text[text.len()..], self.ctx)
            };

            self.ctx = ctx;
            self.src = src;

            Some(Lexeme(kind, text))
        }
    }
}

impl<'s> FusedIterator for Lexer<'s> {}

pub fn unescape(src: &str) -> Cow<str> {
    lazy_static! {
        static ref ESCAPE_RE: Regex = Regex::new(r#"\\([\\"'])"#).unwrap();
    }
    let dst = ESCAPE_RE.replace_all(src, "$1");
    if src == dst {
        src.into()
    } else {
        dst
    }
}

#[cfg(test)]
mod test {
    use super::LexemeKind::*;
    use super::*;

    #[test]
    fn single_def() {
        assert_eq!(
            Lexer::new("param?").collect::<Vec<_>>(),
            vec![Lexeme(Par, "param"), Lexeme(Cmd, "?")]
        );
    }

    #[test]
    fn single_get() {
        assert_eq!(
            Lexer::new("param").collect::<Vec<_>>(),
            vec![Lexeme(Par, "param")]
        );
    }

    #[test]
    fn single_set() {
        assert_eq!(
            Lexer::new("param=value").collect::<Vec<_>>(),
            vec![Lexeme(Par, "param"), Lexeme(Cmd, "="), Lexeme(Val, "value")]
        );
    }

    #[test]
    fn spaced_def() {
        assert_eq!(
            Lexer::new(" param  ?   ").collect::<Vec<_>>(),
            vec![
                Lexeme(Spc, " "),
                Lexeme(Par, "param"),
                Lexeme(Spc, "  "),
                Lexeme(Cmd, "?"),
                Lexeme(Spc, "   ")
            ]
        );
    }

    #[test]
    fn spaced_get() {
        assert_eq!(
            Lexer::new(" param   ").collect::<Vec<_>>(),
            vec![Lexeme(Spc, " "), Lexeme(Par, "param"), Lexeme(Spc, "   ")]
        );
    }

    #[test]
    fn spaced_set() {
        assert_eq!(
            Lexer::new("  param  =  value ").collect::<Vec<_>>(),
            vec![
                Lexeme(Spc, "  "),
                Lexeme(Par, "param"),
                Lexeme(Spc, "  "),
                Lexeme(Cmd, "="),
                Lexeme(Spc, "  "),
                Lexeme(Val, "value"),
                Lexeme(Spc, " "),
            ]
        );
    }

    #[test]
    fn multiple_cmds() {
        assert_eq!(
            Lexer::new("param?  param.subparam= va_lue ab.cde.f").collect::<Vec<_>>(),
            vec![
                Lexeme(Par, "param"),
                Lexeme(Cmd, "?"),
                Lexeme(Spc, "  "),
                Lexeme(Par, "param.subparam"),
                Lexeme(Cmd, "="),
                Lexeme(Spc, " "),
                Lexeme(Val, "va_lue"),
                Lexeme(Spc, " "),
                Lexeme(Par, "ab.cde.f"),
            ]
        );

        assert_eq!(
            Lexer::new("some.param? param.subparam abc.def=\"abc def\"").collect::<Vec<_>>(),
            vec![
                Lexeme(Par, "some.param"),
                Lexeme(Cmd, "?"),
                Lexeme(Spc, " "),
                Lexeme(Par, "param.subparam"),
                Lexeme(Spc, " "),
                Lexeme(Par, "abc.def"),
                Lexeme(Cmd, "="),
                Lexeme(Val, "\"abc def\""),
            ]
        );
    }

    #[test]
    fn single_quoted_set() {
        assert_eq!(
            Lexer::new("param = 'quoted \\\'string \" value '").collect::<Vec<_>>(),
            vec![
                Lexeme(Par, "param"),
                Lexeme(Spc, " "),
                Lexeme(Cmd, "="),
                Lexeme(Spc, " "),
                Lexeme(Val, "'quoted \\\'string \" value '"),
            ]
        );
    }

    #[test]
    fn double_quoted_set() {
        assert_eq!(
            Lexer::new("param=\" quoted\\\\\\\" string value\"").collect::<Vec<_>>(),
            vec![
                Lexeme(Par, "param"),
                Lexeme(Cmd, "="),
                Lexeme(Val, "\" quoted\\\\\\\" string value\""),
            ]
        );
    }

    #[test]
    fn partial_par_get() {
        assert_eq!(
            Lexer::new("par").collect::<Vec<_>>(),
            vec![Lexeme(Par, "par"),]
        );
        assert_eq!(
            Lexer::new("par.").collect::<Vec<_>>(),
            vec![Lexeme(Par, "par."),]
        );
    }

    #[test]
    fn unescape_str() {
        assert_eq!(unescape("ab \\\"cd\\\\e\\'f"), "ab \"cd\\e'f");
    }
}
