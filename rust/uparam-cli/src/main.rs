mod batch;
mod command;
mod lexer;
mod result;
mod shell;

use std::thread;

#[cfg(feature = "tokio")]
use tokio_rs as tokio;

#[cfg(feature = "async-std")]
use async_std_rs as async_std;

use flexi_logger::Logger;
use structopt::StructOpt;

use uparam::{reconnect, Manager};

#[cfg(feature = "tcp")]
use {unicom_nres::DefaultResolver, unicom_tcp::TcpSocket};

#[cfg(feature = "unix")]
use unicom_unix::UnixSocket;

#[cfg(feature = "serial")]
use unicom_serial::SerialPort;

use batch::Batch;
use command::Commands;
use result::Result;
use shell::Shell;

#[derive(Debug, StructOpt)]
#[structopt(about)]
struct Args {
    /// Specify device URL
    #[structopt(short = "d", long, env)]
    device: Option<String>,

    /// Specify log directory
    #[structopt(short = "l", long, env)]
    logdir: Option<String>,

    /// Disable colors
    #[structopt(short = "t", long, env)]
    nocolors: bool,

    /// Execute commands
    ///
    /// Commands:
    ///    ?                    Describe root parameter
    ///    <param>?             Describe parameter
    ///    <param>              Get parameter value
    ///    <param>=<value>      Set parameter value
    ///    <param>!             Reset parameter value to default
    ///
    /// Examples:
    ///    some_param?
    ///    other_param
    ///    enum_param=abc
    ///    def_param!
    ///    group.number=123
    ///    group.string="a b c"
    ///    group.enum=abc
    #[structopt(verbatim_doc_comment)]
    command: Vec<String>,

    /// Prints version information
    #[structopt(short = "V", long)]
    version: bool,
}

#[paw::main]
#[cfg_attr(feature = "tokio", tokio::main)]
#[cfg_attr(feature = "async-std", async_std::main)]
async fn main(args: Args) -> Result<()> {
    if args.version {
        println!("Version: {}", env!("CARGO_PKG_VERSION"));
        return Ok(());
    }

    if let Some(path) = args.logdir {
        Logger::with_env()
            .log_to_file()
            .directory(path)
            .suppress_timestamp()
            .append()
            .start()?;
    }

    let commands: Option<Commands> = if !args.command.is_empty() {
        Some(Commands::from_args(&args.command)?)
    } else {
        None
    };

    let url = args.device.ok_or("Device URL is missing")?;

    let manager = Manager::default();

    #[cfg(feature = "tcp")]
    manager.register(TcpSocket::new(DefaultResolver::default()))?;

    #[cfg(feature = "unix")]
    manager.register(UnixSocket::default())?;

    #[cfg(feature = "serial")]
    manager.register(SerialPort::default())?;

    let options = manager
        .from_url(&url)?
        .with_reconnect_strategy(reconnect::Delayed::limited(5));

    let (handle, mut process) = options.open_process();

    thread::spawn(move || {
        let app = async {
            if let Some(commands) = commands {
                Batch::new(handle, commands).run().await
            } else {
                Shell::new(handle, url).run().await
            }
        };

        #[cfg(feature = "tokio")]
        tokio::runtime::Runtime::new().unwrap().block_on(app);

        #[cfg(feature = "async-std")]
        async_std::task::block_on(app);
    });

    process.run().await?;

    Ok(())
}
