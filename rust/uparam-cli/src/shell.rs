use std::{borrow::Cow, process::exit};

use crate::{
    command::Commands,
    lexer::{Lexeme, LexemeKind, Lexer},
};
use ansi_term::{Color, Style};
use dirs::home_dir;
use futures::StreamExt;
use log::info;
use rustyline::{
    completion::{Candidate, Completer},
    error::ReadlineError,
    highlight::Highlighter,
    hint::Hinter,
    validate::Validator,
    ColorMode, CompletionType, Config, Context, EditMode, Editor, Helper, Result as ReadlineResult,
};
use uparam::{Event, Handle, Status};

#[cfg(unix)]
const HISTORY_FILE: &str = concat!(".", env!("CARGO_PKG_NAME"), ".history");

#[cfg(windows)]
const HISTORY_FILE: &str = concat!(env!("CARGO_PKG_NAME"), ".history");

pub struct Shell {
    device: String,
    handle: Handle,
}

const HELLO: &str = concat!(
    env!("CARGO_PKG_NAME"),
    " v",
    env!("CARGO_PKG_VERSION"),
    "
",
    env!("CARGO_PKG_DESCRIPTION"),
);

const USAGE: &str = "
Enter commands:
    <param>?          Describe parameter
    <param>           Get current parameter value
    <param>=<value>   Set new parameter value
    <param>!          Reset parameter to default
Press Ctrl-D to terminate shell or Ctrl-C to interrupt immediatelly.
";

impl Shell {
    pub fn new(handle: Handle, device: String) -> Self {
        Self { handle, device }
    }

    pub async fn run(self) {
        let config = Config::builder()
            .completion_type(CompletionType::List)
            .edit_mode(EditMode::Emacs)
            .color_mode(ColorMode::Enabled)
            .build();
        let helper = CliHelper::new(&self.handle);

        let mut rl = Editor::with_config(config);
        rl.set_helper(Some(helper));

        if let Some(home) = home_dir() {
            let _ = rl.load_history(&home.join(HISTORY_FILE));
        }

        println!("{}", HELLO);
        println!("{}", USAGE);

        let style = Style::new();
        let bad_style = style.fg(Color::Red);

        loop {
            match rl.readline(&format!("[{}] ", self.device)) {
                Ok(line) => {
                    let status = self.handle.status();
                    if status == Status::Offline {
                        let mut events = self.handle.on();

                        loop {
                            if let Some(event) = events.next().await {
                                match event {
                                    Event::Retrieving => {
                                        info!("Connecting... ");
                                    }
                                    Event::Retrieved => {
                                        info!("Connected");
                                        break;
                                    }
                                    _ => {}
                                }
                            } else {
                                eprintln!("{}", bad_style.paint("Disconnected."));
                            }
                        }
                    }
                    match line.parse::<Commands>() {
                        Ok(commands) => {
                            commands.run(&self.handle).await;
                            rl.add_history_entry(&line);
                        }
                        Err(error) => {
                            eprintln!("{}", bad_style.paint(format!("Bad command: {}", error)))
                        }
                    }
                }
                Err(ReadlineError::Interrupted) => {
                    exit(1);
                }
                Err(ReadlineError::Eof) => {
                    break;
                }
                Err(err) => {
                    eprintln!("Error: {:?}", err);
                    break;
                }
            }
        }

        if let Some(home) = home_dir() {
            rl.save_history(&home.join(HISTORY_FILE)).unwrap();
        }

        info!("Bye!");
    }
}

struct CliHelper {
    handle: Handle,

    hint_style: Style,
    prompt_style: Style,

    par_style_good: Style,
    par_style_bad: Style,
    cmd_style: Style,
    val_style: Style,

    no_style: Style,
}

impl CliHelper {
    fn new(handle: &Handle) -> Self {
        let style = Style::new();

        Self {
            handle: handle.clone(),

            hint_style: style.dimmed(),
            prompt_style: style.bold().fg(Color::Blue),

            par_style_good: style.fg(Color::Green),
            par_style_bad: style.fg(Color::Red),
            cmd_style: style.bold().fg(Color::Blue),
            val_style: style.italic().fg(Color::Yellow),

            no_style: style,
        }
    }

    fn complete_param(&self, path: &str) -> (usize, Vec<CliCandidate>) {
        let (offset, candidates) = if let Ok(param) = self.handle.param(path) {
            // complete sub-parameter names
            let mut completions = param
                .children()
                .unwrap()
                .map(|param| {
                    if let Ok(Some(name)) = param.with_def(|par| par.name.clone()) {
                        Some(if path.is_empty() {
                            name.clone()
                        } else {
                            ".".to_owned() + &name
                        })
                    } else {
                        None
                    }
                })
                .filter(Option::is_some)
                .map(Option::unwrap)
                .map(CliCandidate::Par)
                .collect::<Vec<_>>();
            // add def command
            completions.push(CliCandidate::Def);
            // add get command if geatble
            if param.with_def(|par| par.getable).unwrap() {
                completions.push(CliCandidate::Get);
            }
            // add set command if setable
            if param.with_def(|par| par.setable).unwrap() {
                completions.push(CliCandidate::Set);
                // add reset command if has default
                if param.with_def(|par| par.def.clone()).unwrap().is_some() {
                    completions.push(CliCandidate::Res);
                }
            }
            (path.len(), completions)
        } else {
            // complete parameter name
            let mut names = path.rsplitn(2, '.');
            let (path_pfx, name_pfx) = match (names.next(), names.next()) {
                (Some(name_pfx), Some(path_pfx)) => (path_pfx, name_pfx),
                (Some(name_pfx), None) => ("", name_pfx),
                _ => ("", ""),
            };
            if let Ok(param) = self.handle.param(path_pfx) {
                (
                    path_pfx.len(),
                    param
                        .children()
                        .unwrap()
                        .map(|param| {
                            param
                                .with_def(|par| par.name.clone())
                                .unwrap()
                                .and_then(|name| {
                                    if name.starts_with(name_pfx) {
                                        Some(name)
                                    } else {
                                        None
                                    }
                                })
                                .map(|name| {
                                    if path_pfx.is_empty() {
                                        name.clone()
                                    } else {
                                        ".".to_owned() + &name
                                    }
                                })
                        })
                        .filter(Option::is_some)
                        .map(Option::unwrap)
                        .map(CliCandidate::Par)
                        .collect(),
                )
            } else {
                return (0, Vec::new());
            }
        };

        (offset, candidates)
    }

    fn complete_value(&self, path: &str, pfx: &str) -> (usize, Vec<CliCandidate>) {
        if let Ok(param) = self.handle.param(path) {
            let opts = &param.with_def(|par| par.opt.clone()).unwrap();

            if !opts.is_empty() {
                return (
                    0,
                    opts.into_iter()
                        .map(|opt| {
                            if let Some(name) = &opt.name {
                                // use name if it's present
                                name.clone()
                            } else {
                                // use value as is if name is missing
                                opt.val.to_string()
                            }
                        })
                        .filter(|val| val.starts_with(pfx))
                        .map(CliCandidate::Val)
                        .collect(),
                );
            }
        }
        (0, Vec::new())
    }
}

impl Validator for CliHelper {}

impl Helper for CliHelper {}

#[derive(Debug)]
enum CliCandidate {
    Par(String),
    Get,
    Def,
    Set,
    Res,
    Val(String),
}

impl Candidate for CliCandidate {
    fn display(&self) -> &str {
        use self::CliCandidate::*;
        match self {
            Par(name) => &name,
            Get => "(get value)",
            Def => "? (describe)",
            Set => "=<value> (set value)",
            Res => "! (reset to default)",
            Val(data) => &data,
        }
    }

    fn replacement(&self) -> &str {
        use self::CliCandidate::*;
        match self {
            Par(name) => &name,
            Get => " ",
            Def => "? ",
            Set => "=",
            Res => "! ",
            Val(data) => &data,
        }
    }
}

impl Completer for CliHelper {
    type Candidate = CliCandidate;

    fn complete(
        &self,
        line: &str,
        pos: usize,
        _ctx: &Context,
    ) -> ReadlineResult<(usize, Vec<Self::Candidate>)> {
        use self::LexemeKind::*;
        let start = &line[..pos];
        let (last_par, last_lexeme) =
            Lexer::new(start).fold((None, None), |(last_par, _), lexeme| match lexeme {
                Lexeme(Par, par) => (Some(par), Some(lexeme)),
                lexeme => (last_par, Some(lexeme)),
            });

        let comp = match last_lexeme {
            None => self.complete_param(""),
            Some(Lexeme(Spc, _)) => {
                let (offset, completions) = self.complete_param("");
                (start.len() + offset, completions)
            }
            Some(Lexeme(Cmd, "?")) => (pos - 1, vec![CliCandidate::Def]),
            Some(Lexeme(Par, par_pfx)) => {
                let (offset, completions) = self.complete_param(par_pfx);
                (start.len() + offset - par_pfx.len(), completions)
            }
            Some(Lexeme(Cmd, "=")) if last_par.is_some() => {
                let (offset, completions) = self.complete_value(last_par.unwrap(), "");
                (start.len() + offset, completions)
            }
            Some(Lexeme(Cmd, "!")) => (pos - 1, vec![CliCandidate::Res]),
            Some(Lexeme(Val, val_pfx)) if last_par.is_some() => {
                let (offset, completions) = self.complete_value(last_par.unwrap(), val_pfx);
                (start.len() + offset - val_pfx.len(), completions)
            }
            _ => (0, Vec::new()),
        };
        Ok(comp)
    }
}

impl Hinter for CliHelper {
    fn hint(&self, line: &str, pos: usize, _ctx: &Context) -> Option<String> {
        match line.split_at(pos) {
            ("", "") => Some("enter parameter name".into()),
            _ => None,
        }
    }
}

impl Highlighter for CliHelper {
    fn highlight<'l>(&self, line: &'l str, _pos: usize) -> Cow<'l, str> {
        use self::LexemeKind::*;
        Lexer::new(line)
            .map(|Lexeme(kind, text)| {
                match (kind, text) {
                    (Par, text) => {
                        if let Ok(_param) = self.handle.param(text) {
                            self.par_style_good.paint(text)
                        } else {
                            self.par_style_bad.paint(text)
                        }
                    }
                    (Cmd, text) => self.cmd_style.paint(text),
                    (Val, text) => self.val_style.paint(text),
                    (_, text) => self.no_style.paint(text),
                }
                .to_string()
            })
            .collect::<String>()
            .into()
    }

    fn highlight_prompt<'b, 's: 'b, 'p: 'b>(
        &'s self,
        prompt: &'p str,
        _default: bool,
    ) -> Cow<'b, str> {
        format!(
            "[{}] ",
            self.prompt_style.paint(&prompt[1..prompt.len() - 2])
        )
        .into()
    }

    fn highlight_hint<'h>(&self, hint: &'h str) -> Cow<'h, str> {
        match hint {
            "enter parameter name" => self
                .hint_style
                .paint("enter parameter name")
                .to_string()
                .into(),
            other => other.into(),
        }
    }

    fn highlight_candidate<'c>(
        &self,
        candidate: &'c str,
        _completion: CompletionType,
    ) -> Cow<'c, str> {
        if candidate.starts_with('?') || candidate.starts_with('=') || candidate.starts_with('!') {
            let (fst, rst) = candidate.split_at(1);
            let fst = self.cmd_style.paint(fst).to_string();
            let mut it = rst.splitn(2, ' ');
            match (it.next(), it.next()) {
                (Some(""), Some(rst)) => (fst + " " + rst).into(),
                (Some(val), Some(rst)) => {
                    (fst + &self.val_style.paint(val).to_string() + " " + rst).into()
                }
                (Some(rst), _) => (fst + rst).into(),
                _ => fst.into(),
            }
        } else {
            candidate.into()
        }
    }
}
