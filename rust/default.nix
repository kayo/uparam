{ pkgs ? import <nixpkgs> {} }:
let llvmPackages = pkgs.llvmPackages_latest;
    libclang = llvmPackages.libclang;
    pkgconfig = pkgs.pkgconfig;
    libudev = pkgs.libudev;
    stdenv = llvmPackages.stdenv;
in {
  uparam = stdenv.mkDerivation {
    name = "uparam";
    src = ".";
    buildInputs = [
      pkgconfig libclang libudev
    ];
    LIBCLANG_PATH = "${libclang}/lib";
  };
}
