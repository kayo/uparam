use core::{
    fmt::{Display, Formatter, Result as FmtResult, Write},
    i64,
    iter::{once, FromIterator},
    result::Result as StdResult,
    str::FromStr,
};

#[cfg(feature = "std")]
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::{CastError, Date, Error, HexError, Result, Time, Type};

#[cfg(any(feature = "alloc", feature = "std"))]
use crate::{String, Vec};

/// Value of parameter
///
/// - `S` - string data type parameter
/// - `B` - binary data type parameter
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(untagged))]
pub enum Val<S, B> {
    Int(i64),
    Float(f64),
    String(S),
    Binary(B),
    Date(Date),
    Time(Time),
    Mac([u8; 6]),
    IPv4([u8; 4]),
    IPv6([u16; 8]),
}

impl<S, B> From<u8> for Val<S, B> {
    fn from(v: u8) -> Self {
        Val::Int(i64::from(v))
    }
}

impl<S, B> From<i8> for Val<S, B> {
    fn from(v: i8) -> Self {
        Val::Int(i64::from(v))
    }
}

impl<S, B> From<u16> for Val<S, B> {
    fn from(v: u16) -> Self {
        Val::Int(i64::from(v))
    }
}

impl<S, B> From<i16> for Val<S, B> {
    fn from(v: i16) -> Self {
        Val::Int(i64::from(v))
    }
}

impl<S, B> From<u32> for Val<S, B> {
    fn from(v: u32) -> Self {
        Val::Int(i64::from(v))
    }
}

impl<S, B> From<i32> for Val<S, B> {
    fn from(v: i32) -> Self {
        Val::Int(i64::from(v))
    }
}

impl<S, B> From<u64> for Val<S, B> {
    fn from(v: u64) -> Self {
        Val::Int(if v < i64::MAX as u64 {
            v as i64
        } else {
            i64::MAX
        })
    }
}

impl<S, B> From<i64> for Val<S, B> {
    fn from(v: i64) -> Self {
        Val::Int(v)
    }
}

impl<S, B> From<f32> for Val<S, B> {
    fn from(v: f32) -> Self {
        Val::Float(f64::from(v))
    }
}

impl<S, B> From<f64> for Val<S, B> {
    fn from(v: f64) -> Self {
        Val::Float(v)
    }
}

impl<'a, S, B> From<&'a str> for Val<S, B>
where
    S: From<&'a str>,
{
    fn from(v: &'a str) -> Self {
        Val::String(S::from(v))
    }
}

#[cfg(any(feature = "alloc", feature = "std"))]
impl<B> From<String> for Val<String, B> {
    fn from(v: String) -> Self {
        Val::String(v)
    }
}

impl<'a, S, B> From<&'a [u8]> for Val<S, B>
where
    B: From<&'a [u8]>,
{
    fn from(v: &'a [u8]) -> Self {
        Val::Binary(B::from(v))
    }
}

#[cfg(any(feature = "alloc", feature = "std"))]
impl<S> From<Vec<u8>> for Val<S, Vec<u8>> {
    fn from(v: Vec<u8>) -> Self {
        Val::Binary(v)
    }
}

#[cfg(feature = "std")]
impl<S, B> From<IpAddr> for Val<S, B> {
    fn from(v: IpAddr) -> Self {
        match v {
            IpAddr::V4(ip) => Val::from(ip),
            IpAddr::V6(ip) => Val::from(ip),
        }
    }
}

#[cfg(feature = "std")]
impl<S, B> From<Ipv4Addr> for Val<S, B> {
    fn from(v: Ipv4Addr) -> Self {
        Val::IPv4(v.octets())
    }
}

#[cfg(feature = "std")]
impl<S, B> From<Ipv6Addr> for Val<S, B> {
    fn from(v: Ipv6Addr) -> Self {
        Val::IPv6(v.segments())
    }
}

macro_rules! format_string {
    ($type: ty, $fmt: literal, $($rest: tt)*) => {
        {
            let mut out = <$type>::default();
            write!(out, $fmt, $($rest)*)?;
            out
        }
    };
}

impl<S, B> Val<S, B> {
    pub fn get_type(&self) -> Type {
        use self::Val::*;

        match self {
            Int(_) => Type::SInt,
            Float(_) => Type::Real,
            String(_) => Type::CStr,
            Binary(_) => Type::HBin,
            Date(_) => Type::Date,
            Time(_) => Type::Time,
            Mac(_) => Type::Mac,
            IPv4(_) => Type::IPv4,
            IPv6(_) => Type::IPv6,
        }
    }

    pub fn cast(self, type_: Type) -> Result<Self>
    where
        S: AsRef<str> + Write + Display + Default,
        B: AsRef<[u8]> + FromIterator<u8>,
    {
        use self::Val::*;

        Ok(match (self, type_) {
            // integer conversion
            (Int(v), Type::UInt) => {
                if v < 0 {
                    Err(CastError::Negative(Type::SInt, Type::UInt))?;
                }
                Int(v)
            }
            (this @ Int(_), Type::SInt) => this,
            (Int(v), Type::UFix) => {
                if v < 0 {
                    Err(CastError::Negative(Type::SInt, Type::UFix))?;
                }
                Float(v as f64)
            }
            (Int(v), Type::SFix) => Float(v as f64),
            (Int(v), Type::Real) => Float(v as f64),
            (Int(v), Type::CStr) => String(format_string!(S, "{}", v)),

            // float conversion
            (this @ Float(_), Type::Real) => this,
            (Float(v), Type::UInt) => Int(v as u64 as i64),
            (Float(v), Type::SInt) => Int(v as i64),
            (Float(v), Type::UFix) => {
                if v < 0.0 {
                    Err(CastError::Negative(Type::Real, Type::UFix))?;
                }
                Float(v)
            }
            (this @ Float(_), Type::SFix) => this,
            (Float(v), Type::CStr) => String(format_string!(S, "{}", v)),

            // string conversion
            (this @ String(_), Type::CStr) => this,
            (String(v), Type::Real) => Float(v.as_ref().parse().map_err(Error::InvalidFloat)?),
            (String(v), Type::UInt) => {
                let v = v.as_ref().parse::<i64>()?;
                if v < 0 {
                    Err(CastError::Negative(Type::CStr, Type::UInt))?;
                }
                Int(v)
            }
            (String(v), Type::SInt) => Int(v.as_ref().parse::<i64>()?),
            (String(v), Type::UFix) => {
                let v = v.as_ref().parse::<f64>()?;
                if v < 0.0 {
                    Err(CastError::Negative(Type::CStr, Type::UInt))?;
                }
                Float(v)
            }
            (String(v), Type::SFix) => Float(v.as_ref().parse::<f64>()?),
            (String(v), Type::HBin) => Binary(parse_hbin(v.as_ref())?),
            (String(v), Type::Time) => Time(v.as_ref().parse()?),
            (String(v), Type::Date) => Date(v.as_ref().parse()?),
            (String(v), Type::Mac) => Mac(parse_mac(v.as_ref()).ok_or_else(|| Error::InvalidMac)?),
            #[cfg(feature = "std")]
            (String(v), Type::IPv4) => v
                .as_ref()
                .parse::<Ipv4Addr>()
                .map(Val::from)
                .map_err(|_| Error::InvalidIPv4)?,
            #[cfg(not(feature = "std"))]
            (String(v), Type::IPv4) => {
                IPv4(parse_ipv4(v.as_ref()).ok_or_else(|| Error::InvalidIPv4)?)
            }
            #[cfg(feature = "std")]
            (String(v), Type::IPv6) => v
                .as_ref()
                .parse::<Ipv6Addr>()
                .map(Val::from)
                .map_err(|_| Error::InvalidIPv6)?,
            #[cfg(not(feature = "std"))]
            (String(v), Type::IPv6) => {
                IPv6(parse_ipv6(v.as_ref()).ok_or_else(|| Error::InvalidIPv6)?)
            }

            // binary conversion
            (this @ Binary(_), Type::HBin) => this,
            (this @ Binary(_), Type::CStr) => String(format_string!(S, "{}", this)),

            // time conversion
            (this @ Time(_), Type::Time) => this,
            (Time(v), Type::CStr) => String(format_string!(S, "{}", v)),

            // date conversion
            (this @ Date(_), Type::Date) => this,
            (Date(v), Type::CStr) => String(format_string!(S, "{}", v)),

            // mac address conversion
            (this @ Mac(_), Type::Mac) => this,
            (this @ Mac(_), Type::CStr) => String(format_string!(S, "{}", this)),

            // ip v4 address conversion
            (this @ IPv4(_), Type::IPv4) => this,
            (this @ IPv4(_), Type::CStr) => String(format_string!(S, "{}", this)),

            // ip v6 address conversion
            (this @ IPv6(_), Type::IPv6) => this,
            (this @ IPv6(_), Type::CStr) => String(format_string!(S, "{}", this)),

            (val, type_) => return Err(CastError::Unsupported(val.get_type(), type_).into()),
        })
    }
}

fn parse_hbin<B>(v: &str) -> Result<B>
where
    B: FromIterator<u8>,
{
    if let Some(c) = v.chars().find(|c| char_isnt_hex(*c)) {
        return Err(HexError::InvalidChar(c).into());
    }
    let v = v.as_bytes();
    let l = v.len();
    if l % 2 != 0 {
        return Err(HexError::InvalidLength(l).into());
    }
    Ok(v.chunks(2)
        .map(|v| (char_into_hex(v[0]) << 4) | char_into_hex(v[1]))
        .collect())
}

#[cfg(not(feature = "std"))]
#[inline]
fn char_isnt_dec(c: char) -> bool {
    c < '0' || c > '9'
}

#[cfg(not(feature = "std"))]
#[inline]
fn char_isnt_ipv4(c: char) -> bool {
    c != '.' && char_isnt_dec(c)
}

#[cfg(not(feature = "std"))]
#[inline]
fn char_isnt_ipv6(c: char) -> bool {
    c != ':' && char_isnt_hex(c)
}

#[inline]
fn char_isnt_hex(c: char) -> bool {
    c < '0' || (c > '9' && c < 'A') || (c > 'F' && c < 'a') || c > 'f'
}

#[inline]
fn char_into_hex(c: u8) -> u8 {
    if c <= b'9' {
        c - b'0'
    } else if c <= b'F' {
        c - (b'A' - 10)
    } else {
        c - (b'a' - 10)
    }
}

#[inline]
fn char_isnt_mac(c: char) -> bool {
    c != ':' && char_isnt_hex(c)
}

fn parse_mac(v: &str) -> Option<[u8; 6]> {
    if v.len() < 11 || v.len() > 17 || v.find(char_isnt_mac).is_some() {
        return None;
    }

    let mut mac = [0; 6];
    let mut ri = mac.iter_mut();
    let mut vi = v.split(':');

    loop {
        match (ri.next(), vi.next()) {
            (Some(r), Some(v)) => {
                *r = v
                    .as_bytes()
                    .iter()
                    .fold(0u8, |r, c| (r << 4) | char_into_hex(*c));
            }
            (None, None) => break,
            _ => return None,
        }
    }

    Some(mac)
}

#[cfg(not(feature = "std"))]
fn parse_ipv4(v: &str) -> Option<[u8; 4]> {
    if v.len() < 7 || v.len() > 15 || v.find(char_isnt_ipv4).is_some() {
        return None;
    }

    let mut ip = [0; 4];
    let mut ri = ip.iter_mut();
    let mut vi = v.split('.');

    loop {
        match (ri.next(), vi.next()) {
            (Some(r), Some(v)) => {
                *r = v.parse().ok()?;
            }
            (None, None) => break,
            _ => return None,
        }
    }

    Some(ip)
}

#[cfg(not(feature = "std"))]
fn parse_ipv6(v: &str) -> Option<[u16; 8]> {
    if v.find(char_isnt_ipv6).is_some() || v.chars().filter(|c| *c == ':').count() > 7 {
        return None;
    }

    let mut ip = [0; 8];
    let mut pi = v.split("::");

    match (pi.next(), pi.next(), pi.next()) {
        (Some(p), Some(s), None) => {
            parse_parts(ip.iter_mut(), p.split(':'), false)?;
            parse_parts(ip.iter_mut().rev(), s.rsplit(':'), false)?;
        }
        (Some(v), None, _) => parse_parts(ip.iter_mut(), v.split(':'), true)?,
        _ => return None,
    };

    #[inline]
    fn parse_parts<'r, 'v>(
        mut ri: impl Iterator<Item = &'r mut u16>,
        mut vi: impl Iterator<Item = &'v str>,
        full: bool,
    ) -> Option<()> {
        loop {
            match (ri.next(), vi.next()) {
                (Some(r), Some(v)) => {
                    *r = v
                        .as_bytes()
                        .iter()
                        .fold(0u16, |r, c| (r << 4) | (char_into_hex(*c) as u16));
                }
                (None, None) => return Some(()),
                (Some(_), None) => return if full { None } else { Some(()) },
                _ => return None,
            }
        }
    }

    Some(ip)
}

impl<S, B> FromStr for Val<S, B>
where
    for<'a> S: From<&'a str>,
{
    type Err = Error;

    fn from_str(src: &str) -> StdResult<Self, Self::Err> {
        Ok(Val::String(src.into()))
    }
}

impl<S, B> Display for Val<S, B>
where
    S: Display,
    B: AsRef<[u8]>,
{
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Val::Int(v) => v.fmt(f),
            Val::Float(v) => v.fmt(f),
            Val::String(v) => v.fmt(f),
            Val::Binary(v) => format_hbin(v.as_ref(), f),
            Val::Time(v) => v.fmt(f),
            Val::Date(v) => v.fmt(f),
            Val::Mac(v) => format_mac(v, f),
            #[cfg(feature = "std")]
            Val::IPv4(v) => Ipv4Addr::from(*v).fmt(f),
            #[cfg(not(feature = "std"))]
            Val::IPv4(v) => format_ipv4(v, f),
            #[cfg(feature = "std")]
            Val::IPv6(v) => Ipv6Addr::from(*v).fmt(f),
            #[cfg(not(feature = "std"))]
            Val::IPv6(v) => format_ipv6(v, f),
        }
    }
}

fn format_hbin(v: &[u8], f: &mut Formatter) -> FmtResult {
    v.iter()
        .map(|b| once(b >> 4).chain(once(b & 0xf)))
        .flatten()
        .map(hex_into_char)
        .try_for_each(|p| p.fmt(f))
}

fn format_mac(v: &[u8; 6], f: &mut Formatter) -> FmtResult {
    v.iter()
        .try_fold(0, |cnt, hex| {
            if cnt > 0 {
                ':'.fmt(f)?;
            }
            format_hex8_lz(*hex, f, true)?;
            Ok(cnt + 1)
        })
        .map(|_| ())
}

#[cfg(any(not(feature = "std"), test))]
fn format_ipv4(v: &[u8; 4], f: &mut Formatter) -> FmtResult {
    v.iter()
        .try_fold(false, |init, num| {
            if init {
                '.'.fmt(f)?;
            }
            num.fmt(f)?;
            Ok(true)
        })
        .map(|_| ())
}

#[cfg(any(not(feature = "std"), test))]
fn format_ipv6(v: &[u16; 8], f: &mut Formatter) -> FmtResult {
    // find longest zero group
    let lzg = match v
        .iter()
        .enumerate()
        .fold((None, None), |(longest, current), (index, value)| {
            if let Some((start, len)) = current {
                if *value == 0 {
                    (longest, Some((start, len + 1)))
                } else {
                    if let Some((_, plen)) = longest {
                        if len > plen {
                            (Some((start, len)), None)
                        } else {
                            (longest, None)
                        }
                    } else {
                        (Some((start, len)), None)
                    }
                }
            } else {
                if *value == 0 {
                    (longest, Some((index, 1)))
                } else {
                    (longest, None)
                }
            }
        }) {
        (Some((pstart, plen)), Some((start, len))) => {
            if len > plen {
                Some(start..start + len)
            } else {
                Some(pstart..pstart + plen)
            }
        }
        (Some((start, len)), None) => Some(start..start + len),
        (None, Some((start, len))) => Some(start..start + len),
        _ => None,
    };

    fn format_part(v: &[u16], f: &mut Formatter) -> FmtResult {
        v.iter()
            .try_fold(0, |cnt, hex| {
                if cnt > 0 {
                    ':'.fmt(f)?;
                }
                format_hex16(*hex, f)?;
                Ok(cnt + 1)
            })
            .map(|_| ())
    }

    if let Some(range) = lzg {
        format_part(&v[..range.start], f)?;
        "::".fmt(f)?;
        format_part(&v[range.end..], f)
    } else {
        format_part(v, f)
    }
}

#[inline]
fn hex_into_char(v: u8) -> char {
    (if v < 10 { v + b'0' } else { v + (b'a' - 10) }) as char
}

#[cfg(any(not(feature = "std"), test))]
#[inline]
fn format_hex8(v: u8, f: &mut Formatter) -> FmtResult {
    format_hex8_lz(v, f, false)
}

fn format_hex8_lz(v: u8, f: &mut Formatter, lz: bool) -> FmtResult {
    let h = v >> 4;
    let l = v & 0xf;
    if lz || h > 0 {
        hex_into_char(h).fmt(f)?;
    }
    hex_into_char(l).fmt(f)
}

#[cfg(any(not(feature = "std"), test))]
fn format_hex16(v: u16, f: &mut Formatter) -> FmtResult {
    let h = (v >> 8) as u8;
    let l = (v & 0xff) as u8;
    if h > 0 {
        format_hex8(h, f)?;
    }
    format_hex8_lz(l, f, h > 0)
}

#[cfg(test)]
mod test {
    use super::*;

    struct Format<F>(F);

    impl<F> Display for Format<F>
    where
        for<'f> F: Fn(&'f mut Formatter) -> FmtResult,
    {
        fn fmt(&self, f: &mut Formatter) -> FmtResult {
            (self.0)(f)
        }
    }

    #[test]
    fn test_char_into_hex() {
        let mut i = 0;
        for x in b'0'..=b'9' {
            assert_eq!(char_into_hex(x), i);
            i += 1;
        }
        let mut i = 10;
        for x in b'a'..=b'f' {
            assert_eq!(char_into_hex(x), i);
            i += 1;
        }
        let mut i = 10;
        for x in b'A'..=b'F' {
            assert_eq!(char_into_hex(x), i);
            i += 1;
        }
    }

    mod parse_hbin {
        use super::*;
        use arrayvec::*;

        #[test]
        fn pass() {
            let v: ArrayVec<[u8; 4]> = parse_hbin("120ad30f").unwrap();
            assert_eq!(v.as_ref(), &[0x12, 0xa, 0xd3, 0xf]);
        }

        #[test]
        fn nohex() {
            let v: Result<ArrayVec<[u8; 4]>> = parse_hbin("120ag30f");
            assert!(v.is_err());
        }

        #[test]
        fn nolen() {
            let v: Result<ArrayVec<[u8; 4]>> = parse_hbin("120a30f");
            assert!(v.is_err());
        }
    }

    mod format_hbin {
        use super::*;
        use arrayvec::*;

        #[test]
        fn test() {
            let mut s = ArrayString::<[u8; 8]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_hbin(&[0x1, 0x23, 0xad, 0xf], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "0123ad0f");
        }
    }

    mod parse_mac {
        use super::*;

        #[test]
        fn full() {
            assert_eq!(
                parse_mac("11:55:00:aa:cc:ff"),
                Some([0x11, 0x55, 0, 0xaa, 0xcc, 0xff])
            );
        }

        #[test]
        fn short() {
            assert_eq!(parse_mac("1:5:0:a:c:f"), Some([0x1, 0x5, 0, 0xa, 0xc, 0xf]));
        }

        #[test]
        fn vari() {
            assert_eq!(
                parse_mac("1:23:0:ad:c:ef"),
                Some([0x1, 0x23, 0, 0xad, 0xc, 0xef])
            );
        }

        #[test]
        fn nohex() {
            assert_eq!(parse_mac("1:2g:0:ad:c:ef"), None);
        }

        #[test]
        fn nocol() {
            assert_eq!(parse_mac("1.23.0.ad.c.ef"), None);
        }

        #[test]
        fn nofull() {
            assert_eq!(parse_mac("1:23:0:ad:ef"), None);
        }
    }

    mod format_mac {
        use super::*;
        use arrayvec::*;

        #[test]
        fn test() {
            let mut s = ArrayString::<[u8; 20]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_mac(&[0x11, 0x55, 0, 0xaa, 0xcc, 0xff], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "11:55:00:aa:cc:ff");
        }
    }

    #[cfg(not(feature = "std"))]
    mod parse_ipv4 {
        use super::*;

        #[test]
        fn full() {
            assert_eq!(parse_ipv4("123.167.255.189"), Some([123, 167, 255, 189]));
        }

        #[test]
        fn short() {
            assert_eq!(parse_ipv4("1.2.3.0"), Some([1, 2, 3, 0]));
        }

        #[test]
        fn vari() {
            assert_eq!(parse_ipv4("1.43.178.255"), Some([1, 43, 178, 255]));
        }

        #[test]
        fn zero() {
            assert_eq!(parse_ipv4("0.0.0.0"), Some([0, 0, 0, 0]));
        }

        #[test]
        fn nodec() {
            assert_eq!(parse_ipv4("1.a.5.0"), None);
        }

        #[test]
        fn nodot() {
            assert_eq!(parse_ipv4("1:2:5:0"), None);
        }

        #[test]
        fn nofull() {
            assert_eq!(parse_ipv4("1.5.0"), None);
        }
    }

    mod format_ipv4 {
        use super::*;
        use arrayvec::*;

        #[test]
        fn zero() {
            let mut s = ArrayString::<[u8; 20]>::new();
            write!(s, "{}", Format(|f: &mut Formatter| format_ipv4(&[0; 4], f))).unwrap();
            assert_eq!(s.as_ref(), "0.0.0.0");
        }

        #[test]
        fn short() {
            let mut s = ArrayString::<[u8; 20]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv4(&[1, 2, 3, 0], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "1.2.3.0");
        }

        #[test]
        fn full() {
            let mut s = ArrayString::<[u8; 20]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv4(&[1, 43, 178, 255], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "1.43.178.255");
        }
    }

    #[cfg(not(feature = "std"))]
    mod parse_ipv6 {
        use super::*;

        #[test]
        fn full() {
            assert_eq!(
                parse_ipv6("1111:2222:5555:0000:aaaa:bbbb:dddd:ffff"),
                Some([0x1111, 0x2222, 0x5555, 0, 0xaaaa, 0xbbbb, 0xdddd, 0xffff])
            );
        }

        #[test]
        fn xshort() {
            assert_eq!(parse_ipv6("ffff::1"), Some([0xffff, 0, 0, 0, 0, 0, 0, 1]));
        }

        #[test]
        fn nopfx() {
            assert_eq!(parse_ipv6("::1"), Some([0, 0, 0, 0, 0, 0, 0, 1]));
        }

        #[test]
        fn nosfx() {
            assert_eq!(parse_ipv6("ffff::"), Some([0xffff, 0, 0, 0, 0, 0, 0, 0]));
        }

        #[test]
        fn zero() {
            assert_eq!(parse_ipv6("::"), Some([0, 0, 0, 0, 0, 0, 0, 0]));
        }

        #[test]
        fn vari() {
            assert_eq!(
                parse_ipv6("1:23:456:7890:a:bc:def:a9fe"),
                Some([1, 0x23, 0x456, 0x7890, 0xa, 0xbc, 0xdef, 0xa9fe])
            );
        }

        #[test]
        fn short() {
            assert_eq!(
                parse_ipv6("1:2:0:5:a:d:f:c"),
                Some([1, 2, 0, 5, 0xa, 0xd, 0xf, 0xc])
            );
        }

        #[test]
        fn nofull() {
            assert_eq!(parse_ipv6("1:2:0:a:f:e:5"), None);
        }

        #[test]
        fn nohex() {
            assert_eq!(parse_ipv6("1:2:0:a:g:f:e:5"), None);
        }
    }

    mod format_ipv6 {
        use super::*;
        use arrayvec::*;

        #[test]
        fn zero() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(s, "{}", Format(|f: &mut Formatter| format_ipv6(&[0; 8], f))).unwrap();
            assert_eq!(s.as_ref(), "::");
        }

        #[test]
        fn lzgp() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv6(&[0, 0, 1, 2, 0, 3, 4, 0], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "::1:2:0:3:4:0");
        }

        #[test]
        fn lzgs() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv6(&[0, 1, 2, 0, 3, 4, 0, 0], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "0:1:2:0:3:4::");
        }

        #[test]
        fn lzg1() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv6(&[0, 1, 2, 0, 0, 3, 4, 0], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "0:1:2::3:4:0");
        }

        #[test]
        fn lzg2() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv6(&[1, 2, 0, 0, 3, 4, 0, 0], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "1:2::3:4:0:0");
        }

        #[test]
        fn lzg3() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv6(&[1, 0, 0, 2, 0, 0, 0, 4], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "1:0:0:2::4");
        }

        #[test]
        fn lzg4() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv6(&[1, 0, 0, 0, 2, 0, 0, 4], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "1::2:0:0:4");
        }

        #[test]
        fn lzg5() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv6(&[0xffff, 0, 0, 0, 0, 0, 0, 1], f))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "ffff::1");
        }

        #[test]
        fn vari() {
            let mut s = ArrayString::<[u8; 40]>::new();
            write!(
                s,
                "{}",
                Format(|f: &mut Formatter| format_ipv6(
                    &[1, 0x23, 0x456, 0x7890, 0xa, 0xbc, 0xdef, 0xa9fe],
                    f
                ))
            )
            .unwrap();
            assert_eq!(s.as_ref(), "1:23:456:7890:a:bc:def:a9fe");
        }
    }
}
