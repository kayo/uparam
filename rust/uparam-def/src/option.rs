use core::fmt::{Display, Formatter, Result as FmtResult};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::Val;

/// Optional constraint of parameter
#[derive(Debug, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Opt<S, B, T> {
    pub val: Val<S, B>,
    // text fields
    pub name: Option<T>,
    pub unit: Option<T>,
    pub item: Option<T>,
    pub info: Option<T>,
    pub hint: Option<T>,
}

impl<S, B, T> Opt<S, B, T> {
    pub fn with_val(val: Val<S, B>) -> Self {
        Self {
            val,
            name: None,
            unit: None,
            item: None,
            info: None,
            hint: None,
        }
    }

    pub fn format(&self, f: &mut Formatter, ident: usize, unit: &Option<T>) -> FmtResult
    where
        S: Display,
        B: AsRef<[u8]>,
        T: Display,
    {
        if let Some(name) = &self.name {
            write!(
                f,
                "{:ident$}@{name} = {val}",
                "",
                ident = ident,
                name = name,
                val = self.val,
            )?;
        } else {
            write!(f, "{:ident$}@ = {val}", "", ident = ident, val = self.val,)?;
        }
        if let Some(unit) = &self.unit {
            writeln!(f, " {unit}", unit = unit)?;
        } else if let Some(unit) = unit {
            writeln!(f, " {unit}", unit = unit)?;
        }
        if let Some(info) = &self.info {
            writeln!(f, "{:ident$}; {text}", "", ident = ident, text = info)?;
        }
        if let Some(item) = &self.item {
            writeln!(f, "{:ident$}! {text}", "", ident = ident, text = item)?;
        }
        if let Some(hint) = &self.hint {
            writeln!(f, "{:ident$}? {text}", "", ident = ident, text = hint)?;
        }
        Ok(())
    }
}
