use core::{
    convert::TryFrom,
    fmt::{Display, Formatter, Result as FmtResult},
    mem::transmute,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::{typeid, Error, Result, TypeId};

/// Type of parameter
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[repr(u8)]
pub enum Type {
    /// Unsigned integer number
    UInt = typeid::UINT,
    /// Signed integer number
    SInt = typeid::SINT,
    /// Unsigned fixed-point number
    UFix = typeid::UFIX,
    /// Signed fixed-point number
    SFix = typeid::SFIX,
    /// Floating-point number
    Real = typeid::REAL,
    /// String (ASCII/UTF-8) of variable length
    CStr = typeid::CSTR,
    /// Binary data of fixed length
    HBin = typeid::HBIN,
    /// Real time (hours/minutes/seconds)
    Time = typeid::TIME,
    /// Real date (year/month/date)
    Date = typeid::DATE,
    /// Interface MAC
    Mac = typeid::MAC,
    /// IPv4 address
    IPv4 = typeid::IPV4,
    /// IPv6 address
    IPv6 = typeid::IPV6,
}

impl TryFrom<TypeId> for Type {
    type Error = Error;

    fn try_from(val: TypeId) -> Result<Type> {
        if typeid::is(val) {
            Ok(unsafe { transmute(val) })
        } else {
            Err(Error::UnknownType(val))
        }
    }
}

impl Display for Type {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::Type::*;
        match self {
            UInt => f.write_str("uint"),
            SInt => f.write_str("sint"),
            UFix => f.write_str("ufix"),
            SFix => f.write_str("sfix"),
            Real => f.write_str("real"),
            CStr => f.write_str("cstr"),
            HBin => f.write_str("hbin"),
            Time => f.write_str("time"),
            Date => f.write_str("date"),
            Mac => f.write_str("mac"),
            IPv4 => f.write_str("ipv4"),
            IPv6 => f.write_str("ipv6"),
        }
    }
}
