use core::{
    fmt::{Display, Formatter, Result as FmtResult},
    result::Result as StdResult,
    str::FromStr,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "chrono")]
use chrono::{Local, Timelike};

use crate::{InvalidNumber, InvalidTimePart, ParseDateTimeError, ParseTimeError};

/// Time type
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Time {
    /// Hour value `0 ..= 23`
    pub hour: u8,
    /// Minutes value `0 ..= 59`
    pub min: u8,
    /// Seconds value `0 ..= 59`
    pub sec: u8,
}

impl Time {
    /// Create from parts
    pub fn new(hour: u8, min: u8, sec: u8) -> Self {
        Self { hour, min, sec }
    }

    /// Create from current
    #[cfg(feature = "chrono")]
    pub fn current() -> Self {
        let d = Local::now();
        Self::new(d.hour() as u8, d.minute() as u8, d.second() as u8)
    }
}

impl FromStr for Time {
    type Err = ParseTimeError;

    fn from_str(src: &str) -> StdResult<Self, Self::Err> {
        use self::ParseDateTimeError::*;

        #[cfg(feature = "chrono")]
        {
            if src == "now" {
                return Ok(Self::current());
            }
        }

        let mut parts = src.split(':');

        let (hour, min, sec) = match (parts.next(), parts.next(), parts.next(), parts.next()) {
            (Some(hour), Some(min), Some(sec), None) => (hour, min, sec),
            (Some(_), None, _, _) => return Err(InvalidFormat.into()),
            (Some(_), Some(_), Some(_), Some(_)) => return Err(TooManyParts(':').into()),
            (Some(_), Some(_), None, _) => return Err(TooLessParts(':').into()),
            (_, _, _, _) => return Err(InvalidFormat.into()),
        };

        let hour = hour
            .parse::<u8>()
            .map_err(InvalidNumber::BadFormat)
            .and_then(|hour| {
                if hour > 23 {
                    Err(InvalidNumber::TooBig(hour, 0..=23))
                } else {
                    Ok(hour)
                }
            })
            .map_err(InvalidTimePart::Hour)?;

        let min = min
            .parse::<u8>()
            .map_err(InvalidNumber::BadFormat)
            .and_then(|min| {
                if min > 59 {
                    Err(InvalidNumber::TooBig(min, 0..=59))
                } else {
                    Ok(min)
                }
            })
            .map_err(InvalidTimePart::Minute)?;

        let sec = sec
            .parse::<u8>()
            .map_err(InvalidNumber::BadFormat)
            .and_then(|sec| {
                if sec > 59 {
                    Err(InvalidNumber::TooBig(min, 0..=59))
                } else {
                    Ok(sec)
                }
            })
            .map_err(InvalidTimePart::Second)?;

        Ok(Self::new(hour, min, sec))
    }
}

impl Display for Time {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{:02}:{:02}:{:02}", self.hour, self.min, self.sec)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn time_parse() {
        assert_eq!(
            "09:20:00".parse::<Time>().unwrap(),
            Time {
                hour: 9,
                min: 20,
                sec: 0,
            }
        );
    }
}
