use core::{
    fmt::{Display, Formatter, Result as FmtResult, Write},
    i64,
    iter::FromIterator,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::{Error, Opt, ParId, Result, Type, TypeError, Val};

/// Parameter description
#[derive(Debug, Clone, Default)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Par<S, B, T, O> {
    /// Serial parameter identifier
    pub id: ParId,

    /// Parameter type
    #[cfg_attr(feature = "serde", serde(rename = "type"))]
    pub type_: Option<Type>,

    /// Parameter size
    ///
    /// Size measured in bytes for string or binary or in bits for numbers
    pub size: Option<usize>,

    /// Fraction bits for fixed-point values
    pub frac: Option<u8>,

    /// Sub parameters
    pub sub: Option<(ParId, ParId)>,

    /// Value of parameter
    pub val: Option<Val<S, B>>,

    /// Textual fields

    /// Parameter name
    pub name: Option<T>,

    /// Units of parameter value
    pub unit: Option<T>,

    /// Menu item
    pub item: Option<T>,

    /// Parameter info
    pub info: Option<T>,

    /// Parameter hint
    pub hint: Option<T>,

    /// Flags

    /// Parameter value can be get
    pub getable: bool,

    /// Parameter value can be set
    pub setable: bool,

    /// Parameter is persistent
    pub persist: bool,

    /// Parameter value must be one of options
    pub options: bool,

    /// Constraints

    /// Default value of parameter
    pub def: Option<Val<S, B>>,

    /// Minimum value of parameter
    pub min: Option<Val<S, B>>,

    /// Maximum value of parameter
    pub max: Option<Val<S, B>>,

    /// Step of parameter value
    pub stp: Option<Val<S, B>>,

    /// Optional constraints or value options
    pub opt: O, //Vec<Opt<S, B, T>>,

    /// Preferred polling interval in milliseconds
    pub poll: Option<u32>,
}

impl<S, B, T, O> Par<S, B, T, O> {
    /// Create new using id
    #[inline]
    pub fn from_id(id: ParId) -> Self
    where
        Self: Default,
    {
        let mut par = Self::default();
        par.id = id;
        par
    }

    /// Add new value
    #[inline]
    pub fn with_val(mut self, val: Val<S, B>) -> Self {
        self.val = Some(val);
        self
    }

    /// Remove value
    #[inline]
    pub fn without_val(mut self) -> Self {
        self.val = None;
        self
    }

    /// Set new value
    #[inline]
    pub fn set_val(&mut self, val: Val<S, B>) {
        self.val = Some(val);
    }

    /// Clear value
    #[inline]
    pub fn clear_val(&mut self) {
        self.val = None;
    }

    /// Make thin copy suitable for messaging
    pub fn to_thin(&self) -> Self
    where
        Self: Default,
    {
        let mut def = Self::from_id(self.id);
        def.type_ = self.type_;
        def.size = self.size;
        def.frac = self.frac;
        def
    }

    pub fn format(&self, f: &mut Formatter, level: usize, val: &Option<Val<S, B>>) -> FmtResult
    where
        S: Display,
        B: AsRef<[u8]>,
        T: Display,
        for<'a> &'a O: IntoIterator<Item = &'a Opt<S, B, T>>,
    {
        let ident = level * 4;
        if let Some(name) = &self.name {
            write!(
                f,
                "{:ident$}{name} #{id}",
                "",
                ident = ident,
                name = name,
                id = self.id
            )?;
        } else {
            write!(f, "{:ident$}#{id}", "", ident = ident, id = self.id)?;
        }
        writeln!(
            f,
            " ({}{}{})",
            if self.getable { 'r' } else { '-' },
            if self.setable { 'w' } else { '-' },
            if self.setable { 'p' } else { '-' }
        )?;
        let ident = ident + 2;
        if let Some(info) = &self.info {
            writeln!(f, "{:ident$}; {text}", "", ident = ident, text = info)?;
        }
        if let Some(item) = &self.item {
            writeln!(f, "{:ident$}! {text}", "", ident = ident, text = item)?;
        }
        if let Some(hint) = &self.hint {
            writeln!(f, "{:ident$}? {text}", "", ident = ident, text = hint)?;
        }
        if let Some(val) = &val {
            write!(f, "{:ident$}= {val}", "", ident = ident, val = val,)?;
            if let Some(unit) = &self.unit {
                writeln!(f, " {unit}", unit = unit)?;
            } else {
                writeln!(f)?;
            }
        }
        if self.def.is_some() || self.min.is_some() || self.max.is_some() || self.stp.is_some() {
            write!(f, "{:ident$}", "", ident = ident)?;
            if let Some(val) = &self.def {
                write!(f, "* {val}", val = val)?;
                if let Some(unit) = &self.unit {
                    write!(f, " {unit}", unit = unit)?;
                }
            }
            if self.min.is_some() || self.max.is_some() || self.stp.is_some() {
                write!(f, " [")?;
                if let Some(val) = &self.min {
                    write!(f, "{min}", min = val)?;
                }
                write!(f, " .. ")?;
                if let Some(val) = &self.max {
                    write!(f, "{max}", max = val)?;
                }
                if let Some(val) = &self.stp {
                    write!(f, "; {stp}", stp = val)?;
                }
                write!(f, "]")?;
            }
            writeln!(f)?;
        }
        for opt in &self.opt {
            opt.format(f, ident, &self.unit)?;
        }
        if let Some(poll) = self.poll {
            writeln!(f, "{:ident$}% {poll} mS", "", ident = ident, poll = poll)?;
        }
        Ok(())
    }

    /// Check when parameter is enum
    pub fn is_enum(&self) -> bool
    where
        for<'a> &'a O: IntoIterator,
    {
        self.min.is_none()
            && self.max.is_none()
            && self.stp.is_none()
            && self.opt.into_iter().next().is_some()
    }

    /// Convert option name to value
    pub fn opt_name_to_val(&self, val: Val<S, B>) -> Result<Val<S, B>>
    where
        T: Clone + PartialEq + From<S>,
        Val<S, B>: Clone + PartialEq,
        for<'a> &'a O: IntoIterator<Item = &'a Opt<S, B, T>>,
    {
        if (&self.opt).into_iter().next().is_none() {
            return Ok(val);
        }
        let orig_val = val.clone();
        let name = &match val {
            Val::String(name) => name.into(),
            _ => return Ok(val),
        };
        (&self.opt)
            .into_iter()
            .find(|opt| {
                if let Some(opt_name) = &opt.name {
                    opt_name == name
                } else {
                    false
                }
            })
            .map(|opt| opt.val.clone())
            // prevent error for non-enums
            .or_else(|| if self.is_enum() { None } else { Some(orig_val) })
            .ok_or(Error::MismatchEnumValue)
    }

    /// Convert option value to name
    pub fn opt_val_to_name(&self, val: Val<S, B>) -> Result<Val<S, B>>
    where
        T: Clone,
        S: From<T>,
        Val<S, B>: Clone + PartialEq,
        for<'a> &'a O: IntoIterator<Item = &'a Opt<S, B, T>>,
    {
        if (&self.opt).into_iter().next().is_none() {
            return Ok(val);
        }
        if let Val::String(_) = &val {
            return Ok(val);
        }
        let orig_val = val.clone();
        (&self.opt)
            .into_iter()
            .find(|opt| opt.val == val)
            .map(|opt| {
                opt.name
                    .clone()
                    .map(|name| Val::String(name.into()))
                    .unwrap_or(val)
            })
            // prevent error for non-enums
            .or_else(|| if self.is_enum() { None } else { Some(orig_val) })
            .ok_or(Error::MismatchEnumValue)
    }

    /// Check value type
    pub fn check_type(&self, val: &Val<S, B>) -> Result<()> {
        let type_ = if let Some(type_) = self.type_ {
            type_
        } else {
            return Err(Error::MissingType);
        };

        match (val, type_) {
            // integer
            (Val::Int(_), Type::UInt) => Ok(()),
            (Val::Int(_), Type::SInt) => Ok(()),
            // fixed point
            (Val::Float(_), Type::UFix) => Ok(()),
            (Val::Float(_), Type::SFix) => Ok(()),
            // floating point
            (Val::Float(_), Type::Real) => Ok(()),
            // string & binary
            (Val::String(_), Type::CStr) => Ok(()),
            (Val::Binary(_), Type::HBin) => Ok(()),
            // date & time
            (Val::Date(_), Type::Date) => Ok(()),
            (Val::Time(_), Type::Time) => Ok(()),
            // mac, ipv4 & ipv6
            (Val::Mac(_), Type::Mac) => Ok(()),
            (Val::IPv4(_), Type::IPv4) => Ok(()),
            (Val::IPv6(_), Type::IPv6) => Ok(()),

            (val, type_) => Err(TypeError::new(type_, val.get_type()).into()),
        }
    }

    /// Get type of parameter
    pub fn get_type(&self) -> Result<Type> {
        self.type_.ok_or_else(|| Error::MissingType)
    }

    /// Check when value fits to constraints
    pub fn check_val(&self, val: &Val<S, B>) -> Result<()>
    where
        S: AsRef<str>,
        B: AsRef<[u8]>,
        Val<S, B>: PartialEq,
        for<'a> &'a O: IntoIterator<Item = &'a Opt<S, B, T>>,
    {
        let type_ = self.get_type()?;

        // check range
        match (val, &self.min, &self.max) {
            // integer constraints
            (Val::Int(val), Some(Val::Int(min)), None) => Error::check_value_range(val, min..)?,
            (Val::Int(val), None, Some(Val::Int(max))) => Error::check_value_range(val, ..=max)?,
            (Val::Int(val), Some(Val::Int(min)), Some(Val::Int(max))) => {
                Error::check_value_range(val, min..=max)?
            }

            // floating point constraints
            (Val::Float(val), Some(Val::Float(min)), None) => Error::check_value_range(val, min..)?,
            (Val::Float(val), None, Some(Val::Float(max))) => {
                Error::check_value_range(val, ..=max)?
            }
            (Val::Float(val), Some(Val::Float(min)), Some(Val::Float(max))) => {
                Error::check_value_range(val, min..=max)?
            }

            _ => (),
        }

        // check step
        // TODO:

        // check options
        if self.is_enum() &&
            // value must be equal to one of options
            !(&self.opt).into_iter().any(|opt| &opt.val == val)
        {
            return Err(Error::MismatchEnumValue);
        }

        // check atomic type size
        match (val, &self.size, &self.frac, type_) {
            // integer type
            (Val::Int(val), Some(size), None, Type::UInt) => {
                Error::check_value_range(*val, 0..=((1u64 << size) - 1) as i64)?
            }
            (Val::Int(val), Some(size), None, Type::SInt) => Error::check_value_range(
                *val,
                -((1u64 << (size - 1)) as i64)..=((1u64 << (size - 1)) - 1) as i64,
            )?,

            // fixed point type
            (Val::Float(val), Some(size), Some(frac), Type::UFix) => {
                Error::check_value_range(*val, 0.0..=((1u64 << (*size as u8 - *frac)) - 1) as f64)?
            }

            (Val::Float(val), Some(size), Some(frac), Type::SFix) => Error::check_value_range(
                *val,
                -((1u64 << (*size as u8 - *frac - 1)) as f64)
                    ..=((1u64 << (*size as u8 - *frac - 1)) - 1) as f64,
            )?,

            _ => (),
        }

        // check array type size
        match (val, &self.size, type_) {
            (Val::String(val), Some(size), Type::CStr) => {
                Error::check_value_length(val.as_ref().as_bytes().len(), ..=*size)?
            }
            (Val::Binary(val), Some(size), Type::HBin) => {
                Error::check_value_length(val.as_ref().len(), *size..=*size)?
            }
            _ => (),
        }

        Ok(())
    }

    /// Convert and check value using type info and constraints
    pub fn cast_val(&self, val: Val<S, B>) -> Result<Val<S, B>>
    where
        S: AsRef<str> + Write + Display + Default,
        B: AsRef<[u8]> + FromIterator<u8>,
        Val<S, B>: PartialEq,
        for<'a> &'a O: IntoIterator<Item = &'a Opt<S, B, T>>,
    {
        if let Some(type_) = self.type_ {
            let val = val.cast(type_)?;
            self.check_type(&val)?;
            self.check_val(&val)?;
            Ok(val)
        } else {
            Err(Error::MissingType)
        }
    }
}
