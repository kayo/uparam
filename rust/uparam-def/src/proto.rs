use bytes::{buf::BufExt, Buf, BufMut};
use core::{convert::TryFrom, iter::FromIterator, str::from_utf8};

use crate::{field, flag, Date, Error, Opt, Par, ParId, Result, Time, Type, Val};

/// Get the parameter value from buffer
pub fn get_val<S, B, T, O>(par: &Par<S, B, T, O>, buf: &mut impl Buf) -> Result<Val<S, B>>
where
    for<'a> S: From<&'a str>,
    for<'a> B: From<&'a [u8]>,
{
    Ok(match (par.type_, par.size, par.frac) {
        (Some(Type::UInt), Some(bits), None) => {
            let size = bits >> 3;
            Error::check_data_length(buf.remaining(), size)?;
            buf.get_uint_le(size).into()
        }
        (Some(Type::SInt), Some(bits), None) => {
            let size = bits >> 3;
            Error::check_data_length(buf.remaining(), size)?;
            buf.get_int_le(size).into()
        }
        (Some(Type::UFix), Some(bits), Some(frac)) => {
            let size = bits >> 3;
            Error::check_data_length(buf.remaining(), size)?;
            (buf.get_uint_le(size) as f64 / f64::from(1 << frac)).into()
        }
        (Some(Type::SFix), Some(bits), Some(frac)) => {
            let size = bits >> 3;
            Error::check_data_length(buf.remaining(), size)?;
            (buf.get_int_le(size) as f64 / f64::from(1 << frac)).into()
        }
        (Some(Type::Real), Some(32), None) => {
            Error::check_data_length(buf.remaining(), 4)?;
            buf.get_f32_le().into()
        }
        (Some(Type::Real), Some(64), None) => {
            Error::check_data_length(buf.remaining(), 4)?;
            buf.get_f64_le().into()
        }
        (Some(Type::CStr), Some(_), None) => Val::String(get_str(buf)?),
        (Some(Type::HBin), Some(size), None) => {
            Error::check_data_length(buf.remaining(), size)?;
            Val::Binary(buf.take(size).bytes().into())
        }
        (Some(Type::Date), None, None) => {
            Error::check_data_length(buf.remaining(), 4)?;
            let year = buf.get_i16_le();
            let mon = buf.get_u8();
            let date = buf.get_u8();
            Val::Date(Date { year, mon, date })
        }
        (Some(Type::Time), None, None) => {
            Error::check_data_length(buf.remaining(), 3)?;
            let hour = buf.get_u8();
            let min = buf.get_u8();
            let sec = buf.get_u8();
            Val::Time(Time { hour, min, sec })
        }
        (Some(Type::Mac), None, None) => {
            Error::check_data_length(buf.remaining(), 6)?;
            let mut mac = [0; 6];
            buf.take(6).copy_to_slice(&mut mac);
            Val::Mac(mac)
        }
        (Some(Type::IPv4), None, None) => {
            Error::check_data_length(buf.remaining(), 4)?;
            let mut ip4 = [0; 4];
            buf.take(4).copy_to_slice(&mut ip4);
            Val::IPv4(ip4)
        }
        (Some(Type::IPv6), None, None) => {
            Error::check_data_length(buf.remaining(), 16)?;
            let mut ip6 = [0u16; 8];
            let mut buf = buf.take(16);
            for p in &mut ip6 {
                *p = buf.get_u16_le();
            }
            Val::IPv6(ip6)
        }
        (type_, size, _frac) => {
            return Err(Error::cant_get_value(type_, size));
        }
    })
}

/// Get the string from buffer
///
/// The string data preceded by length which determines the number of bytes of actual string data (excluding '\0').
pub fn get_str<'b, S>(buf: &mut impl Buf) -> Result<S>
where
    for<'a> S: From<&'a str>,
{
    let len = get_uint(buf)?;
    Error::check_data_length(buf.remaining(), len)?;
    let txt = from_utf8(buf.take(len).bytes())?.into();
    buf.advance(len);
    Ok(txt)
}

/// Get length value from buffer
///
/// The length encodes in protobuf way as unsigned int values.
pub fn get_uint(buf: &mut impl Buf) -> Result<usize> {
    let mut val: usize = 0;
    let mut off: usize = 0;

    {
        let raw = buf.bytes();
        let mut shift: u8 = 0;

        loop {
            Error::check_data_length(raw.len(), off + 1)?;
            let byte = raw[off];
            val |= ((byte & 0x7f) as usize) << shift;
            shift += 7;
            off += 1;
            if byte & (1 << 7) == 0 {
                break;
            }
        }
    }

    buf.advance(off);

    Ok(val)
}

/// Put parameter value into buffer
pub fn put_val<S, B, T, O>(
    par: &Par<S, B, T, O>,
    buf: &mut impl BufMut,
    val: &Val<S, B>,
) -> Result<()>
where
    S: AsRef<str>,
    B: AsRef<[u8]>,
{
    match (par.type_, par.size, par.frac, val) {
        (Some(Type::UInt), Some(bits), None, Val::Int(val)) => {
            buf.put_uint_le(*val as u64, bits >> 3);
        }
        (Some(Type::UInt), Some(bits), None, Val::Float(val)) => {
            buf.put_uint_le(*val as u64, bits >> 3);
        }
        (Some(Type::SInt), Some(bits), None, Val::Int(val)) => {
            buf.put_int_le(*val, bits >> 3);
        }
        (Some(Type::SInt), Some(bits), None, Val::Float(val)) => {
            buf.put_int_le(*val as i64, bits >> 3);
        }
        (Some(Type::UFix), Some(bits), Some(frac), Val::Int(val)) => {
            buf.put_uint_le((val * (1 << frac)) as u64, bits >> 3);
        }
        (Some(Type::UFix), Some(bits), Some(frac), Val::Float(val)) => {
            buf.put_uint_le((val * f64::from(1 << frac)) as u64, bits >> 3);
        }
        (Some(Type::SFix), Some(bits), Some(frac), Val::Int(val)) => {
            buf.put_int_le((val * (1 << frac)) as i64, bits >> 3);
        }
        (Some(Type::SFix), Some(bits), Some(frac), Val::Float(val)) => {
            buf.put_int_le((val * f64::from(1 << frac)) as i64, bits >> 3);
        }
        (Some(Type::Real), Some(32), None, Val::Int(val)) => {
            buf.put_f32_le(*val as f32);
        }
        (Some(Type::Real), Some(32), None, Val::Float(val)) => {
            buf.put_f32_le(*val as f32);
        }
        (Some(Type::Real), Some(64), None, Val::Int(val)) => {
            buf.put_f64_le(*val as f64);
        }
        (Some(Type::Real), Some(64), None, Val::Float(val)) => {
            buf.put_f64_le(*val as f64);
        }
        (Some(Type::CStr), Some(size), None, Val::String(val)) => {
            if val.as_ref().len() > size {
                return Err(Error::ExceededData);
            }
            put_str(buf, val);
        }
        (Some(Type::HBin), Some(size), None, Val::Binary(val)) => {
            if val.as_ref().len() != size {
                return Err(Error::ExceededData);
            }
            buf.put_slice(val.as_ref())
        }
        (Some(Type::Date), None, None, Val::Date(Date { year, mon, date })) => {
            buf.put_i16_le(*year);
            buf.put_u8(*mon);
            buf.put_u8(*date);
        }
        (Some(Type::Time), None, None, Val::Time(Time { hour, min, sec })) => {
            buf.put_u8(*hour);
            buf.put_u8(*min);
            buf.put_u8(*sec);
        }
        (Some(Type::Mac), None, None, Val::Mac(val)) => {
            buf.put_slice(val);
        }
        (Some(Type::IPv4), None, None, Val::IPv4(val)) => {
            buf.put_slice(val);
        }
        (Some(Type::IPv6), None, None, Val::IPv6(val)) => {
            for p in val {
                buf.put_u16_le(*p);
            }
        }
        (type_, size, _frac, _val) => {
            return Err(Error::cant_put_value(type_, size));
        }
    }

    Ok(())
}

/// Put string into buffer
///
/// The string data preceded by length which determines the number of bytes of actual string data (excluding '\0').
pub fn put_str(buf: &mut impl BufMut, val: impl AsRef<str>) {
    let val = val.as_ref().as_bytes();
    let len = val.len();

    put_uint(buf, len);
    buf.put_slice(val);
}

/// Put length value into buffer
///
/// The length encodes in protobuf way as unsigned int values.
pub fn put_uint(buf: &mut impl BufMut, val: usize) {
    // produce output
    let mut val = val;
    while val >= (1 << 7) {
        buf.put_u8((val & 0x7f) as u8 | (1 << 7));
        val >>= 7;
    }
    buf.put_u8((val & 0x7f) as u8);
}

/// Put parameter definition request into buffer
///
/// The parameter definition request consists of request flags and parameter identifier.
pub fn put_def_req<S, B, T, O>(buf: &mut impl BufMut, par: &Par<S, B, T, O>) {
    use self::field::*;

    let qry = TYPE | SUB | NAME | UNIT | ITEM | INFO | HINT | GET | DEF | MIN | MAX | STP | OPT;

    put_uint(buf, qry as usize);
    put_uint(buf, par.id as usize);
}

/// Put parameter value get request into buffer
///
/// The parameter value getting request consists of request flags and parameter identifier.
pub fn put_get_req<S, B, T, O>(buf: &mut impl BufMut, par: &Par<S, B, T, O>) {
    use self::field::*;

    let qry = GET;

    put_uint(buf, qry as usize);
    put_uint(buf, par.id as usize);
}

/// Put parameter value set request into buffer
///
/// The parameter value setting request consists of request flags and parameter identifier.
///
/// The value setting request combines with value getting to check actual value after set.
///
/// That decision is due to the fact that value can be modified by host. For example, the strings which length exceeds the parameter value size may be trimmed, or maximum and minimum constraints can be applied to numeric values and etc.
pub fn put_set_req<S, B, T, O>(buf: &mut impl BufMut, par: &Par<S, B, T, O>) -> Result<()>
where
    S: AsRef<str>,
    B: AsRef<[u8]>,
{
    if let Some(val) = &par.val {
        use self::field::*;

        let qry = SET | GET;

        put_uint(buf, qry as usize);
        put_uint(buf, par.id as usize);
        put_val(par, buf, val)
    } else {
        Err(Error::MissingValue)
    }
}

/// Process response from host for specified parameter data
pub fn get_res<S, B, T, O>(buf: &mut impl Buf, par: &mut Par<S, B, T, O>) -> Result<()>
where
    for<'a> S: From<&'a str>,
    for<'a> B: From<&'a [u8]>,
    for<'a> T: From<&'a str>,
    O: FromIterator<Opt<S, B, T>>,
{
    use self::field::*;

    let qry = get_uint(buf)? as u16;

    if qry & TYPE > 0 {
        get_type(buf, par)?;
    }

    if qry & SUB > 0 {
        let from = get_uint(buf)? as ParId;
        let to = get_uint(buf)? as ParId;
        par.sub = Some((from, to));
    }

    // get
    if qry & GET > 0 {
        par.val = Some(get_val(par, buf)?);
    }

    // text
    if qry & NAME > 0 {
        par.name = Some(get_str(buf)?);
    }
    if qry & UNIT > 0 {
        par.unit = Some(get_str(buf)?);
    }
    if qry & ITEM > 0 {
        par.item = Some(get_str(buf)?);
    }
    if qry & INFO > 0 {
        par.info = Some(get_str(buf)?);
    }
    if qry & HINT > 0 {
        par.hint = Some(get_str(buf)?);
    }

    // val
    if qry & DEF > 0 {
        par.def = Some(get_val(par, buf)?);
    }
    if qry & MIN > 0 {
        par.min = Some(get_val(par, buf)?);
    }
    if qry & MAX > 0 {
        par.max = Some(get_val(par, buf)?);
    }
    if qry & STP > 0 {
        par.stp = Some(get_val(par, buf)?);
    }

    // opt
    if qry & OPT > 0 {
        par.opt = get_opts(buf, par)?;
    }

    if qry & POLL > 0 {
        par.poll = Some(get_uint(buf)? as u32);
    }

    Ok(())
}

/// Get the type of parameter with flags and extra type info
///
/// Data encoded like length values in protobuf manner.
///
/// The extra info includes:
/// - The size
///   - For string is the maximum length in bytes
///   - For binary data is the actual length in bytes
///   - For numeric values is the size in bits
/// - The fraction
///   - For fixed-point numbers is the position of point
pub fn get_type<S, B, T, O>(buf: &mut impl Buf, par: &mut Par<S, B, T, O>) -> Result<()> {
    use self::flag::*;

    let type_ = get_uint(buf)?;
    par.type_ = Some(Type::try_from((type_ & 0xf) as u8)?);

    if type_ as u8 & GETABLE > 0 {
        par.getable = true;
    }
    if type_ as u8 & SETABLE > 0 {
        par.setable = true;
    }
    if type_ as u8 & PERSIST > 0 {
        par.persist = true;
    }
    if type_ as u8 & OPTIONS > 0 {
        par.options = true;
    }

    let size = if par.type_ == Some(Type::UFix) || par.type_ == Some(Type::SFix) {
        par.frac = Some((type_ >> 16) as u8);
        (type_ >> 8) & 0xff
    } else {
        type_ >> 8
    };

    par.size = if size > 0 { Some(size) } else { None };

    Ok(())
}

/// Get parameter options or typical values from response
///
/// The options preceded by length value which describes the number of actually present options.
/// Each option value preceded by fields bitset which describes the actually presented fields.
/// After value sits all extra fields which corresponds to set bits.
/// For now only text fields can be set for options.
pub fn get_opts<S, B, T, O>(buf: &mut impl Buf, par: &mut Par<S, B, T, O>) -> Result<O>
where
    for<'a> S: From<&'a str>,
    for<'a> B: From<&'a [u8]>,
    for<'a> T: From<&'a str>,
    O: FromIterator<Opt<S, B, T>>,
{
    use self::field::*;

    let size = get_uint(buf)?;

    (0..size)
        .map(|_| -> Result<_> {
            let opt_qry = get_uint(buf)? as u16;

            // val
            let mut opt = Opt::with_val(get_val(par, buf)?);

            // text
            if opt_qry & NAME > 0 {
                opt.name = Some(get_str(buf)?);
            }
            if opt_qry & UNIT > 0 {
                opt.unit = Some(get_str(buf)?);
            }
            if opt_qry & ITEM > 0 {
                opt.item = Some(get_str(buf)?);
            }
            if opt_qry & INFO > 0 {
                opt.info = Some(get_str(buf)?);
            }
            if opt_qry & HINT > 0 {
                opt.hint = Some(get_str(buf)?);
            }

            Ok(opt)
        })
        .collect::<Result<_>>()
}
