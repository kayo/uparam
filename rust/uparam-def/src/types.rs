#[cfg(feature = "std")]
pub use std::{string::String, vec::Vec};

#[cfg(all(not(feature = "std"), feature = "alloc"))]
pub use alloc::{string::String, vec::Vec};

/// Unique parameter identifier
pub type ParId = u32;
