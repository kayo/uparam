use core::{
    fmt,
    fmt::{Display, Error as FmtError, Formatter, Result as FmtResult},
    num::{ParseFloatError, ParseIntError},
    ops::{Bound, RangeBounds, RangeInclusive},
    result::Result as StdResult,
    str::Utf8Error,
};

#[cfg(feature = "std")]
use std::error::Error as StdError;

use crate::{Type, TypeId};

/// Result type
pub type Result<T> = StdResult<T, Error>;

/// Error type
#[derive(Debug)]
pub enum Error {
    InvalidFloat(ParseFloatError),
    InvalidInt(ParseIntError),
    InvalidDate(ParseDateError),
    InvalidTime(ParseTimeError),
    InvalidCast(CastError),
    InvalidType(TypeError),
    InvalidUtf8(Utf8Error),
    InvalidHex(HexError),
    InvalidMac,
    InvalidIPv4,
    InvalidIPv6,
    UnknownType(TypeId),
    MissingType,
    MissingValue,
    NotEnoughData {
        required: usize,
        provided: usize,
    },
    ExceededData,
    CantGetValue {
        type_: Option<Type>,
        size: Option<usize>,
    },
    CantPutValue {
        type_: Option<Type>,
        size: Option<usize>,
    },
    FormatError(FmtError),
    TooSmallValue,
    TooBigValue,
    MismatchEnumValue,
    TooShortValue {
        required: usize,
        provided: usize,
    },
    TooLongValue {
        required: usize,
        provided: usize,
    },
}

impl Error {
    pub fn check_data_length(len: usize, req: usize) -> Result<()> {
        if len < req {
            Err(Error::NotEnoughData {
                required: req,
                provided: len,
            })
        } else {
            Ok(())
        }
    }

    pub fn cant_get_value(type_: Option<Type>, size: Option<usize>) -> Self {
        Self::CantGetValue { type_, size }
    }

    pub fn cant_put_value(type_: Option<Type>, size: Option<usize>) -> Self {
        Self::CantPutValue { type_, size }
    }

    pub fn check_value_range<T>(val: T, range: impl RangeBounds<T>) -> Result<()>
    where
        T: PartialOrd,
    {
        use self::Bound::*;

        match range.start_bound() {
            Included(min) if val < *min => return Err(Self::TooSmallValue),
            Excluded(min) if val <= *min => return Err(Self::TooSmallValue),
            _ => (),
        }

        match range.end_bound() {
            Included(max) if val > *max => return Err(Self::TooBigValue),
            Excluded(max) if val >= *max => return Err(Self::TooBigValue),
            _ => (),
        }

        Ok(())
    }

    pub fn check_value_length(len: usize, range: impl RangeBounds<usize>) -> Result<()> {
        use self::Bound::*;

        match range.start_bound() {
            Included(min) if len < *min => {
                return Err(Self::TooShortValue {
                    required: *min,
                    provided: len,
                })
            }
            Excluded(min) if len <= *min => {
                return Err(Self::TooShortValue {
                    required: *min,
                    provided: len,
                })
            }
            _ => (),
        }

        match range.end_bound() {
            Included(max) if len > *max => {
                return Err(Self::TooLongValue {
                    required: *max,
                    provided: len,
                })
            }
            Excluded(max) if len >= *max => {
                return Err(Self::TooLongValue {
                    required: *max,
                    provided: len,
                })
            }
            _ => (),
        }

        Ok(())
    }
}

macro_rules! from_impl {
    ($($var: ident: $type: ty;)*) => {
        $( impl From<$type> for Error {
            fn from(e: $type) -> Self {
                Self::$var(e)
            }
        } )*
    };
}

from_impl! {
    InvalidFloat: ParseFloatError;
    InvalidInt: ParseIntError;
    InvalidDate: ParseDateError;
    InvalidTime: ParseTimeError;
    InvalidCast: CastError;
    InvalidType: TypeError;
    InvalidUtf8: Utf8Error;
    InvalidHex: HexError;
    FormatError: FmtError;
}

#[cfg(feature = "std")]
impl StdError for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::Error::*;
        match self {
            InvalidFloat(error) => {
                "Invalid float: ".fmt(f)?;
                error.fmt(f)
            }
            InvalidInt(error) => {
                "Invalid int: ".fmt(f)?;
                error.fmt(f)
            }
            InvalidDate(error) => {
                "Invalid date: ".fmt(f)?;
                error.fmt(f)
            }
            InvalidTime(error) => {
                "Invalid time: ".fmt(f)?;
                error.fmt(f)
            }
            InvalidUtf8(error) => {
                "Invalid utf-8: ".fmt(f)?;
                error.fmt(f)
            }
            InvalidHex(error) => {
                "Invalid hex: ".fmt(f)?;
                error.fmt(f)
            }
            InvalidMac => "Expected MAC address format: AA:BB:CC:DD:EE:FF".fmt(f),
            InvalidIPv4 => "Expected IPv4 address format: xxx.yyy.zzz.www".fmt(f),
            InvalidIPv6 => "Expected IPv6 address format: xxx:yyy:zzz:www".fmt(f),
            UnknownType(type_id) => {
                "Unknown type: ".fmt(f)?;
                type_id.fmt(f)
            }
            InvalidType(error) => {
                "Invalid type: ".fmt(f)?;
                error.fmt(f)
            }
            InvalidCast(error) => {
                "Invalid cast: ".fmt(f)?;
                error.fmt(f)
            }
            MissingType => "Untyped parameter".fmt(f),
            MissingValue => "No value to set".fmt(f),
            NotEnoughData { required, provided } => {
                "At least ".fmt(f)?;
                required.fmt(f)?;
                " bytes required but ".fmt(f)?;
                provided.fmt(f)?;
                " provided.".fmt(f)
            }
            ExceededData => "Exceeded data".fmt(f),
            CantGetValue { type_, size } => {
                "Cannot get value".fmt(f)?;
                if let Some(type_) = type_ {
                    " of type ".fmt(f)?;
                    type_.fmt(f)?;
                }
                if let Some(size) = size {
                    size.fmt(f)?;
                }
                Ok(())
            }
            CantPutValue { type_, size } => {
                "Cannot put value".fmt(f)?;
                if let Some(type_) = type_ {
                    " of type ".fmt(f)?;
                    type_.fmt(f)?;
                }
                if let Some(size) = size {
                    size.fmt(f)?;
                }
                Ok(())
            }
            TooSmallValue => "Value too small".fmt(f),
            TooBigValue => "Value too big".fmt(f),
            MismatchEnumValue => "Mismatch enum value".fmt(f),
            TooShortValue { required, provided } => {
                "Value too short: at least ".fmt(f)?;
                required.fmt(f)?;
                " bytes required but ".fmt(f)?;
                provided.fmt(f)?;
                " provided".fmt(f)
            }
            TooLongValue { required, provided } => {
                "Value too long: up to ".fmt(f)?;
                required.fmt(f)?;
                " bytes required but ".fmt(f)?;
                provided.fmt(f)?;
                " provided".fmt(f)
            }
            FormatError(error) => {
                "Unable to format value: ".fmt(f)?;
                error.fmt(f)
            }
        }
    }
}

/// Date parsing error
#[derive(Debug)]
pub enum ParseDateTimeError<InvalidPart> {
    /// Unexpected date or time format
    InvalidFormat,
    /// Too less separated parts in date or time
    TooLessParts(char),
    /// Too many separated parts in date or time
    TooManyParts(char),
    /// Invalid date part
    InvalidPart(InvalidPart),
}

impl From<InvalidDatePart> for ParseDateTimeError<InvalidDatePart> {
    fn from(e: InvalidDatePart) -> Self {
        Self::InvalidPart(e)
    }
}

impl From<InvalidTimePart> for ParseDateTimeError<InvalidTimePart> {
    fn from(e: InvalidTimePart) -> Self {
        Self::InvalidPart(e)
    }
}

#[cfg(feature = "std")]
impl<InvalidPartType> StdError for ParseDateTimeError<InvalidPartType> where
    InvalidPartType: Display + fmt::Debug + ExpectedDateTimeFormat
{
}

pub trait ExpectedDateTimeFormat {
    fn expected_format(sep: char) -> &'static str;
}

impl<InvalidPartType> Display for ParseDateTimeError<InvalidPartType>
where
    InvalidPartType: Display + ExpectedDateTimeFormat,
{
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::ParseDateTimeError::*;
        match self {
            InvalidFormat => {
                "Invalid format. Expected: ".fmt(f)?;
                InvalidPartType::expected_format('_').fmt(f)
            }
            TooLessParts(sep) => {
                "Too less parts. Expected: ".fmt(f)?;
                InvalidPartType::expected_format(*sep).fmt(f)
            }
            TooManyParts(sep) => {
                "Too many parts. Expected: ".fmt(f)?;
                InvalidPartType::expected_format(*sep).fmt(f)
            }
            InvalidPart(part) => {
                "Malformed part: ".fmt(f)?;
                part.fmt(f)
            }
        }
    }
}

/// Invalid date part
#[derive(Debug)]
pub enum InvalidDatePart {
    Year(ParseIntError),
    Month(InvalidNumber<u8, RangeInclusive<u8>, ParseIntError>),
    Date(InvalidNumber<u8, RangeInclusive<u8>, ParseIntError>),
}

/// Date parsing error
pub type ParseDateError = ParseDateTimeError<InvalidDatePart>;

#[cfg(feature = "std")]
impl StdError for InvalidDatePart {}

impl Display for InvalidDatePart {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::InvalidDatePart::*;
        match self {
            Year(reason) => {
                "Invalid year value. ".fmt(f)?;
                reason.fmt(f)
            }
            Month(reason) => {
                "Invalid month value. ".fmt(f)?;
                reason.fmt(f)
            }
            Date(reason) => {
                "Invalid date value. ".fmt(f)?;
                reason.fmt(f)
            }
        }
    }
}

impl ExpectedDateTimeFormat for InvalidDatePart {
    fn expected_format(sep: char) -> &'static str {
        match sep {
            '-' => "yyyy-mm-dd",
            '/' => "mm/dd/yyyy",
            '.' => "dd.mm.yyyy",
            _ => "yyyy-mm-dd, mm/dd/yyyy or dd.mm.yyyy",
        }
    }
}

/// Invalid time part
#[derive(Debug)]
pub enum InvalidTimePart {
    Hour(InvalidNumber<u8, RangeInclusive<u8>, ParseIntError>),
    Minute(InvalidNumber<u8, RangeInclusive<u8>, ParseIntError>),
    Second(InvalidNumber<u8, RangeInclusive<u8>, ParseIntError>),
}

/// Time parsing error
pub type ParseTimeError = ParseDateTimeError<InvalidTimePart>;

#[cfg(feature = "std")]
impl StdError for InvalidTimePart {}

impl Display for InvalidTimePart {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::InvalidTimePart::*;
        match self {
            Hour(reason) => {
                "Invalid hours value. ".fmt(f)?;
                reason.fmt(f)
            }
            Minute(reason) => {
                "Invalid minutes value. ".fmt(f)?;
                reason.fmt(f)
            }
            Second(reason) => {
                "Invalid seconds value. ".fmt(f)?;
                reason.fmt(f)
            }
        }
    }
}

impl ExpectedDateTimeFormat for InvalidTimePart {
    fn expected_format(_sep: char) -> &'static str {
        "hh:mm:ss"
    }
}

/// Invalid number
#[derive(Debug)]
pub enum InvalidNumber<V, R, E> {
    BadFormat(E),
    TooSmall(V, R),
    TooBig(V, R),
}

#[cfg(feature = "std")]
impl<V, R, E> StdError for InvalidNumber<V, R, E>
where
    V: Display + fmt::Debug,
    R: fmt::Debug,
    E: Display + fmt::Debug,
{
}

impl<V, R, E> Display for InvalidNumber<V, R, E>
where
    V: Display,
    R: fmt::Debug,
    E: Display,
{
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::InvalidNumber::*;
        match self {
            BadFormat(error) => {
                "Bad number: ".fmt(f)?;
                error.fmt(f)
            }
            TooSmall(value, range) => {
                "Too small value: ".fmt(f)?;
                value.fmt(f)?;
                ". Expected: ".fmt(f)?;
                range.fmt(f)
            }
            TooBig(value, range) => {
                "Too big value: ".fmt(f)?;
                value.fmt(f)?;
                ". Expected: ".fmt(f)?;
                range.fmt(f)
            }
        }
    }
}

/// Type matching error
#[derive(Debug)]
pub struct TypeError {
    pub expected: Type,
    pub actual: Type,
}

impl TypeError {
    pub fn new(expected: Type, actual: Type) -> Self {
        Self { expected, actual }
    }
}

#[cfg(feature = "std")]
impl StdError for TypeError {}

impl Display for TypeError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        "Expected: ".fmt(f)?;
        self.expected.fmt(f)?;
        " actual: ".fmt(f)?;
        self.actual.fmt(f)
    }
}

/// Value casting error
#[derive(Debug)]
pub enum CastError {
    Negative(Type, Type),
    Unsupported(Type, Type),
}

#[cfg(feature = "std")]
impl StdError for CastError {}

impl Display for CastError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::CastError::*;
        match self {
            Negative(from, to) => {
                "Unable to convert negative ".fmt(f)?;
                from.fmt(f)?;
                " to ".fmt(f)?;
                to.fmt(f)
            }
            Unsupported(from, to) => {
                "Unsupported cast from ".fmt(f)?;
                from.fmt(f)?;
                " to ".fmt(f)?;
                to.fmt(f)
            }
        }
    }
}

/// Hexadecimal parsing error
#[derive(Debug)]
pub enum HexError {
    InvalidLength(usize),
    InvalidChar(char),
}

#[cfg(feature = "std")]
impl StdError for HexError {}

impl Display for HexError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        use self::HexError::*;
        match self {
            InvalidLength(len) => {
                "Invalid length: ".fmt(f)?;
                len.fmt(f)
            }
            InvalidChar(chr) => {
                "Invalid char: ".fmt(f)?;
                chr.fmt(f)
            }
        }
    }
}
