/// The fields bitset
///
/// Those bits describes exchange operation and affected fields.
///
pub mod field {
    /// Request to set value of parameter
    ///
    /// When this bit is set you should also send new value in request.
    pub const SET: u16 = 1;

    /// Request the type of parameter
    ///
    /// The requested type will be put at beginning of response.
    /// If parameter hasn't value this bit will be unset in response fields.
    ///
    /// The typical example of value-less parameters is a parameter groups.
    /// Whereas constant parameters usually has type but its not readable / writable.
    pub const TYPE: u16 = 1 << 1;

    /// Request the identifiers of sub-parameters
    ///
    /// The response will contain two identifiers which corresponds to the first and last of sub-parameters.
    /// When parameter has single sub-parameter the first and last identifiers will be same.
    /// If parameter hasn't sub-parameters this bit will be unset in response fields.
    pub const SUB: u16 = 1 << 2;

    /// Request the value of parameter
    ///
    /// The requested value will be put after type and sub-parameter ids in reponse.
    pub const GET: u16 = 1 << 3;

    /// Request the name of parameter
    pub const NAME: u16 = 1 << 4;

    /// Request the units of parameter value
    pub const UNIT: u16 = 1 << 5;

    /// Requests the menu item name
    pub const ITEM: u16 = 1 << 6;

    /// Request the parameter description
    pub const INFO: u16 = 1 << 7;

    /// Request the parameter hint
    pub const HINT: u16 = 1 << 8;

    /// Request the default value of parameter
    pub const DEF: u16 = 1 << 9;

    /// Request the minimum value of parameter
    pub const MIN: u16 = 1 << 10;

    /// Request the maximum value of parameter
    pub const MAX: u16 = 1 << 11;

    /// Request the stepping value of parameter
    pub const STP: u16 = 1 << 12;

    /// Request the options or typical values of parameter
    ///
    /// The options is a enumeration of possible parameter values.
    /// Each option besides value also can has text fields.
    pub const OPT: u16 = 1 << 13;

    /// Requests the typical polling interval for parameter value
    pub const POLL: u16 = 1 << 14;
}

/// The type identifier
pub type TypeId = u8;

/// The supported parameter types enumeration
pub mod typeid {
    use super::TypeId;

    /// Unsigned integer number
    pub const UINT: TypeId = 1;

    /// Signed integer number
    pub const SINT: TypeId = 2;

    /// Unsigned fixed-point number
    pub const UFIX: TypeId = 3;

    /// Signed fixed-point number
    pub const SFIX: TypeId = 4;

    /// Signed floating-point number (in IEEE 754 format)
    pub const REAL: TypeId = 5;

    /// Generic text string
    pub const CSTR: TypeId = 6;

    /// Generic binary data
    pub const HBIN: TypeId = 7;

    /// Time value (hour 0..23, minute 0..59, second 0..59)
    pub const TIME: TypeId = 8;

    /// Date value (year -32768..32767, month 1..12, day of month 1..31)
    pub const DATE: TypeId = 9;

    /// Six-byte MAC address
    pub const MAC: TypeId = 10;

    /// IPv4 network address
    pub const IPV4: TypeId = 11;

    /// IPv6 network address
    pub const IPV6: TypeId = 12;

    /// Check type for validity
    #[inline]
    pub fn is(val: TypeId) -> bool {
        val >= UINT && val <= IPV6
    }
}

/// The flags described the parameter value
///
/// Those flags combined with parameter type using bitwise OR.
pub mod flag {
    /// The parameter value can be read
    pub const GETABLE: u8 = 1 << 4;

    /// The parameter value can be written
    pub const SETABLE: u8 = 1 << 5;

    /// The parameter value is persistent
    ///
    /// Persistency means that the value will be stored into non-volatile memory (like FLASH or EEPROM).
    pub const PERSIST: u8 = 1 << 6;

    /// The parameter value should be one of options
    ///
    /// Enumerable parameters should have this flag.
    pub const OPTIONS: u8 = 1 << 7;
}
