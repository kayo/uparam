use core::{
    fmt::{Display, Formatter, Result as FmtResult},
    result::Result as StdResult,
    str::FromStr,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "chrono")]
use chrono::{Datelike, Local};

use crate::{InvalidDatePart, InvalidNumber, ParseDateError, ParseDateTimeError};

/// Date type
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Date {
    /// Full year value `-32768 ..= 32767`
    pub year: i16,
    /// Month value `1 ..= 12`
    pub mon: u8,
    /// Date value `1 ..= 31`
    ///
    /// The day of month
    pub date: u8,
}

impl Date {
    /// Create from parts
    pub fn new(year: i16, mon: u8, date: u8) -> Self {
        Self { year, mon, date }
    }

    #[cfg(feature = "chrono")]
    /// Create from current
    pub fn current() -> Self {
        let d = Local::now();
        Self::new(d.year() as i16, d.month() as u8, d.day() as u8)
    }

    /// Calculate day of week
    pub fn day(&mut self) -> u8 {
        let (y, m, d) = if self.mon < 3 {
            (
                self.year - 1,
                i16::from(self.mon),
                i16::from(self.date) + self.year,
            )
        } else {
            (
                self.year,
                i16::from(self.mon),
                i16::from(self.date) + self.year - 2,
            )
        };

        ((23 * m / 9 + d + 4 + y / 4 - y / 100 + y / 400) % 7) as u8
    }

    /// Check is leap year
    #[inline]
    pub fn leap_year(self) -> bool {
        is_leap_year(self.year)
    }

    /// Calculate days in month
    #[inline]
    pub fn mon_days(self) -> u8 {
        days_in_month(self.year, self.mon)
    }
}

fn is_leap_year(year: i16) -> bool {
    if year % 4 > 0 {
        false
    } else if year % 100 > 0 {
        true
    } else {
        year % 400 <= 0
    }
}

fn days_in_month(year: i16, mon: u8) -> u8 {
    match mon {
        1 => 31,
        2 => {
            if is_leap_year(year) {
                29
            } else {
                28
            }
        }
        3 => 31,
        4 => 30,
        5 => 31,
        6 => 30,
        7 => 31,
        8 => 31,
        9 => 31,
        10 => 31,
        11 => 30,
        12 => 31,
        _ => 0, //unreachable!(),
    }
}

impl FromStr for Date {
    type Err = ParseDateError;

    fn from_str(src: &str) -> StdResult<Self, Self::Err> {
        use self::ParseDateTimeError::*;

        #[cfg(feature = "chrono")]
        {
            if src == "now" {
                return Ok(Self::current());
            }
        }

        let sep_char = src
            .chars()
            .find(|c| *c == '-' || *c == '/' || *c == '.')
            .ok_or(InvalidFormat)?;

        let mut parts = src.split(sep_char);

        let (year, mon, date) = match (parts.next(), parts.next(), parts.next(), parts.next()) {
            (Some(part0), Some(part1), Some(part2), None) => match sep_char {
                // yyyy-mm-dd
                '-' => (part0, part1, part2),
                // mm/dd/yyyy
                '/' => (part1, part2, part0),
                // dd.mm.yyyy
                '.' => (part2, part1, part0),
                _ => unreachable!(),
            },
            (Some(_), Some(_), Some(_), Some(_)) => return Err(TooManyParts(sep_char).into()),
            (Some(_), Some(_), None, _) => return Err(TooLessParts(sep_char).into()),
            (_, _, _, _) => return Err(InvalidFormat.into()),
        };

        let year = year.parse::<i16>().map_err(InvalidDatePart::Year)?;

        let mon = mon
            .parse::<u8>()
            .map_err(InvalidNumber::BadFormat)
            .and_then(|mon| {
                if mon < 1 {
                    Err(InvalidNumber::TooSmall(mon, 1..=12))
                } else if mon > 12 {
                    Err(InvalidNumber::TooBig(mon, 1..=12))
                } else {
                    Ok(mon)
                }
            })
            .map_err(InvalidDatePart::Month)?;

        let mon_days = days_in_month(year, mon);

        let date = date
            .parse::<u8>()
            .map_err(InvalidNumber::BadFormat)
            .and_then(|date| {
                if date < 1 {
                    Err(InvalidNumber::TooSmall(date, 1..=mon_days))
                } else if date > mon_days {
                    Err(InvalidNumber::TooBig(date, 1..=mon_days))
                } else {
                    Ok(date)
                }
            })
            .map_err(InvalidDatePart::Date)?;

        Ok(Self::new(year, mon, date))
    }
}

impl Display for Date {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        write!(f, "{:04}-{:02}-{:02}", self.year, self.mon, self.date)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn leap_year() {
        assert_eq!(is_leap_year(2012), true);
        assert_eq!(is_leap_year(2013), false);
        assert_eq!(is_leap_year(2014), false);
        assert_eq!(is_leap_year(2015), false);
        assert_eq!(is_leap_year(2016), true);
        assert_eq!(is_leap_year(2018), false);
    }

    #[test]
    fn month_days() {
        assert_eq!(days_in_month(2012, 2), 29);
        assert_eq!(days_in_month(2012, 1), 31);
        assert_eq!(days_in_month(2018, 9), 31);
        assert_eq!(days_in_month(2018, 11), 30);
    }

    #[test]
    fn date_parse() {
        assert_eq!(
            "2012-08-29".parse::<Date>().unwrap(),
            Date {
                year: 2012,
                mon: 8,
                date: 29
            }
        );
    }
}
