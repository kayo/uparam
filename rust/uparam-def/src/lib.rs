/*!

# Low level exchange protocol

Parameters-based device configuration and control library.

## Crate features

* __alloc__ Enables _alloc_ crate.
* __std__ Enables _std_ crate.
* __serde__ Enables _Serialize_/_Deserialize_ traits implementations for defined types.
* __chrono__ Enables _chrono_ support for date/time values. Particularly use current date and time values.

## Protocol overview

The binary protocol is dead-simple to understand and implementation.

The protocol build on frames which can be send in requests and received in responses.
Each request frame starts from bitset of fields which determines requested operation and affected data.
Next follows the identifier which points to requested parameter (zero value points to the root parameter).
At the end of request frame can be send the new value to set when setting of parameter is requested.

Each response frame also starts with bitset of fields which determines which data is actually present in response.
Next follows the fields values which can be parsed in different ways according to it kinds (see implementation details).

Because the protocol highly optimized to use with hardware accelerated IO (like simple DMA or advanced IO controllers) on the device side relatively large static buffers might be used to data exchange (1024 bytes and more).
Strongly recommended prevent overflowing of this buffer when sending frames to device.

## Special parameters (NEW)

There are few parameters with special meaning for client.

* Firmware serial number (id: 0xffffffff)
  Unique value which identifies the version of parameters set.
  Client can skip retrieving of full parameters info each time on repeated connections when it already retrieved before.
  But client should check this serial number and retrieve parameters info repeatedly in case when it doesn't match.

* Request buffer length (id: 0xfffffffe, type: UINT)
  The maximum number of bytes per frame which can be parsed by device.
  Strongly recommended prevent overflowing of this buffer when sending frames to device.
  But to reach the maximum throughput it is recommended send as large frames as possible.

 */

#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(feature = "alloc")]
extern crate alloc;

mod consts;
mod date;
mod option;
mod param;
mod proto;
mod result;
mod time;
mod type_;
mod types;
mod value;

pub use self::consts::*;
pub use self::date::*;
pub use self::option::*;
pub use self::param::*;
pub use self::proto::*;
pub use self::result::*;
pub use self::time::*;
pub use self::type_::*;
pub use self::types::*;
pub use self::value::*;

#[cfg(any(feature = "alloc", feature = "std"))]
pub mod alloc_ {
    //! Type aliases with specialization for alloc/std

    use super::{String, Vec};

    /// Binary data type
    pub type Binary = Vec<u8>;

    /// Value type alias
    pub type Val = super::Val<String, Binary>;

    /// Option type alias
    pub type Opt = super::Opt<String, Binary, String>;

    /// List of options
    pub type Opts = Vec<Opt>;

    /// Parameter type alias
    pub type Par = super::Par<String, Binary, String, Opts>;
}
