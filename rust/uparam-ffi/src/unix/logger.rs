use log::Level;

pub fn init(level: Option<Level>) {
    use flexi_logger::Logger;

    let config = if let Some(level) = level {
        Logger::with_str(level.to_string())
    } else {
        Logger::with_env()
    };

    config.start().unwrap();
}
