use log::Level;

pub fn init(level: Option<Level>) {
    use android_logger::{init_once, Config};

    let config = Config::default();
    //.with_tag("uparam");

    let config = if let Some(level) = level {
        config.with_min_level(level)
    } else {
        config
    };

    init_once(config);

    log::info!("Logger initialized");
}
