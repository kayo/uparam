mod common;

#[cfg(all(target_family = "unix", not(target_os = "android")))]
mod unix;

#[cfg(target_os = "android")]
mod android;

pub use common::*;

#[cfg(all(target_family = "unix", not(target_os = "android")))]
pub use unix::*;

#[cfg(target_os = "android")]
pub use android::*;

#[cfg(feature = "extern_executor")]
pub(crate) use extern_executor::spawn;

#[cfg(all(feature = "tokio", not(feature = "extern_executor")))]
pub(crate) use tokio::task::spawn;

#[cfg(all(feature = "async-std", not(feature = "extern_executor")))]
pub(crate) use async_std::task::spawn;
