mod backend;
mod handle;
mod logger;
mod options;
mod param;
mod result;
mod value;

pub use backend::*;
pub use handle::*;
pub use logger::*;
pub use options::*;
pub use param::*;
pub use result::*;
pub use value::*;

pub use std::os::raw::{c_char, c_void};

/// Parameter identifier
pub type ParId = u32;

/// Raw C Userdata type
pub type RawUserData = *mut c_void;

/// C Userdata type
#[repr(transparent)]
pub struct UserData(RawUserData);

unsafe impl Send for UserData {}
unsafe impl Sync for UserData {}

impl From<RawUserData> for UserData {
    fn from(raw: RawUserData) -> Self {
        Self(raw)
    }
}

impl Into<RawUserData> for UserData {
    fn into(self) -> RawUserData {
        self.0
    }
}

impl core::ops::Deref for UserData {
    type Target = RawUserData;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl core::ops::DerefMut for UserData {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
