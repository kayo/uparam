use crate::c_char;

/// Result code
#[repr(i32)]
pub enum ResultCode {
    Success = 0,
    NotFound = 1,
    CantGet = 2,
    CantSet = 3,
    FailedIo = 4,
    FailedQueue = 5,
    Disconnected = 6,
    Inapplicable = 7,
    Unimplemented = 8,

    // Backend errors
    AlreadyRegistered = 10,
    InvalidUrl = 11,
    UnsupportedUrl = 12,
    FailedResolve = 13,
    FailedConnect = 14,

    // Protocol errors
    InvalidFloat = 20,
    InvalidInt = 21,
    InvalidDate = 22,
    InvalidTime = 23,
    InvalidCast = 24,
    InvalidType = 25,
    InvalidUtf8 = 26,
    InvalidHex = 27,
    InvalidMac = 28,
    InvalidIPv4 = 29,
    InvalidIPv6 = 30,
    UnknownType = 31,
    MissingType = 32,
    MissingValue = 33,
    NotEnoughData = 34,
    ExceededData = 35,
    CantGetValue = 36,
    CantPutValue = 37,
    FormatError = 38,
    TooSmallValue = 39,
    TooBigValue = 40,
    MismatchEnumValue = 41,
    TooShortValue = 42,
    TooLongValue = 43,
}

impl Default for ResultCode {
    fn default() -> Self {
        Self::Success
    }
}

impl<T> From<uparam::Result<T>> for ResultCode {
    fn from(result: uparam::Result<T>) -> Self {
        use ResultCode::*;
        match result {
            Ok(_) => Success,
            Err(error) => error.into(),
        }
    }
}

impl From<uparam::Error> for ResultCode {
    fn from(result: uparam::Error) -> Self {
        use uparam::Error;
        use ResultCode::*;
        match result {
            Error::FailedProto(error) => error.into(),
            Error::FailedBackend(error) => error.into(),
            Error::NotFound => NotFound,
            Error::CantGet => CantGet,
            Error::CantSet => CantSet,
            Error::FailedIo(_) => FailedIo,
            Error::FailedQueue => FailedQueue,
            Error::FailedResolve => FailedResolve,
            Error::Disconnected => Disconnected,
        }
    }
}

impl<T> From<unicom::Result<T>> for ResultCode {
    fn from(result: unicom::Result<T>) -> Self {
        use ResultCode::*;
        match result {
            Ok(_) => Success,
            Err(error) => error.into(),
        }
    }
}

impl From<unicom::Error> for ResultCode {
    fn from(error: unicom::Error) -> Self {
        use unicom::Error;
        use ResultCode::*;
        match error {
            Error::AlreadyRegistered(..) => AlreadyRegistered,
            Error::InvalidUrl(..) => InvalidUrl,
            Error::UnsupportedUrl(..) => UnsupportedUrl,
            Error::FailedResolve(..) => FailedResolve,
            Error::FailedConnect(..) => FailedConnect,
        }
    }
}

impl From<uparam::low::Error> for ResultCode {
    fn from(error: uparam::low::Error) -> Self {
        use uparam::low::Error;
        use ResultCode::*;
        match error {
            Error::InvalidFloat(..) => InvalidFloat,
            Error::InvalidInt(..) => InvalidInt,
            Error::InvalidDate(..) => InvalidDate,
            Error::InvalidTime(..) => InvalidTime,
            Error::InvalidCast(..) => InvalidCast,
            Error::InvalidType(..) => InvalidType,
            Error::InvalidUtf8(..) => InvalidUtf8,
            Error::InvalidHex(..) => InvalidHex,
            Error::InvalidMac => InvalidMac,
            Error::InvalidIPv4 => InvalidIPv4,
            Error::InvalidIPv6 => InvalidIPv6,
            Error::UnknownType(..) => UnknownType,
            Error::MissingType => MissingType,
            Error::MissingValue => MissingValue,
            Error::NotEnoughData { .. } => NotEnoughData,
            Error::ExceededData => ExceededData,
            Error::CantGetValue { .. } => CantGetValue,
            Error::CantPutValue { .. } => CantPutValue,
            Error::FormatError { .. } => FormatError,
            Error::TooSmallValue => TooSmallValue,
            Error::TooBigValue => TooBigValue,
            Error::MismatchEnumValue => MismatchEnumValue,
            Error::TooShortValue { .. } => TooShortValue,
            Error::TooLongValue { .. } => TooLongValue,
        }
    }
}

impl From<core::str::Utf8Error> for ResultCode {
    fn from(_error: core::str::Utf8Error) -> Self {
        Self::InvalidUtf8
    }
}

/// Get string representation of error code
#[export_name = r#"uparam_result_string"#]
pub extern "C" fn result_string(code: ResultCode, ptr: *mut *const c_char, len: *mut u32) {
    use ResultCode::*;

    let text = match code {
        Success => "Success\0",
        NotFound => "Not found\0",
        CantGet => "Cannot set\0",
        CantSet => "Cannot set\0",
        FailedIo => "Failed I/O\0",
        FailedQueue => "Failed queue\0",
        Disconnected => "Disconnected\0",
        Inapplicable => "Inapplicable\0",
        Unimplemented => "Unimplemented\0",

        // Backend errors
        AlreadyRegistered => "Backend already registered\0",
        InvalidUrl => "Invalid device URL\0",
        UnsupportedUrl => "Unsupported device URL\0",
        FailedResolve => "Unresolved domain-name\0",
        FailedConnect => "Failed connect\0",

        // Protocol errors
        InvalidFloat => "Invalid float\0",
        InvalidInt => "Invalid integer\0",
        InvalidDate => "Invalid date\0",
        InvalidTime => "Invalid time\0",
        InvalidCast => "Invalid casting\0",
        InvalidType => "Invalid type\0",
        InvalidUtf8 => "Invalid UTF8\0",
        InvalidHex => "Invalid HEX\0",
        InvalidMac => "Invalid MAC\0",
        InvalidIPv4 => "Invalid IPv4\0",
        InvalidIPv6 => "Invalid IPv6\0",
        UnknownType => "Unknown type\0",
        MissingType => "Missing type\0",
        MissingValue => "Missing value\0",
        NotEnoughData => "Not enough data\0",
        ExceededData => "Exceeded data\0",
        CantGetValue => "Cannot get value\0",
        CantPutValue => "Cannot put value\0",
        FormatError => "Format error\0",
        TooSmallValue => "Too small value\0",
        TooBigValue => "Too big value\0",
        MismatchEnumValue => "Mismatch enum value\0",
        TooShortValue => "Too short value\0",
        TooLongValue => "Too long value\0",
    };

    if !ptr.is_null() {
        let ptr = unsafe { &mut *ptr };
        *ptr = text.as_ptr() as _;
    }

    if !len.is_null() {
        let len = unsafe { &mut *len };
        *len = (text.len() - 1) as _;
    }
}
