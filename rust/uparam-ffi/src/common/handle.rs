use crate::{c_char, c_void, spawn, Options, ParId, Param, ResultCode, UserData, Value};
use core::ptr::null_mut;
use futures::{channel::oneshot, pin_mut, select, FutureExt, StreamExt};

/// Connection handle
pub struct Handle;

/// Open client handle using options
#[export_name = r#"uparam_handle_open"#]
pub extern "C" fn handle_open(options: *mut Options, ret_handle: *mut *mut Handle) {
    let options = unsafe { &*(options as *mut uparam::Options) };

    let (handle, mut process) = options.clone().open_process();

    spawn(async move {
        let _ = process.run().await;
    });

    let handle = Box::into_raw(Box::new(handle));

    let ret_handle = unsafe { &mut *ret_handle };

    *ret_handle = handle as _;
}

/// Delete handle
#[export_name = r#"uparam_handle_delete"#]
pub extern "C" fn handle_delete(handle: *mut Handle) {
    let _ = unsafe { Box::from_raw(handle as *mut uparam::Handle) };
}

/// Get device URL from handle
#[export_name = r#"uparam_handle_url"#]
pub extern "C" fn handle_url(
    handle: *const Handle,
    ret_url_ptr: *mut *const c_char,
    ret_url_len: *mut u32,
) {
    let handle = unsafe { &*(handle as *const uparam::Handle) };
    let string = handle.url().as_str();

    if !ret_url_ptr.is_null() {
        let ret_url_ptr = unsafe { &mut *ret_url_ptr };
        *ret_url_ptr = string.as_ptr() as _;
    }

    if !ret_url_len.is_null() {
        let ret_url_len = unsafe { &mut *ret_url_len };
        *ret_url_len = string.len() as _;
    }
}

/// Get connection status from handle
#[export_name = r#"uparam_handle_status"#]
pub extern "C" fn handle_status(handle: *const Handle) -> Status {
    let handle = unsafe { &*(handle as *const uparam::Handle) };
    handle.status().into()
}

/// Status code
#[derive(Clone, Copy)]
#[repr(i32)]
pub enum Status {
    /// Connection is offline
    Offline = 0,
    /// Connection is online
    Online = 1,
    /// Connection is active
    Active = 2,
}

impl From<uparam::Status> for Status {
    fn from(status: uparam::Status) -> Self {
        use self::Status::*;
        use uparam::Status;
        match status {
            Status::Offline => Offline,
            Status::Online => Online,
            Status::Active => Active,
        }
    }
}

/// Event kind
#[derive(Clone, Copy)]
#[repr(i32)]
pub enum EventKind {
    /// Status changed
    Status = 1,
    /// Retrieving parameters definitions
    Retrieving = 2,
    /// Single parameter definition retrieved
    Retrieve = 3,
    /// Parameters definitions completely retrieved
    Retrieved = 4,
    /// Updating parameter values
    Updating = 5,
    /// Single parameter value updated
    Update = 6,
    /// Parameters values completely updated
    Updated = 7,
}

impl<'a> From<&'a uparam::Event> for EventKind {
    fn from(event: &'a uparam::Event) -> Self {
        use self::EventKind::*;
        use uparam::Event;
        match event {
            Event::Status(_) => Status,
            Event::Retrieving => Retrieving,
            Event::Retrieve(_) => Retrieve,
            Event::Retrieved => Retrieved,
            Event::Updating => Updating,
            Event::Update(_, _) => Update,
            Event::Updated => Updated,
        }
    }
}

/// Event object
pub struct Event;

/// Get kind of event
#[export_name = r#"uparam_event_kind"#]
pub extern "C" fn event_kind(event: *mut Event) -> EventKind {
    let event = unsafe { &*(event as *mut uparam::Event) };
    event.into()
}

/// Get status code from event
#[export_name = r#"uparam_event_status"#]
pub extern "C" fn event_status(event: *mut Event, status_ret: *mut Status) -> ResultCode {
    let event = unsafe { &*(event as *mut uparam::Event) };
    match event {
        uparam::Event::Status(status) => {
            let status_ret = unsafe { &mut *status_ret };
            *status_ret = (*status).into();
        }
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// Get parameter identifier from event
#[export_name = r#"uparam_event_param"#]
pub extern "C" fn event_param(event: *mut Event, id_ret: *mut ParId) -> ResultCode {
    let event = unsafe { &*(event as *mut uparam::Event) };
    match event {
        uparam::Event::Retrieve(id) => {
            let id_ret = unsafe { &mut *id_ret };
            *id_ret = *id;
        }
        uparam::Event::Update(id, _) => {
            let id_ret = unsafe { &mut *id_ret };
            *id_ret = *id;
        }
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// Get parameter value from event
#[export_name = r#"uparam_event_value"#]
pub extern "C" fn event_value(event: *mut Event, value_ret: *mut *const Value) -> ResultCode {
    let event = unsafe { &*(event as *mut uparam::Event) };
    match event {
        uparam::Event::Update(_, value) => {
            let value_ret = unsafe { &mut *value_ret };
            *value_ret = value as *const _ as _;
        }
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// Subscription object
pub struct Subscription;

/// Unsubscribe listener
#[export_name = r#"uparam_unsubscribe"#]
pub extern "C" fn unsubscribe(subscription: *mut Subscription) {
    let _ = unsafe { Box::from_raw(subscription as *mut oneshot::Sender<()>) };
}

/// Event handler callback
pub type EventHandler = fn(*const Event, *mut c_void);

/// Listen client events
#[export_name = r#"uparam_handle_on_event"#]
pub extern "C" fn handle_on_event(
    handle: *mut Handle,
    callback: EventHandler,
    userdata: *mut c_void,
) -> *mut Subscription {
    let handle = unsafe { &*(handle as *mut uparam::Handle) }.clone();
    let userdata = UserData::from(userdata);
    let (subscription, keeper) = oneshot::channel::<()>();

    let mut events = handle.on().fuse();
    let mut keeper = keeper.fuse();

    spawn(async move {
        loop {
            select! {
                event = events.next() => if let Some(event) = &event { (callback)(event as *const _ as _, *userdata) },
                _ = keeper => break,
            }
        }
    });

    Box::into_raw(Box::new(subscription)) as *mut Subscription
}

/// Get parameter definition
#[export_name = r#"uparam_handle_get_param"#]
pub extern "C" fn handle_get_param(
    handle: *const Handle,
    id: ParId,
    param_ret: *mut *mut Param,
) -> ResultCode {
    let handle = unsafe { &*(handle as *const uparam::Handle) };
    handle
        .with_def(&id, |entry| {
            let param_ret = unsafe { &mut *param_ret };
            *param_ret = Box::into_raw(Box::new((*entry).clone())) as _;
        })
        .into()
}

/// Get parameter value
#[export_name = r#"uparam_handle_get_value"#]
pub extern "C" fn handle_get_value(
    handle: *const Handle,
    id: ParId,
    value_ret: *mut *mut Value,
) -> ResultCode {
    let handle = unsafe { &*(handle as *const uparam::Handle) };
    handle
        .get_val(&id)
        .map(|val| {
            let value_ret = unsafe { &mut *value_ret };
            *value_ret = Box::into_raw(Box::new(val)) as _;
        })
        .into()
}

/// Value handler callback
pub type GetHandler = fn(ParId, *const Value, *mut c_void);

/// Listen value changes
#[export_name = r#"uparam_handle_on_value"#]
pub extern "C" fn handle_on_value(
    handle: *mut Handle,
    id: ParId,
    callback: GetHandler,
    userdata: *mut c_void,
) -> *mut Subscription {
    let handle = unsafe { &*(handle as *mut uparam::Handle) }.clone();
    let userdata = UserData::from(userdata);
    let (subscription, keeper) = oneshot::channel::<()>();

    let updates = match handle.on_val(&id) {
        Ok(updates) => updates,
        Err(_error) => return null_mut(),
    };
    let mut updates = updates.fuse();
    let mut keeper = keeper.fuse();

    spawn(async move {
        loop {
            select! {
                update = updates.next() => if let Some(value) = &update { (callback)(id, value as *const _ as _, *userdata) },
                _ = keeper => break,
            }
        }
    });

    Box::into_raw(Box::new(subscription)) as *mut Subscription
}

/// Parameter set callback
pub type SetHandler = fn(ResultCode, ParId, *mut Value, *mut c_void);

/// Set parameter value
#[export_name = r#"uparam_handle_set_value"#]
pub extern "C" fn handle_set_value(
    handle: *mut Handle,
    id: ParId,
    value: *const Value,
    callback: SetHandler,
    userdata: *mut c_void,
) {
    let handle = unsafe { &mut *(handle as *mut uparam::Handle) };
    let mut handle = handle.clone();
    let value = unsafe { &*(value as *const uparam::types::Val) };
    let value = value.clone();
    let userdata = UserData::from(userdata);

    spawn(async move {
        let set = handle.set_val(&id, value).fuse();

        pin_mut!(set);

        select! {
            res = set => match res {
                Ok(val) => (callback)(ResultCode::Success, id, Box::into_raw(Box::new(val)) as *const _ as _, *userdata),
                Err(err) => (callback)(err.into(), id, null_mut(), *userdata),
            },
        }
    });
}
