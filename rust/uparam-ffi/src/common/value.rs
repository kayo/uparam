use crate::{c_char, ResultCode};
use core::{ptr::null_mut, slice::from_raw_parts, str::from_utf8};
use uparam::{low, types};

/// Value
pub struct Value;

/// Get kind of value
#[export_name = "uparam_value_kind"]
pub extern "C" fn value_kind(value: *const Value) -> ValKind {
    let value = unsafe { &*(value as *mut types::Val) };
    value.into()
}

/// New integer value
#[export_name = "uparam_value_new_int"]
pub extern "C" fn value_new_int(value: i64) -> *const Value {
    Box::into_raw(Box::new(types::Val::Int(value))) as _
}

/// Get integer value
#[export_name = "uparam_value_get_int"]
pub extern "C" fn value_get_int(value: *const Value, value_ret: *mut i64) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::Int(val) => unsafe {
            *value_ret = *val;
        },
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// New float value
#[export_name = "uparam_value_new_float"]
pub extern "C" fn value_new_float(value: f64) -> *const Value {
    Box::into_raw(Box::new(types::Val::Float(value))) as _
}

/// Get floating-point value
#[export_name = "uparam_value_get_float"]
pub extern "C" fn value_get_float(value: *const Value, value_ret: *mut f64) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::Float(val) => unsafe {
            *value_ret = *val;
        },
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// New string value
#[export_name = "uparam_value_new_string"]
pub extern "C" fn value_new_string(ptr: *const c_char, len: u32) -> *const Value {
    let val = unsafe { from_raw_parts(ptr as _, len as _) };
    match from_utf8(val) {
        Ok(val) => Box::into_raw(Box::new(types::Val::String(val.into()))) as _,
        Err(_) => null_mut(),
    }
}

/// Get string value
#[export_name = "uparam_value_get_string"]
pub extern "C" fn value_get_string(
    value: *const Value,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::String(val) => unsafe {
            *ptr_ret = val.as_ptr() as _;
            *len_ret = val.as_bytes().len() as _;
        },
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// New binary value
#[export_name = "uparam_value_new_binary"]
pub extern "C" fn value_new_binary(ptr: *const u8, len: u32) -> *const Value {
    let val = unsafe { from_raw_parts(ptr as _, len as _) };
    Box::into_raw(Box::new(types::Val::Binary(val.into()))) as _
}

/// Get binary value
#[export_name = "uparam_value_get_binary"]
pub extern "C" fn value_get_binary(
    value: *const Value,
    ptr_ret: *mut *const u8,
    len_ret: *mut u32,
) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::Binary(val) => unsafe {
            *ptr_ret = val.as_ptr();
            *len_ret = val.len() as _;
        },
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// New date value
#[export_name = "uparam_value_new_date"]
pub extern "C" fn value_new_date(value: *const Date) -> *const Value {
    let value = unsafe { &*value };
    Box::into_raw(Box::new(types::Val::Date(value.into()))) as _
}

/// Get date value
#[export_name = "uparam_value_get_date"]
pub extern "C" fn value_get_date(value: *const Value, value_ret: *mut Date) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::Date(val) => {
            let value_ret = unsafe { &mut *value_ret };
            *value_ret = val.into();
        }
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// New time value
#[export_name = "uparam_value_new_time"]
pub extern "C" fn value_new_time(value: *const Time) -> *const Value {
    let value = unsafe { &*value };
    Box::into_raw(Box::new(types::Val::Time(value.into()))) as _
}

/// Get time value
#[export_name = "uparam_value_get_time"]
pub extern "C" fn value_get_time(value: *const Value, value_ret: *mut Time) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::Time(val) => {
            let value_ret = unsafe { &mut *value_ret };
            *value_ret = val.into();
        }
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// New MAC value
#[export_name = "uparam_value_new_mac"]
pub extern "C" fn value_new_mac(value: *const Mac) -> *const Value {
    let value = unsafe { &*value };
    Box::into_raw(Box::new(types::Val::Mac(value.into()))) as _
}

/// Get MAC address value
#[export_name = "uparam_value_get_mac"]
pub extern "C" fn value_get_mac(value: *const Value, value_ret: *mut Mac) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::Mac(val) => {
            let value_ret = unsafe { &mut *value_ret };
            *value_ret = val.into();
        }
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// New IPv4 value
#[export_name = "uparam_value_new_ipv4"]
pub extern "C" fn value_new_ipv4(value: *const Ipv4) -> *const Value {
    let value = unsafe { &*value };
    Box::into_raw(Box::new(types::Val::IPv4(value.into()))) as _
}

/// Get IPv4 address value
#[export_name = "uparam_value_get_ipv4"]
pub extern "C" fn value_get_ipv4(value: *const Value, value_ret: *mut Ipv4) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::IPv4(val) => {
            let value_ret = unsafe { &mut *value_ret };
            *value_ret = val.into();
        }
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// New IPv6 value
#[export_name = "uparam_value_new_ipv6"]
pub extern "C" fn value_new_ipv6(value: *const Ipv6) -> *const Value {
    let value = unsafe { &*value };
    Box::into_raw(Box::new(types::Val::IPv6(value.into()))) as _
}

/// Get IPv6 address value
#[export_name = "uparam_value_get_ipv6"]
pub extern "C" fn value_get_ipv6(value: *const Value, value_ret: *mut Ipv6) -> ResultCode {
    let value = unsafe { &*(value as *mut types::Val) };
    match value {
        low::Val::IPv6(val) => {
            let value_ret = unsafe { &mut *value_ret };
            *value_ret = val.into();
        }
        _ => return ResultCode::Inapplicable,
    }
    ResultCode::Success
}

/// Properly delete value
#[export_name = "uparam_value_delete"]
pub extern "C" fn value_delete(value: *mut Value) {
    let _ = unsafe { Box::from_raw(value) };
}

/// Value kind
#[derive(Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum ValKind {
    Int = 1,
    Float = 2,
    String = 3,
    Binary = 4,
    Date = 5,
    Time = 6,
    Mac = 7,
    IPv4 = 8,
    IPv6 = 9,
}

impl<'a> From<&'a types::Val> for ValKind {
    fn from(val: &'a types::Val) -> Self {
        use low::Val::*;
        match val {
            Int(..) => Self::Int,
            Float(..) => Self::Float,
            String(..) => Self::String,
            Binary(..) => Self::Binary,
            Date(..) => Self::Date,
            Time(..) => Self::Time,
            Mac(..) => Self::Mac,
            IPv4(..) => Self::IPv4,
            IPv6(..) => Self::IPv6,
        }
    }
}

/// Date value
#[derive(Clone, Copy)]
#[repr(C)]
pub struct Date {
    /// Full year value `-32768 ..= 32767`
    pub year: i16,
    /// Month value `1 ..= 12`
    pub mon: u8,
    /// Date value `1 ..= 31`
    ///
    /// The day of month
    pub date: u8,
}

impl<'a> From<&'a low::Date> for Date {
    fn from(date: &'a low::Date) -> Self {
        Self {
            year: date.year,
            mon: date.mon,
            date: date.date,
        }
    }
}

impl<'a> Into<low::Date> for &'a Date {
    fn into(self) -> low::Date {
        low::Date {
            year: self.year,
            mon: self.mon,
            date: self.date,
        }
    }
}

/// Time type
#[derive(Clone, Copy)]
#[repr(C)]
pub struct Time {
    /// Hour value `0 ..= 23`
    pub hour: u8,
    /// Minutes value `0 ..= 59`
    pub min: u8,
    /// Seconds value `0 ..= 59`
    pub sec: u8,
}

impl<'a> From<&'a low::Time> for Time {
    fn from(time: &'a low::Time) -> Self {
        Self {
            hour: time.hour,
            min: time.min,
            sec: time.sec,
        }
    }
}

impl<'a> Into<low::Time> for &'a Time {
    fn into(self) -> low::Time {
        low::Time {
            hour: self.hour,
            min: self.min,
            sec: self.sec,
        }
    }
}

/// Mac address
#[derive(Clone, Copy)]
#[repr(C)]
pub struct Mac {
    pub a: u8,
    pub b: u8,
    pub c: u8,
    pub d: u8,
    pub e: u8,
    pub f: u8,
}

impl<'a> From<&'a [u8; 6]> for Mac {
    fn from(mac: &'a [u8; 6]) -> Self {
        Self {
            a: mac[0],
            b: mac[1],
            c: mac[2],
            d: mac[3],
            e: mac[4],
            f: mac[5],
        }
    }
}

impl<'a> Into<[u8; 6]> for &'a Mac {
    fn into(self) -> [u8; 6] {
        [self.a, self.b, self.c, self.d, self.e, self.f]
    }
}

/// IPv4 address
#[derive(Clone, Copy)]
#[repr(C)]
pub struct Ipv4 {
    pub a: u8,
    pub b: u8,
    pub c: u8,
    pub d: u8,
}

impl<'a> From<&'a [u8; 4]> for Ipv4 {
    fn from(ip: &'a [u8; 4]) -> Self {
        Self {
            a: ip[0],
            b: ip[1],
            c: ip[2],
            d: ip[3],
        }
    }
}

impl<'a> Into<[u8; 4]> for &'a Ipv4 {
    fn into(self) -> [u8; 4] {
        [self.a, self.b, self.c, self.d]
    }
}

/// IPv6 address
#[derive(Clone, Copy)]
#[repr(C)]
pub struct Ipv6 {
    pub a: u16,
    pub b: u16,
    pub c: u16,
    pub d: u16,
    pub e: u16,
    pub f: u16,
    pub g: u16,
    pub h: u16,
}

impl<'a> From<&'a [u16; 8]> for Ipv6 {
    fn from(ip: &'a [u16; 8]) -> Self {
        Self {
            a: ip[0],
            b: ip[1],
            c: ip[2],
            d: ip[3],
            e: ip[4],
            f: ip[5],
            g: ip[6],
            h: ip[7],
        }
    }
}

impl<'a> Into<[u16; 8]> for &'a Ipv6 {
    fn into(self) -> [u16; 8] {
        [
            self.a, self.b, self.c, self.d, self.e, self.f, self.g, self.h,
        ]
    }
}
