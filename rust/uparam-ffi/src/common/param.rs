use crate::{c_char, ResultCode, Value};
use core::mem::transmute;
use uparam::types;

/// Type of parameter
#[derive(Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Type {
    /// Unsigned integer number
    UInt = 1,
    /// Signed integer number
    SInt = 2,
    /// Unsigned fixed-point number
    UFix = 3,
    /// Signed fixed-point number
    SFix = 4,
    /// Floating-point number
    Real = 5,
    /// String (ASCII/UTF-8) of variable length
    CStr = 6,
    /// Binary data of fixed length
    HBin = 7,
    /// Real time (hours/minutes/seconds)
    Time = 8,
    /// Real date (year/month/date)
    Date = 9,
    /// Interface MAC
    Mac = 10,
    /// IPv4 address
    IPv4 = 11,
    /// IPv6 address
    IPv6 = 12,
}

impl From<types::Type> for Type {
    fn from(type_: types::Type) -> Self {
        unsafe { transmute(type_) }
    }
}

/// Get type name
#[export_name = "uparam_type_string"]
pub extern "C" fn type_string(type_: Type, ptr: *mut *const c_char, len: *mut u32) {
    use Type::*;

    let text = match type_ {
        UInt => "uint\0",
        SInt => "sint\0",
        UFix => "ufix\0",
        SFix => "sfix\0",
        Real => "real\0",
        CStr => "cstr\0",
        HBin => "hbin\0",
        Time => "time\0",
        Date => "date\0",
        Mac => "mac\0",
        IPv4 => "ipv4\0",
        IPv6 => "ipv6\0",
    };

    if !ptr.is_null() {
        let ptr = unsafe { &mut *ptr };
        *ptr = text.as_ptr() as _;
    }

    if !len.is_null() {
        let len = unsafe { &mut *len };
        *len = (text.len() - 1) as _;
    }
}

/// Parameter definition
pub struct Param;

/// Option of parameter
pub struct OptVal;

/// Properly delete of parameter definition
#[export_name = "uparam_param_delete"]
pub extern "C" fn param_delete(param: *mut Param) {
    let _ = unsafe { Box::from_raw(param as *mut types::Par) };
}

/// Get type of parameter
#[export_name = "uparam_param_id"]
pub extern "C" fn param_id(param: *const Param) -> types::ParId {
    let param = unsafe { &*(param as *const types::Par) };
    param.id
}

/// Get type of parameter
#[export_name = "uparam_param_type"]
pub extern "C" fn param_type(param: *const Param, type_ret: *mut Type) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(type_) = param.type_ {
        let type_ret = unsafe { &mut *type_ret };
        *type_ret = type_.into();
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get size of parameter
#[export_name = "uparam_param_size"]
pub extern "C" fn param_size(param: *const Param, size_ret: *mut u32) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(size) = param.size {
        let size_ret = unsafe { &mut *size_ret };
        *size_ret = size as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get fraction of parameter
#[export_name = "uparam_param_frac"]
pub extern "C" fn param_frac(param: *const Param, frac_ret: *mut u8) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(frac) = param.frac {
        let frac_ret = unsafe { &mut *frac_ret };
        *frac_ret = frac.into();
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get subparameters range
#[export_name = "uparam_param_sub"]
pub extern "C" fn param_sub(
    param: *const Param,
    fst_id_ret: *mut types::ParId,
    lst_id_ret: *mut types::ParId,
) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(sub) = param.sub {
        let fst_id_ret = unsafe { &mut *fst_id_ret };
        let lst_id_ret = unsafe { &mut *lst_id_ret };
        *fst_id_ret = sub.0;
        *lst_id_ret = sub.1;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get value of parameter
#[export_name = "uparam_param_val"]
pub extern "C" fn param_val(param: *const Param, val_ret: *mut *const Value) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(val) = &param.val {
        let val_ret = unsafe { &mut *val_ret };
        *val_ret = val as *const _ as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get name of parameter
#[export_name = "uparam_param_name"]
pub extern "C" fn param_name(
    param: *const Param,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(name) = &param.name {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = name.as_ptr() as _;
        *len_ret = name.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get unit of parameter
#[export_name = "uparam_param_unit"]
pub extern "C" fn param_unit(
    param: *const Param,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(unit) = &param.unit {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = unit.as_ptr() as _;
        *len_ret = unit.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get item of parameter
#[export_name = "uparam_param_item"]
pub extern "C" fn param_item(
    param: *const Param,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(item) = &param.item {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = item.as_ptr() as _;
        *len_ret = item.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get info of parameter
#[export_name = "uparam_param_info"]
pub extern "C" fn param_info(
    param: *const Param,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(info) = &param.info {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = info.as_ptr() as _;
        *len_ret = info.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get hint of parameter
#[export_name = "uparam_param_hint"]
pub extern "C" fn param_hint(
    param: *const Param,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(hint) = &param.hint {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = hint.as_ptr() as _;
        *len_ret = hint.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get getable flag of parameter
#[export_name = "uparam_param_getable"]
pub extern "C" fn param_getable(param: *const Param) -> bool {
    let param = unsafe { &*(param as *const types::Par) };
    param.getable
}

/// Get setable flag of parameter
#[export_name = "uparam_param_setable"]
pub extern "C" fn param_setable(param: *const Param) -> bool {
    let param = unsafe { &*(param as *const types::Par) };
    param.setable
}

/// Get persist flag of parameter
#[export_name = "uparam_param_persist"]
pub extern "C" fn param_persist(param: *const Param) -> bool {
    let param = unsafe { &*(param as *const types::Par) };
    param.persist
}

/// Get options flag of parameter
#[export_name = "uparam_param_options"]
pub extern "C" fn param_options(param: *const Param) -> bool {
    let param = unsafe { &*(param as *const types::Par) };
    param.options
}

/// Get default value of parameter
#[export_name = "uparam_param_def"]
pub extern "C" fn param_def(param: *const Param, def_ret: *mut *const Value) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(def) = &param.def {
        let def_ret = unsafe { &mut *def_ret };
        *def_ret = def as *const _ as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get minimum value of parameter
#[export_name = "uparam_param_min"]
pub extern "C" fn param_min(param: *const Param, min_ret: *mut *const Value) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(min) = &param.min {
        let min_ret = unsafe { &mut *min_ret };
        *min_ret = min as *const _ as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get maximum value of parameter
#[export_name = "uparam_param_max"]
pub extern "C" fn param_max(param: *const Param, max_ret: *mut *const Value) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(max) = &param.max {
        let max_ret = unsafe { &mut *max_ret };
        *max_ret = max as *const _ as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get stepping value of parameter
#[export_name = "uparam_param_step"]
pub extern "C" fn param_step(param: *const Param, stp_ret: *mut *const Value) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(stp) = &param.stp {
        let stp_ret = unsafe { &mut *stp_ret };
        *stp_ret = stp as *const _ as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get number of optional values of parameter
#[export_name = "uparam_param_option_len"]
pub extern "C" fn param_option_len(param: *const Param) -> u32 {
    let param = unsafe { &*(param as *const types::Par) };
    param.opt.len() as _
}

/// Get optional value of parameter
#[export_name = "uparam_param_option"]
pub extern "C" fn param_option(
    param: *const Param,
    index: u32,
    opt_ret: *mut *const OptVal,
) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(opt) = param.opt.get(index as usize) {
        let opt_ret = unsafe { &mut *opt_ret };
        *opt_ret = opt as *const _ as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get parameter polling time
#[export_name = "uparam_param_poll"]
pub extern "C" fn param_poll(param: *const Param, poll_ret: *mut u32) -> ResultCode {
    let param = unsafe { &*(param as *const types::Par) };
    if let Some(poll) = param.poll {
        let poll_ret = unsafe { &mut *poll_ret };
        *poll_ret = poll.into();
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get stepping value of parameter
#[export_name = "uparam_option_val"]
pub extern "C" fn option_val(option: *const OptVal) -> *const Value {
    let option = unsafe { &*(option as *const types::Opt) };
    &option.val as *const _ as _
}

/// Get name of option
#[export_name = "uparam_option_name"]
pub extern "C" fn option_name(
    option: *const OptVal,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let option = unsafe { &*(option as *const types::Opt) };
    if let Some(name) = &option.name {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = name.as_ptr() as _;
        *len_ret = name.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get unit of option
#[export_name = "uparam_option_unit"]
pub extern "C" fn option_unit(
    option: *const OptVal,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let option = unsafe { &*(option as *const types::Opt) };
    if let Some(unit) = &option.unit {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = unit.as_ptr() as _;
        *len_ret = unit.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get item of option
#[export_name = "uparam_option_item"]
pub extern "C" fn option_item(
    option: *const OptVal,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let option = unsafe { &*(option as *const types::Opt) };
    if let Some(item) = &option.item {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = item.as_ptr() as _;
        *len_ret = item.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get info of option
#[export_name = "uparam_option_info"]
pub extern "C" fn option_info(
    option: *const OptVal,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let option = unsafe { &*(option as *const types::Opt) };
    if let Some(info) = &option.info {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = info.as_ptr() as _;
        *len_ret = info.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}

/// Get hint of option
#[export_name = "uparam_option_hint"]
pub extern "C" fn option_hint(
    option: *const OptVal,
    ptr_ret: *mut *const c_char,
    len_ret: *mut u32,
) -> ResultCode {
    let option = unsafe { &*(option as *const types::Opt) };
    if let Some(hint) = &option.hint {
        let ptr_ret = unsafe { &mut *ptr_ret };
        let len_ret = unsafe { &mut *len_ret };
        *ptr_ret = hint.as_ptr() as _;
        *len_ret = hint.as_bytes().len() as _;
        ResultCode::Success
    } else {
        ResultCode::NotFound
    }
}
