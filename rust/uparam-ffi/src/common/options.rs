use crate::{c_char, Manager, ResultCode};
use std::{slice::from_raw_parts, str::from_utf8, time::Duration};

/// Connection options
pub struct Options;

/// Create options using backend managed and device URL
#[no_mangle]
#[export_name = r#"uparam_options_from_url"#]
pub extern "C" fn options_from_url(
    backends: *mut Manager,
    url_ptr: *const c_char,
    url_len: u32,
    ret_options: *mut *mut Options,
) -> ResultCode {
    let backends = unsafe { &*(backends as *mut uparam::Manager) };

    let url = match from_utf8(unsafe { from_raw_parts(url_ptr as _, url_len as _) }) {
        Ok(url) => url,
        Err(err) => return err.into(),
    };

    let options = match backends.from_url(url) {
        Ok(options) => options,
        Err(error) => return error.into(),
    };

    if !ret_options.is_null() {
        let options = Box::into_raw(Box::new(options)) as *mut _;

        unsafe { *ret_options = options };
    }

    ResultCode::default()
}

/// Delete options
#[export_name = r#"uparam_options_delete"#]
pub extern "C" fn options_delete(options: *mut Options) {
    let _ = unsafe { Box::from_raw(options as *mut uparam::Options) };
}

/// Reconnect strategy
pub struct ReconnectStrategy;

/// Set reconnect strategy
#[export_name = r#"uparam_options_set_reconnect_strategy"#]
pub extern "C" fn options_set_reconnect_strategy(
    options: *mut Options,
    reconnect_strategy: *mut ReconnectStrategy,
) {
    let options = unsafe { &mut *(options as *mut uparam::Options) };
    let reconnect_strategy: Box<uparam::BoxedReconnectStrategy> =
        unsafe { Box::from_raw(reconnect_strategy as *mut uparam::BoxedReconnectStrategy) };
    options.set_reconnect_strategy(*reconnect_strategy);
}

/// Set minimum polling interval (seconds)
#[export_name = r#"uparam_options_set_min_poll_interval"#]
pub extern "C" fn options_set_min_poll_interval(options: *mut Options, poll_interval: f32) {
    let options = unsafe { &mut *(options as *mut uparam::Options) };
    let poll_interval = Duration::from_secs_f32(poll_interval);
    options.set_min_poll_interval(poll_interval);
}

/// Set maximum polling interval (seconds)
#[export_name = r#"uparam_options_set_max_poll_interval"#]
pub extern "C" fn options_set_max_poll_interval(options: *mut Options, poll_interval: f32) {
    let options = unsafe { &mut *(options as *mut uparam::Options) };
    let poll_interval = Duration::from_secs_f32(poll_interval);
    options.set_max_poll_interval(poll_interval);
}

/// Set requests limit
#[export_name = r#"uparam_options_set_request_limit"#]
pub extern "C" fn options_set_request_limit(options: *mut Options, request_limit: u32) {
    let options = unsafe { &mut *(options as *mut uparam::Options) };
    options.set_request_limit(request_limit as _);
}

/// Set request timeout (seconds)
#[export_name = r#"uparam_options_set_request_timeout"#]
pub extern "C" fn options_set_request_timeout(options: *mut Options, request_timeout: f32) {
    let options = unsafe { &mut *(options as *mut uparam::Options) };
    let request_timeout = Duration::from_secs_f32(request_timeout);
    options.set_request_timeout(request_timeout);
}

/// Set requests attempts before reconnect
#[export_name = r#"uparam_options_set_request_attempts"#]
pub extern "C" fn options_set_request_attempts(options: *mut Options, request_attempts: u32) {
    let options = unsafe { &mut *(options as *mut uparam::Options) };
    options.set_request_attempts(request_attempts as _);
}

/// Create no-reconnect strategy
#[export_name = r#"uparam_no_reconnect"#]
pub extern "C" fn no_reconnect_new() -> *mut ReconnectStrategy {
    let reconnect_strategy = Box::new(uparam::reconnect::No::default());
    Box::into_raw(Box::new(
        reconnect_strategy as Box<dyn uparam::ReconnectStrategy>,
    )) as *mut _
}

/// Create immediate-reconnect strategy
#[export_name = r#"uparam_immediate_reconnect"#]
pub extern "C" fn immediate_reconnect_new() -> *mut ReconnectStrategy {
    let reconnect_strategy = Box::new(uparam::reconnect::Immediate::default());
    Box::into_raw(Box::new(
        reconnect_strategy as Box<dyn uparam::ReconnectStrategy>,
    )) as *mut _
}

/// Create delayed-reconnect strategy
#[export_name = r#"uparam_delayed_reconnect"#]
pub extern "C" fn delayed_reconnect_new() -> *mut ReconnectStrategy {
    let reconnect_strategy = Box::new(uparam::reconnect::Delayed::default());
    Box::into_raw(Box::new(
        reconnect_strategy as Box<dyn uparam::ReconnectStrategy>,
    )) as *mut _
}

/// Create progressive-reconnect strategy
#[export_name = r#"uparam_progressive_reconnect"#]
pub extern "C" fn progressive_reconnect_new() -> *mut ReconnectStrategy {
    let reconnect_strategy = Box::new(uparam::reconnect::Progressive::default());
    Box::into_raw(Box::new(
        reconnect_strategy as Box<dyn uparam::ReconnectStrategy>,
    )) as *mut _
}
