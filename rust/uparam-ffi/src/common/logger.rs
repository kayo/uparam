use crate::ResultCode;

/// Logging level specifier
#[repr(i32)]
pub enum LogLevel {
    None = 0,
    Error = 1,
    Warn = 2,
    Info = 3,
    Debug = 4,
    Trace = 5,
}

/// Initialize platform specific logger
#[export_name = r#"uparam_logger_init"#]
pub extern "C" fn logger_init(level: LogLevel) -> ResultCode {
    #[cfg(feature = "logger")]
    {
        use crate::logger;
        use log::Level::*;

        logger::init(match level {
            LogLevel::None => None,
            LogLevel::Error => Some(Error),
            LogLevel::Warn => Some(Warn),
            LogLevel::Info => Some(Info),
            LogLevel::Debug => Some(Debug),
            LogLevel::Trace => Some(Trace),
        });

        ResultCode::default()
    }

    #[cfg(not(feature = "logger"))]
    ResultCode::Unimplemened
}
