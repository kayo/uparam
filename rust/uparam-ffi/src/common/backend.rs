use crate::ResultCode;

/// Backends manager
pub struct Manager;

/// Backend type
pub struct Backend;

/// Create backends manager
#[export_name = r#"uparam_manager_new"#]
pub extern "C" fn manager_new() -> *mut Manager {
    Box::into_raw(Box::new(uparam::Manager::default())) as *mut _
}

/// Delete backends manager
#[export_name = r#"uparam_manager_delete"#]
pub extern "C" fn manager_delete(manager: *mut Manager) {
    let _ = unsafe { Box::from_raw(manager as *mut uparam::Manager) };
}

/// Register backend
#[export_name = r#"uparam_manager_register"#]
pub extern "C" fn manager_register(manager: *mut Manager, backend: *mut Backend) -> ResultCode {
    let manager = unsafe { &*(manager as *mut uparam::Manager) };
    let backend: Box<unicom::BoxedBackend> =
        unsafe { Box::from_raw(backend as *mut unicom::BoxedBackend) };

    manager.register(*backend).into()
}

/// Create tcp-socket backend type
#[cfg(feature = "tcp-socket")]
#[export_name = r#"uparam_backend_tcp_socket_new"#]
pub extern "C" fn backend_tcp_socket_new() -> *mut Backend {
    let backend = Box::new(unicom_tcp::TcpSocket::new(
        unicom_nres::DefaultResolver::default(),
    ));
    Box::into_raw(Box::new(backend as unicom::BoxedBackend)) as *mut _
}

/// Create unix-socket backend type
#[cfg(feature = "unix-socket")]
#[export_name = r#"uparam_backend_unix_socket_new"#]
pub extern "C" fn backend_unix_socket_new() -> *mut Backend {
    let backend = Box::new(unicom_unix::UnixSocket::default());
    Box::into_raw(Box::new(backend as unicom::BoxedBackend)) as *mut _
}

/// Create serial-port backend type
#[cfg(feature = "serial-port")]
#[export_name = r#"uparam_backend_serial_port_new"#]
pub extern "C" fn backend_serial_port_new() -> *mut Backend {
    let backend = Box::new(unicom_serial::SerialPort::default());
    Box::into_raw(Box::new(backend as unicom::BoxedBackend)) as *mut _
}
