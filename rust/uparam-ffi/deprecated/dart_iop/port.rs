use std::{
    ptr::null,
    mem::transmute,
};
pub use dart_rs::{
    dart_cobject::{CObject},
};
use crate::{c_void};

/// Dart port id
pub type DartPort = i64;

/// Dart c object
pub struct DartCObject;

/// Dart post c object to port
pub type DartPostCObject = fn(DartPort, *mut DartCObject) -> bool;

static mut POST_COBJECT: *const c_void = null();

pub fn dart_post_cobject(port_id: DartPort, message: CObject) -> bool {
    let post_cobject: DartPostCObject = unsafe { transmute(POST_COBJECT) };
    let mut message = message.into_leak();
    (post_cobject)(port_id, &mut message as *mut _ as *mut _)
}

/// Export Dart API calls
///
/// Dart API calls with must be exported by vm
#[no_mangle]
pub extern fn dart_init(
    post_cobject: DartPostCObject,
) {
    unsafe {
        POST_COBJECT = post_cobject as _;
    }
}
