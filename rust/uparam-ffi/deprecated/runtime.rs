use std::{
    sync::Arc,
    task::{Context, Poll},
};
use futures::future::BoxFuture;

#[cfg(not(feature = "spin"))]
use std::sync::{Mutex, MutexGuard};

#[cfg(feature = "spin")]
use spin::{Mutex, MutexGuard};

#[cfg(not(feature = "woke"))]
use futures::task::{ArcWake as Wake, waker_ref};

#[cfg(feature = "woke")]
use woke::{Woke as Wake, waker_ref};

use crate::{UserData, UserDataRaw};

/// Create task for polling specified future by external event loop
pub fn task_new_raw<T>(future: BoxFuture<'static, T>, wake: WakeFnRaw, data: UserDataRaw) -> (PollFnRaw, DropFnRaw, UserDataRaw) {
    let data = UserData::from(data);
    let notify = || { wake(*data); };

    let task = Arc::new(Task::new(future, notify));

    PollerFn::from(task.poller()).into()
}

fn mutex_lock<T>(mutex: &Mutex<T>) -> MutexGuard<T> {
    #[cfg(not(feature = "spin"))]
    {
        mutex.lock().unwrap()
    }

    #[cfg(feature = "spin")]
    {
        mutex.lock()
    }
}

struct Task<T, U> {
    future: Mutex<BoxFuture<'static, T>>,
    notify: Mutex<U>,
}

impl<T, U> Wake for Task<T, U>
where
    U: FnMut() + Send,
{
    fn wake_by_ref(arc_self: &Arc<Self>) {
        let notify = &mut *mutex_lock(&arc_self.notify);
        notify();
    }
}

impl<T, U> Task<T, U>
where
    U: FnMut() + Send,
{
    pub fn new(future: BoxFuture<'static, T>, notify: U) -> Self {
        let future = Mutex::new(future);
        let notify = Mutex::new(notify);
        Self { future, notify }
    }

    pub fn poll(self: &Arc<Self>) -> bool {
        let mut future = mutex_lock(&self.future);
        let waker = waker_ref(&self);
        let context = &mut Context::from_waker(&*waker);

        if let Poll::Pending = future.as_mut().poll(context) {
            true
        } else {
            false
        }
    }

    pub fn poller(self: &Arc<Self>) -> impl FnMut() -> bool {
        let cloned_self = self.clone();
        move || cloned_self.poll()
    }
}

#[repr(transparent)]
pub struct PollerFn<U> {
    func: U,
}

impl<U> From<U> for PollerFn<U>
where
    U: FnMut() -> bool,
{
    fn from(func: U) -> Self {
        Self { func }
    }
}

/// Raw C wake function
///
/// This function will be called when pending future need to be polled again
pub type WakeFnRaw = fn(UserDataRaw);

/// Raw C poll function
///
/// This function must be called to poll future on each wake event
pub type PollFnRaw = fn(UserDataRaw) -> bool;

/// Raw C drop function
///
/// This function must be called to cleanup either pending or completed future
pub type DropFnRaw = fn(UserDataRaw);

impl<U> Into<(PollFnRaw, DropFnRaw, UserDataRaw)> for PollerFn<U>
where
    U: FnMut() -> bool,
{
    fn into(self) -> (PollFnRaw, DropFnRaw, UserDataRaw) {
        let poller = Box::into_raw(Box::new(self.func));
        (Self::poll_fn, Self::drop_fn, poller as UserDataRaw)
    }
}

impl<U> PollerFn<U>
where
    U: FnMut() -> bool,
{
    fn poll_fn(data: UserDataRaw) -> bool {
        let poller = unsafe { &mut *(data as *mut U) };
        poller()
    }

    fn drop_fn(data: UserDataRaw) {
        let _ = unsafe { Box::from_raw(data as *mut U) };
    }
}
