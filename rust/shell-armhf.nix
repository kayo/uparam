let pkgs = import <nixpkgs> {
    crossSystem = {
        config = "armv7l-unknown-linux-gnueabihf";
    };
};
    hostPkgs = import <nixpkgs> {};
in pkgs.callPackage ({mkShell, pkgconfig, libudev}: mkShell {
    nativeBuildInputs = [ pkgconfig hostPkgs.gcc ];
    buildInputs = [ libudev ];
    CC_armv7_unknown_linux_gnueabihf = "armv7l-unknown-linux-gnueabihf-gcc";
    CARGO_TARGET_armv7_unknown_linux_gnueabihf_LINKER = "armv7l-unknown-linux-gnueabihf-gcc";
}) {}
