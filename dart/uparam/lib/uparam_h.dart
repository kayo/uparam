/* This file was generated using c4dart v0.1.0 tool and should not be modified manually. */
import 'dart:ffi';

/**
 * Backend type
 */
class Backend extends Struct {}
/**
 * Reconnect strategy
 */
class ReconnectStrategy extends Struct {}
/**
 * Event object
 */
class Event extends Struct {}
/**
 * Value
 */
class Value extends Struct {}
/**
 * Connection handle
 */
class Handle extends Struct {}
/**
 * Parameter definition
 */
class Param extends Struct {}
/**
 * Subscription object
 */
class Subscription extends Struct {}
/**
 * Connection options
 */
class Options extends Struct {}
/**
 * Backends manager
 */
class Manager extends Struct {}
/**
 * Option of parameter
 */
class OptVal extends Struct {}
class Date extends Struct {
    /**
     * Full year value `-32768 ..= 32767`
     */
    @Int16() int year;
    /**
     * Month value `1 ..= 12`
     */
    @Uint8() int mon;
    /**
     * Date value `1 ..= 31`
     *
     * The day of month
     */
    @Uint8() int date;
}
class Ipv4 extends Struct {
    @Uint8() int a;
    @Uint8() int b;
    @Uint8() int c;
    @Uint8() int d;
}
class Ipv6 extends Struct {
    @Uint16() int a;
    @Uint16() int b;
    @Uint16() int c;
    @Uint16() int d;
    @Uint16() int e;
    @Uint16() int f;
    @Uint16() int g;
    @Uint16() int h;
}
class Mac extends Struct {
    @Uint8() int a;
    @Uint8() int b;
    @Uint8() int c;
    @Uint8() int d;
    @Uint8() int e;
    @Uint8() int f;
}
class Time extends Struct {
    /**
     * Hour value `0 ..= 23`
     */
    @Uint8() int hour;
    /**
     * Minutes value `0 ..= 59`
     */
    @Uint8() int min;
    /**
     * Seconds value `0 ..= 59`
     */
    @Uint8() int sec;
}
/**
 * Event kind
 */
class EventKind {
    static const Status = 1;
    static const Retrieving = 2;
    static const Retrieve = 3;
    static const Retrieved = 4;
    static const Updating = 5;
    static const Update = 6;
    static const Updated = 7;
}
/**
 * Logging level specifier
 */
class LogLevel {
    static const None = 0;
    static const Error = 1;
    static const Warn = 2;
    static const Info = 3;
    static const Debug = 4;
    static const Trace = 5;
}
/**
 * Result code
 */
class ResultCode {
    static const Success = 0;
    static const NotFound = 1;
    static const CantGet = 2;
    static const CantSet = 3;
    static const FailedIo = 4;
    static const FailedQueue = 5;
    static const Disconnected = 6;
    static const Inapplicable = 7;
    static const Unimplemented = 8;
    static const AlreadyRegistered = 10;
    static const InvalidUrl = 11;
    static const UnsupportedUrl = 12;
    static const FailedResolve = 13;
    static const FailedConnect = 14;
    static const InvalidFloat = 20;
    static const InvalidInt = 21;
    static const InvalidDate = 22;
    static const InvalidTime = 23;
    static const InvalidCast = 24;
    static const InvalidType = 25;
    static const InvalidUtf8 = 26;
    static const InvalidHex = 27;
    static const InvalidMac = 28;
    static const InvalidIPv4 = 29;
    static const InvalidIPv6 = 30;
    static const UnknownType = 31;
    static const MissingType = 32;
    static const MissingValue = 33;
    static const NotEnoughData = 34;
    static const ExceededData = 35;
    static const CantGetValue = 36;
    static const CantPutValue = 37;
    static const FormatError = 38;
    static const TooSmallValue = 39;
    static const TooBigValue = 40;
    static const MismatchEnumValue = 41;
    static const TooShortValue = 42;
    static const TooLongValue = 43;
}
/**
 * Status code
 */
class Status {
    static const Offline = 0;
    static const Online = 1;
    static const Active = 2;
}
/**
 * Type of parameter
 */
class Type {
    static const UInt = 1;
    static const SInt = 2;
    static const UFix = 3;
    static const SFix = 4;
    static const Real = 5;
    static const CStr = 6;
    static const HBin = 7;
    static const Time = 8;
    static const Date = 9;
    static const Mac = 10;
    static const IPv4 = 11;
    static const IPv6 = 12;
}
/**
 * Value kind
 */
class ValKind {
    static const Int = 1;
    static const Float = 2;
    static const String = 3;
    static const Binary = 4;
    static const Date = 5;
    static const Time = 6;
    static const Mac = 7;
    static const IPv4 = 8;
    static const IPv6 = 9;
}
/*Library class
 */
class UParam {
    /*Callbacks
     */
    final Pointer<NativeFunction<Void Function(Pointer<Event>, Pointer<Void>)>> handle_on_event_callback;
    final Pointer<NativeFunction<Void Function(Uint32, Pointer<Value>, Pointer<Void>)>> handle_on_value_callback;
    final Pointer<NativeFunction<Void Function(Int32, Uint32, Pointer<Value>, Pointer<Void>)>> handle_set_value_callback;
    /*Functions
     */
    /**
     * Create tcp-socket backend type
     */
    final Pointer<Backend> Function() backend_tcp_socket_new;
    /**
     * Create unix-socket backend type
     */
    final Pointer<Backend> Function() backend_unix_socket_new;
    /**
     * Create delayed-reconnect strategy
     */
    final Pointer<ReconnectStrategy> Function() delayed_reconnect;
    /**
     * Get kind of event
     */
    final int Function(Pointer<Event> event) event_kind;
    /**
     * Get parameter identifier from event
     */
    final int Function(Pointer<Event> event, Pointer<Uint32> id_ret) event_param;
    /**
     * Get status code from event
     */
    final int Function(Pointer<Event> event, Pointer<Int32> status_ret) event_status;
    /**
     * Get parameter value from event
     */
    final int Function(Pointer<Event> event, Pointer<Pointer<Value>> value_ret) event_value;
    /**
     * Delete handle
     */
    final void Function(Pointer<Handle> handle) handle_delete;
    /**
     * Get parameter definition
     */
    final int Function(Pointer<Handle> handle, int id, Pointer<Pointer<Param>> param_ret) handle_get_param;
    /**
     * Get parameter value
     */
    final int Function(Pointer<Handle> handle, int id, Pointer<Pointer<Value>> value_ret) handle_get_value;
    /**
     * Listen client events
     */
    final Pointer<Subscription> Function(Pointer<Handle> handle, Pointer<NativeFunction<Void Function(Pointer<Event>, Pointer<Void>)>> callback, Pointer<Void> userdata) handle_on_event;
    /**
     * Listen value changes
     */
    final Pointer<Subscription> Function(Pointer<Handle> handle, int id, Pointer<NativeFunction<Void Function(Uint32, Pointer<Value>, Pointer<Void>)>> callback, Pointer<Void> userdata) handle_on_value;
    /**
     * Open client handle using options
     */
    final void Function(Pointer<Options> options, Pointer<Pointer<Handle>> ret_handle) handle_open;
    /**
     * Set parameter value
     */
    final void Function(Pointer<Handle> handle, int id, Pointer<Value> value, Pointer<NativeFunction<Void Function(Int32, Uint32, Pointer<Value>, Pointer<Void>)>> callback, Pointer<Void> userdata) handle_set_value;
    /**
     * Get connection status from handle
     */
    final int Function(Pointer<Handle> handle) handle_status;
    /**
     * Get device URL from handle
     */
    final void Function(Pointer<Handle> handle, Pointer<Pointer<Int8>> ret_url_ptr, Pointer<Uint32> ret_url_len) handle_url;
    /**
     * Create immediate-reconnect strategy
     */
    final Pointer<ReconnectStrategy> Function() immediate_reconnect;
    /**
     * Initialize platform specific logger
     */
    final int Function(int level) logger_init;
    /**
     * Delete backends manager
     */
    final void Function(Pointer<Manager> manager) manager_delete;
    /**
     * Create backends manager
     */
    final Pointer<Manager> Function() manager_new;
    /**
     * Register backend
     */
    final int Function(Pointer<Manager> manager, Pointer<Backend> backend) manager_register;
    /**
     * Create no-reconnect strategy
     */
    final Pointer<ReconnectStrategy> Function() no_reconnect;
    /**
     * Get hint of option
     */
    final int Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) option_hint;
    /**
     * Get info of option
     */
    final int Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) option_info;
    /**
     * Get item of option
     */
    final int Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) option_item;
    /**
     * Get name of option
     */
    final int Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) option_name;
    /**
     * Get unit of option
     */
    final int Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) option_unit;
    /**
     * Get stepping value of parameter
     */
    final Pointer<Value> Function(Pointer<OptVal> option) option_val;
    /**
     * Delete options
     */
    final void Function(Pointer<Options> options) options_delete;
    /**
     * Create options using backend managed and device URL
     */
    final int Function(Pointer<Manager> backends, Pointer<Int8> url_ptr, int url_len, Pointer<Pointer<Options>> ret_options) options_from_url;
    /**
     * Set maximum polling interval (seconds)
     */
    final void Function(Pointer<Options> options, double poll_interval) options_set_max_poll_interval;
    /**
     * Set minimum polling interval (seconds)
     */
    final void Function(Pointer<Options> options, double poll_interval) options_set_min_poll_interval;
    /**
     * Set reconnect strategy
     */
    final void Function(Pointer<Options> options, Pointer<ReconnectStrategy> reconnect_strategy) options_set_reconnect_strategy;
    /**
     * Set requests attempts before reconnect
     */
    final void Function(Pointer<Options> options, int request_attempts) options_set_request_attempts;
    /**
     * Set requests limit
     */
    final void Function(Pointer<Options> options, int request_limit) options_set_request_limit;
    /**
     * Set request timeout (seconds)
     */
    final void Function(Pointer<Options> options, double request_timeout) options_set_request_timeout;
    /**
     * Get default value of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Value>> def_ret) param_def;
    /**
     * Properly delete of parameter definition
     */
    final void Function(Pointer<Param> param) param_delete;
    /**
     * Get fraction of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Uint8> frac_ret) param_frac;
    /**
     * Get getable flag of parameter
     */
    final int Function(Pointer<Param> param) param_getable;
    /**
     * Get hint of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) param_hint;
    /**
     * Get type of parameter
     */
    final int Function(Pointer<Param> param) param_id;
    /**
     * Get info of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) param_info;
    /**
     * Get item of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) param_item;
    /**
     * Get maximum value of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Value>> max_ret) param_max;
    /**
     * Get minimum value of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Value>> min_ret) param_min;
    /**
     * Get name of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) param_name;
    /**
     * Get optional value of parameter
     */
    final int Function(Pointer<Param> param, int index, Pointer<Pointer<OptVal>> opt_ret) param_option;
    /**
     * Get number of optional values of parameter
     */
    final int Function(Pointer<Param> param) param_option_len;
    /**
     * Get options flag of parameter
     */
    final int Function(Pointer<Param> param) param_options;
    /**
     * Get persist flag of parameter
     */
    final int Function(Pointer<Param> param) param_persist;
    /**
     * Get parameter polling time
     */
    final int Function(Pointer<Param> param, Pointer<Uint32> poll_ret) param_poll;
    /**
     * Get setable flag of parameter
     */
    final int Function(Pointer<Param> param) param_setable;
    /**
     * Get size of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Uint32> size_ret) param_size;
    /**
     * Get stepping value of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Value>> stp_ret) param_step;
    /**
     * Get subparameters range
     */
    final int Function(Pointer<Param> param, Pointer<Uint32> fst_id_ret, Pointer<Uint32> lst_id_ret) param_sub;
    /**
     * Get type of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Uint8> type_ret) param_type;
    /**
     * Get unit of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) param_unit;
    /**
     * Get value of parameter
     */
    final int Function(Pointer<Param> param, Pointer<Pointer<Value>> val_ret) param_val;
    /**
     * Create progressive-reconnect strategy
     */
    final Pointer<ReconnectStrategy> Function() progressive_reconnect;
    /**
     * Get string representation of error code
     */
    final void Function(int code, Pointer<Pointer<Int8>> ptr, Pointer<Uint32> len) result_string;
    /**
     * Get type name
     */
    final void Function(int type_, Pointer<Pointer<Int8>> ptr, Pointer<Uint32> len) type_string;
    /**
     * Unsubscribe listener
     */
    final void Function(Pointer<Subscription> subscription) unsubscribe;
    /**
     * Properly delete value
     */
    final void Function(Pointer<Value> value) value_delete;
    /**
     * Get binary value
     */
    final int Function(Pointer<Value> value, Pointer<Pointer<Uint8>> ptr_ret, Pointer<Uint32> len_ret) value_get_binary;
    /**
     * Get date value
     */
    final int Function(Pointer<Value> value, Pointer<Date> value_ret) value_get_date;
    /**
     * Get floating-point value
     */
    final int Function(Pointer<Value> value, Pointer<Double> value_ret) value_get_float;
    /**
     * Get integer value
     */
    final int Function(Pointer<Value> value, Pointer<Int64> value_ret) value_get_int;
    /**
     * Get IPv4 address value
     */
    final int Function(Pointer<Value> value, Pointer<Ipv4> value_ret) value_get_ipv4;
    /**
     * Get IPv6 address value
     */
    final int Function(Pointer<Value> value, Pointer<Ipv6> value_ret) value_get_ipv6;
    /**
     * Get MAC address value
     */
    final int Function(Pointer<Value> value, Pointer<Mac> value_ret) value_get_mac;
    /**
     * Get string value
     */
    final int Function(Pointer<Value> value, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret) value_get_string;
    /**
     * Get time value
     */
    final int Function(Pointer<Value> value, Pointer<Time> value_ret) value_get_time;
    /**
     * Get kind of value
     */
    final int Function(Pointer<Value> value) value_kind;
    /**
     * New binary value
     */
    final Pointer<Value> Function(Pointer<Uint8> ptr, int len) value_new_binary;
    /**
     * New date value
     */
    final Pointer<Value> Function(Pointer<Date> value) value_new_date;
    /**
     * New float value
     */
    final Pointer<Value> Function(double value) value_new_float;
    /**
     * New integer value
     */
    final Pointer<Value> Function(int value) value_new_int;
    /**
     * New IPv4 value
     */
    final Pointer<Value> Function(Pointer<Ipv4> value) value_new_ipv4;
    /**
     * New IPv6 value
     */
    final Pointer<Value> Function(Pointer<Ipv6> value) value_new_ipv6;
    /**
     * New MAC value
     */
    final Pointer<Value> Function(Pointer<Mac> value) value_new_mac;
    /**
     * New string value
     */
    final Pointer<Value> Function(Pointer<Int8> ptr, int len) value_new_string;
    /**
     * New time value
     */
    final Pointer<Value> Function(Pointer<Time> value) value_new_time;
    /*Constructor
     */
    UParam(
        DynamicLibrary dylib
      , this.handle_on_event_callback
      , this.handle_on_value_callback
      , this.handle_set_value_callback
    )
    /*Init functions
     */
    : backend_tcp_socket_new = dylib.lookup<NativeFunction<Pointer<Backend> Function()>>('uparam_backend_tcp_socket_new').asFunction()
    , backend_unix_socket_new = dylib.lookup<NativeFunction<Pointer<Backend> Function()>>('uparam_backend_unix_socket_new').asFunction()
    , delayed_reconnect = dylib.lookup<NativeFunction<Pointer<ReconnectStrategy> Function()>>('uparam_delayed_reconnect').asFunction()
    , event_kind = dylib.lookup<NativeFunction<Int32 Function(Pointer<Event> event)>>('uparam_event_kind').asFunction()
    , event_param = dylib.lookup<NativeFunction<Int32 Function(Pointer<Event> event, Pointer<Uint32> id_ret)>>('uparam_event_param').asFunction()
    , event_status = dylib.lookup<NativeFunction<Int32 Function(Pointer<Event> event, Pointer<Int32> status_ret)>>('uparam_event_status').asFunction()
    , event_value = dylib.lookup<NativeFunction<Int32 Function(Pointer<Event> event, Pointer<Pointer<Value>> value_ret)>>('uparam_event_value').asFunction()
    , handle_delete = dylib.lookup<NativeFunction<Void Function(Pointer<Handle> handle)>>('uparam_handle_delete').asFunction()
    , handle_get_param = dylib.lookup<NativeFunction<Int32 Function(Pointer<Handle> handle, Uint32 id, Pointer<Pointer<Param>> param_ret)>>('uparam_handle_get_param').asFunction()
    , handle_get_value = dylib.lookup<NativeFunction<Int32 Function(Pointer<Handle> handle, Uint32 id, Pointer<Pointer<Value>> value_ret)>>('uparam_handle_get_value').asFunction()
    , handle_on_event = dylib.lookup<NativeFunction<Pointer<Subscription> Function(Pointer<Handle> handle, Pointer<NativeFunction<Void Function(Pointer<Event>, Pointer<Void>)>> callback, Pointer<Void> userdata)>>('uparam_handle_on_event').asFunction()
    , handle_on_value = dylib.lookup<NativeFunction<Pointer<Subscription> Function(Pointer<Handle> handle, Uint32 id, Pointer<NativeFunction<Void Function(Uint32, Pointer<Value>, Pointer<Void>)>> callback, Pointer<Void> userdata)>>('uparam_handle_on_value').asFunction()
    , handle_open = dylib.lookup<NativeFunction<Void Function(Pointer<Options> options, Pointer<Pointer<Handle>> ret_handle)>>('uparam_handle_open').asFunction()
    , handle_set_value = dylib.lookup<NativeFunction<Void Function(Pointer<Handle> handle, Uint32 id, Pointer<Value> value, Pointer<NativeFunction<Void Function(Int32, Uint32, Pointer<Value>, Pointer<Void>)>> callback, Pointer<Void> userdata)>>('uparam_handle_set_value').asFunction()
    , handle_status = dylib.lookup<NativeFunction<Int32 Function(Pointer<Handle> handle)>>('uparam_handle_status').asFunction()
    , handle_url = dylib.lookup<NativeFunction<Void Function(Pointer<Handle> handle, Pointer<Pointer<Int8>> ret_url_ptr, Pointer<Uint32> ret_url_len)>>('uparam_handle_url').asFunction()
    , immediate_reconnect = dylib.lookup<NativeFunction<Pointer<ReconnectStrategy> Function()>>('uparam_immediate_reconnect').asFunction()
    , logger_init = dylib.lookup<NativeFunction<Int32 Function(Int32 level)>>('uparam_logger_init').asFunction()
    , manager_delete = dylib.lookup<NativeFunction<Void Function(Pointer<Manager> manager)>>('uparam_manager_delete').asFunction()
    , manager_new = dylib.lookup<NativeFunction<Pointer<Manager> Function()>>('uparam_manager_new').asFunction()
    , manager_register = dylib.lookup<NativeFunction<Int32 Function(Pointer<Manager> manager, Pointer<Backend> backend)>>('uparam_manager_register').asFunction()
    , no_reconnect = dylib.lookup<NativeFunction<Pointer<ReconnectStrategy> Function()>>('uparam_no_reconnect').asFunction()
    , option_hint = dylib.lookup<NativeFunction<Int32 Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_option_hint').asFunction()
    , option_info = dylib.lookup<NativeFunction<Int32 Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_option_info').asFunction()
    , option_item = dylib.lookup<NativeFunction<Int32 Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_option_item').asFunction()
    , option_name = dylib.lookup<NativeFunction<Int32 Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_option_name').asFunction()
    , option_unit = dylib.lookup<NativeFunction<Int32 Function(Pointer<OptVal> option, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_option_unit').asFunction()
    , option_val = dylib.lookup<NativeFunction<Pointer<Value> Function(Pointer<OptVal> option)>>('uparam_option_val').asFunction()
    , options_delete = dylib.lookup<NativeFunction<Void Function(Pointer<Options> options)>>('uparam_options_delete').asFunction()
    , options_from_url = dylib.lookup<NativeFunction<Int32 Function(Pointer<Manager> backends, Pointer<Int8> url_ptr, Uint32 url_len, Pointer<Pointer<Options>> ret_options)>>('uparam_options_from_url').asFunction()
    , options_set_max_poll_interval = dylib.lookup<NativeFunction<Void Function(Pointer<Options> options, Float poll_interval)>>('uparam_options_set_max_poll_interval').asFunction()
    , options_set_min_poll_interval = dylib.lookup<NativeFunction<Void Function(Pointer<Options> options, Float poll_interval)>>('uparam_options_set_min_poll_interval').asFunction()
    , options_set_reconnect_strategy = dylib.lookup<NativeFunction<Void Function(Pointer<Options> options, Pointer<ReconnectStrategy> reconnect_strategy)>>('uparam_options_set_reconnect_strategy').asFunction()
    , options_set_request_attempts = dylib.lookup<NativeFunction<Void Function(Pointer<Options> options, Uint32 request_attempts)>>('uparam_options_set_request_attempts').asFunction()
    , options_set_request_limit = dylib.lookup<NativeFunction<Void Function(Pointer<Options> options, Uint32 request_limit)>>('uparam_options_set_request_limit').asFunction()
    , options_set_request_timeout = dylib.lookup<NativeFunction<Void Function(Pointer<Options> options, Float request_timeout)>>('uparam_options_set_request_timeout').asFunction()
    , param_def = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Value>> def_ret)>>('uparam_param_def').asFunction()
    , param_delete = dylib.lookup<NativeFunction<Void Function(Pointer<Param> param)>>('uparam_param_delete').asFunction()
    , param_frac = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Uint8> frac_ret)>>('uparam_param_frac').asFunction()
    , param_getable = dylib.lookup<NativeFunction<Uint8 Function(Pointer<Param> param)>>('uparam_param_getable').asFunction()
    , param_hint = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_param_hint').asFunction()
    , param_id = dylib.lookup<NativeFunction<Uint32 Function(Pointer<Param> param)>>('uparam_param_id').asFunction()
    , param_info = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_param_info').asFunction()
    , param_item = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_param_item').asFunction()
    , param_max = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Value>> max_ret)>>('uparam_param_max').asFunction()
    , param_min = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Value>> min_ret)>>('uparam_param_min').asFunction()
    , param_name = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_param_name').asFunction()
    , param_option = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Uint32 index, Pointer<Pointer<OptVal>> opt_ret)>>('uparam_param_option').asFunction()
    , param_option_len = dylib.lookup<NativeFunction<Uint32 Function(Pointer<Param> param)>>('uparam_param_option_len').asFunction()
    , param_options = dylib.lookup<NativeFunction<Uint8 Function(Pointer<Param> param)>>('uparam_param_options').asFunction()
    , param_persist = dylib.lookup<NativeFunction<Uint8 Function(Pointer<Param> param)>>('uparam_param_persist').asFunction()
    , param_poll = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Uint32> poll_ret)>>('uparam_param_poll').asFunction()
    , param_setable = dylib.lookup<NativeFunction<Uint8 Function(Pointer<Param> param)>>('uparam_param_setable').asFunction()
    , param_size = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Uint32> size_ret)>>('uparam_param_size').asFunction()
    , param_step = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Value>> stp_ret)>>('uparam_param_step').asFunction()
    , param_sub = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Uint32> fst_id_ret, Pointer<Uint32> lst_id_ret)>>('uparam_param_sub').asFunction()
    , param_type = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Uint8> type_ret)>>('uparam_param_type').asFunction()
    , param_unit = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_param_unit').asFunction()
    , param_val = dylib.lookup<NativeFunction<Int32 Function(Pointer<Param> param, Pointer<Pointer<Value>> val_ret)>>('uparam_param_val').asFunction()
    , progressive_reconnect = dylib.lookup<NativeFunction<Pointer<ReconnectStrategy> Function()>>('uparam_progressive_reconnect').asFunction()
    , result_string = dylib.lookup<NativeFunction<Void Function(Int32 code, Pointer<Pointer<Int8>> ptr, Pointer<Uint32> len)>>('uparam_result_string').asFunction()
    , type_string = dylib.lookup<NativeFunction<Void Function(Uint8 type_, Pointer<Pointer<Int8>> ptr, Pointer<Uint32> len)>>('uparam_type_string').asFunction()
    , unsubscribe = dylib.lookup<NativeFunction<Void Function(Pointer<Subscription> subscription)>>('uparam_unsubscribe').asFunction()
    , value_delete = dylib.lookup<NativeFunction<Void Function(Pointer<Value> value)>>('uparam_value_delete').asFunction()
    , value_get_binary = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Pointer<Uint8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_value_get_binary').asFunction()
    , value_get_date = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Date> value_ret)>>('uparam_value_get_date').asFunction()
    , value_get_float = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Double> value_ret)>>('uparam_value_get_float').asFunction()
    , value_get_int = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Int64> value_ret)>>('uparam_value_get_int').asFunction()
    , value_get_ipv4 = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Ipv4> value_ret)>>('uparam_value_get_ipv4').asFunction()
    , value_get_ipv6 = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Ipv6> value_ret)>>('uparam_value_get_ipv6').asFunction()
    , value_get_mac = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Mac> value_ret)>>('uparam_value_get_mac').asFunction()
    , value_get_string = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Pointer<Int8>> ptr_ret, Pointer<Uint32> len_ret)>>('uparam_value_get_string').asFunction()
    , value_get_time = dylib.lookup<NativeFunction<Int32 Function(Pointer<Value> value, Pointer<Time> value_ret)>>('uparam_value_get_time').asFunction()
    , value_kind = dylib.lookup<NativeFunction<Uint32 Function(Pointer<Value> value)>>('uparam_value_kind').asFunction()
    , value_new_binary = dylib.lookup<NativeFunction<Pointer<Value> Function(Pointer<Uint8> ptr, Uint32 len)>>('uparam_value_new_binary').asFunction()
    , value_new_date = dylib.lookup<NativeFunction<Pointer<Value> Function(Pointer<Date> value)>>('uparam_value_new_date').asFunction()
    , value_new_float = dylib.lookup<NativeFunction<Pointer<Value> Function(Double value)>>('uparam_value_new_float').asFunction()
    , value_new_int = dylib.lookup<NativeFunction<Pointer<Value> Function(Int64 value)>>('uparam_value_new_int').asFunction()
    , value_new_ipv4 = dylib.lookup<NativeFunction<Pointer<Value> Function(Pointer<Ipv4> value)>>('uparam_value_new_ipv4').asFunction()
    , value_new_ipv6 = dylib.lookup<NativeFunction<Pointer<Value> Function(Pointer<Ipv6> value)>>('uparam_value_new_ipv6').asFunction()
    , value_new_mac = dylib.lookup<NativeFunction<Pointer<Value> Function(Pointer<Mac> value)>>('uparam_value_new_mac').asFunction()
    , value_new_string = dylib.lookup<NativeFunction<Pointer<Value> Function(Pointer<Int8> ptr, Uint32 len)>>('uparam_value_new_string').asFunction()
    , value_new_time = dylib.lookup<NativeFunction<Pointer<Value> Function(Pointer<Time> value)>>('uparam_value_new_time').asFunction()
    {}
}

