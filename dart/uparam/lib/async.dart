import 'dart:ffi';
import 'dart:collection';

class Dispatcher {
  int _lastId = 0;
  HashMap<int, dynamic> _pending = HashMap();

  Pointer<Void> initiate<X>(X context) {
    _lastId += 1;
    _pending[_lastId] = context;
    return Pointer<Void>.fromAddress(_lastId);
  }

  bool complete<X>(Pointer<Void> id, bool Function(X context) func) {
    if (_pending.containsKey(id.address)) {
      if (func(_pending[id.address])) {
        _pending.remove(id.address);
      }
      return true;
    }
    return false;
  }

  bool cancel(Pointer<Void> id) {
    if (_pending.containsKey(id.address)) {
      _pending.remove(id.address);
      return true;
    }
    return false;
  }
}

final dispatcher = Dispatcher();
