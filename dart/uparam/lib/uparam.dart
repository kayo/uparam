import 'dart:core';
import 'dart:ffi';
import 'dart:async';
import 'dart:typed_data';
import 'dart:math' show max;
import 'alloc.dart';
import 'async.dart';
import 'slice.dart';
import 'uparam_h.dart' as low;

export 'uparam_h.dart' show ResultCode, LogLevel;

class UParam {
  final low.UParam _low;

  UParam(DynamicLibrary dylib)
      : _low = low.UParam(
            dylib,
            Pointer.fromFunction(_handle_on_event_callback),
            Pointer.fromFunction(_handle_on_value_callback),
            Pointer.fromFunction(_handle_set_value_callback)) {}

  Manager get manager => Manager.fromRaw(_low, _low.manager_new());

  void loggerInit(int level) {
    _low.logger_init(level);
  }
}

class Manager {
  final low.UParam _low;
  final Pointer<low.Manager> _raw;

  Manager.fromRaw(this._low, this._raw) {}

  void dispose() {
    _low.manager_delete(_raw);
  }

  void register(Backend backend) {
    _checkResult(_low, _low.manager_register(_raw, backend.rawBackend(_low)));
  }

  Error checkUrl(String url) {
    final rawUrl = strIntoRawSlice(url);
    final res = _low.options_from_url(_raw, rawUrl.ptr, rawUrl.len, nullptr);
    rawUrl.dispose();
    return res == low.ResultCode.Success ? null : Error.fromRaw(_low, res);
  }

  Options fromUrl(String url) {
    final rawUrl = strIntoRawSlice(url);
    final res =
        _low.options_from_url(_raw, rawUrl.ptr, rawUrl.len, _rawOptsPtr);
    rawUrl.dispose();
    _checkResult(_low, res);
    return Options.fromRaw(_low, _rawOptsPtr.value);
  }
}

abstract class Backend {
  Pointer<low.Backend> rawBackend(low.UParam low);
}

class TcpSocket extends Backend {
  Pointer<low.Backend> rawBackend(low.UParam low) =>
      low.backend_tcp_socket_new();
}

class UnixSocket extends Backend {
  Pointer<low.Backend> rawBackend(low.UParam low) =>
      low.backend_unix_socket_new();
}

class Options {
  final low.UParam _low;
  final Pointer<low.Options> _raw;

  Options.fromRaw(this._low, this._raw) {}

  void dispose() {
    _low.options_delete(_raw);
  }

  void setMaxPollInterval(double pollInterval) {
    _low.options_set_max_poll_interval(_raw, pollInterval);
  }

  void setMinPollInterval(double pollInterval) {
    _low.options_set_min_poll_interval(_raw, pollInterval);
  }

  void setReconnectStrategy(ReconnectStrategy reconnectStrategy) {
    _low.options_set_reconnect_strategy(
        _raw, reconnectStrategy.rawReconnectStrategy(_low));
  }

  void setRequestAttempts(int requestAttempts) {
    _low.options_set_request_attempts(_raw, requestAttempts);
  }

  void setRequestLimit(int requestLimit) {
    _low.options_set_request_limit(_raw, requestLimit);
  }

  void setRequestTimeout(double requestTimeout) {
    _low.options_set_request_timeout(_raw, requestTimeout);
  }

  Handle open() {
    _low.handle_open(_raw, _rawHndPtr);
    return Handle.fromRaw(_low, _rawHndPtr.value);
  }
}

abstract class ReconnectStrategy {
  Pointer<low.ReconnectStrategy> rawReconnectStrategy(low.UParam low);
}

class NoReconnect extends ReconnectStrategy {
  Pointer<low.ReconnectStrategy> rawReconnectStrategy(low.UParam low) =>
      low.no_reconnect();
}

class ImmediateReconnect extends ReconnectStrategy {
  Pointer<low.ReconnectStrategy> rawReconnectStrategy(low.UParam low) =>
      low.immediate_reconnect();
}

class DelayedReconnect extends ReconnectStrategy {
  Pointer<low.ReconnectStrategy> rawReconnectStrategy(low.UParam low) =>
      low.delayed_reconnect();
}

class ProgressiveReconnect extends ReconnectStrategy {
  Pointer<low.ReconnectStrategy> rawReconnectStrategy(low.UParam low) =>
      low.progressive_reconnect();
}

class Handle {
  low.UParam _low;
  Pointer<low.Handle> _raw;

  Handle.fromRaw(this._low, this._raw) {}

  void dispose() {
    _low.handle_delete(_raw);
  }

  Status get status => _statusFromRaw(_low.handle_status(_raw));

  String get url {
    _low.handle_url(_raw, _rawStrPtr, _rawLenPtr);
    return strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  Stream<Event> events() {
    Pointer<low.Subscription> subscription;
    StreamController<Event> streamCtrl;
    Pointer<Void> taskId;

    streamCtrl = StreamController<Event>.broadcast(onListen: () {
      final state = _ListenState(_low, streamCtrl);
      taskId = dispatcher.initiate(state);
      subscription =
          _low.handle_on_event(_raw, _low.handle_on_event_callback, taskId);
    }, onCancel: () {
      _low.unsubscribe(subscription);
      dispatcher.cancel(taskId);
    });

    return streamCtrl.stream;
  }

  // Get parameter description
  Param param([int id = 0]) {
    _checkResult(_low, _low.handle_get_param(_raw, id, _rawParPtr));
    final rawPar = _rawParPtr.value;
    final param = _paramFromRaw(_low, rawPar);
    _low.param_delete(rawPar);
    return param;
  }

  // Get value
  dynamic value(int id) {
    _checkResult(_low, _low.handle_get_value(_raw, id, _rawValPtr));
    final rawVal = _rawValPtr.value;
    var value = _valueFromRaw(_low, rawVal);
    _low.value_delete(rawVal);
    return value;
  }

  // Set value
  Future<dynamic> setValue(int id, dynamic val) {
    final valPtr = _valueIntoRaw(_low, val);

    final completer = Completer<dynamic>();
    final state = _ListenState(_low, completer);
    final taskId = dispatcher.initiate(state);

    _low.handle_set_value(
        _raw, id, valPtr, _low.handle_set_value_callback, taskId);

    return completer.future;
  }

  Stream<dynamic> values(int id) {
    Pointer<low.Subscription> subscription;
    StreamController<dynamic> streamCtrl;
    Pointer<Void> taskId;

    streamCtrl = StreamController<dynamic>.broadcast(onListen: () {
      final state = _ListenState(_low, streamCtrl);
      taskId = dispatcher.initiate(state);
      subscription =
          _low.handle_on_value(_raw, id, _low.handle_on_value_callback, taskId);
    }, onCancel: () {
      _low.unsubscribe(subscription);
      dispatcher.cancel(taskId);
    });

    return streamCtrl.stream;
  }
}

class _ListenState<X> {
  final low.UParam lib;
  final X ctx;

  _ListenState(this.lib, this.ctx) {}
}

void _handle_on_event_callback(Pointer<low.Event> event, Pointer<Void> taskId) {
  dispatcher.complete(taskId, (_ListenState<StreamController<Event>> state) {
    state.ctx.add(_eventFromRaw(state.lib, event));
    return false;
  });
}

void _handle_on_value_callback(
    int id, Pointer<low.Value> value, Pointer<Void> taskId) {
  dispatcher.complete(taskId, (_ListenState<StreamController<dynamic>> state) {
    state.ctx.add(_valueFromRaw(state.lib, value));
    return false;
  });
}

void _handle_set_value_callback(
    int result, int id, Pointer<low.Value> value, Pointer<Void> taskId) {
  dispatcher.complete(taskId, (_ListenState<Completer<dynamic>> state) {
    if (result == low.ResultCode.Success) {
      state.ctx.complete(_valueFromRaw(state.lib, value));
    } else {
      state.ctx.completeError(Error.fromRaw(state.lib, result));
    }
    return true;
  });
}

abstract class Event {}

class StatusEvent extends Event {
  final Status status;
  StatusEvent(this.status) {}
  String toString() {
    return "StatusEvent(${status})";
  }
}

enum Status {
  offline,
  online,
  active,
}

Status _statusFromRaw(int rawStatus) {
  switch (rawStatus) {
    case low.Status.Offline:
      return Status.offline;
    case low.Status.Online:
      return Status.online;
    case low.Status.Active:
      return Status.active;
  }
}

class RetrievingEvent extends Event {
  String toString() {
    return "RetrievingEvent";
  }
}

class RetrieveEvent extends Event {
  final int id;
  RetrieveEvent(this.id) {}
  String toString() {
    return "RetrieveEvent(${id})";
  }
}

class RetrievedEvent extends Event {
  String toString() {
    return "RetrievedEvent";
  }
}

class UpdatingEvent extends Event {
  String toString() {
    return "UpdatingEvent";
  }
}

class UpdateEvent extends Event {
  final int id;
  final dynamic value;
  UpdateEvent(this.id, this.value) {}
  String toString() {
    return "UpdateEvent(${id}, ${value})";
  }
}

class UpdatedEvent extends Event {
  String toString() {
    return "UpdatedEvent";
  }
}

Event _eventFromRaw(low.UParam _low, Pointer<low.Event> _raw) {
  switch (_low.event_kind(_raw)) {
    case low.EventKind.Status:
      _checkResult(_low, _low.event_status(_raw, _rawNumPtr));
      return StatusEvent(_statusFromRaw(_rawNumPtr.value));
    case low.EventKind.Retrieving:
      return RetrievingEvent();
    case low.EventKind.Retrieve:
      _checkResult(_low, _low.event_param(_raw, _rawLenPtr));
      return RetrieveEvent(_rawLenPtr.value);
    case low.EventKind.Retrieved:
      return RetrievedEvent();
    case low.EventKind.Updating:
      return UpdatingEvent();
    case low.EventKind.Update:
      _checkResult(_low, _low.event_param(_raw, _rawLenPtr));
      _checkResult(_low, _low.event_value(_raw, _rawValPtr));
      return UpdateEvent(
          _rawLenPtr.value, _valueFromRaw(_low, _rawValPtr.value));
    case low.EventKind.Updated:
      return UpdatedEvent();
  }
}

final Pointer<Pointer<low.Handle>> _rawHndPtr = alloc();
final Pointer<Pointer<low.Options>> _rawOptsPtr = alloc();
final Pointer<Pointer<low.Param>> _rawParPtr = alloc();
final Pointer<Pointer<low.OptVal>> _rawOptPtr = alloc();
final Pointer<Pointer<low.Value>> _rawValPtr = alloc();
final Pointer<Int64> _rawIntPtr = alloc();
final Pointer<Double> _rawFloatPtr = alloc();
final Pointer<Pointer<Int8>> _rawStrPtr = alloc();
final Pointer<Pointer<Uint8>> _rawBinPtr = alloc();
final Pointer<low.Date> _rawDatePtr = alloc();
final Pointer<low.Time> _rawTimePtr = alloc();
final Pointer<low.Mac> _rawMacPtr = alloc();
final Pointer<low.Ipv4> _rawIpv4Ptr = alloc();
final Pointer<low.Ipv6> _rawIpv6Ptr = alloc();
final Pointer<Int32> _rawNumPtr = alloc();
final Pointer<Uint32> _rawLenPtr = alloc();
final Pointer<Uint8> _rawBytePtr = alloc();
final Pointer<Uint32> _rawId1Ptr = alloc();
final Pointer<Uint32> _rawId2Ptr = alloc();

dynamic _valueFromRaw(low.UParam _low, Pointer<low.Value> _raw) {
  switch (_low.value_kind(_raw)) {
    case low.ValKind.Int:
      _checkResult(_low, _low.value_get_int(_raw, _rawIntPtr));
      return _rawIntPtr.value;
    case low.ValKind.Float:
      _checkResult(_low, _low.value_get_float(_raw, _rawFloatPtr));
      return _rawFloatPtr.value;
    case low.ValKind.String:
      _checkResult(_low, _low.value_get_string(_raw, _rawStrPtr, _rawLenPtr));
      return strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
    case low.ValKind.Binary:
      _checkResult(_low, _low.value_get_binary(_raw, _rawBinPtr, _rawLenPtr));
      return binFromRawSlice(RawSlice.fromRawPtr(_rawBinPtr, _rawLenPtr));
    case low.ValKind.Date:
      _checkResult(_low, _low.value_get_date(_raw, _rawDatePtr));
      return Date.fromRaw(_rawDatePtr);
    case low.ValKind.Time:
      _checkResult(_low, _low.value_get_time(_raw, _rawTimePtr));
      return Time.fromRaw(_rawTimePtr);
    case low.ValKind.Mac:
      _checkResult(_low, _low.value_get_mac(_raw, _rawMacPtr));
      return Mac.fromRaw(_rawMacPtr);
    case low.ValKind.IPv4:
      _checkResult(_low, _low.value_get_ipv4(_raw, _rawIpv4Ptr));
      return Ipv4.fromRaw(_rawIpv4Ptr);
    case low.ValKind.IPv6:
      _checkResult(_low, _low.value_get_ipv6(_raw, _rawIpv6Ptr));
      return Ipv6.fromRaw(_rawIpv6Ptr);
  }
}

Pointer<low.Value> _valueIntoRaw(low.UParam _low, dynamic val) {
  if (val is int) {
    return _low.value_new_int(val);
  } else if (val is double) {
    return _low.value_new_float(val);
  } else if (val is String) {
    final raw = strIntoRawSlice(val);
    final ret = _low.value_new_string(raw.ptr, raw.len);
    raw.dispose();
    return ret;
  } else if (val is ByteBuffer) {
    final raw = binIntoRawSlice(val);
    final ret = _low.value_new_binary(raw.ptr, raw.len);
    raw.dispose();
    return ret;
  } else if (val is Date) {
    val.intoRaw(_rawDatePtr);
    return _low.value_new_date(_rawDatePtr);
  } else if (val is Time) {
    val.intoRaw(_rawTimePtr);
    return _low.value_new_time(_rawTimePtr);
  } else if (val is Mac) {
    val.intoRaw(_rawMacPtr);
    return _low.value_new_mac(_rawMacPtr);
  } else if (val is Ipv4) {
    val.intoRaw(_rawIpv4Ptr);
    return _low.value_new_ipv4(_rawIpv4Ptr);
  } else if (val is Ipv6) {
    val.intoRaw(_rawIpv6Ptr);
    return _low.value_new_ipv6(_rawIpv6Ptr);
  }
  throw "Unsupported value type";
}

class Date {
  final int year;
  final int month;
  final int date;

  Date({this.year = 2000, this.month = 1, this.date = 1});

  Date withYear(int newYear) => Date(year: newYear, month: month, date: date);
  Date withMonth(int newMonth) => Date(year: year, month: newMonth, date: date);
  Date withDate(int newDate) => Date(year: year, month: month, date: newDate);

  Date addYear(int dYear) => Date(year: year + dYear, month: month, date: date);
  Date addMonth(int dMonth) =>
      Date(year: year, month: month + dMonth, date: date);
  Date addDate(int dDate) => Date(year: year, month: month, date: date + dDate);

  Date.fromDateTime(DateTime dt)
      : year = dt.year,
        month = dt.month,
        date = dt.day;

  Date.fromRaw(Pointer<low.Date> raw)
      : year = raw.ref.year,
        month = raw.ref.mon,
        date = raw.ref.date;

  DateTime toDateTime() => DateTime(year, month, date);

  DateTime get dateTime => toDateTime();

  void intoRaw(Pointer<low.Date> raw) {
    raw.ref.year = year;
    raw.ref.mon = month;
    raw.ref.date = date;
  }

  String toString() => '${_formatIntLZ(year, 4)}-${_formatIntLZ(month, 2)}-${_formatIntLZ(date, 2)}';
}

class Time {
  final int hour;
  final int minute;
  final int second;

  Time({this.hour = 0, this.minute = 0, this.second = 0});

  Time withHour(int newHour) =>
      Time(hour: newHour, minute: minute, second: second);
  Time withMinute(int newMinute) =>
      Time(hour: hour, minute: newMinute, second: second);
  Time withSecond(int newSecond) =>
      Time(hour: hour, minute: minute, second: newSecond);

  Time addHour(int dHour) =>
      Time(hour: hour + dHour, minute: minute, second: second);
  Time addMinute(int dMinute) =>
      Time(hour: hour, minute: minute + dMinute, second: second);
  Time addSecond(int dSecond) =>
      Time(hour: hour, minute: minute, second: second + dSecond);

  Time.fromDateTime(DateTime dt)
      : hour = dt.hour,
        minute = dt.minute,
        second = dt.second;

  Time.fromRaw(Pointer<low.Time> raw)
      : hour = raw.ref.hour,
        minute = raw.ref.min,
        second = raw.ref.sec;

  DateTime toDateTime() => DateTime(0, 1, 1, hour, minute, second);

  DateTime get dateTime => toDateTime();

  void intoRaw(Pointer<low.Time> raw) {
    raw.ref.hour = hour;
    raw.ref.min = minute;
    raw.ref.sec = second;
  }

  String toString() => '${_formatIntLZ(hour, 2)}:${_formatIntLZ(minute, 2)}:${_formatIntLZ(second, 2)}';
}

String _formatIntLZ(int val, [int len = 0, int rdx = 10]) {
  String str = val.toRadixString(rdx);
  return '0' * max(0, len - str.length) + str;
}

class Mac {
  final List<int> data;

  Mac([List<int> val]) : data = val ?? [0, 0, 0, 0, 0, 0];

  Mac.fromRaw(Pointer<low.Mac> raw)
      : data = [
          raw.ref.a,
          raw.ref.b,
          raw.ref.c,
          raw.ref.d,
          raw.ref.e,
          raw.ref.f
        ];

  //Mac.fromString

  void intoRaw(Pointer<low.Mac> raw) {
    raw.ref.a = data[0];
    raw.ref.b = data[1];
    raw.ref.c = data[2];
    raw.ref.d = data[3];
    raw.ref.e = data[4];
    raw.ref.f = data[5];
  }

  String toString() => '${_formatIntLZ(data[0], 2, 16)}:${_formatIntLZ(data[1], 2, 16)}:${_formatIntLZ(data[2], 2, 16)}:${_formatIntLZ(data[3], 2, 16)}:${_formatIntLZ(data[4], 2, 16)}:${_formatIntLZ(data[5], 2, 16)}';
}

class Ipv4 {
  final List<int> data;

  Ipv4([List<int> val]) : data = val ?? [0, 0, 0, 0];

  Ipv4.fromRaw(Pointer<low.Ipv4> raw)
      : data = [raw.ref.a, raw.ref.b, raw.ref.c, raw.ref.d];

  void intoRaw(Pointer<low.Ipv4> raw) {
    raw.ref.a = data[0];
    raw.ref.b = data[1];
    raw.ref.c = data[2];
    raw.ref.d = data[3];
  }

  String toString() => '${data[0]}.${data[1]}.${data[2]}.${data[3]}';
}

class Ipv6 {
  final List<int> data;

  Ipv6([List<int> val]) : data = val ?? [0, 0, 0, 0, 0, 0, 0, 0];

  Ipv6.fromRaw(Pointer<low.Ipv6> raw)
      : data = [
          raw.ref.a,
          raw.ref.b,
          raw.ref.c,
          raw.ref.d,
          raw.ref.e,
          raw.ref.f,
          raw.ref.g,
          raw.ref.h
        ];

  void intoRaw(Pointer<low.Ipv6> raw) {
    raw.ref.a = data[0];
    raw.ref.b = data[1];
    raw.ref.c = data[2];
    raw.ref.d = data[3];
    raw.ref.e = data[4];
    raw.ref.f = data[5];
    raw.ref.g = data[6];
    raw.ref.h = data[7];
  }

  String toString() => '${_formatIntLZ(data[0], 0, 16)}:${_formatIntLZ(data[1], 0, 16)}:${_formatIntLZ(data[2], 0, 16)}:${_formatIntLZ(data[3], 0, 16)}:${_formatIntLZ(data[4], 0, 16)}:${_formatIntLZ(data[5], 0, 16)}:${_formatIntLZ(data[6], 0, 16)}:${_formatIntLZ(data[7], 0, 16)}';
}

enum Type {
  uint,
  sint,
  ufix,
  sfix,
  real,
  cstr,
  hbin,
  time,
  date,
  mac,
  ipv4,
  ipv6,
}

Type _typeFromRaw(int rawType) {
  switch (rawType) {
    case low.Type.UInt:
      return Type.uint;
    case low.Type.SInt:
      return Type.sint;
    case low.Type.UFix:
      return Type.ufix;
    case low.Type.SFix:
      return Type.sfix;
    case low.Type.Real:
      return Type.real;
    case low.Type.CStr:
      return Type.cstr;
    case low.Type.HBin:
      return Type.hbin;
    case low.Type.Time:
      return Type.time;
    case low.Type.Date:
      return Type.date;
    case low.Type.Mac:
      return Type.mac;
    case low.Type.IPv4:
      return Type.ipv4;
    case low.Type.IPv6:
      return Type.ipv6;
  }
}

class IdRange {
  final int start;
  final int end;

  IdRange(this.start, this.end);
}

/// Parameter descriptor
class Param {
  final int id;
  final Type type;
  final int size;
  final int frac;
  final IdRange sub;
  dynamic value;
  final String name;
  final String unit;
  final String item;
  final String info;
  final String hint;
  final bool getable;
  final bool setable;
  final bool persist;
  final bool options;
  final dynamic def;
  final dynamic min;
  final dynamic max;
  final dynamic step;
  final List<OptVal> option;
  final int poll;

  Param(
      this.id,
      this.type,
      this.size,
      this.frac,
      this.sub,
      this.value,
      this.name,
      this.unit,
      this.item,
      this.info,
      this.hint,
      this.getable,
      this.setable,
      this.persist,
      this.options,
      this.def,
      this.min,
      this.max,
      this.step,
      this.option,
      this.poll) {}
}

class OptVal {
  final dynamic value;
  final String name;
  final String unit;
  final String item;
  final String info;
  final String hint;

  OptVal(this.value, this.name, this.unit, this.item, this.info, this.hint) {}
}

Param _paramFromRaw(low.UParam _low, Pointer<low.Param> _raw) {
  final id = _low.param_id(_raw);

  Type type;
  if (_low.param_type(_raw, _rawBytePtr) == low.ResultCode.Success) {
    type = _typeFromRaw(_rawBytePtr.value);
  }

  int size;
  if (_low.param_size(_raw, _rawLenPtr) == low.ResultCode.Success) {
    size = _rawLenPtr.value;
  }

  int frac;
  if (_low.param_frac(_raw, _rawBytePtr) == low.ResultCode.Success) {
    frac = _rawBytePtr.value;
  }

  IdRange sub;
  if (_low.param_sub(_raw, _rawId1Ptr, _rawId2Ptr) == low.ResultCode.Success) {
    sub = IdRange(_rawId1Ptr.value, _rawId2Ptr.value);
  }

  dynamic value;
  if (_low.param_val(_raw, _rawValPtr) == low.ResultCode.Success) {
    value = _valueFromRaw(_low, _rawValPtr.value);
  }

  String name;
  if (_low.param_name(_raw, _rawStrPtr, _rawLenPtr) == low.ResultCode.Success) {
    name = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  String unit;
  if (_low.param_unit(_raw, _rawStrPtr, _rawLenPtr) == low.ResultCode.Success) {
    unit = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  String item;
  if (_low.param_item(_raw, _rawStrPtr, _rawLenPtr) == low.ResultCode.Success) {
    item = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  String info;
  if (_low.param_info(_raw, _rawStrPtr, _rawLenPtr) == low.ResultCode.Success) {
    info = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  String hint;
  if (_low.param_hint(_raw, _rawStrPtr, _rawLenPtr) == low.ResultCode.Success) {
    hint = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  bool getable = _low.param_getable(_raw) != 0;
  bool setable = _low.param_setable(_raw) != 0;
  bool persist = _low.param_persist(_raw) != 0;
  bool options = _low.param_options(_raw) != 0;

  dynamic def;
  if (_low.param_def(_raw, _rawValPtr) == low.ResultCode.Success) {
    def = _valueFromRaw(_low, _rawValPtr.value);
  }

  dynamic min;
  if (_low.param_min(_raw, _rawValPtr) == low.ResultCode.Success) {
    min = _valueFromRaw(_low, _rawValPtr.value);
  }

  dynamic max;
  if (_low.param_max(_raw, _rawValPtr) == low.ResultCode.Success) {
    max = _valueFromRaw(_low, _rawValPtr.value);
  }

  dynamic step;
  if (_low.param_step(_raw, _rawValPtr) == low.ResultCode.Success) {
    step = _valueFromRaw(_low, _rawValPtr.value);
  }

  final numOptions = _low.param_option_len(_raw);
  final option = List<OptVal>();

  for (var index = 0; index < numOptions; index++) {
    if (_low.param_option(_raw, index, _rawOptPtr) == low.ResultCode.Success) {
      option.add(_optionFromRaw(_low, _rawOptPtr.value));
    }
  }

  int poll;
  if (_low.param_poll(_raw, _rawLenPtr) == low.ResultCode.Success) {
    poll = _rawLenPtr.value;
  }

  return Param(id, type, size, frac, sub, value, name, unit, item, info, hint,
      getable, setable, persist, options, def, min, max, step, option, poll);
}

OptVal _optionFromRaw(low.UParam _low, Pointer<low.OptVal> _raw) {
  dynamic value = _valueFromRaw(_low, _low.option_val(_raw));

  String name;
  if (_low.option_name(_raw, _rawStrPtr, _rawLenPtr) ==
      low.ResultCode.Success) {
    name = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  String unit;
  if (_low.option_unit(_raw, _rawStrPtr, _rawLenPtr) ==
      low.ResultCode.Success) {
    unit = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  String item;
  if (_low.option_item(_raw, _rawStrPtr, _rawLenPtr) ==
      low.ResultCode.Success) {
    item = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  String info;
  if (_low.option_info(_raw, _rawStrPtr, _rawLenPtr) ==
      low.ResultCode.Success) {
    info = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  String hint;
  if (_low.option_hint(_raw, _rawStrPtr, _rawLenPtr) ==
      low.ResultCode.Success) {
    hint = strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }

  return OptVal(
    value,
    name,
    unit,
    item,
    info,
    hint,
  );
}

class Error {
  final low.UParam _low;
  final int _raw;

  int get code => _raw;

  Error.fromRaw(this._low, this._raw) {}

  String toString() {
    _low.result_string(_raw, _rawStrPtr, _rawLenPtr);
    return strFromRawSlice(RawSlice.fromRawPtr(_rawStrPtr, _rawLenPtr));
  }
}

void _checkResult(low.UParam _low, int res) {
  if (res != low.ResultCode.Success) {
    throw Error.fromRaw(_low, res);
  }
}
