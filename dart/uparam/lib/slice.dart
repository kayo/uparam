import 'dart:ffi';
import 'dart:convert';
import 'dart:typed_data';
import 'alloc.dart';

class RawSlice<T extends NativeType> {
  final Pointer<T> ptr;
  final int len;

  RawSlice.fromRaw(this.ptr, this.len) {}

  RawSlice.fromRawPtr(Pointer<Pointer<T>> ptrPtr, Pointer<Uint32> lenPtr)
  : ptr = ptrPtr.value
  , len = lenPtr.value {}

  RawSlice(RawSlice<T> raw)
  : ptr = raw.ptr
  , len = raw.len {}

  void dispose() {
    free(ptr);
  }
}

String strFromRawSlice(RawSlice<Int8> raw) {
  return utf8.decode(
    Uint8List.view(
      raw.ptr.cast<Uint8>()
        .asTypedList(raw.len)
        .buffer,
      0, raw.len
    )
  );
}

RawSlice<Int8> strIntoRawSlice(String str) {
  final bytes = utf8.encode(str);
  final len = bytes.length;
  final Pointer<Uint8> ptr = alloc(len);
  ptr.asTypedList(len).setAll(0, bytes);
  return RawSlice.fromRaw(ptr.cast(), len);
}

ByteBuffer binFromRawSlice(RawSlice<Uint8> raw) {
  return raw.ptr.asTypedList(raw.len).buffer;
}

RawSlice<Uint8> binIntoRawSlice(ByteBuffer bin) {
  final len = bin.lengthInBytes;
  final Pointer<Uint8> ptr = alloc(len);
  ptr.asTypedList(len).setAll(0, bin.asUint8List(len));
  return RawSlice.fromRaw(ptr, len);
}
