import 'dart:io' show Platform;
import 'dart:ffi';

class Allocator {
  Pointer Function(Pointer, int) _realloc;
  
  Allocator() {
    if (Platform.isWindows) {
      final lib = DynamicLibrary.open('kernel32.dll');
      
      final getProcessHeap = lib.lookupFunction<Pointer Function(), Pointer Function()>('GetProcessHeap');
      final heapAlloc = lib.lookupFunction<Pointer Function(Pointer, Uint32, IntPtr), Pointer Function(Pointer, int, int)>('HeapAlloc');
      final heapFree = lib.lookupFunction<Void Function(Pointer, Uint32, Pointer), void Function(Pointer, int, Pointer)>('HeapFree');
      final heapReAlloc = lib.lookupFunction<Pointer Function(Pointer, Uint32, Pointer, IntPtr), Pointer Function(Pointer, int, Pointer, int)>('HeapReAlloc');

      final heap = getProcessHeap();

      _realloc = (Pointer ptr, int len) {
        if (ptr != nullptr) {
          if (len > 0) {
            return heapReAlloc(heap, 0, ptr, len);
          }
          heapFree(heap, 0, ptr);
        } else if (len > 0) {
          return heapAlloc(heap, 0, len);
        }
        return nullptr;
      };
    } else {
      final lib = DynamicLibrary.process();
      
      _realloc = lib.lookup<NativeFunction<Pointer Function(Pointer, IntPtr)>>('realloc').asFunction();
    }
  }

  Pointer<T> alloc<T extends NativeType>([int len = 1]) {
    return realloc(nullptr, len);
  }

  void free<T extends NativeType>(Pointer<T> ptr) {
    realloc(ptr, 0);
  }

  Pointer<T> realloc<T extends NativeType>(Pointer<T> ptr, int len) {
    final int bytes = len * sizeOf<T>();
    final Pointer<T> rptr = _realloc(ptr, bytes).cast();
    if (rptr == nullptr && len > 0) {
      throw ArgumentError("Could not reallocate $bytes bytes.");
    }
    return rptr;
  }
}

final Allocator allocator = Allocator();

Pointer<T> alloc<T extends NativeType>([int len = 1]) {
  return allocator.alloc(len);
}

void free<T extends NativeType>(Pointer<T> ptr) {
  allocator.free(ptr);
}

Pointer<T> realloc<T extends NativeType>(Pointer<T> ptr, int len) {
  return allocator.realloc(ptr, len);
}
