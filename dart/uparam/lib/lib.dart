import 'dart:ffi';
import 'executor.dart';
import 'uparam.dart';

export 'executor.dart';
export 'uparam.dart';

class Lib {
  Executor executor;
  UParam uparam;
  
  Lib(String path) {
    final dylib = DynamicLibrary.open(path);
    
    executor = Executor(dylib);
    uparam = UParam(dylib);
  }
}
