#ifndef __UPAR__TYPES_H__
#define __UPAR__TYPES_H__

#include <type/ustr.h>
#include <type/urtc.h>
#include <type/unet.h>

/**
 * @brief The parameter definition fields
 */
enum {
  /**
   * @brief Set new parameter value
   */
  upar_field_set = 1 << 0,
  
  /**
   * @brief Get/has value type
   *
   * The parameters with value associated must has a type info.
   * The type info is type dependent so the parser must support corresponding type to proper operation.
   */
  upar_field_type = 1 << 1,
  /**
   * @brief Get/has sub-parameters
   *
   * Gets first and last sub-parameter ids.
   */
  upar_field_sub = 1 << 2,

  /**
   * @brief Get/has current parameter value
   */
  upar_field_get = 1 << 3,
  
  /**
   * @brief Get/has the name of parameter
   */
  upar_field_name = 1 << 4,
  /**
   * @brief Get/has the measurement unit
   */
  upar_field_unit = 1 << 5,
  /**
   * @brief Get/has the menu item name
   */
  upar_field_item = 1 << 6,
  /**
   * @brief Get/has the description info
   */
  upar_field_info = 1 << 7,
  /**
   * @brief Get/has the hint text field
   */
  upar_field_hint = 1 << 8,

  /**
   * @brief Get all text fields
   */
  upar_field_text = upar_field_name | upar_field_info | upar_field_unit | upar_field_item | upar_field_hint,

  /**
   * @brief Get/has default value field
   */
  upar_field_def = 1 << 9,
  /**
   * @brief Get/has minimum value field
   */
  upar_field_min = 1 << 10,
  /**
   * @brief Get/has maximum value field
   */
  upar_field_max = 1 << 11,
  /**
   * @brief Get/has step value field
   */
  upar_field_stp = 1 << 12,
  /**
   * @brief Get/has optional values fields
   */
  upar_field_opt = 1 << 13,

  /**
   * @brief Get all constraint fields
   */
  upar_field_cons = upar_field_def | upar_field_min | upar_field_max | upar_field_stp | upar_field_opt,

  /**
   * @brief Get/has preferred polling interval field
   *
   * Polling interval measures in milliseconds.
   */
  upar_field_poll = 1 << 14,
};

/**
 * @brief The parameter bit-field type
 */
typedef unum_uint_t upar_query_t;

/**
 * @brief The parameter identifier type
 */
typedef unum_uint_t upar_id_t;

/**
 * @brief The parameter unique identifier type
 */
typedef uint32_t upar_uid_t;

/**
 * @brief The parameter pointer type
 */
typedef void* upar_ptr_t;

/**
 * @brief The parameter length type
 */
typedef ulen_t upar_len_t;

/**
 * @brief The parameter types
 */
enum {
  /**
   * @brief The unsigned integer number
   */
  upar_type_uint = 1,
  /**
   * @brief The signed integer number
   */
  upar_type_sint = 2,
  /**
   * @brief The unsigned fixed point number
   */
  upar_type_ufix = 3,
  /**
   * @brief The signed fixed point number
   */
  upar_type_sfix = 4,
  /**
   * @brief The floating point number
   */
  upar_type_real = 5,
  /**
   * @brief The string data
   */
  upar_type_cstr = 6,
  /**
   * @brief The binary data
   */
  upar_type_hbin = 7,

  /**
   * @brief The time
   *
   * Consists of three values:
   * * Hour (0-23)
   * * Minute (0-59)
   * * Second (0-59)
   */
  upar_type_time = 8,
  /**
   * @brief The date
   *
   * Consists of three values:
   * * Year (0-65535)
   * * Month (1-12)
   * * Day of month (1-31)
   */
  upar_type_date = 9,
  
  /**
   * @brief The MAC address (aa:bb:cc:dd:ee:ff)
   */
  upar_type_mac = 10,
  
  /**
   * @brief The IP v4 address (www.xxx.yyy.zzz)
   */
  upar_type_ipv4 = 11,

  /**
   * @brief The IP v6 address (1111:2222:3333:4444:5555:6666:7777:8888)
   */
  upar_type_ipv6 = 12,
  
  /**
   * @brief The type mask
   */
  upar_type_mask = 0x0f,
};

#define upar_type(type) ((type) & upar_type_mask)

/**
 * @brief The parameter value flags
 */
enum {
  /**
   * @brief The value can be read
   */
  upar_getable = 1 << 4,
  /**
   * @brief The value can be written
   */
  upar_setable = 1 << 5,
  /**
   * @brief The value do not lost when power off
   */
  upar_persist = 1 << 6,
  /**
   * @brief Valid value should be equal to one of options
   */
  upar_options = 1 << 7,

  upar_flag_mask = 0xf0,

  upar_extra_off = 8,
};

#define upar_extra(type) ((type) >> upar_extra_off)

enum {
  upar_none = 0,
};

#endif /* __UPAR__TYPES_H__ */
