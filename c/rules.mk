param.BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
param.SRCDIR := $(param.BASEPATH)src

# library, definition path -> file path
param.GEN_P = $(if $(2),$(addprefix $(if $(1),gen/$(1)/),$(if $(2),$(addprefix upar/$(notdir $(basename $(2))).,h c))),$(if $(1),gen/$(1)/))
param.GEN = NODE_PATH=$(param.SRCDIR) $(param.SRCDIR)/codegen.js $(1) $(2) $(3)

# bootstrap codegen
$(param.BASEPATH)/node_modules:
	@echo PARAM PREPARE
	$(Q)cd $(dir $@) && npm install

# library, config
define PAR_RULE
ifndef param.$(1).$(2)
param.$(1).$(2) := $(call param.GEN_P,$(1),$(2))
$$(param.$(1).$(2)): $(2) $(param.BASEPATH)/node_modules
	@echo TARGET $(1) PARAM $(2) GEN
	$(Q)mkdir -p $$(dir $$@)
	$(Q)$$(call param.GEN,$$<,$(call param.GEN_P,$(1)),$$(call param.GEN_P,,$(2)))
$(1).CDIRS += $$(dir $(call param.GEN_P,$(1)))
build.param: $$(param.$(1).$(2))
$$($(1).SRCS): $$(param.$(1).$(2))
$(1).SRCS += $$(filter %.c,$$(param.$(1).$(2)))
clean: clean.param.$(1).$(2)
clean.param.$(1).$(2):
	@echo TARGET $(1) PARAM $(2) CLEAN
	$(Q)rm -f $$(param.$(1).$(2))
endif
endef

# library
define PAR_RULES
ifneq (,$($(1).PARS))
$(foreach f,$($(1).PARS),
$(call PAR_RULE,$(1),$(f)))
endif
endef

TARGET.LIBS += libparam
libparam.INHERIT ?= firmware
libparam.CDIRS := $(param.BASEPATH)include

libparamclient.CDIRS := $(param.BASEPATH)include
libparamclient.SRCS := $(param.BASEPATH)src/client.c
