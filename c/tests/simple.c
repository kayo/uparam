#include <uparam/simple.h>
#include "test.h"

uint32_t fixed_val;

#define test_pre(INFO, QUERY, ID)               \
  ustr_extern_init(&req, req_buf);              \
  ustr_extern_init(&res, res_buf);              \
                                                \
  info(INFO " for param: " #ID);                \
                                                \
  const upar_query_t query = QUERY;             \
  ustr_putuv(&req, query);                      \
                                                \
  const upar_id_t id = ID;                      \
  ustr_putuv(&req, id)

#define test_pre_def(INFO, ID, FIELDS)          \
  test_pre("Get definition of " INFO,           \
           FIELDS, ID);                         \
                                                \
  upar__proc(&req, &res)

#define test_fields_get()                       \
  upar_query_t fields;                          \
  cond(ustr_getuv(&res, &fields),               \
       >=, 0, "%d",                             \
       "No fields in response")

#define test_fields_is(FIELDS)                  \
  cond(fields, ==, FIELDS, "0x%x",              \
       "Mismatch fields in response")

#define test_type_get()                         \
  upar_query_t rtype;                           \
  cond(ustr_getuv(&res, &rtype),                \
       >, 0, "%d",                              \
       "No type and flags in response")

#define test_type_is(TYPE, FLAGS, EXTRA)        \
  cond(rtype, ==, TYPE | FLAGS |                \
       (EXTRA << upar_extra_off), "0x%x",       \
       "Mismatch type or flags in response")

/* test type definition */
#define test_def_type(ID, TYPE, FLAGS, EXTRA, ...) \
  {                                                \
    test_pre_def("type", ID, upar_field_type);     \
                                                   \
    test_fields_get() {                            \
      test_fields_is(TYPE ? upar_field_type :      \
                     upar_none) {                  \
        if (!TYPE) {                               \
          pass();                                  \
        } else {                                   \
          test_type_get() {                        \
            test_type_is(TYPE, FLAGS, EXTRA) {     \
              pass();                              \
            }                                      \
          }                                        \
        }                                          \
      }                                            \
    }                                              \
  }

/* test text fields */
#define test_def_txf(ID, FIELDS, ...)           \
  {                                             \
    const char *text_fields[] = {               \
      __VA_ARGS__                               \
    };                                          \
                                                \
    test_pre_def("text fields", ID,             \
                 upar_field_text);              \
                                                \
    test_fields_get() {                         \
      test_fields_is(FIELDS) {                  \
        uint8_t i = 0;                          \
        for (; i < sizeof(text_fields) /        \
               sizeof(text_fields[0]); i++) {   \
          unum_uint_t len;                      \
          cond(ustr_getuv(&res, &len),          \
               >, 0, "%d",                      \
               "Unable to get text length") {   \
            cond(len, ==,                       \
                 (unsigned)                     \
                 strlen(text_fields[i]),        \
                 "%u", "Mismatch text len") {   \
              cond(memcmp(text_fields[i],       \
                          ustr_beg(&res), len), \
                   ==, 0,                       \
                   "%d", "Mismatch text") {     \
                ustr_pos(&res) += len;          \
                continue;                       \
              }                                 \
            }                                   \
          }                                     \
          break;                                \
        }                                       \
        if (i == sizeof(text_fields) /          \
            sizeof(text_fields[0])) {           \
          pass();                               \
        }                                       \
      }                                         \
    }                                           \
  }

/* test sub params */
#define test_def_sub(ID, FST_ID, LST_ID) {         \
    test_pre_def("sub-parameters", ID,             \
                 upar_field_sub);                  \
                                                   \
    test_fields_get() {                            \
      if (FST_ID == upar_none &&                   \
          LST_ID == upar_none) {                   \
        test_fields_is(upar_none) {                \
          pass();                                  \
        }                                          \
      } else { /* has children */                  \
        test_fields_is(upar_field_sub) {           \
          upar_id_t fst_id;                        \
          cond(ustr_getuv(&res, &fst_id), >,       \
               0, "%d", "No first param id") {     \
            cond(fst_id, ==, FST_ID, "%u",         \
                 "Mismatch first param id") {      \
              upar_id_t lst_id;                    \
              cond(ustr_getuv(&res, &lst_id), >,   \
                   0, "%d", "No last param id") {  \
                cond(lst_id, ==, LST_ID, "%u",     \
                     "Mismatch last param id") {   \
                  pass();                          \
                }                                  \
              }                                    \
            }                                      \
          }                                        \
        }                                          \
      }                                            \
    }                                              \
  }

static void ident(ulen_t lvl) {
  for (; lvl; lvl--) {
    putchar(' ');
    putchar(' ');
  }
}

static void show_tree(ulen_t lvl, upar_id_t id) {
  char req_buf[256], res_buf[256];
  ustr_t req, res;

  ustr_extern_init(&req, req_buf);
  ustr_extern_init(&res, res_buf);

  ustr_putuv(&req, upar_field_sub | upar_field_name);
  ustr_putuv(&req, id);

  upar__proc(&req, &res);
  
  upar_query_t fields;
  ustr_getuv(&res, &fields);

  upar_id_t fst_id, lst_id;
  if (fields & upar_field_sub) {
    ustr_getuv(&res, &fst_id);
    ustr_getuv(&res, &lst_id);
  }

  if (id != upar_none) {
    ident(lvl);
    if (fields & upar_field_name) {
      char name[32];
      ustr_getsl0(&res, name, 32);
      printf("#%u '%s'\n", id, name);
    } else {
      printf("#%u\n", id);
    }
  }
  
  if (fields & upar_field_sub
      /*par.fst != upar_none &&
        par.lst != upar_none*/) {
    upar_id_t sub_id = fst_id;
    for (; sub_id <= lst_id; sub_id++) {
      show_tree(lvl + 1, sub_id);
    }
  }
}

int main(void) {
  printf("sizeof(upar_query_t) = %u\n",
         (unsigned)sizeof(upar_query_t));
  printf("sizeof(upar_id_t) = %u\n",
         (unsigned)sizeof(upar_id_t));

  upar__init();

  char req_buf[256], res_buf[256];
  ustr_t req, res;
  
  test_def_sub(upar_none,
               upar__constant,
               upar__group);
  test_def_sub(upar__group,
               upar__group__param,
               upar__group__subgroup);
  test_def_sub(upar__group__subgroup,
               upar__group__subgroup__subparam,
               upar__group__subgroup__subparam);
  test_def_sub(upar__immutable,
               upar_none,
               upar_none);
  test_def_sub(upar__group__param,
               upar_none,
               upar_none);
  test_def_sub(upar__group__subgroup__subparam,
               upar_none,
               upar_none);

  test_def_type(upar__constant, upar_type_uint, upar_none, 8);
  test_def_type(upar__immutable, upar_type_sint, upar_getable, 16);
  test_def_type(upar__variable, upar_type_real, upar_getable | upar_setable, 32);
  test_def_type(upar__group__param, upar_type_cstr, upar_none, 19);

  test_def_txf(upar__constant,
               upar_field_name | upar_field_info,
               "constant", "Constant parameter");
  test_def_txf(upar__immutable,
               upar_field_name | upar_field_info | upar_field_unit,
               "immutable", "°C", "Getable parameter");

  show_tree(0, upar_none);
  
  return 0;
}
