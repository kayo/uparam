import { uparServer } from "param";

const byte = {
  bits: 8,
};

const half = {
  bits: 16,
};

const word = {
  bits: 32,
} as { bits: 32 };

const q16 = {
  frac: 16,
};

const sens = {
  getable: true,
};

const pref = {
  setable: true,
  persist: true,
  ...sens,
};

const often = {
  poll: 1000,
};

const rarely = {
  poll: 10000,
};

const temp = {
  unit: "°C",
};

const mode = {
  type: "uint",
  opt: [{
    val: 0,
    name: "off",
    info: "Disabled"
  }, {
    val: 1,
    name: "on",
    info: "Enabled"
  }, {
    val: 3,
    name: "auto",
    info: "Automatic"
  }],
  ...byte
};

uparServer({
  name: '',

  includes: [
    "test.h",
    "simple.h",
  ],

  storage: {
    backend: 'simulator',
    uidType: 'djb2',
  },

  params: [
    {
      name: 'constant',
      type: 'uint',
      bits: 8,
      def: 25,
      bind: '', //true,
      info: "Constant parameter",
    },

    {
      name: 'immutable',
      type: 'sint',
      bits: 16,
      def: -54,
      info: "Getable parameter",
      ...sens, ...temp
    },

    {
      name: 'variable',
      type: 'real',
      ...word, ...rarely,
      def: -0.123456,
      min: -1.5,
      max: 1.5,
      stp: 0.1,
      getable: true,
      setable: true,
      //info: "Setable parameter",
      info: "Масштабируемый параметр",
    },

    {
      name: 'string',
      type: 'cstr',
      size: 30,
      ...pref,
      def: "This is a string value.",
      info: "Persistent parameter",
    },

    {
      name: 'binded',
      type: 'ufix',
      def: 0.123456,
      min: 0.5,
      max: 1.5,
      stp: 0.1,
      bind: 'fixed_val',
      info: "Binded parameter",
      ...word, ...q16, ...pref,
      ...often,
    },

    {
      name: 'ctrl',
      params: [
        {
          name: 'heater_mode',
          info: "The heater control",
          ...mode,
        },
        {
          name: 'cooler_mode',
          info: "The cooler control",
          ...mode,
        },
      ],
    },

    {
      name: 'group',
      params: [
        {
          name: 'param',
          type: 'cstr',
          size: 20,
          bind: '', // true
        },

        {
          name: 'subgroup',
          params: [
            {
              name: 'subparam',
              type: 'sint',
              ...half,
            },
          ],
        },
      ],
    },
  ],
});
