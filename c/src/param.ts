import { join } from "path";
import { writeFileSync } from "fs";

import { UParDecl, UParServer, UParGroup, UParTyped, UParNumCons, UParOpt, UParData, UParText, uparTextFields, uparConsFields, UParDef, uparTypeCreate } from "./types";
import "./types/num";
import "./types/str";
import "./types/tim";
import "./types/net";
import { storage } from "./flash";
import { CodeGen } from "./coder";
import { c_name, mk_name, find_arg, args_to_attrs, put_once, put_include, put_try, put_if, put_switch, put_case, put_block } from "./utils";
import { genUidByType, GenUidFunc } from "./genid";

function enumerate(params: UParDecl[], fn: (param: UParDecl, path: string, id: number) => void, parent: string = "", id: number = 1) {
  for (const param of params) {
    fn(param, `${parent}${param.name || ""}`, id++);
  }

  for (const param of params) {
    const group = param as UParGroup;
    if (group.params) {
      const prefix = `${parent}${param.name || ""}`;
      id = enumerate(group.params, fn, prefix.length ? `${prefix}.` : "", id);
    }
  }

  return id;
}

function param_by_path(params: UParDef[], path: string): UParDef {
  for (let param of params) {
    if (param.path == path) {
      return param;
    }
  }

  throw `Something went wrong when searching parameter by path: ${path}`;
}

function has_field(params: UParDef[], field: string): boolean {
  for (let param of params) {
    const { decl } = param;
    if (field in decl) {
      return true;
    }
  }
  return false;
}

export function uparServer(spec: UParServer): void {
  const args = process.argv.slice(3);

  const dir = find_arg(args, (arg) => !/\.[hc]/.test(arg)) || "";
  const fn_h = find_arg(args, (arg) => /\.h$/.test(arg));
  const fn_c = find_arg(args, (args) => /\.c$/.test(args));

  let gen_uid: GenUidFunc | undefined;

  const params: UParDef[] = [];
  enumerate(spec.params, (decl, path, id) => {
    const typed = decl as UParTyped;
    const name = typed.bind ? typed.bind : c_name(path);
    const def: UParDef = { id, path, name, decl };

    if (typed.type) {
      def.type = uparTypeCreate(decl as UParTyped);
    }

    if (typed.persist) {
      if (!gen_uid) {
        if (!spec.storage) {
          throw "The storage configuration required for persistent parameters.";
        }
        gen_uid = genUidByType(spec.storage.uidType);
      }
      def.uid = gen_uid(name);
    }

    params.push(def);
  });

  const root_params: UParDef[] = [{ id: 0, path: "", name: "", decl: spec as UParDecl }, ...params];

  /* generate C header */
  if (fn_h) {
    const cg_h = new CodeGen();

    put_once(fn_h, cg_h, (cg) => {
      if (spec.includes) {
        for (const include of spec.includes) {
          put_include(cg, include);
        }
      }
      cg.space();
      put_include(cg, "upar/types.h");
      cg.space();
      cg.line("enum {", "The enumeration of parameters");
      cg.block((cg: CodeGen) => {
        for (let param of params) {
          const { id, path } = param;
          const name = c_name(path);
          cg.line(`${mk_name(name, spec.name)} = ${id},`);
        }
      });
      cg.line("};");
      cg.space();
      cg.comment("The parameter variables");
      for (let param of params) {
        const { type, name, decl } = param;
        const { bind } = decl as UParTyped;
        if (type && !bind) {
          type.defVar(cg, name, { extern: true });
        }
      }
      cg.space();
      cg.line(`void ${mk_name('init', spec.name)}(void);`,
        "Initialize parameters");
      cg.space();
      cg.line(`int ${mk_name('proc', spec.name)}(ustr_t * req, ustr_t * res);`,
        "Process parameter request");
      cg.space();
      cg.line(`int ${mk_name('proc_all', spec.name)}(ustr_t * req, ustr_t * res);`,
        "Process all parameter requests sequentially");
    });

    writeFileSync(join(dir, fn_h), cg_h.source());
  }

  if (fn_c) {
    /* generate C source */
    const cg_c = new CodeGen();

    ((cg) => {
      if (fn_h) {
        put_include(cg, fn_h);
      }
      put_include(cg, `string.h`);
      cg.space();
      cg.comment("The parameter variables");
      for (let param of params) {
        const { name, type, decl } = param;
        const { bind } = decl as UParTyped;
        if (type && !bind) {
          type.defVar(cg, name, {});
        }
      }
      cg.space();
      if (spec.storage) {
        storage(cg, spec.storage);
      }
      cg.space();
      cg.line(`void ${mk_name('init', spec.name)}(void) {`,
        "Initialize parameters");
      cg.block((cg) => {
        if (has_field(params, "persist")) {
          cg.line("int res;", "The result of operation");
        }
        for (let param of params) {
          const { uid, name, type, decl } = param;
          if (type) {
            const { persist, preSet, postSet, bind } = decl as UParTyped;
            const put_cb = (cg: CodeGen, cb?: string, cmt?: string) => {
              if (cb) {
                cg.line(cb.replace(/<(var)>/g,
                  (_, sub) => sub == 'var' ? name : ""),
                  cmt);
              }
            };
            const { def } = decl as UParNumCons;
            const load_def = (cg: CodeGen) => {
              if (typeof bind == "undefined" || typeof bind == "string") {
                type.setVar(cg, name, type.putVal(`${def}`), { const: true });
              }
            };

            if (persist && uid) {
              put_cb(cg, preSet, "Before set the value");
              type.loadVar(cg, uid, name);
              if (def) {
                put_if(cg, "res < 0", load_def);
              }
              put_cb(cg, postSet, "After set the value");
            } else if (def) {
              put_cb(cg, preSet, "Before set the value");
              load_def(cg);
              put_cb(cg, postSet, "After set the value");
            }
          }
        }
      });
      cg.line("}");
      if (has_field(params, "setable") && has_field(params, "persist")) {
        cg.space();
        cg.line("static void upar_store(upar_id_t id) {",
          "Store parameter value");
        cg.block((cg) => {
          cg.line("int res;");
          put_switch(cg, "id", (cg) => {
            for (let param of root_params) {
              const { id, uid, name, type, decl } = param;
              const { setable, persist } = decl as UParTyped;
              if (type && setable && persist && uid) {
                put_case(cg, [`${id}`], (cg) => {
                  type.saveVar(cg, uid, name);
                });
              }
            }
            cg.line("default: res = 0;");
          });
          put_if(cg, "res < 0", (cg) => {
            cg.line("upar_drop();");
            for (let param of root_params) {
              const { uid, name, type, decl } = param;
              const { setable, persist } = decl as UParTyped;
              if (type && setable && persist && uid) {
                type.saveVar(cg, uid, name);
              }
            }
          });
        });
        cg.line("}");
      }
      cg.space();
      cg.line("#define upar_try(m) if ((m) < 0) { return -1; }");
      cg.space();
      cg.line(`int ${mk_name('proc', spec.name)}(ustr_t *req, ustr_t *res) {`,
        "Process parameter request");
      cg.block((cg) => {
        cg.line("upar_query_t query;", "Request query");
        put_try(cg, "ustr_getuv(req, &query)", "Get query");
        cg.space();
        cg.line("upar_id_t id;", "Parameter id");
        put_try(cg, "ustr_getuv(req, &id)", "Get parameter id");
        cg.space();

        cg.line("upar_query_t fields = upar_none;", "Fields of parameter");
        const ids_by_fields: { [key: string]: { paths: string[], ids: string[] } } = {};
        for (let param of root_params) {
          const { id, path, type, decl } = param;
          const { getable, setable, poll } = decl as UParTyped;
          let fields = "";
          const add_field = (name: string) =>
            fields = fields.length
              ? `${fields} | upar_field_${name}`
              : `upar_field_${name}`;

          if (type) add_field("type");
          if ((decl as UParGroup).params) add_field("sub");
          if (getable) add_field("get");
          if (setable) add_field("set");
          if (poll) add_field("poll");

          const text = decl as UParText;
          for (let field of uparTextFields) {
            if (field in text) {
              add_field(field);
            }
          }

          const cons = decl as UParNumCons;
          for (let field of uparConsFields) {
            if (field in cons) {
              if (field != 'opt' || (cons.opt as UParOpt[]).length) {
                add_field(field);
              }
            }
          }

          if (fields.length) {
            const { ids, paths } = ids_by_fields[fields] || (ids_by_fields[fields] = { paths: [], ids: [] });
            ids.push(`${id}`);
            paths.push(path);
          }
        }

        put_switch(cg, "id", (cg) => {
          for (const fields in ids_by_fields) {
            const { ids, paths } = ids_by_fields[fields];
            put_case(cg, ids, (cg) => {
              cg.line(`fields = ${fields};`);
            }, `For the parameters: ${paths.join(", ")}`);
          }
        }, "Get fields of parameter");

        cg.space();
        cg.line("fields &= query;", "Remove empty fields");
        put_try(cg, "ustr_putuv(res, fields)", "Put available fields");

        if (has_field(params, "setable")) {
          cg.space();
          put_if(cg, "fields & upar_field_set", (cg) => {
            put_switch(cg, "id", (cg) => {
              for (let param of params) {
                const { id, type, name, decl } = param;
                const { setable, persist, preSet, set, postSet } = decl as UParTyped;
                const put_cb = (cg: CodeGen, cb?: string, cmt?: string) => {
                  if (cb) {
                    cg.line(cb.replace(/<(var|req)>/g,
                      (_, sub) => sub == 'var' ? name : "req"),
                      cmt);
                  }
                };
                if (setable) {
                  put_case(cg, [`${id}`], (cg) => {
                    put_cb(cg, preSet, "Before set the value");
                    if (set !== undefined) {
                      put_cb(cg, set, "Set the value");
                    } else if (type) {
                      type.getVar(cg, name);
                    }
                    put_cb(cg, postSet, "After set the value");
                    if (persist) {
                      cg.line(`upar_store(${id});`);
                    }
                  });
                }
              }
            });
          }, "Parameter setting query");
        }

        cg.space();
        /* parameter type */
        put_if(cg, "fields & upar_field_type", (cg) => {
          cg.line("upar_query_t type = upar_none;");
          cg.space();
          put_switch(cg, "id", (cg) => {
            for (let param of params) {
              const { id, path, type, decl } = param;
              if (type) {
                const typed = decl as UParTyped;
                let flags = `upar_type_${typed.type}`;
                const add_flag = (name: string) => flags += ` | upar_${name}`;
                if (typed.getable) add_flag("getable");
                if (typed.setable) add_flag("setable");
                if (typed.persist) add_flag("persist");
                if (typed.options) add_flag("options");
                const extra = type.typeDef();
                if (extra != "") {
                  flags += ` | ((uint32_t)(${extra}) << upar_extra_off)`;
                }

                put_case(cg, [`${id}`], (cg) => {
                  cg.line(`type = ${flags};`);
                }, `For the parameter: ${path}`);
              }
            }
          }, "Get the type of parameter");
          cg.space();
          put_try(cg, `ustr_putuv(res, type)`,
            "Put the type definition");
        });

        cg.space();
        /* sub-parameters */
        put_if(cg, "fields & upar_field_sub", (cg) => {
          put_switch(cg, "id", (cg) => {
            for (let param of root_params) {
              const { id, path, decl } = param;
              const prefix = path.length ? `${path}.` : "";
              const group = decl as UParGroup;
              if (group.params && group.params.length) {
                put_case(cg, [`${id}`], (cg) => {
                  const { id: fst_id } = param_by_path(params, `${prefix}${group.params[0].name}`);
                  const { id: lst_id } = param_by_path(params, `${prefix}${group.params[group.params.length - 1].name}`);
                  put_try(cg, `ustr_putuv(res, ${fst_id})`);
                  put_try(cg, `ustr_putuv(res, ${lst_id})`);
                }, `For the parameter: ${path}`);
              }
            }
          }, "Get the range of sub-parameters");
        });

        /* parameter polling interval */
        if (has_field(params, "poll")) {
          cg.space();
          put_if(cg, "fields & upar_field_poll", (cg) => {
            put_switch(cg, "id", (cg) => {
              for (const param of params) {
                const { id, decl } = param;
                const { poll } = decl as UParTyped;
                if (poll) {
                  put_case(cg, [`${id}`], (cg) => {
                    put_try(cg, `ustr_putuv(res, ${poll})`);
                  });
                }
              }
            });
          }, "Get polling interval of parameter");
        }

        /* parameter value */
        if (has_field(params, "getable")) {
          cg.space();
          put_if(cg, "fields & upar_field_get", (cg) => {
            put_switch(cg, "id", (cg) => {
              for (const param of params) {
                const { id, type, name, decl } = param;
                const { getable, preGet, get, postGet } = decl as UParTyped;
                const put_cb = (cg: CodeGen, cb?: string, cmt?: string) => {
                  if (cb) {
                    cg.line(cb.replace(/<(var|res)>/g,
                      (_, sub) => sub == 'var' ? name : "res"),
                      cmt);
                  }
                };
                if (getable) {
                  put_case(cg, [`${id}`], (cg) => {
                    put_cb(cg, preGet, "Before get the value");
                    if (get !== undefined) {
                      put_cb(cg, get, "Get the value");
                    } else if (type) {
                      type.putVar(cg, name);
                    }
                    put_cb(cg, postGet, "After get the value");
                  });
                }
              }
            });
          }, "Get the value of parameter");
        }

        /* text fields */
        for (const key of uparTextFields) {
          if (!has_field(params, key)) continue;
          cg.space();
          put_if(cg, `fields & upar_field_${key}`, (cg) => {
            cg.line("ulen_t len = 0;");
            cg.line("const char *str;");
            cg.space();
            put_switch(cg, "id", (cg) => {
              for (let param of params) {
                const { id, decl } = param;
                const val = decl[key];
                if (typeof val != "string") continue;
                put_case(cg, [`${id}`], (cg) => {
                  const str = JSON.stringify(val);
                  cg.line(`len = sizeof(${str}) - 1;`);
                  cg.line(`str = ${str};`);
                });
              }
            }, `Put text field: ${key}`);
            cg.space();
            put_try(cg, "ustr_putsl(res, str, len)");
          });
        }

        /* value fields */
        for (const key of uparConsFields) {
          if (!has_field(params, key)) continue;
          if (key == 'opt') {
            cg.space();
            cg.line("upar_query_t opt_fields;", "Fields of option");
          }
          cg.space();
          put_if(cg, `fields & upar_field_${key}`, (cg) => {
            put_switch(cg, "id", (cg) => {
              for (const param of params) {
                const { id, type, decl } = param;
                const cons = decl as UParNumCons;
                if (cons[key] === undefined || !type ||
                  (key == 'opt' && (cons.opt as UParOpt[]).length == 0)) continue;
                put_case(cg, [`${id}`], (cg) => {
                  if (key == 'opt') {
                    const opts: UParOpt[] = cons.opt as UParOpt[];
                    cg.line(`opt_fields = ${opts.length};`, "Set the number of options");
                    put_try(cg, "ustr_putuv(res, opt_fields)", "Put the number of options");
                    for (let opt of opts) {
                      const text = opt as UParText;
                      let fields = "upar_field_opt";
                      for (let field of uparTextFields) {
                        const val = text[field];
                        if (typeof val != "string") continue;
                        fields += ` | upar_field_${field}`;
                      }
                      cg.line(`opt_fields = (${fields}) & query;`, "Set the option fields");
                      put_try(cg, "ustr_putuv(res, opt_fields)", "Put the option fields");
                      put_block(cg, (cg) => {
                        const val: UParData = opt.val;
                        type.defVar(cg, "val", { const: true }, type.putVal(`${val}`));
                        type.putVar(cg, "val");
                      }, "Put the option value");
                      for (let field of uparTextFields) {
                        const val = text[field];
                        if (typeof val != "string") continue;
                        const str = JSON.stringify(val);
                        put_if(cg, `opt_fields & upar_field_${field}`, (cg) => {
                          put_try(cg, `ustr_putsl(res, ${str}, sizeof(${str}) - 1)`);
                        }, `Put the option text field: ${field}`);
                      }
                    }
                  } else {
                    const val: UParData = cons[key] as UParData;
                    type.defVar(cg, "val", { const: true }, type.putVal(`${val}`));
                    type.putVar(cg, "val");
                  }
                });
              }
            });
          });
        }

        cg.space();
        cg.line("return 0;");
      });
      cg.line("}");

      cg.space();
      cg.line(`int ${mk_name('proc_all', spec.name)}(ustr_t * req, ustr_t * res)`,
        "Process all parameter requests sequentially");
      cg.include(`${__dirname}/proc_all.c`, {
        proc: mk_name('proc', spec.name)
      });
    })(cg_c);

    writeFileSync(join(dir, fn_c), cg_c.source());
  }
}

export function uparAttrs<Attrs extends Object>(defaults: Attrs): Attrs {
  return {
    ...(defaults as any),
    ...args_to_attrs(process.argv.slice(3)),
  };
}
