#define store_page 1024
#define store_align 4

static struct {
  uint8_t start[0];
  uint8_t data[store_page];
  uint8_t end[0];
} _store;

static const addr_t store_start = (addr_t)_store.start;
static const addr_t store_end = (addr_t)_store.end;

static inline void store_lock(void) {}
static inline void store_unlock(void) {}

static const uint8_t free_byte = ~0;

static char _store_init = 0;
static void store_init(void) {
  if (!_store_init) {
    _store_init = 1;
    memset(_store.start, free_byte, _store.end - _store.start);
  }
}

static void store_erase(addr_t addr, upar_len_t len) {
  memset((void*)(addr & ~(store_page-1)), free_byte, len);
}

static void store_read(addr_t addr, upar_ptr_t ptr, upar_len_t len) {
  store_init();
  memcpy(ptr, (const void*)addr, len);
}

static void store_write(addr_t addr, const upar_ptr_t ptr, upar_len_t len) {
  store_init();
  {
    const uint8_t *mem = (const void*)addr, *end = (const void*)(addr + len);
    for (; mem < end; ) {
      if (*mem++ != free_byte) {
        return;
      }
    }
  }
  memcpy((void*)addr, ptr, len);
}
