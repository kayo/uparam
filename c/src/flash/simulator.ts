import { CodeGen } from "../coder";
import { uparStorageReg, UParStorageConfig } from "../types";

uparStorageReg("simulator", (cg: CodeGen, _config: UParStorageConfig) => {
  cg.include(`${__dirname}/simulator.h`);
});
