import { CodeGen } from "../coder";
import { uparStorageReg, UParStorageConfig } from "../types";

uparStorageReg("mcuhw", (cg: CodeGen, _config: UParStorageConfig) => {
  cg.include(`${__dirname}/mcuhw.h`);
});
