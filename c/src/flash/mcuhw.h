#include HWDEF_HEADER

#define store_align FLASH_WORD_SIZE

#define HWDEF_REGION_(region)              \
  static const addr_t store_start =        \
    (addr_t)&_##region##_start;            \
  static const addr_t store_end =          \
    (addr_t)&_##region##_end

#define HWDEF_REGION(region)               \
  HWDEF_REGION_(region)

HWDEF_REGION(HWDEF_STORE);

static inline void store_lock(void) {
  flash_lock((void*)store_start);
}

static inline void store_unlock(void) {
  flash_unlock((void*)store_start);
}

static inline void store_erase(addr_t addr, upar_len_t len) {
  flash_erase((void*)addr, len);
}

static void store_read(addr_t addr, upar_ptr_t ptr, upar_len_t len) {
  memcpy(ptr, (const void*)addr, len);
}

static void store_write(addr_t addr, const upar_ptr_t ptr, upar_len_t len) {
  flash_write((void*)addr, ptr, len);
}
