import { CodeGen } from "./coder";

import { uparStorageGen, UParStorage } from "./types";
import "./flash/simulator";
import "./flash/mcuhw";
//import "./flash/opencm3-stm32";
//import "./flash/esp8266-spi";

export function storage(cg: CodeGen, config: UParStorage) {
  cg.include(`${__dirname}/flash.h`);
  uparStorageGen(cg, config);
  cg.include(`${__dirname}/flash.c`);
}
