#!/usr/bin/env node

require("ts-node").register({
  compilerOptions: {
    alwaysStrict: true,
    diagnostics: true,
    target: "es5",
    module: "commonjs",
    moduleResolution: "node",
    noImplicitAny: true,
    noImplicitReturns: true,
    noImplicitThis: true,
    noUnusedLocals: true,
    noUnusedParameters: true,
    preserveConstEnums: true,
    pretty: true,
    removeComments: false,
    strictNullChecks: true,
    sourceMap: true,
    baseUrl: __dirname,
    typeRoots: [
      __dirname + "/../node_modules/@types"
    ]
  }
});

require(require("path").join(process.cwd(), process.argv[2]));
