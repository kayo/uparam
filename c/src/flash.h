typedef uintptr_t addr_t;

static const upar_uid_t free_uid = ~0;

typedef struct {
  upar_uid_t uid;
  upar_len_t off;
  upar_len_t len;
} store_cell_t;

/*
 * Storage organization:
 *
 *      Start
 *    +-------+  <---+
 *    | Data0 |      |
 *    +-------+      |  .off
 *    | Data1 |      |<----------+
 *    +-------+      |           |
 *    ~  ...  ~      |           |
 *    +-------+  <---+  .len     |
 *    | DataN |      |<------+   |
 *    +-------+  <---+       |   |
 *    |       |              |   |
 *    ~ Free  ~              |   |
 *    ~ Space ~              |   |
 *    |       |              |   |
 *    +-------+              |   |
 *    | CellN |  ============+---+
 *    +-------+
 *    ~  ...  ~
 *    +-------+
 *    | Cell1 |
 *    +-------+
 *    | Cell0 |
 *    +-------+
 *       End
 */
