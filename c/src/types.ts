import { CodeGen } from "./coder";
import { GenUidType } from "./genid";

export type UParTextField = 'name' | 'unit' | 'item' | 'info' | 'hint';
export const uparTextFields: UParTextField[] = ["name", "unit", "item", "info", "hint"];

export interface UParText {
  name?: string;
  info?: string;
  unit?: string;
  item?: string;
  hint?: string;
}

export type UParData = string | number | boolean;

export type UParConsField = 'def' | 'min' | 'max' | 'stp' | 'opt';
export const uparConsFields: UParConsField[] = ["def", "min", "max", "stp", "opt"];

export interface UParOpt extends UParText {
  val: UParData;
}

export interface UParCons {
  def?: UParData;
  opt?: UParOpt[];
}

export interface UParNumCons extends UParCons {
  min?: UParData;
  max?: UParData;
  stp?: UParData;
}

export interface UParStrCons extends UParCons { }

export interface UParEmpty extends UParText { }

export interface UParGroup extends UParText {
  params: UParDecl[];
}

export interface UParVal {
  bind?: string;
  getable?: boolean;
  setable?: boolean;
  persist?: boolean;
  options?: boolean;
  get?: string;
  set?: string;
  preGet?: string;
  postGet?: string;
  preSet?: string;
  postSet?: string;
  poll?: number;
}

export interface UParNum extends UParText, UParNumCons, UParVal { }

export interface UParInt extends UParNum {
  bits: 8 | 16 | 32 | 64;
}

export interface UParUint extends UParInt {
  type: 'uint';
}

export interface UParSint extends UParInt {
  type: 'sint';
}

export interface UParFix extends UParInt {
  frac: number;
}

export interface UParUfix extends UParFix {
  type: 'ufix';
}

export interface UParSfix extends UParFix {
  type: 'sfix';
}

export interface UParReal extends UParNum {
  type: 'real';
  bits: 32 | 64;
}

export interface UParStr extends UParText, UParStrCons, UParVal {
  size: number;
}

export interface UParCstr extends UParStr {
  type: 'cstr';
}

export interface UParHbin extends UParStr {
  type: 'hbin';
}

export interface UParTime extends UParStr {
  type: 'time';
}

export interface UParDate extends UParStr {
  type: 'date';
}

export interface UParMac extends UParStr {
  type: 'mac';
}

export interface UParIPv4 extends UParStr {
  type: 'ipv4';
}

export interface UParIPv6 extends UParStr {
  type: 'ipv6';
}

export type UParTyped =
  UParUint | UParSint
  | UParUfix | UParSfix
  | UParReal
  | UParCstr | UParHbin
  | UParTime | UParDate
  | UParMac
  | UParIPv4 | UParIPv6;

export type UParDecl = UParEmpty | UParGroup | UParTyped;

export interface UParDef {
  id: number;
  path: string;
  name: string;
  decl: UParDecl;
  type?: UParType;
  uid?: string;
}

export interface UParStorageConfig {
  section?: string;
  uidType: GenUidType;
}

export interface UParStorage extends UParStorageConfig {
  backend: string;
}

export interface UParServer {
  name?: string;
  includes?: string[];
  storage?: UParStorage;
  params: UParDecl[];
}

export interface UParTypeClass {
  new (param: UParTyped): UParType;
}

export interface UParTypeRegistry {
  [name: string]: UParTypeClass;
}

export interface UParVarFlags {
  const?: true;
  extern?: true;
  static?: true;
}

export interface UParType {
  equal(type: UParType): boolean;
  /* Put the type definition to the response */
  typeDef(): string;
  /* Define parameter variable */
  defVar(cg: CodeGen, name: string, flags: UParVarFlags, value?: string): void;
  /* Convert the value to C representation */
  putVal(value: string): string;
  /* Put the value from variable to response */
  putVar(cg: CodeGen, name: string): void;
  /* Get the value from request to variable */
  getVar(cg: CodeGen, name: string): void;
  /* Set the value to the variable */
  setVar(cg: CodeGen, name: string, value: string, flags?: UParVarFlags): void;
  /* Load the value from storage to variable */
  loadVar(cg: CodeGen, uid: string, name: string): void;
  /* Save the value from variable to storage */
  saveVar(cg: CodeGen, uid: string, name: string): void;
}

const uparTypes: UParTypeRegistry = {};

export function uparTypeReg(typeName: string, typeClass: UParTypeClass) {
  uparTypes[typeName] = typeClass;
}

export function uparTypeCreate(parDecl: UParTyped): UParType {
  const typeName = parDecl.type;
  const typeClass = uparTypes[typeName];

  if (typeClass) {
    return new typeClass(parDecl);
  }

  throw `Unsupported type: ${typeName}`;
}

export type UParStorageBackend = (cg: CodeGen, config: UParStorageConfig) => void;

export interface UParStorageRegistry {
  [name: string]: UParStorageBackend;
}

const uparStorages: UParStorageRegistry = {};

export function uparStorageReg(backendName: string, backendImpl: UParStorageBackend) {
  uparStorages[backendName] = backendImpl;
}

export function uparStorageGen(cg: CodeGen, config: UParStorage) {
  const gen = uparStorages[config.backend];

  if (gen) {
    gen(cg, config);
    return;
  }

  throw `Unsupported storage backend: ${config.backend}`;
}
