import { UParTyped, UParInt, UParFix, UParReal, UParVarFlags, UParType, uparTypeReg } from "../types";
import { CodeGen } from "../coder";
import { put_try } from "../utils";

class NumType implements UParType {
  bits: string;
  type: string;

  constructor(param: UParTyped) {
    const { bits } = param as UParInt;

    if (bits) {
      this.bits = `${bits}`;
    } else if (param.bind) {
      this.bits = `(sizeof(${param.bind}) << 3)`;
    } else {
      throw `Unable to determine the size of parameter ${param.name}`;
    }
  }

  equal(type: UParType): boolean {
    return type instanceof NumType &&
      type instanceof this.constructor &&
      type.type == this.type;
  }

  typeDef(): string {
    return `${this.bits}`;
  }

  defVar(cg: CodeGen, name: string, flags: UParVarFlags, value?: string) {
    cg.line(`${flags.static ? "static " : flags.extern ? "extern " : ""}` +
      `${flags.const ? "const " : ""}` +
      `${this.type} ${name}` + (value ? ` = ${value}` : "") + ";");
  }

  putVal(value: string): string {
    return value;
  }

  setVar(cg: CodeGen, name: string, value: string) {
    cg.line(`${name} = ${value};`);
  }

  getVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_getvr(req, ${name})`);
  }

  putVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_putvr(res, ${name})`);
  }

  loadVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_load(${uid}, &${name}, sizeof(${name}));`);
  }

  saveVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_save(${uid}, &${name}, sizeof(${name}));`);
  }
}

class UIntType extends NumType {
  constructor(param: UParTyped) {
    super(param);
    const { bits } = param as UParInt;
    this.type = `uint${bits}_t`;
  }
}

uparTypeReg("uint", UIntType);

class SIntType extends NumType {
  constructor(param: UParTyped) {
    super(param);
    const { bits } = param as UParInt;
    this.type = `int${bits}_t`;
  }
}

uparTypeReg("sint", SIntType);

class FixType extends NumType {
  frac: number;

  constructor(param: UParTyped) {
    super(param);
    this.frac = (param as UParFix).frac;
  }

  typeDef(): string {
    return `(${this.bits} | (${this.frac} << 8))`;
  }

  putVal(inValue: string): string {
    return `((${this.type})((double)(${inValue}) * ((uint64_t)1 << ${this.frac})))`;
  }
}

class UFixType extends FixType {
  constructor(param: UParTyped) {
    super(param);
    const { bits } = param as UParFix;
    this.type = `uint${bits}_t`;
  }
}

uparTypeReg("ufix", UFixType);

class SFixType extends FixType {
  constructor(param: UParTyped) {
    super(param);
    const { bits } = param as UParFix;
    this.type = `int${bits}_t`;
  }
}

uparTypeReg("sfix", SFixType);

class RealType extends NumType {
  constructor(param: UParTyped) {
    super(param);
    const { bits } = param as UParReal;
    this.type = bits == 32 ? "float" : "double";
  }
}

uparTypeReg("real", RealType);
