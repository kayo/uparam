import { UParVarFlags, UParType, uparTypeReg } from "../types";
import { CodeGen } from "../coder";
import { put_try } from "../utils";

class NetType implements UParType {
  type: string;

  equal(type: UParType): boolean {
    return type instanceof NetType
      && type instanceof this.constructor;
  }

  typeDef(): string {
    return 'upar_none';
  }

  defVar(cg: CodeGen, name: string, flags: UParVarFlags, value?: string) {
    cg.line(`${flags.static ? "static " : flags.extern ? "extern " : ""}` +
      `${flags.const ? "const " : ""}` +
      `${this.type} ${name}` + (value ? ` = { ${value} }` : "") + ";");
  }

  putVal(value: string): string {
    return value.replace(/\.\:/g, ", ");
  }

  setVar(cg: CodeGen, name: string, value: string) {
    cg.line(`memcpy(${name}, ${value}, sizeof(${this.type}));`);
  }

  getVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_getvr(req, ${name})`);
  }

  putVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_putvr(res, ${name})`);
  }

  loadVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_load(${uid}, ${name}, sizeof(${this.type}));`);
  }

  saveVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_save(${uid}, ${name}, sizeof(${this.type}));`);
  }
}

class MacType extends NetType {
  type = "umac_t";
}

uparTypeReg("mac", MacType);

class IPv4Type extends NetType {
  type = "uip4_t";
}

uparTypeReg("ipv4", IPv4Type);

class IPv6Type extends NetType {
  type = "uip6_t";
}

uparTypeReg("ipv6", IPv6Type);
