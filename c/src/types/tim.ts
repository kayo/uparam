import { UParVarFlags, UParType, uparTypeReg } from "../types";
import { CodeGen } from "../coder";
import { put_try, put_block } from "../utils";

class DateTimeType implements UParType {
  type: string;

  equal(type: UParType): boolean {
    return type instanceof DateTimeType
      && type instanceof this.constructor;
  }

  typeDef(): string {
    return 'upar_none';
  }

  defVar(cg: CodeGen, name: string, flags: UParVarFlags, value?: string) {
    cg.line(`${flags.static ? "static " : flags.extern ? "extern " : ""}` +
      `${flags.const ? "const " : ""}` +
      `${this.type} ${name}` + (value ? ` = ${value}` : "") + ";");
  }

  putVal(value: string): string {
    return value.replace(/[\-\.\:]/g, ", ");
  }

  setVar(cg: CodeGen, name: string, value: string) {
    put_block(cg, (cg) => {
      cg.line(`${this.type} _tim_ = ${value};`);
      cg.line(`${name} = _tim_;`);
    });
  }

  getVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_getvr(req, ${name})`);
  }

  putVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_putvr(res, ${name})`);
  }

  loadVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_load(${uid}, &${name}, sizeof(${this.type}));`);
  }

  saveVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_save(${uid}, &${name}, sizeof(${this.type}));`);
  }
}

class TimeType extends DateTimeType {
  type = "utime_t";

  putVal(value: string): string {
    const m = value.match(/^([0-2]?[0-9])\:([0-5][0-9])\:([0-5][0-9])$/);
    if (m) {
      const hour = parseInt(m[1], 10);
      const min = parseInt(m[2], 10);
      const sec = parseInt(m[3], 10);
      return `utime_make(${hour}, ${min}, ${sec})`;
    }
    throw new Error(`Invalid time: ${value}`);
  }
}

uparTypeReg("time", TimeType);

class DateType extends DateTimeType {
  type = "udate_t";

  putVal(value: string): string {
    const m = value.match(/^(\d{4})[\-\.]([0-1]?[0-9])[\-\.]([0-3]?[0-9])$/);
    if (m) {
      const year = parseInt(m[1], 10);
      const mon = parseInt(m[2], 10);
      const date = parseInt(m[3], 10);
      return `udate_make(${year}, ${mon}, ${date})`;
    }
    throw new Error(`Invalid date: ${value}`);
  }
}

uparTypeReg("date", DateType);
