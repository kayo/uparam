import { UParStr, UParTyped, UParVarFlags, UParType, uparTypeReg } from "../types";
import { CodeGen } from "../coder";
import { put_if, put_block, put_try } from "../utils";

class StrType {
  size: string;
  type: string;
  extra: number = 0;

  constructor(param: UParTyped) {
    const { size } = param as UParStr;

    if (size) {
      this.size = `${size} + ${this.extra}`;
    } else if (param.bind) {
      this.size = `sizeof(${param.bind})`;
    } else if (param.def) {
      this.size = `${this.valSize(`${param.def}`)} + ${this.extra}`;
    } else {
      throw `Unable to determine the size of parameter ${param.name}`;
    }
  }

  valSize(_val: string): string {
    return "";
  }

  equal(type: UParType): boolean {
    return type instanceof StrType
      && type instanceof this.constructor
      && type.type == this.type;
  }

  typeDef(): string {
    return `${this.size} - 1`;
  }

  defVar(cg: CodeGen, name: string, flags: UParVarFlags, value?: string) {
    cg.line(`${flags.static ? "static " : flags.extern ? "extern " : ""}` +
      `${flags.const ? "const " : ""}` +
      `${this.type} ${name}[${value ? "" : this.size}]` +
      (value ? ` = ${value}` : "") + ";");
  }
}

class CStrType extends StrType implements UParType {
  type = "char";
  extra = 1;

  valSize(val: string): string {
    return `${Buffer.from(`${val}`).length}`;
  }

  putVal(value: string): string {
    return JSON.stringify(value);
  }

  setVar(cg: CodeGen, name: string, value: string, flags?: UParVarFlags) {
    put_block(cg, (cg) => {
      if (flags && flags.const) {
        cg.line(`const char _str_[${this.size} <= sizeof(${value}) ? ${this.size} : sizeof(${value})] = ${value};`);
        cg.line(`memcpy(${name}, _str_, sizeof(_str_));`);
      } else {
        cg.line(`ulen_t len = strlen(${value});`);
        put_if(cg, `len >= ${this.size}`, (cg) => {
          cg.line(`len = ${this.size} - 1;`);
        });
        cg.line(`memcpy(${name}, ${value}, len);`);
        cg.line(`${name}[len] = '\\0';`);
      }
    });
  }

  getVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_getsl0(req, ${name}, ${this.size})`);
  }

  putVar(cg: CodeGen, name: string) {
    put_block(cg, (cg) => {
      put_try(cg, `ustr_putsl0(res, ${name})`);
    });
  }

  loadVar(cg: CodeGen, uid: string, name: string) {
    put_block(cg, (cg) => {
      cg.line(`res = upar_load(${uid}, ${name}, ${this.size} - 1);`);
      put_if(cg, "res >= 0", (cg) => {
        cg.line(`${name}[res] = '\\0';`);
      });
    });
  }

  saveVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_save(${uid}, ${name}, strlen(${name}));`);
  }
}

uparTypeReg("cstr", CStrType);

class HBinType extends StrType implements UParType {
  type = "uint8_t";

  valSize(val: string): string {
    return `${val.length / 2}`;
  }

  putVal(value: string): string {
    if (!/^[0-9A-Fa-f]+$/.test(value)) {
      throw `The value '${value}' isn't a valid hexadecimal`;
    }

    /*
    if (value.length != this.size * 2) {
      throw `The value '${inValue}' doesn't match type size`;
    }
    */

    const hexes: string[] = value.toLowerCase().match(/.{2}/g) as string[];

    return `{ 0x${hexes.join(", 0x")} }`;
  }

  setVar(cg: CodeGen, name: string, value: string) {
    cg.line(`memcpy(${name}, ${value}, ${this.size});`);
  }

  getVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_getbd(req, ${name}, ${this.size})`);
  }

  putVar(cg: CodeGen, name: string) {
    put_try(cg, `ustr_putbd(res, ${name}, ${this.size})`);
  }

  loadVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_load(${uid}, ${name}, ${this.size});`);
  }

  saveVar(cg: CodeGen, uid: string, name: string) {
    cg.line(`res = upar_save(${uid}, ${name}, ${this.size});`);
  }
}

uparTypeReg("hbin", HBinType);
