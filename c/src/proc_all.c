{
  // process as many requests as possible until output buffer overflowed
  for (; ustr_pos(req) < ustr_len(req); ) {
    // save previous length in response buffer
    ulen_t l = ustr_len(res);
    // try to process parameter request
    int s = ${proc}(req, res);
    // if processing failed with buffer overflow status
    if (s == -1) {
      // backpedal to previous success length
      ustr_len(res) = l;
      // return success status
      return 0;
    }
    // if processing failed with invalid status
    if (s != 0) {
      // return invalid status
      return s;
    }
  }
  // all requests processed
  // return success status
  return 0;
}
