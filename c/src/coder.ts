import { readFileSync } from "fs";

interface ICodeOpts {
  ident: string;
  endline: string;
  comments: boolean;
}

export const defaults: ICodeOpts = {
  ident: "  ",
  endline: "\n",
  comments: true,
};

export interface CodeOpts {
  ident?: string;
  endline?: string;
  comments?: boolean;
}

export class CodeGen {
  _opts: ICodeOpts;
  _ident: string;
  _src: string;

  constructor(cg: CodeGen);
  constructor(opts?: CodeOpts);

  constructor(cg_or_opts: CodeGen | CodeOpts = defaults) {
    if (cg_or_opts instanceof CodeGen) {
      const cg = cg_or_opts as CodeGen;
      this._opts = cg._opts;
      this._ident = cg._ident;
    } else {
      const opts = cg_or_opts as CodeOpts;
      this._opts = { ...opts, ...defaults };
      this._ident = "";
    }
    this._src = "";
  }

  source(): string {
    return this._src;
  }

  derive(): CodeGen {
    return new CodeGen(this);
  }

  space() {
    this._src += this._opts.endline;
  }

  line(src: string | undefined, cmt?: string) {
    if (src) {
      this.comment(cmt);
      this._src += `${this._ident}${src}${this._opts.endline}`;
    }
  }

  lines(srcs: (string | undefined)[], cmt?: string) {
    if (srcs.length > 0) {
      this.line(srcs[0], cmt);

      for (let i = 1; i < srcs.length; i++) {
        this.line(srcs[i]);
      }
    }
  }

  block(fn: (cg: CodeGen) => void, cmt?: string) {
    const cg = this.derive();
    cg._ident += cg._opts.ident;
    fn(cg);
    if (cg._src != "") {
      cg.comment(cmt);
      this._src += cg._src;
    }
  }

  comment(text: string | undefined) {
    if (!text) return;
    if (text.search(/\n/) < 0) {
      this.line(`/* ${text} */`);
    } else {
      const lines = text.split(/\n/);
      this.line("/*");
      for (const line of lines) {
        this.line(` * ${line}`);
      }
      this.line(" */");
    }
  }

  include(path: string, patsub?: Record<string, string>) {
    let text = readFileSync(path, "utf-8")
      .replace(/^/g, this._ident);
    if (patsub) {
      for (const key in patsub) {
        const val = patsub[key];
        text = text.replace(new RegExp('\\$\\{' + key + '\\}', 'g'), val);
      }
    }
    this._src += text;
  }
}
