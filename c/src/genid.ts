function uint32(num: number): number {
  return num >>> 0;
}

function hash_djb2(str: string): string {
  let b = Buffer.from(str, "utf-8");
  let h = 5381;

  for (let i = 0; i < b.length; i++) {
    h = uint32(uint32(h * 33) + b.readUInt8(i));
  }

  return `0x${h.toString(16)}`;
  //return `${h}`;
}

function hash_sdbm(str: string): string {
  let b = Buffer.from(str, "utf-8");
  let h = 0;

  for (let i = 0; i < b.length; i++) {
    h = uint32(uint32(h * 65599) + b.readUInt8(i));
  }

  return `0x${h.toString(16)}`;
}

export type GenUidFunc = (str: string) => string;

export type GenUidType = 'djb2' | 'sdbm';

const hashes: { [name: string]: GenUidFunc } = {
  djb2: hash_djb2,
  sdbm: hash_sdbm,
};

export function genUidByType(genType: GenUidType): GenUidFunc {
  const gen_uid = hashes[genType];

  if (!gen_uid) {
    throw `Unsupported unique identifier generator: ${genType}`;
  }

  return gen_uid;
}
