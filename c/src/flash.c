/*
 * Find last written cell by id
 * If cell is not found then store_end will be returned
 */
static addr_t find_cell(upar_uid_t uid, store_cell_t *cell) {
  addr_t ptr = store_end - sizeof(*cell);
  addr_t res = store_end;

  for (; ; ptr -= sizeof(*cell)) {
    store_read(ptr, cell, sizeof(*cell)); /* read cell */
    
    if (cell->uid == uid) { /* cell found */
      res = ptr; /* set checkpoint and try find yet */
    }

    if (cell->uid == free_uid) { /* free space reached */
      break;
    }
  }

  if (res != store_end) { /* re-read last found cell */
    store_read(res, cell, sizeof(*cell));
  }
  
  return res;
}

#if defined(store_align) && store_align > 0
#define fix_ptr(ptr) (((ptr) + ((store_align) - 1)) & ~((store_align) - 1))
#else
#define fix_ptr(ptr) (ptr)
#endif

static int upar_load(upar_uid_t uid, upar_ptr_t ptr, upar_len_t len) {
  store_cell_t cell;
  
  if (find_cell(uid, &cell) == store_end) {
    return -1;
  }
  
  if (len > cell.len) {
    len = cell.len;
  }
  
  store_read(store_start + cell.off, ptr, len);
  
  return len;
}

static int upar_save(upar_uid_t uid, const upar_ptr_t ptr, upar_len_t len) {
  store_cell_t cell;
  
  addr_t cell_ptr = find_cell(free_uid, &cell);
  addr_t data_ptr = store_start;

  {
    addr_t last_cell_ptr = cell_ptr + sizeof(cell);
    
    if (last_cell_ptr < store_end) {
      /* read the last occupied cell */
      store_read(last_cell_ptr, &cell, sizeof(cell));
      data_ptr = fix_ptr(data_ptr + cell.off + cell.len);
    }
  }
  
  if (fix_ptr(data_ptr + len) >= cell_ptr - sizeof(cell)) {
    /* not enough free space in storage */
    return -1;
  }
  
  cell.uid = uid;
  cell.off = data_ptr - store_start;
  cell.len = len;

  store_unlock();

  /* write cell */
  store_write(cell_ptr, &cell, sizeof(cell));

  /* write data */
  store_write(data_ptr, ptr, len);
  
  store_lock();

  return len;
}

static void upar_drop(void) {
  store_unlock();
  
  store_erase(store_start, store_end - store_start);
  
  store_lock();
}
