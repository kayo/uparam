import { basename } from "path";

import { CodeGen } from "./coder";

export function c_name(path: string): string {
  return path.replace(/\./g, "__");
}

export function mk_name(name: string, group?: string): string {
  return c_name(`upar_${typeof group == 'string' ? `${group}_` : ''}${name}`);
}

export function find_arg(args: string[], fn: (arg: string) => boolean): string | undefined {
  for (const arg of args) {
    if (fn(arg)) {
      return arg;
    }
  }
  return undefined;
}

export type Attrs = { [name: string]: string };

export function args_to_attrs(args: string[]): Attrs {
  let attrs: Attrs = {};

  for (const arg of args) {
    const matches = arg.match(/^([^=]+)=(.*)$/);
    if (matches) {
      attrs[matches[1]] = matches[2];
    }
  }

  return attrs;
}

export function put_once(name: string, cg: CodeGen, fn: (cg: CodeGen) => void) {
  const cname = basename(name)
    .replace(/^[^_a-zA-Z]/, "_")
    .replace(/[^0-9a-zA-Z]/g, "_");
  const defname = `__${cname.toUpperCase()}__`;
  cg.line(`#ifndef ${defname}`);
  cg.line(`#define ${defname}`);
  cg.space();
  fn(cg);
  cg.space();
  cg.line(`#endif /* !${defname} */`);
}

export function put_include(cg: CodeGen, path: string, cmt?: string) {
  if (path != "") {
    cg.line(`#include <${path}>`, cmt);
  }
}

export function put_if(cg: CodeGen, val: string, fn: (cg: CodeGen) => void, cmt?: string) {
  cg.line(`if (${val}) {`, cmt);
  cg.block(fn);
  cg.line("}");
}

export function put_switch(cg: CodeGen, val: string, fn: (cg: CodeGen) => void, cmt?: string) {
  cg.line(`switch (${val}) {`, cmt);
  fn(cg);
  cg.line("}");
}

export function put_case(cg: CodeGen, vals: string[], fn: (cg: CodeGen) => void, cmt?: string) {
  for (let i = 0; i < vals.length; i++) {
    cg.line(`case ${vals[i]}:` + (i == vals.length - 1 ? " {" : ""), i == 0 ? cmt : undefined);
  }
  cg.block(fn);
  cg.line("} break;");
}

export function put_block(cg: CodeGen, fn: (cg: CodeGen) => void, cmt?: string) {
  cg.line("{", cmt);
  cg.block(fn);
  cg.line("}");
}

export function put_try(cg: CodeGen, expr: string, cmt?: string) {
  cg.line(`upar_try(${expr});`, cmt);
}
